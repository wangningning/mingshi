//
//  BaseTabbarController.swift
//  MoDuLive
//
//  Created by 梁毅 on 2017/2/15.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class NavigationController:UINavigationController, UINavigationControllerDelegate{
    override func viewDidLoad() {
        
        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(hexString: "333333"), NSFontAttributeName: defaultFont(size: 17)]
        self.navigationBar.tintColor = UIColor(hexString: "333333")
        self.navigationBar.barTintColor = UIColor.white
        self.navigationBar.isTranslucent = false
        
        
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        
        
        let backItem = UIBarButtonItem()
        backItem.title = ""
        viewController.navigationItem.backBarButtonItem = backItem
        if self.viewControllers.count > 0 {
            viewController.hidesBottomBarWhenPushed = true

        }
        super.pushViewController(viewController, animated: animated)
    }
    
    
}

class BaseTabbarController: UITabBarController, UITabBarControllerDelegate {
    private var adImageView: UIImageView?
    var lastViewController = UIViewController()
    var lastSelectIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupAllViewController()
        
        setupAllTabBarButton()
        
        self.delegate = self
        lastViewController = self.childViewControllers.first!
        
        self.tabBar.barTintColor = UIColor.white
        self.tabBar.isTranslucent = false
        
//        self.tabBar.shadowImage = UIImage()
//        self.tabBar.backgroundImage = #imageLiteral(resourceName: "tabbarbg")
        
        // Do any additional setup after loading the view.
    }
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let index = tabBar.items?.index(of: item)
        if index != self.lastSelectIndex {
            let animationArr = NSMutableArray()
            for btn in self.tabBar.subviews {
                if btn.isKind(of: NSClassFromString("UITabBarButton")!) {
                    animationArr.add(btn)
                }
            }
            let animation = CABasicAnimation.init(keyPath: "transform.scale")
            animation.timingFunction = CAMediaTimingFunction.init(name: kCAMediaTimingFunctionEaseOut)
            animation.duration = 0.2
            animation.repeatCount = 1
            animation.autoreverses = true
            animation.fromValue = NSNumber.init(value: 0.7)
            animation.toValue = NSNumber.init(value: 1.3)
            (animationArr[index!] as! UIView).layer.add(animation, forKey: nil)
            self.lastSelectIndex = index!
        }
        
        
//        if <#condition#> {
//            <#code#>
//        }
    }
    /*
     Z轴旋转
     
     //z轴旋转180度
     CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
     //速度控制函数，控制动画运行的节奏
     animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
     animation.duration = 0.2;       //执行时间
     animation.repeatCount = 1;      //执行次数
     animation.removedOnCompletion = YES;
     animation.fromValue = [NSNumber numberWithFloat:0];   //初始伸缩倍数
     animation.toValue = [NSNumber numberWithFloat:M_PI];     //结束伸缩倍数
     [[arry[index] layer] addAnimation:animation forKey:nil];
     
     Y轴位移
     
     //向上移动
     CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.translation.y"];
     //速度控制函数，控制动画运行的节奏
     animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
     animation.duration = 0.2;       //执行时间
     animation.repeatCount = 1;      //执行次数
     animation.removedOnCompletion = YES;
     animation.fromValue = [NSNumber numberWithFloat:0];   //初始伸缩倍数
     animation.toValue = [NSNumber numberWithFloat:-10];     //结束伸缩倍数
     [[arry[index] layer] addAnimation:animation forKey:nil];
     
     
     //放大效果
     CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
     //速度控制函数，控制动画运行的节奏
     animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
     animation.duration = 0.2;       //执行时间
     animation.repeatCount = 1;      //执行次数
     animation.removedOnCompletion = NO;
     animation.fillMode = kCAFillModeForwards;           //保证动画效果延续
     animation.fromValue = [NSNumber numberWithFloat:1.0];   //初始伸缩倍数
     animation.toValue = [NSNumber numberWithFloat:1.15];     //结束伸缩倍数
     [[arry[index] layer] addAnimation:animation forKey:nil];
     //移除其他tabbar的动画
     for (int i = 0; i<arry.count; i++) {
     if (i != index) {
     [[arry[i] layer] removeAllAnimations];
     }
     }
     */
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension BaseTabbarController {
    func addAllChildViewController() {
        
    }
    
    func setupAllViewController() {
        //首页
        if DLUserInfoHandler.getUserBaseInfo().type == "1" || isTeacher == false {
            
            let homeVC = HomePageViewController()
            let homeNav = HomepageNavigationController(rootViewController: homeVC)
            self.addChildViewController(homeNav)
            
            let BBSVC = BBSViewController()
            let BBSNav = NavigationController(rootViewController: BBSVC)
            self.addChildViewController(BBSNav)
            
//            let ShopVC = ShopViewController()
//            let ShopNav = NavigationController(rootViewController: ShopVC)
//            self.addChildViewController(ShopNav)
//
//            let ShoppingCartVC = ShoppingCartViewController()
//            let ShoppingCartNav = NavigationController(rootViewController: ShoppingCartVC)
//            self.addChildViewController(ShoppingCartNav)
            
            //我的中心
            let mineVC = MineViewController()
            let mineNav = NavigationController(rootViewController: mineVC)
            self.addChildViewController(mineNav)
        } else {
            
            let homeVC = THomePageViewController()
            let homeNav = NavigationController(rootViewController: homeVC)
            self.addChildViewController(homeNav)
            
            //开启直播
            let cameraVC = UIViewController()
            let cameraNav = NavigationController(rootViewController: cameraVC)
            self.addChildViewController(cameraNav)
            
            
            //我的中心
            let mineVC = TMineViewController()
            let mineNav = NavigationController(rootViewController: mineVC)
            self.addChildViewController(mineNav)
            
            addCameraButton()
        }
    }
    func setupAllTabBarButton() {
        //设置TabBar按钮的内容
        if DLUserInfoHandler.getUserBaseInfo().type == "1" || isTeacher == false {
            let homeVC = self.childViewControllers[0]
            homeVC.tabBarItem.image = UIImage(named: "home")
            homeVC.tabBarItem.selectedImage = UIImage(named: "home_pre")?.withRenderingMode(.alwaysOriginal)
            
            let bbsVC = self.childViewControllers[1]
            bbsVC.tabBarItem.image = UIImage(named: "bbs")
            bbsVC.tabBarItem.selectedImage = UIImage(named: "bbs_pre")?.withRenderingMode(.alwaysOriginal)
            
//            let shopVC = self.childViewControllers[2]
//            shopVC.tabBarItem.image = UIImage(named: "mall")
//            shopVC.tabBarItem.selectedImage = UIImage(named: "mall_pre")?.withRenderingMode(.alwaysOriginal)
//
//            let shoppingCartVC = self.childViewControllers[3]
//            shoppingCartVC.tabBarItem.image = UIImage(named: "shopping_car")
//            shoppingCartVC.tabBarItem.selectedImage = UIImage(named: "shopping_car_pre")?.withRenderingMode(.alwaysOriginal)
            
            let mineVC = self.childViewControllers[2]
            mineVC.tabBarItem.image = UIImage(named: "mine-shense")
            mineVC.tabBarItem.selectedImage = UIImage(named: "mine-shense_pre")?.withRenderingMode(.alwaysOriginal)
            
            let insets = UIEdgeInsetsMake(6, 0, -6, 0)
            homeVC.tabBarItem.imageInsets = insets
            bbsVC.tabBarItem.imageInsets = insets
            
//            shopVC.tabBarItem.imageInsets = insets
//            shoppingCartVC.tabBarItem.imageInsets = insets
            mineVC.tabBarItem.imageInsets = insets
            
        } else {
            let homeVC = self.childViewControllers[0]
            homeVC.tabBarItem.image = UIImage(named: "home")
            homeVC.tabBarItem.selectedImage = UIImage(named: "home_pre")?.withRenderingMode(.alwaysOriginal)
            
            let cameraVC = self.childViewControllers[1]
            cameraVC.tabBarItem.isEnabled = false

            
            let mineVC = self.childViewControllers[2]
            mineVC.tabBarItem.image = UIImage(named: "mine-shense")
            mineVC.tabBarItem.selectedImage = UIImage(named: "mine-shense_pre")?.withRenderingMode(.alwaysOriginal)
            
            let insets = UIEdgeInsetsMake(6, 0, -6, 0)
            let cameraInsets = UIEdgeInsetsMake(0, 0, 0, 0)
            
            homeVC.tabBarItem.imageInsets = insets
            cameraVC.tabBarItem.imageInsets = cameraInsets
            mineVC.tabBarItem.imageInsets = insets
        }
    }
    func addCameraButton() {
        let cameraBtn = UIButton(type: .custom)
        cameraBtn.setImage(UIImage(named: "icon_kaibo"), for: .normal)
        cameraBtn.setImage(UIImage(named: "icon_kaibo"), for: .highlighted)
        cameraBtn.frame = CGRect(x: self.tabBar.frame.size.width/2 - 58/2, y: 49-58-3, width: 58, height: 58)
        cameraBtn.addTarget(self, action: #selector(clickCameraBtn), for: .touchUpInside)
        self.tabBar.addSubview(cameraBtn)
    }
    func clickCameraBtn() {
        let cameraVC = StartLiveViewController()
        let cameraNav = NavigationController(rootViewController: cameraVC)
        self.present(cameraNav, animated: true, completion: nil)
    }
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        if lastViewController == viewController {
        }
        lastViewController = viewController
    }
}
extension UINavigationBar {
    // MARK: - 接口
    /**
     *  隐藏导航栏下的横线，背景色置空 viewWillAppear调用
     */
    func star() {
        let shadowImg: UIImageView? = self.findNavLineImageViewOn(view: self)
        shadowImg?.isHidden = true
        self.backgroundColor = nil
        
    }
   
    /**
     在func scrollViewDidScroll(_ scrollView: UIScrollView)调用
     @param color 最终显示颜色
     @param scrollView 当前滑动视图
     @param value 滑动临界值，依据需求设置
     */
    func change(_ color: UIColor, with scrollView: UIScrollView, andValue value: CGFloat) {
        if scrollView.contentOffset.y < 0{
            // 下拉时导航栏隐藏，无所谓，可以忽略
            self.topItem?.titleView?.alpha = 0
            self.isHidden = true
        } else {
            self.isHidden = false
            self.topItem?.titleView?.alpha = 1
            // 计算透明度
            let alpha: CGFloat = scrollView.contentOffset.y / value > 1.0 ? 1 : scrollView.contentOffset.y / value
            //设置一个颜色并转化为图片
            let image: UIImage? = imageFromColor(color: color.withAlphaComponent(alpha))
            self.setBackgroundImage(image, for: .default)
        }
    }
    
    /**
     *  还原导航栏  viewWillDisAppear调用
     */
    func reset() {
        let shadowImg = findNavLineImageViewOn(view: self)
        shadowImg?.isHidden = false
        self.setBackgroundImage(nil,for: .default)
    }
    
    
    // MARK: - 其他内部方法
    //寻找导航栏下的横线  （递归查询导航栏下边那条分割线）
    fileprivate func findNavLineImageViewOn(view: UIView) -> UIImageView? {
        if (view.isKind(of: UIImageView.classForCoder())  && view.bounds.size.height <= 1.0) {
            return view as? UIImageView
        }
        for subView in view.subviews {
            let imageView = findNavLineImageViewOn(view: subView)
            if imageView != nil {
                return imageView
            }
        }
        return nil
    }
    
    // 通过"UIColor"返回一张“UIImage”
    fileprivate func imageFromColor(color: UIColor) -> UIImage {
        //创建1像素区域并开始图片绘图
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        
        //创建画板并填充颜色和区域
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        
        //从画板上获取图片并关闭图片绘图
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
}
extension UINavigationController{
    
}
class HomepageNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.tintColor = UIColor(hexString: "333333")
        self.navigationBar.barTintColor = UIColor.white
        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(hexString: "333333"), NSFontAttributeName: defaultFont(size: 17)]
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        let button = UIButton()
        self.navigationItem.titleView?.addSubview(button)
        button.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44)
        button.setImage(UIImage(named: "sousuo2"), for: .normal)
        
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if self.childViewControllers.count > 0 {
            viewController.hidesBottomBarWhenPushed = true
        }
        let backItem = UIBarButtonItem()
        backItem.title = ""
        viewController.navigationItem.backBarButtonItem = backItem
        super.pushViewController(viewController, animated: animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


