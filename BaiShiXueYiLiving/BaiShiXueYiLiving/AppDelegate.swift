//
//  AppDelegate.swift
//  BaiShiXueYiLiving
//
//  Created by 梁毅 on 2017/5/3.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, JPUSHRegisterDelegate {

    var window: UIWindow?
    var isRotation = false
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        PLStreamingEnv.initEnv()
        
        LocationManager.shared.updateLocation()
        
        IQKeyboardManager.sharedManager().shouldShowTextFieldPlaceholder = false
        
        UMSocialManager.default().openLog(true)
        UMSocialManager.default().umSocialAppkey = "591010e2c62dca2aa7001e69"
        
        UMSocialManager.default().setPlaform(UMSocialPlatformType.wechatSession, appKey: "wx0fdf564e5795c6b5", appSecret: "2b04da9630027848cd62eda9f3ee6ae2", redirectURL: "")
        UMSocialManager.default().setPlaform(UMSocialPlatformType.QQ, appKey: "1106143719", appSecret:"n3nC3YO1HSdquqxr", redirectURL: "")

        
        EMClient.shared().initializeSDK(with: EMOptions(appkey: "1184171027115484#bty"))
        
        JPUSHService.setup(withOption: launchOptions, appKey: "b528a813395f6c59e64727ca", channel: nil, apsForProduction: true)
        let jpush = JPUSHRegisterEntity()
        jpush.types = Int(JPAuthorizationOptions.alert.rawValue) | Int(JPAuthorizationOptions.badge.rawValue) | Int(JPAuthorizationOptions.sound.rawValue)
        JPUSHService.register(forRemoteNotificationConfig: jpush, delegate: self)
        
        if DLUserInfoHandler.getIdAndToken() == nil {
            self.window?.rootViewController = LoginNavigationController.setup()
        } else {
            if let m = DLUserInfoHandler.getUserHXInfo(), EMClient.shared().isLoggedIn == false, EMClient.shared().isAutoLogin == false {
                EMClient.shared().login(withUsername: m.name, password: m.pw, completion: { str, error in
                    if error == nil {
                        print("环信登录成功")
                        EMClient.shared().options.isAutoLogin = true
                    } else {
                        print("环信登录失败")
                    }
                })
            }
            let tabbar = BaseTabbarController()
            self.window?.backgroundColor = UIColor.white
            self.window?.rootViewController = tabbar
        }
        
        return true
    }

    //MARK: - 极光代理
    @available(iOS 10.0, *)
    func jpushNotificationCenter(_ center: UNUserNotificationCenter!, didReceive response: UNNotificationResponse!, withCompletionHandler completionHandler: (() -> Void)!) {
        if response.notification.request.trigger is UNPushNotificationTrigger {
            JPUSHService.handleRemoteNotification(response.notification.request.content.userInfo)
        }
        completionHandler()
    }
    @available(iOS 10.0, *)
    func jpushNotificationCenter(_ center: UNUserNotificationCenter!, willPresent notification: UNNotification!, withCompletionHandler completionHandler: ((Int) -> Void)!) {
        if notification.request.trigger is UNPushNotificationTrigger {
            JPUSHService.handleRemoteNotification(notification.request.content.userInfo)
        }
        completionHandler(7)
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let str = String(describing: url)
        if str.range(of: "pay/?") != nil{
            let cancelUrl = Pingpp.handleOpen(url) { (response, error) in
                if error == nil{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "paySuccess"), object: nil)
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "payFail"), object: nil)
                }
            }
            return cancelUrl
        }
        
        return  UMSocialManager.default().handleOpen(url)
        
        
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

