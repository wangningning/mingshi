//
//  EaseBubbleView+Gift.h
//  Duluo
//
//  Created by apple on 2017/4/18.
//  Copyright © 2017年 tts. All rights reserved.
//

#import "EaseBubbleView.h"

@interface EaseBubbleView (Gift)

- (void)setupGiftBubbleView;

@end
