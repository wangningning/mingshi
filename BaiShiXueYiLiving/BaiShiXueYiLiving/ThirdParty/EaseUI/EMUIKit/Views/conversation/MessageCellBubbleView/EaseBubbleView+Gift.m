//
//  EaseBubbleView+Gift.m
//  Duluo
//
//  Created by apple on 2017/4/18.
//  Copyright © 2017年 tts. All rights reserved.
//

#import "EaseBubbleView+Gift.h"

@implementation EaseBubbleView (Gift)

#pragma mark - private

- (void)_setupGiftBubbleMarginConstraints
{
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.giftBgView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.giftBgView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-22]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.giftBgView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.giftBgView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.giftBgView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0]];

    
//    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.experience attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.giftBgView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-12]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.experience attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.giftBgView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:9]];
}

- (void)_setupGiftBubbleConstraints
{
    [self _setupGiftBubbleMarginConstraints];
}

#pragma mark - public

- (void)setupGiftBubbleView
{
    [self.backgroundImageView removeFromSuperview];
    self.giftBgView = [[UIImageView alloc] init];
    self.giftBgView.frame = CGRectMake(0, 6, 211, 92);
    [self addSubview:self.giftBgView];
    
    self.giftView = [[UIImageView alloc] init];
    [self.giftBgView addSubview:self.giftView];
    self.giftView.frame = CGRectMake(211-66, 10, 51, 51);
    self.giftView.contentMode = UIViewContentModeScaleAspectFit;
    
    self.giftName = [[UILabel alloc] init];
    self.giftName.textColor = [UIColor colorWithRed:1 green:87/255.0 blue:48/255.0 alpha:1];
    [self.giftBgView addSubview:self.giftName];
    self.giftName.frame = CGRectMake(9, 43, 211 - 86, 40);
    self.giftName.font = [UIFont systemFontOfSize:15];
//    self.giftName.textAlignment = 1;
    self.giftName.numberOfLines = 2;
    
    self.widgetView = [[UIImageView alloc] init];
    [self.giftBgView addSubview:self.widgetView];
    self.widgetView.frame = CGRectMake(0, 0, 45, 42);
    
    self.experience = [[UILabel alloc] init];
    self.experience.textColor = [UIColor colorWithRed:1 green:87/255.0 blue:48/255.0 alpha:1];
    self.experience.font = [UIFont systemFontOfSize:14];
//    self.experience.textAlignment = 2;
    self.experience.frame = CGRectMake(36, 101, 211-36, 13);
    [self addSubview:self.experience];
    
    self.experienceImg = [[UIImageView alloc] init];
    self.experienceImg.image = [UIImage imageNamed:@"jingyanzhi"];
    self.experienceImg.frame = CGRectMake(16, 101, 16, 16);
    [self addSubview:self.experienceImg];
    
    [self _setupGiftBubbleConstraints];
}

@end
