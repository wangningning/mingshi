//
//  STInputBar.h
//  STEmojiKeyboard
//
//  Created by zhenlintie on 15/5/29.
//  Copyright (c) 2015年 sTeven. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol STInputBarDelegate;

@interface STInputBar : UIView
@property (strong, nonatomic) UITextView *textView;

+ (instancetype)inputBar;

@property (assign, nonatomic) BOOL fitWhenKeyboardShowOrHide;

- (void)setDidSendClicked:(void(^)(NSString *text))handler;

@property (copy, nonatomic) NSString *placeHolder;
@property (nonatomic, weak) id<STInputBarDelegate>myDelegate;

 
@end

@protocol STInputBarDelegate <NSObject>

@optional
- (void)inputBarShouldReturn:(STInputBar *)inputView;

@end
