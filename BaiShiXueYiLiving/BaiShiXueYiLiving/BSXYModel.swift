
//
//  BSXYModel.swift
//  BaiShiXueYiLiving
//
//  Created by 梁毅 on 2017/5/10.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import Foundation
/// 购物车模块
class AddressList: KeyValueModel {
    var id : String?
    var name : String?
    var phone : String?
    var province : String?
    var city : String?
    var area : String?
    var street : String?
    var is_default : String?
    var address: String?
}
class ShopCart: KeyValueModel {
    var id : String?
    var goods_id : String?
    var number : String?
    var is_check : String?
    var name : String?
    var img : String?
    //    var is_selected: Bool? = false
    //    var price : NSNumber?
    var kinds1 : String?
    var kinds2 : String?
    var kinds_detail2: String?
    var kinds_detail1: String?
    
    var sale_price : String?
    
}
class ConfirmOrderList: KeyValueModel {
    var address: ConfirmOrderAddress?
    var goods: [ConfirmOrder]?
    var amount: String?
}
class ConfirmOrder: KeyValueModel {
    var id: String?
    var goods_id: String?
    var number: String?
    var is_check: String?
    var name: String?
    var img: String?
    var thumb: String?
    var kinds: String?
    var price: String?
    var sale_price: String?
    var kinds_id: String?
    var kinds_detail: [GoodsCartKindDetailModel]?
}

class ConfirmOrderAddress: KeyValueModel {
    var address: String?
  //  var id: String?
    var name: String?
    var phone: String?
  //  var user_id: String?
    var postage: String?
}
class GlobalOrder: KeyValueModel {
    var id : String?
    var order_no : String?
    var user_id : String?
    var amount : String?
    var paid : String?
    var phone : String?
    var name : String?
    var address : String?
    var send_way : String?
    var kuaidi_name : String?
    var pay_type : String?
    var state : String?
    var send_bill : String?
    var item: NSNumber?
    var order_detail: [GlobalOrderDetail]?
}
class GlobalOrderDetail: KeyValueModel {
    var id : String?
    var order_id : String?
    var goods_id : String?
    var kinds_id : String?
    var number : String?
    var name : String?
    var sale_price : NSNumber?
    var thumb: String?
    var kinds1 : String?
    var kinds2 : String?
    var kinds_detail2: String?
    var kinds_detail1: String?
    var img:String?
    
}
class Banner: KeyValueModel {
    var img: String?
    var url: String?
}
class BBSBannerModel: KeyValueModel {
    var b_id: String?
    var b_img: String?
    var url: String?
    var title: String?
}
class BBSModuleModel: KeyValueModel {
    var module_id: String?
    var title: String?
    var picture: String?
    var is_tuijian: String?
    var post_count: String?
}
class BBSImageModel: KeyValueModel {
    var img: String?
    var width: String?
    var height: String?
}
class BBSListModel: KeyValueModel {
    var user_id: String?
    var username: String?
    var head_img: String?
    var sex: String?
    var hx_username: String?
    var alias: String?
    var hx_password: String?
    var title: String?
    var post_id: String?
    var thumb: [BBSImageModel]?
    var img: Array<BBSImageModel>?
    var zan: String?
    var ping: String?
    var content: String?
    var comment: [BBSResponseModel]?
    var intime: String?
    var date_value: String?
    var is_follow: String?
    var cellHeight: CGFloat?
    var is_zan: String?     //1 已经点赞过 2 未点赞
    var share_url: String?
    
   // var likebtnIsSelected = false
}
class BBSResponseModel: KeyValueModel { //回复
    
    var content: String?  //评论
    var comment_id: String? //评论id
    var user_id: String?
    var intime: String?
    var zan: String? //赞
    var username: String?
    var sex: String?  //性别 1男；2女；3未知
    var img: String?
    var response: [BBSResponseModel]? //下级评论
    //    var description: String?
    var cellHeight: CGFloat?
    
}
class KindOfShop: KeyValueModel {
    var kind: String?
    var kinds_detail: [KindDetialOFShop]?
}
class KindDetialOFShop: KeyValueModel {
    var goods_id: String?
    var kind_id: String?
    var kinds_detail: String?
    var price: String?
    var sale_price: String?
    var is_Seleted: Bool = false
    var labelStr :String?
    var labelTag: Int?
}
//产品详情
class ShopDetailData: KeyValueModel {
    var goods_id: String?
    var name: String?
    var intro: String?
    var imgs: [Banner]?
    var brand: String?
    var number: String?
    var sale_number: String?
    var kinds_detail: [kinds_detailModel]?
    var kinds1: String?
    var kinds2: String?
    var is_collect: NSNumber?
    var price: String?
    var sale_price: String?
    var status:NSString?
}
class ShopDetailComment: KeyValueModel {
    var comment_id: String?
    var content: String?
    var user_id: String?
    var username: String?
    var img: String?
    var comment_img: [ImgModel]?
    var comment_thumb: [ImgModel]?
    var goods_mark = 0
    var date_value: String?
    var cellHeight: CGFloat?

}
class ImgModel: KeyValueModel {
    var img: String?
}
class kinds_detailModel: KeyValueModel{
    var kind: String?
    var kind_detail: [kindsDetailModel]?
}
class kindsDetailModel: KeyValueModel {
    var kind_id: String?
    var goods_id: String?
    var price: String?
    var sale_price: String?
    var kinds_detail: String?
    var is_del: String?
    var type: String?
    var is_Seleted  = false
    
}
// 推荐商品
class RecommendModel: KeyValueModel {
    var goods_id: String?
    var thumb: String?
    var img: String?
    var name: String?
    var price: String?
    var sale_price: String?
    var intro: String?
    var brand: String?
}
// 订单详情数据模型
class OrderDetailModel: KeyValueModel {
    var id: String?
    var order_no: String?
    var amount: String?
    var paid: String?
    var name: String?
    var phone: String?
    var address: String?
    var send_bill: String?
    var state: String?
    var remark: String?
    var kuaidi: String?
    var kuaidi_name: String?
    var kuaidi_state: String?
    var order_detail: [GlobalOrderDetail]?
}
class ContributionModel: KeyValueModel {
    var hx_password: String?
    var hx_username: String?
    var img : String?
    var jewel :String?
    var ranking : String?
    var sex : String?
    var user_id : String?
    var username : String?
    var type: String?
}
class PresentationRecordModel: KeyValueModel {//提现记录
    var amount: String?
    var date: String?
    var pay_type: String?
    var bank_card: String?
    var status: String?
    var score: String?
}
class TeacherProfit: KeyValueModel {
    
    var earnings: String?            //金额
    var content: String?          //内容
    var date: String?        //时间
    var type: String?
    
    
}
//我的直播列表
class MyLiveList: KeyValueModel {
    var date_value: String?
    var title: String?
    var play_img: String?
    var live_id: String?
    var url: String?
    var share_url: String?
  
}

// 轮播
class BannerModel: KeyValueModel {
    var b_id: String?
    var b_img: String?
    var b_type: String?
    var url: String?
    var title: String?
    var jump: String?
}
// 直播
class LiveList: KeyValueModel {
    var live_id: String?
    var user_id: String?
    var play_img: String?
    var title: String?
    var push_flow_address: String?
    var play_address: String?
    var play_address_m3u8: String?
    var room_id: String?
    var watch_nums: String?
    var light_up_count: String?
    var date: String?
    var sheng: String?
    var shi: String?
    var qu: String?
    var img: String?
    var play_number: String?
    var username: String?
    var is_type: String?
    var money: String?
    var hx_username: String?
    var sex: String?
    var grade: String?
    var get_money: String?
    var url: String?
    var qiniu_room_id: String?
    var qiniu_room_name: String?
    var qiniu_token: String?
    var grade_img: String?
    var type: String?
    var distance: String?
    var live_store_id: String?
}
class TutorVideoModel: KeyValueModel {
    var video_id: String?
    var title : String?
    var user_id : String?
    var video_img : String?
    var url : String?
    var watch_nums : String?
    var comments : String?
    var uptime : String?
    var date : String?
    var share : String?
    var zan : String?
    var intime : String?
    var is_del : String?
    var is_shenhe : String?
    var content : String?
    var date_value : String?
}
// 视频
class VideoModel: KeyValueModel {
    var video_id: String?
    var title: String?
    var video_img: String?
    var url: String?
    var watch_nums: String?
    var comments: String?
    var share: String?
    var zan: String?
    var user_id: String?
    var username: String?
    var intime: String?
}
class live_store: KeyValueModel {
    var date_value: String?
    var live_id: String?
    var play_img: String?
    var title: String?
    var url: String?
}
class Photo: KeyValueModel {
    var time: String?
    var list: [PhotoList]?
}
class PhotoList : KeyValueModel {
    var imgs: String?
    var user_imgs_id: String?
    
    var section = 0
    var isSelected = false
}
// 充值记录
class RechargeRecordModel: KeyValueModel {//充值记录
    
//    var recharge_record_id: String?
//    var user_id: String?
//    var pay_number: String?
//    var amount: String?  //充值金额
//    var meters: String?  //充值钻石
//    var pay_on: String?
//    var pay_type: String?
//    var pay_return: String?
//    var intime: String?  //时间
//    var uptime: String?
    
    var price_list_id: String?
    var price: String?  //充值金额
    var diamond: String?  //充值钻石
    var intime: String?  //时间
    var uptime: String?
    var zeng:String?
    var sign:String?//苹果内购标签
}
class StuRecharageRecordModel:KeyValueModel  {//个人中心---学员充值记录
    var amount:String?//金额
    var uptime:String?//时间
    var pay_type:String?//途径
    var score:String?  //充值币

}
class PersonInfoModel: KeyValueModel {
    var address : String?
    var alias : String?
    var area : String?
    var autograph : String?
    var banned_dis : String?
    var banned_end_time : String?
    var banned_start_time : String?
    var birth_day : String?
    var city : String?
    var company : String?
    var count1 : String?
    var count2 : String?
    var count3 : String?
    var del_time : String?
    var duty : String?
    var experience : String?
    var fans_count : String?
    var follow_count : String?
    var get_money : String?
    var grade : String?
    var hx_password : String?
    var hx_username : String?
    var id : String?
    var img : String?
    var intime : String?
    var is_authen : String?
    var is_banned : String?
    var is_del : String?
    var is_fans : String?
    var is_remind : String?
    var is_stop : String?
    var is_titles : String?
    var is_tuijian : String?
    var is_wheat : String?
    var mark : String?
    var top: [ContributionModel]?
    var money : String?
   // var openid : String?
    var phone : String?
    var province : String?
   // var pwd : String?
  //  var qq_openid : String?
    var score : String?
    var sex : String?
   // var titles_dis : String?
  //  var titles_end_time : String?
  //  var titles_start_time : String?
    //token = 59339c25492fd;
    var type : String?
  //  var uptime : String?
    var url : String?
    var user_id : String?
    var username : String?
    var video_count : String?
    var weibo : String?
    var zan : String?
}
class FocusVideoModel: KeyValueModel{
    var collection_id: String?
    var video_id: String?
    var title : String?            //
    var video_img : String?
    var url : String?
    var comments : String?               //评论数
    var watch_nums : String?        //观看数
    var username : String?
}
class FollowListModel: KeyValueModel{
//    var user_id:String?
//    var img : String?             //
//    var username : String?
//    var sex : String?
//    var autograph : String?
//    var is_follow : String?             //1是关注
//    var type: String?   //1 老师 2 学生
//    var mark: String? //老师的情况下是真实评分，学生不处理
    var autograph : String?
    var img : String?
    var is_follow : String?
    var mark : String?
    var sex : String?
    var type : String?
    var user_id : String?
    var username : String?
}
class GradeModel: KeyValueModel{
    var img: String?
    var grade: String?
    var is_highest: String?
    var between: String?
    var percentage: String?
}
class HisInfo: KeyValueModel {
    var user_id: String?
    var img: String?
    var username : String?
    var type : String?                   //1是学员；2是导师
    var id : String?
    var sex : String?
    var phone : String?
    var autograph : String?
    var hx_username : String?
    var hx_password : String?
    var mark : String?
    var is_live : String?             //1正在直播；2没有直播
    var live_id : String?             //直播id
    var top: [ContributionModel]?                         //贡献榜
    var live_count: String?             //直播数
    var video_count: String?            //视频数
    var follow_count: String?
    var fans_count: String?
    var is_follow: String?
    var video: [VideoModel]?
    var live_store: [MyLiveList]?
    var auth: String?
    var shield: String?                 //1 没有拉黑过   2 处在被拉黑的状态
    var is_vip: String?                 //1 不是会员     2会员
    
    var isFirst = false
}
class HuoliRankingListModel: KeyValueModel { //火力贡献排行版
    var give_gift_id: String?
    var user_id: String?
    var live_id: String?
    var user_id2: String?
    var gift_id: String?
    var intime: String?
    var date: String?
    var jewel: String?
    var experience: String?
    var grade_img: String?
    var count: String?   //贡献总值
    var username: String?  //昵称
    var img: String?
    var hx_username: String?
}
class PriceListModel: KeyValueModel{
    var price_list_id: String?
    var price: String?
    var diamond: String?
    var zeng: String?
    var sign: String?
   
}
class MyPurseModel: KeyValueModel {
    var id: String?                 //卡号id
    var member_id: String?
    var bank_name: String?
    var bank_card: String?             // 卡号
    var realname: String?
    var card: String?
    var is_default: String?
    var message: String?
    var type: String?
    var pay_type: String?
    var bank_id: String?
    var phone: String?
}
class FensiModel: KeyValueModel {
    var user_id: String?
    var img: String?
    var username: String?
    var sex: String?
    var autograph: String?
    var hx_username: String?
    var is_follow: String?  //是否关注   1：未关注  2：已关注
    var grade:String?
    var grade_img: String?
}
class TScenceModel: KeyValueModel {
    var id: String?
    var name: String?
    var img: String?
    var intro: String?
    var value: String?
    var limit_value: String?
    var address: String?
}
class TPointing: KeyValueModel {
    var banner: [BannerTPointingModel]?
    var list : [ListTPointingModel]?
}
class TStudentModel: KeyValueModel {
    var user_id: String?
    var username: String?
    var sex: String?
    var img: String?
    var autograph: String?
    var state: String?       //1高级；2钻石
}
class TIncomeModel: KeyValueModel {
    var earnings:String?             //金额
    var content:String?          //内容
    var date: String?        //时间
}
class TCurriculumDetailModel: KeyValueModel{
    
    var video_id:String?
    var title: String?
    var video_img: String?
    var url: String?
    var watch_nums: String?
    var comments: String?
    var share: String?
    var zan: String?
    var content: String?
    var user_id: String?
    var username: String?
    var mark: String?
    var sex: String?
    var img: String?
    var collect_nums: String?
    var is_collect: String?
    var is_follow: String?
    var share_url: String?
    var comment: [BBSResponseModel]?
    
}
class BannerTPointingModel: KeyValueModel {
    var b_img: String?
}

class ListTPointingModel: KeyValueModel {
    var user_id: String?
    var sex: String?
    var img: String?
    var autograph: String?
    var mark: String?
    var hx_username: String?
    var hx_password: String?
    var username: String?
    var fee: String?
}
class CityModel: KeyValueModel {
    var shouzimu : String?
    var list: [LetterModel]?
}
class LetterModel: KeyValueModel {
    var city: String?
    var shouzimu: String?
}
class ShopCategory: KeyValueModel {
    var category: String?
    var id: String?
    var picture: String?
    var banner_img: String?
}
class GoodsCartModel: KeyValueModel {
    var id: String?
    var goods_id: String?
    var number: String?
    var is_check: String?
    var kinds_id: String?
    var kinds_id2: String?
    var name: String?
    var thumb: String? //缩略图
    var img: String? //原图
    var price: String?
    var sale_price: String? //单价
    var stock: String?
    var kinds1: String?
    var kinds2: String?
    var kinds: String?
    var kinds_detail: [GoodsCartKindDetailModel]?
}
class GoodsCartKindDetailModel: KeyValueModel{
    var kind_detail: String?
    var kind: String?
}
class orderFormDetailModel: KeyValueModel {
    var order_no: String?
    var amount: String?
    var paid: String?
    var name: String?
    var phone: String?
    var address: String?
    var send_bill: String?
    var state: String?
    var remark: String?
    var kuaidi: String?
    var kuaidi_name: String?
    var kuaidi_node: String?
    var kuaidi_state: String?
    var postage: String?
    var has_postage: String?
    var type: String?
    var deduction: String?
    var score: String?
    var bill_name: String?
    var order_detail: [ConfirmOrder]?
}
class expressageModel: KeyValueModel {
    
}
//class orderFormDetail: KeyValueModel {
//    var order_id: String?
//    var number: String?
//    var goods_id: String?
//    var kinds_id: String?
//    var name: String?
//    var sale_price: String?
//    var price: String?
//    var img: String?
//    var thumb: String?
//    var kinds: String?
//    var kinds_detail: [kindsDetailModel]?
//}
//class kinds_detailModel: KeyValueModel {
//    var kind_detail: String?
//    var kind: String?
//}
class MemberShipModel:KeyValueModel{
    var state:String?
    var user_id2:String?
    var date_value: String?
    var username: String?
    var id: String?
    var is_xu: String?
}
class UpdateMemberShipModel:KeyValueModel{
    var user_id: String?
    var username: String?
    var img: String?
    var id: String?
    var sex: String?
    var zuan_price: NSNumber?
    var gao_price: NSNumber?
    var status: String?
    var date_value: String?
}
class OrderListModel: KeyValueModel{
    var id: String?
    var order_no: String?
    var paid: String?
    var phone: String?
    var deduction: String?
    var score: String?
    var state: String?
    var type: String?
    var has_postage: String?
    var postage: String?
    var item: NSNumber?
    var isCancel = false
    var kuaidi: String?
    var kuaidi_node: String?
    var kuaidi_name: String?
    var kuaidi_state: String?
    var order_detail: [GoodsCartModel]?
    
}
class GoodsCollectionModel:KeyValueModel{
    var collection_id: String?
    var goods_id: String?
    var name: String?
    var price: String?
    var sale_price: String?
    var thumb: String?
    var img: String?
    var sale_number: String?
    var number: String?
}
class BannerCategorysModel: KeyValueModel {
    var id : String?
    var category : String?
    var picture: String?
    var banner_img: String?
}
class SecondCategorysModel: KeyValueModel {
    var id: String?
    var category: String?
    var picture: String?
    
}
class SearchResultModel:KeyValueModel {
    var goods_id: String?
    var name: String?
    var thumb: String?
    var img: String?
    var sale_price: String?
}
class ShopList: KeyValueModel {
    var goods_id: String?
    var img: String?
    var name: String?
    var sale_price: String?
    var thumb: String?
}
class FeedbackModel: KeyValueModel {
    var message_id: String?
    var user_id: String?
    var summary: String?
    var is_read: String?
    var order_id: String?
    var type: String?
    var intime: String?
    var title: String?
}
class TSceneDetailModel: KeyValueModel{
    
    var id: String?
    var img: String?
    var name: String?
    var address: String?
    var intro: String?
    var value: String?
    var limit_value: String?
    var price: String?
    var vip_price: String?
    var content: String?
    var is_vip: String?
    var amount: String?
    var number: String?
    
}
class LogisticsModel: KeyValueModel {
    var EBusinessID: String?
    var ShipperCode: String?
    // var Success: String?
    var LogisticCode: String?
    var State: String?
    var Traces: [TracesModel]?
    
}
class TracesModel: KeyValueModel{
    var isCurrentState = false
    var AcceptTime: String?
    var AcceptStation: String?
}
class GoodsListModel: KeyValueModel {
    var goods_id: String?
    var name: String?
    var thumb: String?
    var img: String?
    var sale_price: String?
}
