//
//  TAAnimatedDotView.h
//  TAPageControl
//
//  Created by Tanguy Aladenise on 2015-01-22.
//  Copyright (c) 2015 Tanguy Aladenise. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TAAnimatedDotView : UIView

@property (nonatomic, strong) UIColor *dotColor;

- (void)changeActivityState:(BOOL)active;

@end
