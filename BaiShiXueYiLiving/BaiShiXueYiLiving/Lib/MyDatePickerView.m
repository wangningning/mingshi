//
//  MyDatePickerView.m
//  蜜蜂数据
//
//  Created by 曾觉新 on 15/12/21.
//  Copyright © 2015年 曾觉新. All rights reserved.
//

#import "MyDatePickerView.h"

@interface MyDatePickerView ()


@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIDatePicker *myDatePicker;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *confirmButton;


@end

@implementation MyDatePickerView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initViews];
    }
    return self;
}

- (void)initViews
{
    self.backgroundColor = [UIColor colorWithRed:0.00 green:0.00 blue:0.00 alpha:0.3];
    self.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    
    
    
    _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, 300)];
    _bgView.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.00];
    [self addSubview:_bgView];
    
    _myDatePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 40, _bgView.frame.size.width, _bgView.frame.size.height - 40)];
    _myDatePicker.backgroundColor = [UIColor whiteColor];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *maxDate = [dateFormatter dateFromString:@"2099-12-1"];
    _myDatePicker.minimumDate = [NSDate dateWithTimeIntervalSinceNow:8*60*60];
    _myDatePicker.maximumDate = maxDate;
    _myDatePicker.datePickerMode = UIDatePickerModeDate;
    [_bgView addSubview:_myDatePicker];
    
    _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    [_cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _cancelButton.frame = CGRectMake(0, 0, 60, 40);
    [_cancelButton addTarget:self action:@selector(cancelButtonCancel:) forControlEvents:UIControlEventTouchUpInside];
    [_bgView addSubview:_cancelButton];
    
    
    _confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_confirmButton setTitle:@"确定" forState:UIControlStateNormal];
    [_confirmButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _confirmButton.frame = CGRectMake(_bgView.frame.size.width - 60, 0, 60, 40);
    [_confirmButton addTarget:self action:@selector(confirmButtonConfirm:) forControlEvents:UIControlEventTouchUpInside];
    [_bgView addSubview:_confirmButton];
}

- (void)cancelButtonCancel:(UIButton *)bu
{
    if ([self.delegate respondsToSelector:@selector(myDatePickerCancel)]) {
        [self.delegate myDatePickerCancel];
    }
    [self dismiss];
}
- (void)confirmButtonConfirm:(UIButton *)bu
{
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//    NSString *destDateString = [dateFormatter stringFromDate:_myDatePicker.date];
    if ([self.delegate respondsToSelector:@selector(myDatePickerView:date:)]) {
        [self.delegate myDatePickerView:self date:_myDatePicker.date];
    }
}

- (void)showView:(UIView *)view{
    [view addSubview:self];
    CGRect frame = self.frame;
    frame.origin.y = view.frame.size.height;
    self.frame = frame;
    
    __block CGRect blockFrame = frame;
    [UIView animateWithDuration:0.3 animations:^{
        blockFrame.origin.y = view.frame.size.height - frame.size.height;
        self.frame = blockFrame;
    }];
}

- (void)show {
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    __block CGRect blockFrame = _bgView.frame;
    blockFrame.origin.y = self.frame.size.height - _bgView.frame.size.height;
    [UIView animateWithDuration:0.3 animations:^{
        _bgView.frame = blockFrame;
    }];
    
}

- (void)dismiss {
    
    __block CGRect blockFrame = _bgView.frame;
    [UIView animateWithDuration:0.3 animations:^{
        blockFrame.origin.y = self.frame.size.height;
        _bgView.frame = blockFrame;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
    
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
