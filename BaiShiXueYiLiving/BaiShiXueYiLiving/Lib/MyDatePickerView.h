//
//  MyDatePickerView.h
//  蜜蜂数据
//
//  Created by 曾觉新 on 15/12/21.
//  Copyright © 2015年 曾觉新. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyDatePickerView.h"


@protocol MyDatePickerViewDelegate;

@interface MyDatePickerView : UIView

@property (nonatomic, weak) id<MyDatePickerViewDelegate>delegate;

//- (void)showView:(UIView *)view;
- (void)show;
- (void)dismiss;

@end


@protocol MyDatePickerViewDelegate <NSObject>

@optional
- (void)myDatePickerCancel;
- (void)myDatePickerView:(MyDatePickerView *)pickView date:(NSDate *)date;

@end


