//
//  NetworkingRequest.swift
//  CrazyEstate
//
//  Created by Luiz on 2017/1/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import Alamofire
import MJRefresh

let focusPersonNumChangeNotification = "focusPersonNumChangeNotification"

class NetworkingHandle: NSObject {
    static var mainHost = "http://show.qqyswh.com/" + "/api.php"
    static var remainHost = "http://show.qqyswh.com/" + "/api.php/"
    static var shareHost = "http://show.qqyswh.com/"
    // POST 请求 Info
    static func fetchReNetworkData(url: String, at: UIResponder, params: Dictionary<String, Any> = [:], isAuthHide: Bool = true, isShowHUD: Bool = true, isShowError: Bool = true, hasHeaderRefresh: UIScrollView? = nil, success: @escaping (Dictionary<String, Any>) -> (), failure: (() -> ())? = nil) {
        var params = params
        
        if let m = DLUserInfoHandler.getIdAndToken() {
            params["uid"] = m.id
            params["token"] = m.token
        }
        print(params)
        var atView: UIView
        if at is UIViewController {
            atView = (at as! UIViewController).view
        } else if at is UIView {
            if let vc = (at as? UIView)?.responderViewController() {
                atView = vc.view
            }
            atView = UIApplication.shared.keyWindow!
        } else {
            atView = UIApplication.shared.keyWindow!
        }
        
        if hasHeaderRefresh == nil, isShowHUD {
            ProgressHUD.showLoading(toView: atView)
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        print("请求url:\(remainHost)/\(url)")
        Alamofire.request(mainHost + url, method: .post, parameters: params).responseJSON { (dataResponse) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            if let scrollView = hasHeaderRefresh {
                scrollView.mj_header?.endRefreshing()
                scrollView.mj_footer?.endRefreshing()
            } else if isAuthHide {
                ProgressHUD.hideLoading(toView: atView)
            }
            if let error = dataResponse.result.error {
                if let status = dataResponse.response?.statusCode {
                    if status != 200{
                        ProgressHUD.showNoticeOnStatusBar(message: "\(status)-服务器访问失败")
                    }
                } else {
                    ProgressHUD.showNoticeOnStatusBar(message: error.localizedDescription)
                }
                failure?()
            } else {
                guard let json = dataResponse.result.value else {
                    failure?()
                    ProgressHUD.showNoticeOnStatusBar(message: "未知错误")
                    return
                }
                print("##########\(json)")
                let result: Dictionary<String, Any> = json as! Dictionary
                let code: String = (result["status"] as? String)!
                if code == "ok" {
                    if atView is UIWindow || atView.superview != nil {
                        success(result)
                    }
                } else {
                    switch code {
                    case "pending":
                        ProgressHUD.showNoticeOnStatusBar(message: result["data"] as! String)
                        DLUserInfoHandler.deleteUserInfo()
                        EMClient.shared().logout(true)
                        UIApplication.shared.keyWindow?.rootViewController = LoginNavigationController.setup()
                    default:
                        failure?()
                        if isShowError {
                            ProgressHUD.showMessage(message: result["data"] as! String)
                        }
                    }
                }
            }
        }
    }
    static func fetchNetworkData(url: String, at: UIResponder, params: Dictionary<String, Any> = [:], isAuthHide: Bool = true, isShowHUD: Bool = true, isShowError: Bool = true, hasHeaderRefresh: UIScrollView? = nil, success: @escaping (Dictionary<String, Any>) -> (), failure: (() -> ())? = nil) {
        var params = params
        
        if let m = DLUserInfoHandler.getIdAndToken() {
            params["uid"] = m.id
            params["token"] = m.token
        }
        print(params)
        var atView: UIView
        if at is UIViewController {
            atView = (at as! UIViewController).view
        } else if at is UIView {
            if let vc = (at as? UIView)?.responderViewController() {
                atView = vc.view
            }
            atView = UIApplication.shared.keyWindow!
        } else {
            atView = UIApplication.shared.keyWindow!
        }
               
        if hasHeaderRefresh == nil, isShowHUD {
            ProgressHUD.showLoading(toView: atView)
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        print("请求url:\(mainHost)/\(url)")
        Alamofire.request(mainHost + url, method: .post, parameters: params).responseJSON { (dataResponse) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            if let scrollView = hasHeaderRefresh {
                scrollView.mj_header?.endRefreshing()
                scrollView.mj_footer?.endRefreshing()
            } else if isAuthHide {
                ProgressHUD.hideLoading(toView: atView)
            }
            if let error = dataResponse.result.error {
                if let status = dataResponse.response?.statusCode {
                    if status != 200{
                        ProgressHUD.showNoticeOnStatusBar(message: "\(status)-服务器访问失败")
                    }
                } else {
                    ProgressHUD.showNoticeOnStatusBar(message: error.localizedDescription)
                }
                failure?()
            } else {
                guard let json = dataResponse.result.value else {
                    failure?()
                    ProgressHUD.showNoticeOnStatusBar(message: "未知错误")
                    return
                }
                print("##########\(json)")
                let result: Dictionary<String, Any> = json as! Dictionary
                let code: String = (result["status"] as? String)!
                if code == "ok" {
                    if atView is UIWindow || atView.superview != nil {
                        success(result)
                    }
                } else {
                    switch code {
                    case "pending":
                        ProgressHUD.showNoticeOnStatusBar(message: result["data"] as! String)
                        DLUserInfoHandler.deleteUserInfo()
                        EMClient.shared().logout(true)
                        UIApplication.shared.keyWindow?.rootViewController = LoginNavigationController.setup()
                    default:
                        failure?()
                        if isShowError {
                            ProgressHUD.showMessage(message: result["data"] as! String)
                        }
                    }
                }
            }
        }
    }
    //GET请求
    static func fetchGetNetworkData(url: String, at: UIResponder, params: Dictionary<String, Any> = [:], isAuthHide: Bool = true, isShowHUD: Bool = true, isShowError: Bool = true, hasHeaderRefresh: UIScrollView? = nil, success: @escaping (Dictionary<String, Any>) -> (), failure: (() -> ())? = nil) {
        var params = params
        
        if let m = DLUserInfoHandler.getIdAndToken() {
            params["uid"] = m.id
            params["token"] = m.token
        }
        var atView: UIView
        if at is UIViewController {
            atView = (at as! UIViewController).view
        } else if at is UIView {
            if let vc = (at as? UIView)?.responderViewController() {
                atView = vc.view
            }
            atView = UIApplication.shared.keyWindow!
        } else {
            atView = UIApplication.shared.keyWindow!
        }
        print(params)
        if hasHeaderRefresh == nil, isShowHUD {
            ProgressHUD.showLoading(toView: atView)
        }
        print("**********\(url)")
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        Alamofire.request(mainHost + url, method: .get, parameters: params).responseJSON { (dataResponse) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            if let scrollView = hasHeaderRefresh {
                scrollView.mj_header?.endRefreshing()
                scrollView.mj_footer?.endRefreshing()
            } else if isAuthHide {
                ProgressHUD.hideLoading(toView: atView)
            }
            if let error = dataResponse.result.error {
                if let status = dataResponse.response?.statusCode {
                    ProgressHUD.showNoticeOnStatusBar(message: "\(status)-服务器访问失败")
                } else {
                    ProgressHUD.showNoticeOnStatusBar(message: error.localizedDescription)
                }
                failure?()
            } else {
                guard let json = dataResponse.result.value else {
                    failure?()
                    ProgressHUD.showNoticeOnStatusBar(message: "未知错误")
                    return
                }
                print("##########\(json)")
                
                let result: Dictionary<String, Any> = json as! Dictionary
                let code: String = (result["status"] as? String)!
                if code == "ok" {
                    if atView is UIWindow || atView.superview != nil {
                        success(result)
                    }
                } else {
                    switch code {
                    case "pending":
                        ProgressHUD.showNoticeOnStatusBar(message: result["data"] as! String)
                        DLUserInfoHandler.deleteUserInfo()
                        EMClient.shared().logout(true)
                        UIApplication.shared.keyWindow?.rootViewController = LoginNavigationController.setup()
                    default:
                        failure?()
                        if isShowError {
                            ProgressHUD.showMessage(message: result["data"] as! String)
                        }
                    }
                }
            }
        }
    }

    /// 图片上传
    // 单图
    static func uploadPicture(url: String, atVC: UIViewController, image: UIImage, params: Dictionary<String, String> = [:], isAuthHide: Bool = true, uploadSuccess: @escaping (_: Dictionary<String, Any>) -> (), failure: (() -> ())? = nil) {
        var params = params
        if let m = DLUserInfoHandler.getIdAndToken() {
            params["uid"] = m.id
            params["token"] = m.token
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        ProgressHUD.showLoading(toView: atVC.view)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            let data = UIImageJPEGRepresentation(image, 0.75)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyyMMddHHmmssms"
            let imgName = dateFormatter.string(from: Date()) + String(arc4random()) + ".png"
            multipartFormData.append(data!, withName: "mlogo", fileName: imgName, mimeType: "image/png")
            for (key, value) in params {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
            multipartFormData.append("mlogo".data(using: String.Encoding.utf8)!, withName: "file_param")
        }, to: URL(string: mainHost + url)!) { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (response) in
                    if isAuthHide {
                        ProgressHUD.hideLoading(toView: atVC.view)
                    }
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    if response.response?.statusCode == 200 {
                        guard let json = response.result.value else {
                            ProgressHUD.showNoticeOnStatusBar(message: "未知错误")
                            failure?()
                            return
                        }
                        let result: Dictionary<String, Any> = json as! Dictionary
                        print(result)
                        let code: String = (result["status"] as? String)!
                        if code == "ok" {
                            if atVC.view.superview != nil {
                                uploadSuccess(result)
                            }
                        } else {
                            switch code {
                            case "pending":
                                ProgressHUD.showNoticeOnStatusBar(message: result["error"] as! String)
                                DLUserInfoHandler.deleteUserInfo()
                                EMClient.shared().logout(true)
                                UIApplication.shared.keyWindow?.rootViewController = LoginNavigationController.setup()
                            default:
                                ProgressHUD.showMessage(message: result["error"] as! String)
                                failure?()
                            }
                        }
                    } else {
                        failure?()
                        ProgressHUD.showNoticeOnStatusBar(message: "\(String(describing: response.response?.statusCode))-服务器访问失败")
                    }
                })
                
            case .failure(let error):
                if isAuthHide {
                    ProgressHUD.hideLoading(toView: atVC.view)
                }
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                failure?()
                ProgressHUD.showNoticeOnStatusBar(message: error.localizedDescription)
            }
        }
    }
    // 多图
    static func uploadOneMorePicture(url: String, atVC: UIViewController, images: [UIImage], params: Dictionary<String, String> = [:], isAuthHide: Bool = true, uploadSuccess: @escaping (_: Dictionary<String, Any>) -> (), failure: (() -> ())? = nil) {
        
        var params = params
        if let m = DLUserInfoHandler.getIdAndToken() {
            params["uid"] = m.id
            params["token"] = m.token
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        ProgressHUD.showLoading(toView: atVC.view)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if images.count == 0 {
                ProgressHUD.showMessage(message: "请选择图片")
                return
            }
            for (index, value) in images.enumerated() {
                let data = UIImageJPEGRepresentation(value, 0.65)
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyyMMddHHmmssms"
                let imgName = dateFormatter.string(from: Date()) + "\(index).png"
                multipartFormData.append(data!, withName: "\(index)", fileName: imgName, mimeType: "image/png")
            }
            for (key, value) in params {
                multipartFormData.append(value.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
                
            }
        }, to: URL(string: mainHost + url)!) { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (response) in
                    if isAuthHide {
                        ProgressHUD.hideLoading(toView: atVC.view)
                    }
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    if response.response?.statusCode == 200 {
                        guard let json = response.result.value else {
                            ProgressHUD.showNoticeOnStatusBar(message: "未知错误")
                            failure?()
                            return
                        }
                        let result: Dictionary<String, Any> = json as! Dictionary
                        print(result)
                        let code: String = (result["status"] as? String)!
                        if code == "ok" {
                            if atVC.view.superview != nil {
                                uploadSuccess(result)
                            }
                        } else {
                            switch code {
                            case "pending":
                                ProgressHUD.showNoticeOnStatusBar(message: result["error"] as! String)
                                DLUserInfoHandler.deleteUserInfo()
                                EMClient.shared().logout(true)
                                UIApplication.shared.keyWindow?.rootViewController = LoginNavigationController.setup()
                            default:
                                ProgressHUD.showMessage(message: result["error"] as! String)
                                failure?()
                            }
                        }
                    } else {
                        failure?()
                        ProgressHUD.showNoticeOnStatusBar(message: "\(String(describing: response.response?.statusCode))-服务器访问失败")
                    }
                })
                
            case .failure(let error):
                if isAuthHide {
                    ProgressHUD.hideLoading(toView: atVC.view)
                }
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                failure?()
                ProgressHUD.showNoticeOnStatusBar(message: error.localizedDescription)
            }
        }
    }
    
//    class func download(model: LivingBGMModel, complete: @escaping ((LivingBGMModel)->())) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
//        let _ = Alamofire.download(model.path!) { (l, response) -> (destinationURL: URL, options: DownloadRequest.DownloadOptions) in
//            if response.statusCode == 200 {
//            } else {
//                ProgressHUD.showNoticeOnStatusBar(message: "\(String(describing: response.statusCode))-服务器访问失败")
//            }
//            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
//            let fileURL = documentsURL?.appendingPathComponent(response.suggestedFilename!)
//            model.pathPostfix = response.suggestedFilename!
//            SQLiteManager.manager.insert(model: model)
//            complete(model)
//            UIApplication.shared.isNetworkActivityIndicatorVisible = false
//            //两个参数表示如果有同名文件则会覆盖，如果路径中文件夹不存在则会自动创建
//            return (fileURL!, [.removePreviousFile, .createIntermediateDirectories])
//        }
//        //        download.downloadProgress(queue: DispatchQueue.main, closure: progress)
//    }

}

func fetchVerificationCodeCountdown(button: UIButton, timeOut: Int) -> DispatchSourceTimer {
    var timeout = timeOut
    let timer = DispatchSource.makeTimerSource(queue: DispatchQueue.global())
    timer.setEventHandler {
        if timeout <= 1 {
            timer.cancel()
            DispatchQueue.main.sync {
                button.setTitle("发送验证码", for: .normal)
                button.isEnabled = true
            }
        } else {
            DispatchQueue.main.sync {
                button.setTitle("\(timeout)秒后重发", for: .normal)
                button.isEnabled = false
            }
            timeout -= 1
        }
    }
    timer.scheduleRepeating(deadline: .now(), interval: .seconds(1))
    timer.resume()
    
    return timer
}
func focusOtherPerson(viewResponder: UIResponder,other_id: String, btn: UIButton, type: String, focusCommpleted: (()->())?) {
    if DLUserInfoHandler.getIdAndToken()?.id == other_id {
        ProgressHUD.showNoticeOnStatusBar(message: "不能关注自己哦！")
        return
    }
    let param = ["user_id2": other_id]
    NetworkingHandle.fetchNetworkData(url: "/Member/follow_user", at: viewResponder, params: param, isShowHUD:false, success: { (response) in
        btn.isSelected = !btn.isSelected
        
        focusCommpleted?()
    })
}
func pushToUserInfoCenter(atViewController vc: UIViewController, uId: String, name: String = "", isHasLive: Bool = false, liveUserName: String = "", isCurrentUserLiving: Bool = false) {
    if DLUserInfoHandler.getIdAndToken()?.id == uId {
        if vc.navigationController?.viewControllers.first is MineViewController {
            _ = vc.navigationController?.popToRootViewController(animated: true)
        } else {
            ProgressHUD.showMessage(message: "你点的是你自己")
        }
        return
    }
    let targetVC = HisPersonalMemberCenterViewController()
    targetVC.userId = uId
    targetVC.isHasLive = isHasLive
    targetVC.isCurrentUserLiving = isCurrentUserLiving
    vc.navigationController?.pushViewController(targetVC, animated: true)
}

//import SQLite
//struct SQLiteManager {
//    
//    private var db: Connection!
//    private let tableName = Table("LvingMusic") //表名
//    private let id = Expression<Int64>("id")      //主键
//    private let songName = Expression<String>("songName")  //列表1
//    private let singer = Expression<String>("singer") //列表2
//    private let pathPostfix = Expression<String>("pathPostfix") //列表3
//    private let time = Expression<String>("time") //列表4
//    
//    static var manager = SQLiteManager()
//    
//    init() {
//        createdsqlite3()
//    }
//    
//    //创建数据库文件
//    mutating func createdsqlite3(filePath: String = "/Documents")  {
//        
//        let sqlFilePath = NSHomeDirectory() + filePath + "/db.sqlite3"
//        do {
//            db = try Connection(sqlFilePath)
//            try db.run(tableName.create { t in
//                t.column(id, primaryKey: true)
//                t.column(songName)
//                t.column(pathPostfix)
//                t.column(singer)
//                t.column(time)
//            })
//        } catch { print(error) }
//    }
//    
//    //插入数据
//    func insert(model: LivingBGMModel){
//        do {
//            let insert = tableName.insert(songName <- model.song_name!, singer <- model.singer!, pathPostfix <- model.pathPostfix!, time <- model.time!)
//            try db.run(insert)
//        } catch {
//            print(error)
//        }
//    }
//    
//    //读取数据
//    func readData() -> [LivingBGMModel] {
//        var dataArr: [LivingBGMModel] = []
//        for model in try! db.prepare(tableName) {
//            let m = LivingBGMModel()
//            m.dbId = model[id]
//            m.singer = model[singer]
//            m.song_name = model[songName]
//            m.time = model[time]
//            m.pathPostfix = model[pathPostfix]
//            dataArr.append(m)
//        }
//        return dataArr
//    }
//    
//    //更新数据
//    //    func updateData(dbId: Int64, old_name: String, new_name: String) {
//    //        let currUser = tableName.filter(id == dbId)
//    //        do {
//    //            try db.run(currUser.update(name <- name.replace(old_name, with: new_name)))
//    //        } catch {
//    //            print(error)
//    //        }
//    //
//    //    }
//    
//    //删除数据
//    func delData(dbId: Int64) {
//        let currUser = tableName.filter(id == dbId)
//        do {
//            try db.run(currUser.delete())
//        } catch {
//            print(error)
//        }
//    }
//}

