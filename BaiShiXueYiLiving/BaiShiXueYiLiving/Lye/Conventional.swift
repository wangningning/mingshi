
//
//  Conventional.swift
//  Duluo
//
//  Created by sh-lx on 2017/3/20.
//  Copyright © 2017年 tts. All rights reserved.
//
import UIKit
import Foundation

let atUserNotifiction = "atUserNotifiction"

// 直播间环信透传命令
let userApplyConferenceCMD = "applyConference" // 观众申请连麦
let anchorAgreeConferenceCMD = "anchorAgreeConference" // 主播接受连麦申请
let conferenceCMD = "conference" // 主播邀请连麦
let userDisagreeConferenceCMD = "userDisagreeConference" //用户拒绝连麦

let anchorStopLiveCMD = "anchorStopLive" // 主播关闭直播

let settingUpManagerCMD = "settingUpManager" // 设置管理员
let cancelManagerCMD = "cancelManager" //取消管理
let kickoutCMD = "kickout" // 剔出
let blacklistCMD = "balcklist" // 拉黑
let cancelBlacklistCMD = "cancelBlacklist" // 取消拉黑
let disableSendMsgCMD = "disableSendMsg" // 禁言
let ableSendMsgCMD = "ableSendMsg" // 取消禁言


let kScreenWidth = UIScreen.main.bounds.width
let kScreenHeight = UIScreen.main.bounds.height

let themeColor = UIColor(hexString: "#EC6B1A")

func defaultFont(size: CGFloat) -> UIFont {
    guard let font = UIFont(name: "PingFang SC", size: size) else {
        return UIFont.systemFont(ofSize: size)
    }
    return font
}
/**
 时间戳转时间
 
 
 :param: timeStamp <#timeStamp description#>
 
 :returns: return time
 */
func timeStampToString(timeStamp:String,format:String)->String {
    
    let string = NSString(string: timeStamp)
    
    let timeSta:TimeInterval = string.doubleValue
    let dfmatter = DateFormatter()
    dfmatter.timeStyle = .short
    dfmatter.dateStyle = .medium
    dfmatter.dateFormat = format
    
    let date = NSDate(timeIntervalSince1970: timeSta)
    
    print(dfmatter.string(from: date as Date))
    return dfmatter.string(from: date as Date)
}
/**
 视频编码
 */
//func convertMP4(movurl: URL) -> URL{
//
//}
func dataPath() -> String{
    let dataPath = String.init(format: "%@/Library/appdata/chatbuffer", NSHomeDirectory())
    let fm = FileManager.default
    if !fm.fileExists(atPath: dataPath) {
         //  fm.createDirectory(atPath: dataPath, withIntermediateDirectories: true, attributes: nil)
        do {
           try fm.createDirectory(atPath: dataPath, withIntermediateDirectories: true, attributes: nil)
            return dataPath
        } catch  {
            print("文件目录出错")
            
        }
        
    }
    return dataPath
}
class ProgressHUD: NSObject {
    static func showSuccess(message: String) -> () {
        SwiftNotice.showNoticeWithText(.success, text: message, autoClear: true, autoClearTime: 3)
    }
    static func showMessage(message: String) -> () {
        UIApplication.shared.keyWindow?.showMessage(message, interval: 1, position: "center" as AnyObject)
    }
    static func showLoading(toView: UIView, message: String = "努力加载中...") -> () {
        toView.showLoadingTilteActivity(message, position: "center" as AnyObject?)
    }
    static func hideLoading(toView: UIView) -> () {
        toView.hideActivity()
    }
    static func showNoticeOnStatusBar(message: String) {
        SwiftNotice.noticeOnStatusBar(message, autoClear: true, autoClearTime: 3)
    }
}
