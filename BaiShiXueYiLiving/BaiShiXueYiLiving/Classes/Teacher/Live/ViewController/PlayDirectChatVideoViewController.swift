//
//  PlayDirectChatVideoViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/9/6.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import AVKit

class PlayDirectChatVideoViewController: UIViewController, AVPlayerViewControllerDelegate {
    var player : AVPlayer!                //播放器
    var playerVC: AVPlayerViewController! //控制器
    var url: String!
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // AVPlayer 播放器
//        let playerItem = AVPlayerItem.init(url: URL.init(string: url)!)
//        self.player = AVPlayer.init(playerItem: playerItem)
//        let playerLayer = AVPlayerLayer.init(player: self.player)
//        playerLayer.bounds = self.view.bounds
//        playerLayer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height/2)
//        playerLayer.videoGravity = AVLayerVideoGravityResizeAspect
//        player.play()
//        self.view.layer.addSublayer(playerLayer)
        self.playerVC = AVPlayerViewController.init()
        self.playerVC.player = AVPlayer.init(url: URL.init(string: url)!)
        self.playerVC.videoGravity = AVLayerVideoGravityResizeAspect
        self.playerVC.showsPlaybackControls = true
        self.playerVC.delegate = self
        self.playerVC.player?.play()
        self.playerVC.view.bounds = self.view.bounds
        self.playerVC.view.center = self.view.center
       // self.present(<#T##viewControllerToPresent: UIViewController##UIViewController#>, animated: <#T##Bool#>, completion: <#T##(() -> Void)?##(() -> Void)?##() -> Void#>)
        self.addChildViewController(self.playerVC)
        self.view.addSubview(self.playerVC.view)
        // Do any additional setup after loading the view.
    }

        override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
