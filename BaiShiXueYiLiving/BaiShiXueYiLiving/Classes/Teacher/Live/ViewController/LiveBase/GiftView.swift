//
//  GiftView.swift
//  CrazyEstate
//
//  Created by 梁毅 on 2017/2/19.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class GiftModel: KeyValueModel {
    
    var gift_id: String?
    var name: String?
    var img: String?
    var price: String?
    var experience: String?
    var is_running: String?
    var is_special: String?
    var isSelected = false
}
let kGiftCache = "kGiftCache"
class GiftView: UIView {
    
    var dataArr = [GiftModel]()
    var live_ids: String!
    
    @IBOutlet weak var moneybutton: UIButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var layout: UICollectionViewFlowLayout!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var snedBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var selectedModel: GiftModel?
    var didSelected: ((_: GiftModel) ->())?
    var moneySum: UInt64 = 0
    var isFirstSend = true
    
    
    static func show(atView: UIView, live_id: String, clickSend: @escaping ((_: GiftModel) ->())) -> GiftView {
        
        let selfView = Bundle.main.loadNibNamed("GiftView", owner: nil
            , options: nil)!.last as! GiftView
        atView.addSubview(selfView)
        selfView.live_ids = live_id
        selfView.didSelected = clickSend
        let view = selfView.contentView
        view?.isHidden = false
        var frame = view?.frame
        let oldFrame =  frame
        frame?.origin.y = selfView.frame.size.height
        view?.frame = frame!
        UIView.animate(withDuration: 0.25, animations: {
            view?.frame = oldFrame!
        })
        
        return selfView
    }
    func show() {
        self.contentView?.isHidden = false
        self.isHidden = false
        var frame = self.contentView?.frame
        frame?.origin.y = kScreenHeight - (frame?.height)!
        UIView.animate(withDuration: 0.25, animations: {
            self.contentView?.frame = frame!
        })
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
        
        snedBtn.setTitleColor(themeColor, for: .normal)
        
        let itemW = kScreenWidth/5
        layout.itemSize = CGSize(width: itemW, height: itemW)
    
        collectionView.register(UINib(nibName: "GiftCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GiftCollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        
        //网络请求 刷新金币
        reloadMoneyLabel()
        
        //请求礼物列表
        loadData()
    }
    //
    @IBAction func dismissButtonAction(_ sender: Any) {
        let view = self.contentView
        var frame = view?.frame
        frame?.origin.y += (frame?.size.height)!
        UIView.animate(withDuration: 0.25, animations: {
            view?.frame = frame!
        }) { (finished) in
            self.contentView.isHidden = true
            self.isHidden = true
        }
    }
    @IBAction func sendButtonAction(_ sender: UIButton) {
        if self.selectedModel == nil {
            return
        }
        if self.moneySum > UInt64((self.selectedModel?.price)!)!{
            
            if self.selectedModel?.is_running == "1" {
                self.dismissButtonAction(self.snedBtn)
            }
            
            self.didSelected?(self.selectedModel!)
            self.moneySum -= UInt64((self.selectedModel?.price)!)!
            self.moneybutton.setTitle("\(self.moneySum)", for: .normal)
            let param = ["live_id": self.live_ids!,"gift_id": selectedModel!.gift_id!]
            NetworkingHandle.fetchNetworkData(url: "/Index/give_gift", at: self, params: param, isShowHUD: false, success: { (result) in
                
                self.reloadMoneyLabel()
            })
        }else{
            
            self.moneybutton.setTitle("\(self.moneySum)", for: .normal)
            return
        }

       
    }
    
    func reloadMoneyLabel() {
        
        NetworkingHandle.fetchNetworkData(url: "/Index/get_money", at: self, isShowHUD: false, success: { (result) in
            let dic = result["data"] as! [String: AnyObject]
            self.moneybutton.setTitle(dic["money"] as? String, for: .normal)
            if self.isFirstSend{
                self.moneySum = UInt64((dic["money"] as? String)!)!
                self.isFirstSend = false
            }
           
        })
    }
    func loadData() {
        
        let giftDic = UserDefaults.standard.value(forKey: kGiftCache) as? Dictionary<String, Any>
        if giftDic != nil{
            
            self.dataArr = GiftModel.modelsWithArray(modelArray:(giftDic!["data"] as? [[String:AnyObject]])!) as! [GiftModel]
            print(giftDic!)
        }else{
            NetworkingHandle.fetchNetworkData(url: "/Index/gift_list", at: self, isShowHUD: false, success: { (result) in
                
                UserDefaults.standard.set(result, forKey: kGiftCache)
                let data = result["data"] as! [[String: AnyObject]]
                self.dataArr = GiftModel.modelsWithArray(modelArray: data) as! [GiftModel]
                self.collectionView.reloadData()
            })
        }
        

    }
}
// collection
extension GiftView: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let temp = dataArr.count % 10
        let count = temp == 0 ? dataArr.count : (10 - temp) + dataArr.count
        pageControl.numberOfPages = count/10
        return count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GiftCollectionViewCell", for: indexPath) as! GiftCollectionViewCell
        if indexPath.row + 1 > dataArr.count {
            cell.model = GiftModel()
        } else {
            cell.model = dataArr[indexPath.row]
        }
        return cell
    }
    // 代理
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x / (scrollView.frame.width))
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row + 1 > dataArr.count {
            return 
        }
        self.selectedModel?.isSelected = false
        self.selectedModel = self.dataArr[indexPath.row]
        self.selectedModel?.isSelected = true
        collectionView.reloadData()
    }
}

class WatchLiveGiftView: UIView {
    
    var giftContainerView: XGiftContainerView = XGiftContainerView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(giftContainerView)
        giftContainerView.snp.makeConstraints { (make) in
           make.edges.equalTo(0)
        }
        giftContainerView.backgroundColor = UIColor.clear
    }
    func showTheGiftView(username: String, senderURL: String, giftName: String, giftURL: String) {
        let gift = XGiftModel(senderName: username, senderURL: senderURL, giftName: giftName, giftURL: giftURL)
        self.giftContainerView.showGiftModel(gift)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}



