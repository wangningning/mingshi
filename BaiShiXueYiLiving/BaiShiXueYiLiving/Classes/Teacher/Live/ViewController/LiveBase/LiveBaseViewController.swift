//
//  LiveBaseViewController.swift
//  Duluo
//
//  Created by sh-lx on 2017/3/22.
//  Copyright © 2017年 tts. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class LiveBaseViewController: UIViewController, EMChatManagerDelegate, EMChatroomManagerDelegate, LiveChatViewDelegate {
    
    var url: String?    //分享的url
    var avatar: String? //分享的主播头像
    var username: String? //分享的主播用户名
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    // 从右到左
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    
    @IBOutlet weak var conferenceMemberView: UIView!
    @IBOutlet weak var conferenceMemberViewHeightConstraint: NSLayoutConstraint!
    
    var player: PLPlayer? //播放控制
    var sesstion: PLMediaStreamingSession?
    var conferenceUserInfo: Dictionary<String, UIView> = [:]
    var plAnchorId: String?
    var fullScreenView: UIView?
    
    var topView: LiveTopView?
    var chatView: LiveChatView?
    var barrageView: BarrageView?
    var giftAnamatin: WatchLiveGiftView?
    var giftView: GiftView?
    var roomId: String?
    var liveId: String?
    var isCurrentUserLiving = true // 是否是当前用户在直播
    var anchorId: String? = DLUserInfoHandler.getIdAndToken()?.id
    
    var chatInputView: LiveRoomChatView?
    
    var enterView: LiveRoomEnterView?
    var enterRoomCacheData: [[String: String]] = []
    var enterViewShowing = false
    
    var reconnectCount = 0
    var isShowHUD = false
    
    var giftData: [GiftModel] = []
    
    lazy var timer: Timer = {
        Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(LiveBaseViewController.livingReconnect), userInfo: nil, repeats: true)
    }()
    func livingReconnect() {
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.conferenceMemberViewHeightConstraint.constant = kScreenHeight - 65 - 91
        
        IQKeyboardManager.sharedManager().enable = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
        EMClient.shared().chatManager.add(self, delegateQueue: nil)
        EMClient.shared().roomManager.add(self, delegateQueue: nil)
        
        setupSubviews()
        
        NotificationCenter.default.addObserver(self, selector: #selector(LiveBaseViewController.keyboardWillHide(noti:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(LiveBaseViewController.keyboardWillShow(noti:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(atUserNotifictionAction(noti:)), name: Notification.Name(atUserNotifiction), object: nil)
        
        isLivingOrWatchLive = true
    }
    func atUserNotifictionAction(noti: Notification) {
        let dic = noti.object as? [String : String]
        talkBtnClicked(otherName: dic?["username"], otherId: dic?["userid"])
    }

    // 设置子视图
    func setupSubviews() {
        UIApplication.shared.isIdleTimerDisabled = true
        
//        if !isCurrentUserLiving{
//            NetworkingHandle.fetchNetworkData(url: "/Index/gift_list", at: self, isShowHUD: false, success: { (result) in
//                let data = result["data"] as! [[String: AnyObject]]
//                self.giftData = GiftModel.modelsWithArray(modelArray: data) as! [GiftModel]
//                print("~~~~~~~~~~~~~~\(self.giftData.count)")
//            })
//        }
        topView = LiveTopView.show(atView: contentView, liveId: liveId!, isCurrentUserLiving: isCurrentUserLiving, anchorId: anchorId!, roomId: roomId!)
        topView?.attentAnchorSuccessBlock = { [unowned self] in
            let m = DLUserInfoHandler.getUserBaseInfo()
            self.sendLivingMessage(content: m.name + " 关注了名师，不错过下一次直播")
        }
        
        barrageView = BarrageView.show(atView: contentView, TopMargin: 100, BottomMargin:140.0/667*kScreenHeight + 70 + 40 + 80)
        
        chatView = LiveChatView.show(atView: contentView)
        chatView?.delegate = self
        
        giftAnamatin = WatchLiveGiftView()
        contentView.addSubview(giftAnamatin!)
        
        giftAnamatin?.snp.makeConstraints { (make) in
            make.bottom.equalTo(chatView!.snp.top).inset(-40)
            make.left.equalTo(10)
            make.right.equalTo(-90)
            make.height.equalTo(80)
        }
        
        if !isCurrentUserLiving {
            let chat = Chat(name: "直播消息", id: "-000", content: "我们提倡绿色直播，请保持房间的干净整洁!！", grade: "")
            chatView?.tableViewReloadData(data: chat)
            
            btn2.setImage(#imageLiteral(resourceName: "kazhb_zhslw"), for: .normal)
            btn4.isHidden = true
        }
    }
    // 发送直播信息
    func sendLivingMessage(content: String) {
        let body = EMTextMessageBody(text: content)
        let ext = ["user_id": "-000", "username": "直播信息"]
        let message = EMMessage(conversationID: self.roomId!, from: EMClient.shared().currentUsername, to: self.roomId!, body: body, ext: ext)
        message?.chatType = .init(2)
        self.sendMessage(message: message!)
    }
    //MARK: -- 环信相关
    // 连接环信聊天室
    func enterRoomConnectHx(isSuccess: @escaping ((Bool) -> ())) {
        var count = 0
        let hxInfo = DLUserInfoHandler.getUserHXInfo()
        func joinChatroom() {
            EMClient.shared().roomManager.joinChatroom(roomId!, completion: { room, error in
                if error != nil {
                    count += 1
                    if let m = hxInfo, EMClient.shared().isLoggedIn == false, EMClient.shared().isAutoLogin == false {
                        EMClient.shared().login(withUsername: m.name, password: m.pw, completion: { str, error in
                            if error == nil { EMClient.shared().options.isAutoLogin = true }
                            if count == 5 { isSuccess(false) } else { joinChatroom() }
                        })
                    } else {
                        if count == 5 { isSuccess(false) } else { joinChatroom() }
                    }
                } else {
                    isSuccess(true)
                }
            })
        }
        joinChatroom()
    }
    // 环信发送命令信息（群发)
    func sendCmdMessageToChatRoom(action: String, ext: [String: Any]? = nil) {
        var extTemp = ext
        if extTemp == nil {
            let model = DLUserInfoHandler.getUserBaseInfo()
            extTemp = ["userid": model.id, "username": model.name]
        }
        let body = EMCmdMessageBody(action: action)
        let message = EMMessage(conversationID: self.roomId!, from: EMClient.shared().currentUsername, to: self.roomId!, body: body!, ext: extTemp!)
        message?.chatType = .init(2)
        EMClient.shared().chatManager.send(message!, progress: nil) { (messages, error) in
        }
    }
    // 环信发送命令信息（单发）
    func sendCmdMessageTo(chatID: String, action: String) {
        let model = DLUserInfoHandler.getUserBaseInfo()
        let body = EMCmdMessageBody(action: action)
        let message = EMMessage(conversationID: chatID, from: EMClient.shared().currentUsername, to: chatID, body: body!, ext: ["userid": model.id, "username": model.name])
        message?.chatType = .init(0)
        EMClient.shared().chatManager.send(message!, progress: nil) { (messages, error) in
        }
    }
    // 环信直播间发送消息
    func sendMessage(message: EMMessage, isShow: Bool = true) {
        EMClient.shared().chatManager.send(message, progress: nil, completion: { emessage , errors in
        })
        if isShow {
            self.messagesDidReceive([message as Any])
        }
    }
    //MARK: -- 环信代理
    // 接收环信消息
    func messagesDidReceive(_ aMessages: [Any]!) {
        let newMessage = aMessages.last as! EMMessage
        let nowTimeInterval = Date().timeIntervalSince1970
        let interval = nowTimeInterval - TimeInterval(newMessage.timestamp/1000)
        print("#########", interval, "#############")
        if interval > 10 {
            return
        }
        //只接收群聊信息
        if newMessage.chatType == EMChatTypeChatRoom, newMessage.ext != nil {
            let body = newMessage.body as! EMTextMessageBody
            let dic = newMessage.ext as NSDictionary
            
            let usid = dic.object(forKey: "user_id") as? String ?? ""
            let uname = dic.object(forKey: "username") as? String ?? ""
            let uimg = dic.object(forKey: "userimg") as? String ?? ""
            let ugrade = dic.object(forKey: "usergrade") as? String ?? ""
            
            if dic.object(forKey: "intoroom") as? String == "1" {
                let chat: Chat = Chat(name: uname, id: usid, content: body.text!, grade: ugrade, isEnterRoom: true)
                chatView?.tableViewReloadData(data: chat)
                showEnterRoomMessage(grade: ugrade, name: uname)
                return
            }
            if dic.object(forKey: "barrage") as? String == "1" {
                let chat: Chat = Chat(name: uname, id: usid, content: body.text!, grade: ugrade)
                chatView?.tableViewReloadData(data: chat)

                self.barrageView?.sendBarrage(name: chat.userName, str: body.text, avatarUrl: uimg)
                
                topView?.reloadAnchorMoney()
                return
            }
            
            if let gimg = dic.object(forKey: "giftimg") as? String {
                let gname = dic.object(forKey: "giftname") as? String ?? ""
                let gift = XGiftModel(senderName: uname, senderURL: uimg, giftName: gname, giftURL: gimg)
                
                let chat: Chat = Chat(name: uname, id: usid, content: "送了主播一个 " + gname, grade: ugrade)
                chatView?.tableViewReloadData(data: chat)
                
                self.giftAnamatin?.giftContainerView.showGiftModel(gift)

                topView?.reloadAnchorMoney()
                return
            }
            
            let chat: Chat = Chat(name: uname, id: usid, content: body.text!, grade: ugrade)
            if dic.object(forKey: "leaveChatroom") as? String == "1" {
                reloadChatroomMember()
                return
            }
            if let otherName = dic.object(forKey: "otherName") as? String {
                chat.atUserName = otherName
                if dic.object(forKey: "otherId") as? String == DLUserInfoHandler.getIdAndToken()?.id {
                    chat.isAtOneself = true
                }
            }
            chatView?.tableViewReloadData(data: chat)
        }
    }
    // 有人加入聊天室
    func userDidJoin(_ aChatroom: EMChatroom!, user aUsername: String!) {
        reloadChatroomMember()
    }
    // 有人退出聊天室
    func userDidLeave(_ aChatroom: EMChatroom!, user aUsername: String!) {
        reloadChatroomMember()
    }
    // 刷新房间成员
    func reloadChatroomMember() {
        NotificationCenter.default.post(name: Notification.Name.init(reloadChatRoomMemberNotifiction), object: nil)
    }
    // 有人进入直播间
    func showEnterRoomMessage(grade: String, name: String) {
        enterRoomCacheData.append(["grade": "", "name": name])
        
        //不显示进房特效
//        if enterViewShowing == false {
//            showEenterRoomView(dic: enterRoomCacheData.first!)
//        }
    }
    func showEenterRoomView(dic: [String: String]) {
        if enterView == nil {
            enterView = LiveRoomEnterView.setup()
            contentView?.addSubview(enterView!)
        }
        //enterView?.grade.text = dic["grade"]! + "级"
        enterView?.content.text = dic["name"]! + "   进入了房间！"
        
        var frame = enterView?.frame
        frame?.origin.x = kScreenWidth
        enterView?.frame = frame!
        
        enterViewShowing = true
        UIView.animate(withDuration: 0.5, animations: {
            frame?.origin.x = kScreenWidth - 276
            self.enterView?.frame = frame!
        }) { (stop) in
            UIView.animate(withDuration: 1.6, delay: 0, options: .curveLinear, animations: {
                frame?.origin.x = 10
                self.enterView?.frame = frame!
            }, completion: { (stop) in
                UIView.animate(withDuration: 0.5, animations: {
                    frame?.origin.x = -266
                    self.enterView?.frame = frame!
                }) { stop in
                    self.enterViewShowing = false
                    self.enterRoomCacheData.removeFirst()
                    if self.enterRoomCacheData.count > 0 {
                        self.showEenterRoomView(dic: self.enterRoomCacheData.first!)
                    }
                }
            })
        }
    }

    // 弹幕相关
    //MARK: - 键盘监听
    func keyboardWillHide(noti: Notification) {
        let kbInfo = noti.userInfo
        let duration = kbInfo?[UIKeyboardAnimationDurationUserInfoKey] as! Double
        
        var frame = chatInputView?.inputBgView.frame
        UIView.animate(withDuration: duration, animations: {
            frame?.origin.y = kScreenHeight
            self.chatInputView?.inputBgView.frame = frame!
            self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        })
    }
    func keyboardWillShow(noti: Notification) {
        if let tf = chatInputView?.textField, tf.isFirstResponder {
            let kbInfo = noti.userInfo
            let kbRect = (kbInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            let duration = kbInfo?[UIKeyboardAnimationDurationUserInfoKey] as! Double
            var frame = chatInputView?.inputBgView.frame
            
            UIView.animate(withDuration: duration, animations: {
                frame?.origin.y = kScreenHeight - kbRect.height - LiveRoomChatView.inputViewHeight
                self.chatInputView?.inputBgView.frame = frame!
                self.scrollView.setContentOffset(CGPoint(x: 0, y: kbRect.height + LiveRoomChatView.inputViewHeight - 70), animated: true)
            })
        }
    }
    // 弹出键盘
    func talkBtnClicked(otherName: String? = nil, otherId: String? = nil) {
        func start() {
            if chatInputView == nil {
                chatInputView = LiveRoomChatView.show(atView: view, send: { (text, isDanmu) in
                    if let name = otherName, text.contains("@" + name) {
                        let content = text.substring(from: text.at(name.characters.count + 1))
                        self.sendLiveRoomMessage(text: content, isDanmu: isDanmu, otherName: otherName, otherId: otherId)
                    } else {
                        self.sendLiveRoomMessage(text: text, isDanmu: isDanmu)
                    }
                })
            }
            chatInputView?.isHidden = false
            chatInputView?.textField.becomeFirstResponder()
            if let name = otherName {
                chatInputView?.textField.text = "@" + name + " "
            }
        }
        if isCurrentUserLiving == false {
            start()
//            NetworkingHandle.fetchNetworkData(url: "/Index/get_banned", at: self, params: ["live_id": liveId!], isShowHUD: false, success: { (result) in
//                if result["data"] as? String == "2" {
//                    
//                } else {
//                    ProgressHUD.showNoticeOnStatusBar(message: "你已被主播禁言")
//                }
//            })
        } else {
            start()
        }
    }
    // 发送信息
    func sendLiveRoomMessage(text: String, isDanmu: Bool, otherName: String? = nil, otherId: String? = nil) {
        
        let body = EMTextMessageBody(text: text)
        
        let m = DLUserInfoHandler.getUserBaseInfo()
        var ext = ["user_id": m.id, "username": m.name, "userimg": m.img, "usergrade": m.grade]
        if isDanmu {
            ext["barrage"] = "1"
        }
        
        if let name = otherName {
            ext["otherName"] = name
            ext["otherId"] = otherId!
        }
        
        let message = EMMessage(conversationID: roomId!, from: EMClient.shared().currentUsername, to: roomId!, body: body, ext: ext)
        message?.chatType = .init(2)
        self.sendMessage(message: message!)
//        if isDanmu {
//            NetworkingHandle.fetchNetworkData(url: "/Index/screen_price", at: self, params: ["live_id": liveId!], isShowHUD: false, success: { (reslut) in
//                self.topView?.reloadAnchorMoney()
//                self.sendMessage(message: message!)
//            })
//        } else {
//            self.sendMessage(message: message!)
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // 退出按钮
    @IBAction func closeButtonAction(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue:RefreshLoadStateNotification), object: nil)
        quitLiveRoom()
    }
    func quitLiveRoom() {
    }
    
    @IBAction func bottomButtonAction(_ sender: UIButton) {
        if sender == btn1 {
            let shareView = ShareView.show(atView: view, url: self.url!, avatar: self.avatar!, username: self.username!, type: "1")
            if isCurrentUserLiving == false {
                shareView.shareLivingBlock = { [unowned self] in
                    let m = DLUserInfoHandler.getUserBaseInfo()
                    self.sendLivingMessage(content: m.name + "分享了名师的直播")
                }
            }
        } else if sender == btn2 {
            if isCurrentUserLiving {
                beautify()
            } else {
                if giftView != nil {
                    giftView?.show()
                } else {
                    giftView = GiftView.show(atView: self.view, live_id: liveId!, clickSend: { model in
                        self.sendGift(model: model)
                    })
                }
            }
        } else if sender == btn3 {
            if isCurrentUserLiving {
                // 消息
                _ = PrivateChatViewController.show(atVC: self, atView: contentView, isCurrentUserLiving: isCurrentUserLiving)
            } else {
                // 消息
                privateChat()
            }
        } else if sender == btn4 {
            if isCurrentUserLiving {
                toggleCamera()
            } else {
                if let s = self.sesstion, s.isRtcRunning {
//                    ProgressHUD.showNoticeOnStatusBar(message: "你正在与主播连麦")
                    self.sesstion?.toggleCamera()
                    return
                }
//                NetworkingHandle.fetchNetworkData(url: "/Index/is_wheat", at: self, params: ["user_id": anchorId!], isShowHUD: false, success: { (reslut) in
//                    if reslut["data"] as? String == "1" {
//                        if let s = self.sesstion, s.isRtcRunning {
//                            return
//                        }
//                        LyAlertView.alert(atVC: self, message: "是否确定申请与主播连麦", ok: "申请", okBlock: {
//                            self.sendCmdMessageToChatRoom(action: userApplyConferenceCMD)
//                        })
//                    } else {
//                        ProgressHUD.showNoticeOnStatusBar(message: "主播不允许连麦！")
//                    }
//                })
            }
        } else {
            talkBtnClicked()
        }
    }
    // 美颜
    func beautify(){}
    // 翻转
    func toggleCamera(){}
    // 消息
    func privateChat(){}
    // 送礼
    func sendGift(model: GiftModel) {
        let body = EMTextMessageBody(text: "")
        
        let m = DLUserInfoHandler.getUserBaseInfo()
        
        let ext = ["user_id": m.id, "username": m.name, "userimg": m.img, "usergrade": m.grade, "giftimg": model.img!, "giftprice": model.price!, "giftname": model.name!, "gift_id": model.gift_id!]
        let message = EMMessage(conversationID: roomId!, from: EMClient.shared().currentUsername, to: roomId!, body: body, ext: ext)
        message?.chatType = .init(2)
        
        self.sendMessage(message: message!)
    }
    // 聊天消息的代理
    func liveChatView(_: LiveChatView, userId: String) {
        NetworkingHandle.fetchNetworkData(url: "/Index/get_live_info", at: self, params: ["user_id": userId], success: { (result) in
            let data = result["data"] as! [String: AnyObject]
            let model = ChatRoomMember.modelWithDictionary(diction: data)
            model.live_id = self.liveId
            model.roomId = self.roomId
            _ = WatchUserInfo.show(atView: self.view, model: model, isCurrentUserLiving: self.isCurrentUserLiving, isAnchor: self.anchorId == userId ? true : false)
        })
    }
    
    deinit {
        isLivingOrWatchLive = false
        print("========主播关闭直播========")
        EMClient.shared().roomManager.leaveChatroom(roomId!) { (error) in
        }
    }
}
extension LiveBaseViewController: PLMediaStreamingSessionDelegate {
    
    var conferenceCloseBtn: UIButton {
        let closeBtn = UIButton(frame: CGRect(x: kScreenWidth/3.0 - 30, y: 0, width: 30, height: 30))
        closeBtn.setImage(#imageLiteral(resourceName: "guanbi"), for: .normal)
        closeBtn.addTarget(self, action: #selector(self.closeConferenceButtonAction), for: .touchUpInside)
        return closeBtn
    }
    func closeConferenceButtonAction() {
        if let session = self.sesstion, session.isRtcRunning {
            session.stopConference()
            self.removeConferenceMember(view: session.previewView)
            session.destroy()
            session.delegate = nil
            self.sesstion = nil
            if let fsv = fullScreenView {
                fsv.removeFromSuperview()
                fullScreenView = nil
            }
            btn4.setImage(#imageLiteral(resourceName: "lianmai"), for: .normal)
            for (_, view) in conferenceUserInfo {
                view.removeFromSuperview()
            }
        }
        self.player?.play()
    }
    //MARK: - 连麦回调
    func mediaStreamingSession(_ session: PLMediaStreamingSession!, rtcStateDidChange state: PLRTCState) {
        print(state)
        
        if isCurrentUserLiving {
            return
        }
        if state == .conferenceStarted {
            btn4.setImage(#imageLiteral(resourceName: "jiepin"), for: .normal)
            let view = self.sesstion!.previewView!
            view.addSubview(conferenceCloseBtn)
            self.addConferenceMember(view: view)
        }
    }
    func mediaStreamingSession(_ session: PLMediaStreamingSession!, userID: String!, didAttachRemoteView remoteView: UIView!) {
        
        if userID == plAnchorId {
            player?.stop()
            remoteView.frame = UIScreen.main.bounds
            self.fullScreenView = remoteView
            self.view.insertSubview(remoteView, aboveSubview: (player?.playerView)!)
            return
        }
        
        addConferenceMember(view: remoteView)
        conferenceUserInfo[userID] = remoteView
        
        if isCurrentUserLiving {
            let closeBtn = UIButton()
            closeBtn.setImage(#imageLiteral(resourceName: "guanbi"), for: .normal)
            remoteView.addSubview(closeBtn)
            closeBtn.snp.makeConstraints({ (make) in
                make.top.equalTo(0)
                make.right.equalTo(0)
                make.height.width.equalTo(30)
            })
            closeBtn.addTarget(self, action: #selector(kickoutButtonAction(btn:)), for: .touchUpInside)
        }
    }
    
    func addConferenceMember(view: UIView) {
        let width = kScreenWidth/3.0
        let height = 16.0/12 * width
        let minY = height * CGFloat(2 - conferenceMemberView.subviews.count)
        view.frame = CGRect(x: kScreenWidth/3 - width, y: minY, width: width, height: height)
        conferenceMemberView.addSubview(view)
    }
    func removeConferenceMember(view: UIView?) {
        let index = conferenceMemberView.subviews.index(of: view!)
        view?.removeFromSuperview()
        let subviews = conferenceMemberView.subviews[index!..<conferenceMemberView.subviews.count]
        let height = view?.frame.height
        for i in 0..<subviews.count {
            var frame = subviews[i].frame
            frame.origin.y = CGFloat(2 - i) * height!
            subviews[i].frame = frame
        }
        if isCurrentUserLiving, (self.sesstion?.rtcParticipantsCount)! == 0 {
            self.sesstion?.stopConference()
        }
    }
    // 踢出
    func kickoutButtonAction(btn: UIButton) {
        let view = btn.superview
        for (key, value) in conferenceUserInfo {
            if value == view {
                sesstion?.kickoutUserID(key)
                break
            }
        }
    }
    func mediaStreamingSession(_ session: PLMediaStreamingSession!, didKickoutByUserID userID: String!) {
        self.sesstion?.stopConference()
        removeConferenceMember(view: self.sesstion?.previewView)
        self.sesstion?.destroy()
        self.sesstion?.delegate = nil
        self.sesstion = nil
        self.player?.play()
        if let fsv = fullScreenView {
            fsv.removeFromSuperview()
            fullScreenView = nil
        }
        for (_, view) in conferenceUserInfo {
            view.removeFromSuperview()
        }
        btn4.setImage(#imageLiteral(resourceName: "lianmai"), for: .normal)
        ProgressHUD.showNoticeOnStatusBar(message: "你被主播踢出连麦了！")
    }
    func mediaStreamingSession(_ session: PLMediaStreamingSession!, userID: String!, didDetachRemoteView remoteView: UIView!) {
        removeConferenceMember(view: remoteView)
        conferenceUserInfo.removeValue(forKey: userID)
    }
    func mediaStreamingSession(_ session: PLMediaStreamingSession!, rtcDidFailWithError error: Error!) {
//        ProgressHUD.showNoticeOnStatusBar(message: error.localizedDescription)
        ProgressHUD.showNoticeOnStatusBar(message: "未知错误")
    }
}
