//
//  DirectChatViewController.swift
//  CrazyEstate
//
//  Created by 梁毅 on 2017/1/19.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

import AVKit
import AFNetworking
import Qiniu
import QNNetDiag

var isLivingOrWatchLive = false

class DirectChatViewController: EaseMessageViewController, EaseMessageViewControllerDataSource, EaseMessageViewControllerDelegate, UINavigationBarDelegate, AVPlayerViewControllerDelegate{
    
    var img: String?          //头像
    var usernameTHEY: String? //标题
    var vcType: String?
    var userId: String?       //用户ID
    var hx_username: String?  //环信名
    var isCustomNav = false   //自定义导航栏
    var isTeacher = false     //是否是导师
    var state = "1"           //1 代表免费 2 代表付费
    var isPaySuccess = false  //是否续费成功
    var isRefundMoney = false //退款
    var isTeachBefore = false //之前有过指点，但需要再次付费
    var isAudit = false       //审核版本
    var playerVC: AVPlayerViewController! //播放器
    var reloadMeassage: (()->())?
    var uploadProgress = "0%" //进度
    var label = UILabel()
    var timer : Timer?
    var video_url = ""        //视频扩展地址
  
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        var vcArr = self.navigationController?.viewControllers
        for (index,vc) in (vcArr?.enumerated())! {
            if vc.isKind(of: SelectPayWayViewController.self){
                vcArr?.remove(at: index)
            }
        }
        self.navigationController?.setViewControllers(vcArr!, animated: false)
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = usernameTHEY
        
        self.dataSource = self
        self.delegate = self
        self.imagePicker.delegate = self
        
        //背景色
        self.tableView.backgroundColor = UIColor(hexString: "#F3F6F9")
        self.view.backgroundColor = UIColor(hexString: "#F3F6F9")
        
        //进度标签
        self.label.textColor = UIColor.white
        self.label.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.label.layer.cornerRadius = 10
        self.label.layer.masksToBounds = true
        self.label.font = UIFont.systemFont(ofSize: 14)
        self.label.textAlignment = .center
        self.label.frame = CGRect.init(x: 0, y: 0,  width: kScreenWidth, height: kScreenHeight)
        self.label.isHidden = true
        UIApplication.shared.keyWindow?.addSubview(self.label)

        
        //教师端布局（不是直播间）
        if isTeacher,!isCustomNav {
            
            self.navigationController?.navigationBar.isHidden = false
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "指点完毕", style: .done, target: self, action: #selector(DirectChatViewController.teachFinish))
            self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSForegroundColorAttributeName : UIColor.black,NSFontAttributeName:UIFont.systemFont(ofSize: 17)], for: .normal)
            
        }else if !isCustomNav{
            
            if DLUserInfoHandler.getUserInfo()?.type == "2"{
                
                if isTeachBefore{
                    
                    self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(DirectChatViewController.teachFinish))
                }
                
            }else{
                if isAudit {}
                else{
                    self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_more"), style: .done, target: self, action: #selector(DirectChatViewController.payFee))
                }
            }
        }
        
        if  vcType == "1" {//直播间返回按钮
            
            self.navigationController?.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_back"), style: .done, target: self, action: #selector(back))
            self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.black
        }
        
        //直播中或者在观看直播
        if isLivingOrWatchLive {
            (chatToolbar as? EaseChatToolbar)?.styleChangeButton.isEnabled = false
        }
        
        //直播间布局
        if isCustomNav {
            self.view.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: naviHeight + differValue - 64)
            self.tableView.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: naviHeight + differValue - 64 - 46)
            NotificationCenter.default.addObserver(self, selector: #selector(DirectChatViewController.keyboardWillHide(noti:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(DirectChatViewController.keyboardWillShow(noti:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        }
        
        
    }
    
    //指点完毕
    func teachFinish(){
        let m = DLUserInfoHandler.getUserBaseInfo()
        self.state = "2"
        NetworkingHandle.fetchNetworkData(url: "/Home/teach_end", at: self, params: ["user_id":self.userId!], success: { (response) in
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(DirectChatViewController.teachFinish))
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            self.sendTextMessage("本次名师指点完毕，请在右上角更多按钮里对实际情况对本次指点进行评分", withExt: ["username":m.name ,"img":m.img, "user_id":m.id, "state":self.state])
            self.reloadMeassage?()
            self.navigationController?.popViewController(animated: true)
        }) {
//            self.sendTextMessage("本次名师指点完毕，请在右上角更多按钮里对实际情况对本次指点进行评分", withExt: ["username":m.name ,"img":m.img, "user_id":m.id, "state":self.state])
        }
    }
    
    //学生端操作
    func payFee(){
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let markSheet = UIAlertAction(title: "评分", style:.default) { (alert) in
            let vc = SMarkViewController()
            vc.user_id = self.userId
            self.navigationController?.pushViewController(vc, animated: true)
        }
        let refundSheet = UIAlertAction(title: "退款", style: .default) { (alert) in
            let vc = RefundViewController()
            vc.teacher_id = self.userId
            vc.successPost = { [unowned self] in
                self.state = "2"
                self.isRefundMoney = true
                self.isTeachBefore = false
                self.isPaySuccess = false
                self.sendTextMessage("退款申请已经提交，等待平台审核", withExt: ["state":self.state])
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
        let cancelSheet = UIAlertAction(title: "取消", style: .cancel) { (alert) in}
        actionSheet.addAction(markSheet)
        actionSheet.addAction(refundSheet)
        actionSheet.addAction(cancelSheet)
        self.navigationController?.present(actionSheet, animated: true, completion: nil)
    }
    //MARK: - 键盘监听
    func keyboardWillHide(noti: Notification) {
        let kbInfo = noti.userInfo
        let duration = kbInfo?[UIKeyboardAnimationDurationUserInfoKey] as! Double
        var frame = self.navigationController?.view.frame
        UIView.animate(withDuration: duration, animations: {
            frame?.origin.y = kScreenHeight - naviHeight
            frame?.size.height = naviHeight + differValue
            self.navigationController?.view.frame = frame!
        })
    }
    func keyboardWillShow(noti: Notification) {
        
        let kbInfo = noti.userInfo
        let kbRect = (kbInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let duration = kbInfo?[UIKeyboardAnimationDurationUserInfoKey] as! Double
        
        var frame = self.navigationController?.view.frame
        UIView.animate(withDuration: duration, animations: {
            frame?.origin.y = kScreenHeight - naviHeight - kbRect.height
            frame?.size.height += kbRect.height
            self.navigationController?.view.frame = frame!
        })
    }

    //MARK: - 返回按钮
    func back() {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK: - 环信收到消息代理
    override func messagesDidReceive(_ aMessages: [Any]!) {
        let messate = aMessages as NSArray
        print(messate)
        for messageNew in messate {
            if  self.shouldMarkMessageAsRead() == true {
                let newMessage = messageNew as! EMMessage
                self.state = (newMessage.ext["state"] as? String)!
               // self.img = newMessage.ext["img"] as? String
                if DLUserInfoHandler.getUserInfo()?.type == "2"{
                    if newMessage.ext["state"] as? String == "2"{
                        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(DirectChatViewController.teachFinish))
                        self.navigationItem.rightBarButtonItem?.isEnabled = false
                    }else{
                        
                        isTeachBefore = false
                        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "指点完毕", style: .done, target: self, action: #selector(DirectChatViewController.teachFinish))
                        self.navigationItem.rightBarButtonItem?.isEnabled = true
                    }
                }
                self.conversation.markMessageAsRead(withId: newMessage.messageId, error: nil)
            }
        }
    }

    //MARK: - 环信标记为已读
    func shouldMarkMessageAsRead() -> Bool {
        var iSMark = true
        if  dataSource != nil {
            if  dataSource.responds(to: #selector(messageViewControllerShouldMarkMessages
                )) != true {
                iSMark = dataSource.messageViewControllerShouldMarkMessages!(asRead: self)
            }
            else {}
        }else {
            if UIApplication.shared.applicationState == UIApplicationState.background || self.isViewDidAppear {
                iSMark = false
            }
        }
        return iSMark;
    }
    //MARK: - 环信是否标记为已读
    func messageViewControllerShouldMarkMessages(asRead viewController: EaseMessageViewController!) -> Bool {
        return true
    }
    //MARK: - 发送扩展字段
    func messageViewController(_ viewController: EaseMessageViewController!, modelFor message: EMMessage!) -> IMessageModel! {
        let model = EaseMessageModel(message: message)
        if  model?.isSender == true {
            let m = DLUserInfoHandler.getUserBaseInfo()
            model?.avatarURLPath = m.img
            model?.nickname = m.name
            
            if model?.message.ext == nil{
                print("扩展为空")
                 model?.message.ext = ["username": m.name, "img": m.img, "user_id": m.id, "state": self.state]
            }else{
                print("扩展不为空")
                model?.message.ext["username"] = m.name
                model?.message.ext["img"] = m.img
                model?.message.ext["user_id"] = m.id
                model?.message.ext["state"] = self.state
            }
            
        }else {
            model?.avatarURLPath = img!
            model?.nickname = usernameTHEY!
        }
        return model
    }

    
    //MARK: - 发送扩展字段环信代理
    
    //继承父类方法发送信息监听，判断是否开启付费(可在本地初始化一个state，每次都发送，当指点完毕的时候，改动state值，然后发送
    override func sendTextMessage(_ text: String!, withExt ext: [AnyHashable : Any]!) {
        
        let m = DLUserInfoHandler.getUserBaseInfo()
        //收到对方发送的最后一条消息为空,还未接受指点,或者清除了聊天记录
        if self.conversation.lastReceivedMessage() == nil {
            
            if isAudit {
                super.sendTextMessage(text, withExt: ["username":m.name,"img":m.img,"user_id":m.id,"state":self.state])
                return
            }
            //有过指点记录，再次发送提醒付费
            if isTeachBefore {
                if DLUserInfoHandler.getUserInfo()?.type == "1"{
                    self.gotoPay()
                }else{
                    ProgressHUD.showMessage(message: "请等待学生再次付费")
                }
                return
            }
            //续费成功以后回来继续发送消息，自动发送一条改变状态
            if isPaySuccess{
                isPaySuccess = false
                super.sendTextMessage(text, withExt: ["username":m.name,"img":m.img,"user_id":m.id,"state":self.state])
                return
            }
            //退款以后回来自动发送消息
            if isRefundMoney{
                
                isPaySuccess = false
                isRefundMoney = false
                isTeachBefore = true
                
                if self.conversation.latestMessage == nil{ //没发消息进来直接退款判断
                    //发送退款说明
                    super.sendTextMessage(text, withExt: ["username":m.name,"img":m.img,"user_id":m.id,"state":self.state])
                }else{
                    let message = self.conversation.latestMessage!
                    
                    //取出付费状态
                    let state = message.ext["state"] as! String
                    
                    if state == "1"{
                        
                        super.sendTextMessage(text, withExt: ["username":m.name,"img":m.img,"user_id":m.id,"state":self.state])
                        return
                    }else{
                        
                        self.gotoPay()
                    }
                }
            }
            super.sendTextMessage(text, withExt: ["username":m.name,"img":m.img, "user_id":m.id,"state":self.state])
        } else{
            
            if isAudit{
                
                super.sendTextMessage(text, withExt: ["username":m.name,"img":m.img,"user_id":m.id,"state":self.state])
                return
            }
            
            //续费成功以后回来继续发送消息，自动发送一条改变状态
            if isPaySuccess{
                
                super.sendTextMessage(text, withExt: ["username":m.name,"img":m.img,"user_id":m.id,"state":self.state])
                isPaySuccess = false
                isTeachBefore = false
                return
            }
            
            //退款
            if isRefundMoney{
                isRefundMoney = false
                isTeachBefore = true
                super.sendTextMessage(text, withExt: ["username":m.name,"img":m.img,"user_id":m.id,"state":self.state])
                
                return
                
            }
            //有过指点记录，再次发送提醒付费
            if isTeachBefore {
                //学生
                if DLUserInfoHandler.getUserInfo()?.type == "1"{
                    self.gotoPay()
                    return
                }else{
                    ProgressHUD.showMessage(message: "请等待学生再次付费")
                    return
                }
                
            }
            //指点状态中
            
            //获取最新一条信息的指定扩展字段(对方会话消息不为空，双方都有消息记录)
            if self.conversation.latestMessage.conversationId != nil{
                
                //获取会话最新一条消息（两边统一传递一个state，所以要获取聊天信息的最新一条，而不是对方的发出来的最后一条）
                let message = self.conversation.latestMessage!
                
                //取出付费状态
                let state = message.ext["state"] as! String
                
                print("付费状态～～～～～～\(state)")
                //还在指点状态中
                if state == "1"{
                    
                    super.sendTextMessage(text, withExt: ["username":m.name,"img":m.img,"user_id":m.id,"state":self.state])
                    
                }else{
                    
                    //教师端结束指点，学生身份再次发送消息需要根据判断去付费（学员有可能是该导师的会员，有免费接受指点的次数，付费消息都需要去判断根据后台结果去判断是否还有免费指点的权限）
                    if DLUserInfoHandler.getUserInfo()?.type == "1" {
                        
                        NetworkingHandle.fetchNetworkData(url: "/Home/check_teach", at: self, params: ["user_id":self.userId!], isShowHUD: false, isShowError: false, success: { (response) in
                            let data = response["data"] as! String
                            print("~~~~~~~~~返回的状态"+data)
                            if data == "1"{
                                self.state = "1"
                                super.sendTextMessage(text, withExt: ["username":m.name,"img":m.img,"user_id":m.id,"state":self.state])
                            }else{
                                self.gotoPay()
                            }
                        }, failure: {
                            
                            self.gotoPay()
                            
                        })
                    } else{
                        
                        //学生端退款申请，教师端无法继续发送消息，提示退出
                        let alert = UIAlertController.init(title: "学生端申请退款，结束此次指点", message: "", preferredStyle: .alert)
                        let confirmAction = UIAlertAction.init(title: "确定", style: .default, handler: { (alert) in
                            self.navigationController?.popViewController(animated: true)
                        })
                        alert.addAction(confirmAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    override func sendVoiceMessage(withLocalPath localPath: String!, duration: Int) {
        if isCanSendMoreTypeMessage(){
            super.sendVoiceMessage(withLocalPath: localPath, duration: duration)
        }
        
    }
    
    //MARK: - 支付
    func gotoPay(){
        let vc = UIAlertController(title: "上次指点已经完毕，如需继续请教需要重新付费", message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: "确定", style: .default, handler: { (action) in
            NetworkingHandle.fetchNetworkData(url: "/Home/teach_fee", at: self, params: ["user_id":self.userId!], isShowHUD: false, success: { (response) in
                let data = response["data"] as! String
                NetworkingHandle.fetchNetworkData(url: "/Home/setup_teach_order", at: self, params: ["user_id":self.userId!],isShowHUD: false, success: { (result) in
                    
                    let vc = SelectPayWayViewController()
                    vc.amount = data
                    vc.order_no = result["data"] as? String
                    vc.payType = "2"
                    vc.img = self.img
                    vc.hx_username = self.hx_username
                    vc.userId = self.userId
                    vc.usernameTHEY = self.usernameTHEY
                    vc.isPopToPointVC = true
                    vc.successPay = {
                        self.isPaySuccess = true
                        self.isRefundMoney = false
                        self.isTeachBefore = false
                        self.state = "1"
                        self.sendTextMessage("三人行必有我师，望老师不吝赐教", withExt: ["state":self.state])
                    }
                    self.navigationController?.pushViewController(vc, animated: false)
                }, failure: {
                    
                })
            }, failure: {
                
            })
        })
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        vc.addAction(action)
        vc.addAction(cancel)
        self.navigationController?.present(vc, animated: true, completion: nil)
        return
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: cell被选中
    override func messageCellSelected(_ model: IMessageModel!) {
        switch model.bodyType {
        case EMMessageBodyTypeVoice:
            print("语音")
        case EMMessageBodyTypeVideo:
            print("视频")
        case EMMessageBodyTypeImage:
            print("图片")
            
            if let videourl = model.message.ext["video_url"] as? String{
                
                self.playerVC = AVPlayerViewController.init()
                self.playerVC.player = AVPlayer.init(url: URL.init(string: videourl)!)
                self.playerVC.videoGravity = AVLayerVideoGravityResizeAspect
                self.playerVC.showsPlaybackControls = true
                self.playerVC.delegate = self
                self.playerVC.player?.play()
                self.playerVC.view.bounds = self.view.bounds
                self.playerVC.view.center = self.view.center
                self.present(self.playerVC, animated: true, completion: nil)
            }else{
                super.messageCellSelected(model)
            }
            return
          
        case EMMessageBodyTypeFile:
            print("文件")
        case EMMessageBodyTypeLocation:
            print("位置")
        case EMMessageBodyTypeCmd:
            print("环信透传命令")
            
        default:
            print("~~~~~")
        }
        super.messageCellSelected(model)
    }
    //MARK: -播放器代理
    func playerViewControllerWillStartPictureInPicture(_ playerViewController: AVPlayerViewController) {
        print("将要开始播放")
    }
    func playerViewControllerDidStartPictureInPicture(_ playerViewController: AVPlayerViewController) {
        print("开始播放")
    }
    func playerViewControllerWillStopPictureInPicture(_ playerViewController: AVPlayerViewController) {
        print("将要停止")
    }
    func playerViewControllerDidStopPictureInPicture(_ playerViewController: AVPlayerViewController) {
        print("停止")
    }
    func playerViewController(_ playerViewController: AVPlayerViewController, failedToStartPictureInPictureWithError error: Error) {
        print("播放失败")
        ProgressHUD.showMessage(message: "播放失败")
        playerViewController.dismiss(animated: true, completion: nil)
    }
    
//    func sendFace() {
//        
//    }
//    func sendFace(with emotion: EaseEmotion!) {
//        
//    }
//    func selectedFacialView(_ str: String!, isDelete: Bool) {
//        
//    }
    override func moreViewPhotoAction(_ moreView: EaseChatBarMoreView!) {
        if isCanSendMoreTypeMessage(){
            
            super.moreViewPhotoAction(moreView)
        }
        return
    }
    override func moreViewAudioCallAction(_ moreView: EaseChatBarMoreView!) {
        if isCanSendMoreTypeMessage(){
            super.moreViewAudioCallAction(moreView)
        }
        return
    }
    override func moreViewTakePicAction(_ moreView: EaseChatBarMoreView!) {
        if isRightCamera() {
            if isCanSendMoreTypeMessage(){
                super.moreViewTakePicAction(moreView)
            }
        }else{
            ProgressHUD.showMessage(message: "请先去设置-隐私里面允许相册权限")
        }

        return
    }
    override func moreViewVideoCallAction(_ moreView: EaseChatBarMoreView!) {
        if isRightPhoto() {
            if isCanSendMoreTypeMessage(){
                super.moreViewVideoCallAction(moreView)
            }
        }else{
            ProgressHUD.showMessage(message: "请先去设置-隐私里面允许相册权限")
        }

        return
    }
    // 相机权限
    func isRightCamera() -> Bool {
        let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        return authStatus != .restricted && authStatus != .denied
    }
    // 相册权限
    func isRightPhoto() -> Bool {
        let library:PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if(library == PHAuthorizationStatus.denied || library == PHAuthorizationStatus.restricted){
            return false
        }else {
            return true
        }
    }
   
    func isCanSendMoreTypeMessage() -> Bool {
        
        //学生端
        if DLUserInfoHandler.getUserInfo()?.type == "1"{
            
            if self.conversation.lastReceivedMessage() == nil{
                if isPaySuccess{return true}
                if isAudit{return true}
                if isRefundMoney{
                    self.gotoPay()
                    return false
                }
                if isTeachBefore{
                    self.gotoPay()
                    return false
                }
                return true
            }else{
                
                let state = self.conversation.latestMessage.ext["state"] as! String
                if state == "1"{
                    return true
                }else{
                    self.gotoPay()
                    return false
                }
            }
        //教师端
        }else{
            if isTeachBefore{
                ProgressHUD.showMessage(message: "请等待您的学生再次付费")
                return false
            }
            if let state = self.conversation.latestMessage.ext["state"] as? String{
                if state == "1"{
                    return true
                }else{
                    //学生端退款申请，教师端无法继续发送消息，提示退出
                    let alert = UIAlertController.init(title: "学生端申请退款，结束此次指点", message: "", preferredStyle: .alert)
                    let confirmAction = UIAlertAction.init(title: "确定", style: .default, handler: { (alert) in
                        self.navigationController?.popViewController(animated: true)
                    })
                    alert.addAction(confirmAction)
                    self.present(alert, animated: true, completion: nil)

                    return false
                }
            }
        }
        
        return false
    }
    
    // 设置 imagePicker 导航栏
    override func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        navigationController.navigationBar.tintColor = UIColor.black
        navigationController.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName:defaultFont(size: 16)]
        navigationController.navigationBar.barTintColor = themeColor
    }
    
     //MARK: - imagePicker 回调代理
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let mediaType = info[UIImagePickerControllerMediaType] as? String{
            if mediaType == String(kUTTypeMovie) {
                let videoUrl = info[UIImagePickerControllerMediaURL] as! URL
                _ = self.convert2Mp4(movUrl: videoUrl)
               // self.uploadVideo(mp4Path: mp4)
                return
            }
        }
        super.imagePickerController(picker, didFinishPickingMediaWithInfo: info)
    }
    //MARK: - 生成url
    func convert2Mp4(movUrl: URL) -> URL{
        var mp4Url: URL? = nil
        let avAsset: AVURLAsset = AVURLAsset.init(url: movUrl, options: nil)
        //压缩对象
        let compatiblePresets = AVAssetExportSession.exportPresets(compatibleWith: avAsset)
        if compatiblePresets.contains(AVAssetExportPresetHighestQuality){
            //压缩品质
            let exportSession = AVAssetExportSession.init(asset: avAsset, presetName: AVAssetExportPresetMediumQuality)
            let mp4Path = NSHomeDirectory() + "/Documents/\(Date.init().timeIntervalSince1970).mp4"
            mp4Url = URL.init(fileURLWithPath: mp4Path)
            exportSession?.outputURL = mp4Url
            exportSession?.shouldOptimizeForNetworkUse = true
            exportSession?.outputFileType = AVFileTypeMPEG4
            exportSession?.exportAsynchronously(completionHandler: {
                if exportSession?.status == .completed{
                    print("成功")
                    self.uploadVideo(mp4Path: mp4Url!)
                }else{
                }
             })
        }
        return mp4Url!
    }
    //MARK: - 上传
    func uploadVideo(mp4Path: URL){
       
        
        NetworkingHandle.fetchNetworkData(url: "/Tools/get_qiniu_token", at: self, success: { (response) in
            let token = response["data"] as! String
            let upmanager = QNUploadManager.init()
            let data = try! Data.init(contentsOf: mp4Path)
            
            upmanager?.put(data, key: "\(Date.init().timeIntervalSince1970)", token: token, complete: { (info, key, resp) in
                if (info?.isOK)!{
                    self.cleanDocumentPathVideo()
                    let data = UIImagePNGRepresentation(#imageLiteral(resourceName: "video"))
                    let body = EMImageMessageBody.init(data: data, displayName: "image.png")
                    self.video_url = "http://msplay.qqyswh.com/" + String(resp!["key"] as! String)
                    let ext = ["video_url":self.video_url]
                    
                    self.sendImageMessage(#imageLiteral(resourceName: "video"), ext: ext)
                    ProgressHUD.showSuccess(message: "发送成功")
                    self.imagePicker.dismiss(animated: true, completion: {
                        self.tableView.reloadData()
                    })
                    
                }else{
                    ProgressHUD.showMessage(message: "上传失败")
                }
                
                
                
            }, option: QNUploadOption.init(progressHandler: { (key, progress) in
                print(progress)
                
                
                if progress == 1.0{
                    ProgressHUD.hideLoading(toView: self.view)
                    self.label.isHidden = true
                    
                }else{
                    
                    DispatchQueue.main.async {
                        self.label.isHidden = false
                        self.label.text =  "正在上传：" + String(format: "%.2f", progress * 100) + "%"
                    }
                }
            }))
            
           
        })
        
//        Alamofire.upload(
//            //同样采用post表单上传
//            multipartFormData: { multipartFormData in
//                multipartFormData.append(mp4Path, withName: "video", fileName: "bsxy.mp4", mimeType: "video/mp4")
//                //服务器地址
//        },to: URL(string: NetworkingHandle.mainHost + "/Tools/upload_video")!,encodingCompletion: { encodingResult in
//            switch encodingResult {
//            case .success(let upload, _, _):
//                //json处理
//                upload.responseJSON { response in
//                    guard let json = response.result.value else { return }
//                    ProgressHUD.hideLoading(toView: UIApplication.shared.keyWindow!)
//                    print("json:\(json)")
//                    let result: Dictionary<String, Any> = json as! Dictionary
//                    let code: String = (result["status"] as? String)!
//                    if code == "ok"{
//                        self.cleanDocumentPathVideo()
//                        let dic: Dictionary<String,String> = result["data"] as! Dictionary<String, String>
//                        let data = try! Data.init(contentsOf: URL(string:dic["image"]!)!)
//                        let body = EMImageMessageBody.init(data: data, displayName: "image.png")
//                        self.video_url = dic["url"]!
//                        let ext = ["video_url":dic["url"]!]
//                        let message = EMMessage(conversationID: self.hx_username, from: EMClient.shared().currentUsername, to: self.hx_username, body: body, ext: ext )
//                        message?.chatType = EMChatTypeChat
//                        self._send(message)
//                        ProgressHUD.showSuccess(message: "发送成功")
//                        
//                    }else{
//                        
//                        ProgressHUD.hideLoading(toView: UIApplication.shared.keyWindow!)
//                        switch code {
//                        case "pending":
//                            ProgressHUD.showNoticeOnStatusBar(message: result["data"] as! String)
//                            DLUserInfoHandler.deleteUserInfo()
//                            EMClient.shared().logout(true)
//                            UIApplication.shared.keyWindow?.rootViewController = LoginNavigationController.setup()
//                        default:
//                            ProgressHUD.showMessage(message: result["data"] as! String)
//                        }
//                    }
//                    
//                }
//                //upload progress
//                upload.uploadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
//                    
//                    if progress.fractionCompleted == 1.0{
//                        
//                        self.uploadProgress = "100%"
//                        
//                    }else{
//                        
//                        self.uploadProgress = String(format: "%.2f", progress.fractionCompleted * 100) + "%"
//                    }
//                }
//            case .failure(let encodingError):
//                
//                print(encodingError)
//            }
//        })
    }
    func showProgress() {
        self.label.text = self.uploadProgress
        if self.uploadProgress == "100%"{
            self.uploadProgress = "0%"
            timer?.fireDate = Date.distantFuture
            self.label.isHidden = true
            self.imagePicker.dismiss(animated: true, completion: {
                self.tableView.reloadData()
            })
        }
        print(self.uploadProgress)
    }
    func cleanDocumentPathVideo(){
        let fm = FileManager.init()
        let contents = try! fm.contentsOfDirectory(atPath: NSHomeDirectory() + "/Documents/")
        for (_,str) in contents.enumerated() {
            if str.contains(".mp4"){
                try! fm.removeItem(atPath:NSHomeDirectory() + "/Documents/" + str)
            }
        }
    }
    deinit {
        timer?.invalidate()
        self.label.removeFromSuperview()
        self.video_url = ""
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
