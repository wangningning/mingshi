//
//  WatchEndViewController.swift
//  Duluo
//
//  Created by sh-lx on 2017/3/23.
//  Copyright © 2017年 tts. All rights reserved.
//

import UIKit

class WatchEndViewController: UIViewController {

    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var nickname: UILabel!
    @IBOutlet weak var watchNumber: UILabel!
    @IBOutlet weak var attentionBtn: UIButton!
    
    var liveId: String?
    var model: WatchEndLive?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        avatar.layer.cornerRadius = 89/2.0
        avatar.layer.masksToBounds = true
        avatar.contentMode = .scaleAspectFill
        avatar.clipsToBounds = true
        
        NetworkingHandle.fetchNetworkData(url: "/Index/live_end", at: self, params: ["live_id": liveId!], isShowHUD: false, isShowError: false, success: { (result) in
            let data = result["data"]
            let model = WatchEndLive.modelWithDictionary(diction: data as! Dictionary<String, AnyObject>)
            self.model = model
            self.avatar.kf.setImage(with: URL(string: model.img!))
            self.nickname.text = model.username
            self.watchNumber.text = model.watch_nums! + "人已看过"
            if self.model?.is_follow == "1"{
                self.attentionBtn.isSelected = false
            }else{
                self.attentionBtn.isSelected = true
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func attentButtonAction(_ sender: UIButton) {
        if let m = model {
            focusOtherPerson(viewResponder: self, other_id: m.user_id!, btn: sender, type: m.is_follow!) {
                if m.is_follow == "1" {
                    m.is_follow = "2"
                } else {
                    m.is_follow = "1"
                }
            }
        }
    }
    @IBAction func toAnchorHomepageButtonAction(_ sender: UIButton) {
        pushToUserInfoCenter(atViewController: self, uId: model?.user_id ?? "")
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.navigationBar.isHidden = false
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
class WatchEndLive: KeyValueModel {
    var user_id: String?
    var play_img: String?
    var title: String?
    var start_time: String?
    var end_time: String?
    var watch_nums: String?
    var img: String?
    var username: String?
    var autograph: String?
    var time: String?
    var is_follow: String?
}

