//
//  WatchLiveViewController.swift
//  Duluo
//
//  Created by sh-lx on 2017/3/23.
//  Copyright © 2017年 tts. All rights reserved.
//

import UIKit

class WatchLiveViewController: LiveBaseViewController, PLPlayerDelegate {

    var liveList: LiveList! {
        willSet(m) {
            self.roomId = m.room_id
            self.liveId = m.live_id
            self.isCurrentUserLiving = false
            self.anchorId = m.user_id
            self.playAddress = m.play_address
            self.playImg = m.play_img
            self.plAnchorId = m.qiniu_room_id
            self.url = m.url
            self.avatar = m.img
            self.username = m.username
        }
    }
    var playAddress: String?
    var playImg: String?
    
    var labelAlert: UILabel?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    private init() {
       super.init(nibName: "LiveBaseViewController", bundle: Bundle.main)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        do { try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback) } catch{}
        
        let option = PLPlayerOption.default()
        option.setOptionValue(10, forKey: PLPlayerOptionKeyTimeoutIntervalForMediaPackets)
        player = PLPlayer(url: NSURL(string: playAddress!) as URL?, option: option)
        player?.delegate = self
        player?.delegateQueue = DispatchQueue.main
        player?.playerView?.frame = UIScreen.main.bounds
        player?.playerView?.contentMode = .scaleAspectFill
        player?.isBackgroundPlayEnable = true
        player?.launchView?.kf.setImage(with: URL(string: playImg!))
        player?.launchView?.clipsToBounds = true
        player?.launchView?.contentMode = .scaleAspectFill
        self.view.insertSubview(player!.playerView!, at: 0)
        
        startPlayer()
        
        enterRoomConnectHx { (isSuccess) in
            DispatchQueue.main.async {
                
                print("~~~~~~~~~~~~~~~~~~~~进入房间结果回调，刷新状态")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue:RefreshLoadStateNotification), object: nil)
                if isSuccess {
                    ProgressHUD.showNoticeOnStatusBar(message: "进入成功")
                    
                    let model = DLUserInfoHandler.getUserBaseInfo()
                    let messageText = "进场了！"
                    let body = EMTextMessageBody(text:  messageText)
                    
                    let ext = ["user_id": model.id, "username": model.name, "userimg": model.img, "intoroom": "1", "usergrade":model.grade]
                    let message = EMMessage(conversationID: self.roomId!, from: EMClient.shared().currentUsername, to: self.roomId!, body: body, ext: ext)
                    message?.chatType = .init(2)
                    self.sendMessage(message: message!)
                } else {
                    ProgressHUD.showNoticeOnStatusBar(message: "进入失败")
                    self.quitLiveRoom()
                }
                
            }
        }
    }
    // 环信代理
    //MARK: 接受透传消息
    func cmdMessagesDidReceive(_ aCmdMessages: [Any]!) {
        let message = aCmdMessages.last as! EMMessage
        
        let nowTimeInterval = Date().timeIntervalSince1970
        let interval = nowTimeInterval - TimeInterval(message.timestamp/1000)
        print("#########", interval, "#############")
        if interval > 10 {
            return
        }

        let body = message.body as! EMCmdMessageBody
        
        if body.action == "home键退出到后台事件" {
            if labelAlert == nil {
                labelAlert = UILabel()
            }
            player?.playerView!.addSubview(labelAlert!)
            labelAlert?.snp.makeConstraints({ (make) in
                make.left.right.equalTo(0)
                make.top.equalTo(200)
            })
            labelAlert?.textAlignment = .center
            labelAlert?.textColor = UIColor.white
            labelAlert?.text = "主播暂时离开，马上回来"
        } else if body.action == "home键返回前台事件" {
            labelAlert?.removeFromSuperview()
        } else if body.action == anchorStopLiveCMD {
            prepareExit()
            let vc = WatchEndViewController()
            vc.liveId = liveId
            self.navigationController?.pushViewController(vc, animated: true)
        } else if message.ext["userid"] as? String == DLUserInfoHandler.getIdAndToken()?.id {
            let action = body.action
            if action == kickoutCMD {
                prepareExit()
                let alert = UIAlertController(title: "您已被主播踢出该房间", message: nil, preferredStyle: .alert)
                let ok = UIAlertAction(title: "知道了", style: .destructive, handler: { (alert) in
                    self.navigationController?.navigationBar.isHidden = false
                    _ = self.navigationController?.popViewController(animated: true)
                })
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
            } else if action == conferenceCMD {
//                LyAlertView.alert(atVC: self, message: "主播邀请你连麦，是否接受邀请", cancel: "拒绝", ok: "接受", okBlock: {
//                    self.startConference()
//                })
            } else if action == anchorAgreeConferenceCMD {
              //  self.startConference()
            } else if action == settingUpManagerCMD {
//                ProgressHUD.showNoticeOnStatusBar(message: "你已被主播设置为管理员")
//                let m = DLUserInfoHandler.getUserBaseInfo()
//                self.sendLivingMessage(content: m.name + " 被主播设置为管理员")
            } else if action == cancelManagerCMD {
              //  ProgressHUD.showNoticeOnStatusBar(message: "你已被主播取消管理")
            }
        }
    }
    func startConference() {
        if self.sesstion == nil {
            self.sesstion = PLMediaStreamingSession(videoCaptureConfiguration: PLVideoCaptureConfiguration.default(), audioCaptureConfiguration: PLAudioCaptureConfiguration.default(), videoStreamingConfiguration: nil, audioStreamingConfiguration: nil, stream: nil)
            self.sesstion?.delegate = self
            
            self.sesstion?.setBeautifyModeOn(true)
            self.sesstion?.setBeautify(0.85)
            self.sesstion?.setWhiten(0.85)
            self.sesstion?.setRedden(0.5)
        }
        if self.sesstion?.isRtcRunning == false {
            self.sesstion?.rtcOption = [kPLRTCRejoinTimesKey: 2, kPLRTCConnetTimeoutKey: 3000]
            self.sesstion?.rtcMinVideoBitrate = 300 * 1000
            self.sesstion?.rtcMaxVideoBitrate = 800 * 1000
            
            let conf = PLRTCConfiguration(videoSize: .preset240x432, conferenceType: .audioAndVideo)
            self.sesstion?.startConference(withRoomName: liveList.qiniu_room_name!, userID: DLUserInfoHandler.getIdAndToken()?.id, roomToken: self.liveList.qiniu_token!, rtcConfiguration: conf)
        }
    }
    // 七牛代理
    //MAKR: 播放器状态改变
    func player(_ player: PLPlayer, statusDidChange state: PLPlayerStatus) {
        switch state {
        case .statusUnknow:
            print("***===播放端状态: 未知状态")
        case .stateAutoReconnecting:
            print("***===播放端状态: 自动重连状态")
        case .statusError:
            print("***===播放端状态: 错误状态")
        case .statusCaching:
            print("***===播放端状态: 缓存数据为空状态")
            if isShowHUD == false {
               //ProgressHUD.showLoading(toView: self.contentView)
                isShowHUD = true
            }
        case .statusReady:
            print("***===播放端状态: 播放组件准备完成，准备开始播放状态")
        case .statusStopped:
            print("***===播放端状态: 停止状态")
        case .statusPaused:
            print("***===播放端状态: 暂停状态")
        case .statusPlaying:
            
            if reconnectCount > 0 {
                timer.pause()
                reconnectCount = 0
            }
            if isShowHUD {
               // ProgressHUD.hideLoading(toView: self.contentView)
                isShowHUD = false
            }
        case .statusPreparing:
            print("***===播放端状态: 正在准备播放所需组件状态")
        case .statusOpen:
            break
        case .statusCompleted:
            break
        case .statusSeeking:
            break
        case .statusSeekFailed:
            break
        }
    }
    override func livingReconnect() {
        if reconnectCount >= 6 {
            timer.pause()
            prepareExit()
            let vc = WatchEndViewController()
            vc.liveId = liveId
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            startPlayer()
            reconnectCount += 1
            if isShowHUD == false {
              //  ProgressHUD.showLoading(toView: self.contentView)
                isShowHUD = true
            }
        }
    }
    func player(_ player: PLPlayer, stoppedWithError error: Error?) {
        print("重连：", error as Any)
        if let s = self.sesstion, s.isRtcRunning {
        } else {
            if reconnectCount < 6 {
                timer.resume()
            }
        }
    }
    //MARK: 开始播放
    func startPlayer() {
        self.player?.play()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // 退出
    override func quitLiveRoom() {
        prepareExit()
        
        self.navigationController?.navigationBar.isHidden = false
        _ = self.navigationController?.popViewController(animated: true)
    }
    // 退出准备
    func prepareExit() {
        // 离开直播间
        NetworkingHandle.fetchNetworkData(url: "/Index/out_live", at: self, params: ["live_id": liveId!], success: { (result) in
        })
        
        NotificationCenter.default.removeObserver(self)
        UIApplication.shared.isIdleTimerDisabled = false
        
        timer.invalidate()
        
        self.player?.stop()
        self.player?.delegate = nil
        self.player = nil
        
        self.sesstion?.stopConference()
        self.sesstion?.delegate = nil
        self.sesstion?.destroy()
        self.sesstion = nil
        
        let model = DLUserInfoHandler.getUserBaseInfo()
        let messageText = "离开了直播间"
        let body = EMTextMessageBody(text:  messageText)
        
        let ext = ["user_id": model.id, "username": model.name, "userimg": model.img, "usergrade":model.grade, "leaveChatroom": "1"]
        let message = EMMessage(conversationID: self.roomId!, from: EMClient.shared().currentUsername, to: self.roomId!, body: body, ext: ext)
        message?.chatType = .init(2)
        self.sendMessage(message: message!, isShow: false)
    }
    // 私聊
    override func privateChat() {
        let chat = PrivateChatViewController.show(atVC: self, atView: contentView, isCurrentUserLiving: isCurrentUserLiving)
        chat.liveList = liveList
    } 
    // 外部调用
    class func toWatch(from vc: UIViewController, model: LiveList) {
        
        NetworkingHandle.fetchNetworkData(url: "/Index/check_anchor_state", at: vc, params: ["user_id": model.user_id!], isAuthHide: false, isShowHUD: false, isShowError: true, hasHeaderRefresh: nil, success: { (result) in
            NetworkingHandle.fetchNetworkData(url: "/Index/into_live", at: vc, params: ["live_id": model.live_id!], isAuthHide: false, isShowHUD: false, isShowError: false, hasHeaderRefresh: nil, success: { (result) in
                let watch = WatchLiveViewController()
                watch.liveList = model
                vc.navigationController?.pushViewController(watch, animated: true)
            }, failure: {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue:RefreshLoadStateNotification), object: nil)
            })
        }, failure:{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue:RefreshLoadStateNotification), object: nil)
        })
        
//        //进入直播间，判断主播是否在播
//        NetworkingHandle.fetchNetworkData(url: "/Index/check_anchor_state", at: vc, params: ["user_id": model.user_id!], success: { (result) in
//            //如果主播在播，进入直播间
//            NetworkingHandle.fetchNetworkData(url: "/Index/into_live", at: vc, params: ["live_id": model.live_id!], success: { (result) in
//                let watch = WatchLiveViewController()
//                watch.liveList = model
//                vc.navigationController?.pushViewController(watch, animated: true)
//            })
//        })
    }
}
