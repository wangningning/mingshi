//
//  PrivateMessageTableViewCell.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/25.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class PrivateMessageTableViewCell: UITableViewCell {

    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var dateLab: UILabel!
    @IBOutlet weak var badge: UILabel!
    
    var model:MessageList!{
        willSet(m){
            
            self.userImg.kf.setImage(with: URL(string: m.img!)!)
            self.userName.text = m.username
            self.dateLab.text = m.date_value
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.userImg.layer.cornerRadius = 51/2
        self.userImg.layer.masksToBounds = true
        
        self.badge.layer.cornerRadius = 19/2
        self.badge.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
