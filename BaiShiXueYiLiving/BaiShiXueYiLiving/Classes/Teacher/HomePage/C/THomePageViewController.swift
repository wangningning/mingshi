//
//  THomePageViewController.swift
//  BaiShiXueYiLiving
//
//  Created by 梁毅 on 2017/5/10.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh
class TeacherRecordList : KeyValueModel{
    var username : String?
    var sex : String?
    var img : String?
    var hx_username : String?
    var hx_password : String?
    var date_value : String?
    var user_id : String?
    var id : String?
    var unreadCount:Int = 0
}
class THomePageViewController: BSBaseViewController,UITableViewDelegate,UITableViewDataSource,EMChatManagerDelegate {

    @IBOutlet weak var tableview: UITableView!
    var chatDataArr: [PrivateChatModel] = []
    var teacherRecordList = [TeacherRecordList]()
    override func viewWillAppear(_ animated: Bool) {
        self.tableview.mj_header.beginRefreshing()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        EMClient.shared().chatManager.add(self as EMChatManagerDelegate, delegateQueue: nil)
        self.navigationItem.title = "指点"
        self.tableview.register(UINib(nibName:"PrivateChatAnchorTableViewCell", bundle:Bundle.main), forCellReuseIdentifier: "PrivateChatAnchorTableViewCell")
        self.tableview.separatorColor = UIColor(hexString: "f5f7fa")
        self.tableview.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        self.tableview.tableFooterView = UIView()
        self.tableview.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.getList()
//            self.loadData()
        })
        //self.tableview.isEditing = true
        // Do any additional setup after loading the view.
    }
    func getList(){
        NetworkingHandle.fetchNetworkData(url: "/member/teach_list", at: self, success:  { (response) in
            let data = response["data"] as! [String:AnyObject]
            let list = data["list"] as! [[String:AnyObject]]
            self.teacherRecordList = TeacherRecordList.modelsWithArray(modelArray: list) as! [TeacherRecordList]
            self.tableview.mj_header.endRefreshing()
            self.tableview.reloadData()
        })
    }
    
    func messagesDidReceive(_ aMessages: [Any]!) {
        let message:EMMessage = aMessages[0] as! EMMessage
        print(message.from)
        
//        for record in teacherRecordList {

//
//        }
        for index in 0..<teacherRecordList.count {
            let record = teacherRecordList[index]
            if record.hx_username == message.from {
                record.unreadCount += 1
                teacherRecordList[index] = record
            }
        }
        self.tableview.reloadData()
//        loadData()
    }
    func timeStampToString(timeStamp: TimeInterval) -> String {
        let nowTimeInterval = Date().timeIntervalSince1970
        let interval = nowTimeInterval - timeStamp
        if interval < 60 {
            return "刚刚"
        }
        if interval < 60*60 {
            return "\(Int(floor(interval/60)))分钟前"
        }
        if interval < 60*60*24 {
            return "\(Int(floor(interval/60/60)))小时前"
        }
        if interval < 60*60*24*30  {
            return "\(Int(floor(interval/60/60/24)))天前"
        }
        if interval < 60*60*24*30*12  {
            return "\(Int(floor(interval/60/60/24/30)))个月前"
        }
        return "一年前"
    }

    func getContent(m: EMConversation) -> String {
        if m.latestMessage == nil {
            return "有新消息"
        }
        let type = m.latestMessage.body.type
        var content = "有新消息"
        if type  == EMMessageBodyTypeText {
            let messageBody = m.latestMessage.body as! EMTextMessageBody
            content = messageBody.text
        } else if type  == EMMessageBodyTypeImage {
            content = "图片消息"
        } else if type  == EMMessageBodyTypeVoice {
            content = "语音消息"
        } else if type  == EMMessageBodyTypeVideo {
            content = "视频信息"
        } else if type  == EMMessageBodyTypeLocation {
            content = "位置信息"
        }
        return content
    }
    func getUserHxIdFrom(model: EMConversation) -> String {
        if model.latestMessage.direction == EMMessageDirectionSend {
            return model.latestMessage.to
        }
        return model.latestMessage.from
    }
    func loadData(){
        self.tableview.mj_header.endRefreshing()
        let list = EMClient.shared().chatManager.getAllConversations() as! [EMConversation]
        chatDataArr.removeAll()
       
        for (index,m) in list.enumerated(){
            if m.latestMessage != nil, m.type == EMConversationTypeChat {
                
                if m.latestMessage.chatType == EMChatTypeChat{
                    
                    if let otherMessage = m.lastReceivedMessage(){
                        
                        if otherMessage.ext["user_id"] as? String != nil{
                            let model = PrivateChatModel(name: otherMessage.ext["username"]! as! String, img: otherMessage.ext["img"] as! String, content: getContent(m: m), time:timeStampToString(timeStamp: TimeInterval(m.latestMessage.timestamp/1000)), id:otherMessage.ext["user_id"] as! String, chatId: m.conversationId, state: otherMessage.ext["state"] as! String)
                            model.unreadCount = Int(m.unreadMessagesCount)
                            model.timestamp = m.latestMessage.timestamp - 1488439927871
                            if m.latestMessage.conversationId == UserDefaults.standard.value(forKey: "service") as! String {
                                
                            } else{
                                self.chatDataArr.append(model)
                                if index == list.count - 1{
                                    chatDataArr.sort(by: {$0.timestamp > $1.timestamp})
                                }
                                self.tableview.reloadData()
                            }
                            
                        }
                        
                    } else{
                        NetworkingHandle.fetchNetworkData(url: "/Index/get_user_info", at: self, params: ["hx_username":getUserHxIdFrom(model: m)], success: { (result) in
                            let data = result["data"] as! [String: String]
                            
                            let name = data["username"] ?? ""
                            let id = data["user_id"] ?? ""
                            
//                            if id == self.liveList.user_id {
//                                self.isShowAnchor = false
//                            }
                            let model = PrivateChatModel(name: name, img: data["img"]!, content: self.getContent(m: m), time: self.timeStampToString(timeStamp: TimeInterval(m.latestMessage.timestamp/1000)), id: id, chatId: m.conversationId, state: "1")
                            model.unreadCount = Int(m.unreadMessagesCount)
                            model.timestamp = m.latestMessage.timestamp - 1488439927871
                    
                            if m.latestMessage.conversationId == UserDefaults.standard.value(forKey: "service") as! String {
                                
                            } else{
                                self.chatDataArr.append(model)
                                if index == list.count - 1 {
                                    self.chatDataArr.sort(by: {$0.timestamp > $1.timestamp})
                                }
                                self.tableview.reloadData()
                            }
                            
                           })
                        }
                    }
                }
            }
        

        }
    
    //MARK tableview 代理
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.teacherRecordList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "PrivateChatAnchorTableViewCell") as! PrivateChatAnchorTableViewCell
        cell.badge.isHidden = true
        cell.model = self.teacherRecordList[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 81
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.teacherRecordList[indexPath.row]
        //model.unreadCount = 0
        tableView.reloadRows(at: [indexPath], with: .automatic)
        //每次需要请求参数上次指点是否结束
        NetworkingHandle.fetchNetworkData(url: "/Home/check_teach", at: self, params: ["user_id":model.user_id!], success: { (response) in
            let data = response["data"] as! String
            if data == "1"{
                let chatController = DirectChatViewController(conversationChatter: model.hx_username, conversationType: EMConversationTypeChat)
                chatController?.img = model.img
                chatController?.usernameTHEY = model.username
                chatController?.userId = model.user_id
                chatController?.isTeacher = true
                chatController?.isCustomNav = false
                chatController?.hx_username = model.hx_username
                chatController?.reloadMeassage = { [unowned self] in
                    self.getList()
                }
                self.navigationController?.pushViewController(chatController!, animated: true)
            }else{
                
                let chatController = DirectChatViewController(conversationChatter: model.hx_username, conversationType: EMConversationTypeChat)
                chatController?.img = model.img
                chatController?.usernameTHEY = model.username
                chatController?.userId = model.user_id
                chatController?.isTeachBefore = true
                chatController?.isCustomNav = false
                chatController?.hx_username = model.hx_username
                chatController?.reloadMeassage = { [unowned self] in
                    self.getList()
                }
                self.navigationController?.pushViewController(chatController!, animated: true)
                

            }
        }) {
            
        }
        
        

    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        LyAlertView.alert(atVC: self, title: "提示", message: "是否确定删除此条记录?", cancel: "取消", ok: "确定", okBlock: {
            let model = self.teacherRecordList[indexPath.row]
            NetworkingHandle.fetchNetworkData(url: "/member/teach_list_del", at: self, params: ["id":model.id!], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
                ProgressHUD.showSuccess(message: "删除成功")
                self.teacherRecordList.remove(at: indexPath.row)
                self.tableview.reloadData()
            }) {
                
            }
        }) {
            
        }
        
        
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
