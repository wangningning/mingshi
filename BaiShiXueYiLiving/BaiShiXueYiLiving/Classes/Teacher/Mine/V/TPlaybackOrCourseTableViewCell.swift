//
//  TPlaybackOrCourseTableViewCell.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class TPlaybackOrCourseTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var date: UILabel!
    var model:MyLiveList!{
        willSet(m){
            title.text = m.title
            date.text = m.date_value
        }
    }
    var vmodel: VideoModel!{
        willSet(m){
            title.text = m.title
            date.text = ""
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
