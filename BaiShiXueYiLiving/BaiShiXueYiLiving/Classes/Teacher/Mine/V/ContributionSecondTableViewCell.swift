//
//  ContributionSecondTableViewCell.swift
//  Duluo
//
//  Created by sh-lx on 2017/4/12.
//  Copyright © 2017年 tts. All rights reserved.
//

import UIKit

class ContributionSecondTableViewCell: UITableViewCell {

    @IBOutlet weak var userImg: UIImageView!
    
    @IBOutlet weak var usernameLab: UILabel!
    
    @IBOutlet weak var countLab: UILabel!
    
    @IBOutlet weak var gradeBtn: UIButton!
    
    @IBOutlet weak var sexImg: UIImageView!
    
    @IBOutlet weak var medalImage: UIImageView!
   
    @IBOutlet weak var noImg: UIImageView!
    var type: String!{
        willSet(type){
            
            if type == "1"{
                
                medalImage.image = UIImage(named:"yinpai")
                noImg.image = UIImage(named:"no2")
            } else if type == "2" {
                
                medalImage.image = UIImage(named:"tongpai")
                noImg.image = UIImage(named:"no3")
            }
        }
    }
    var model: ContributionModel!{
        
        willSet(m){
            
            userImg.kf.setImage(with: URL(string:m.img!))
            usernameLab.text = m.username
            countLab.text = "贡献 " + m.jewel!
          
            
            if m.sex == "1" {
                
                sexImg.image = #imageLiteral(resourceName: "ssjg_man")
                
            } else {
                
                sexImg.image = #imageLiteral(resourceName: "ssjg_woman")
                
            }
            
            
        }
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userImg.layer.cornerRadius = 64 / 2
        userImg.layer.masksToBounds = true
        
        userImg.clipsToBounds = true
        userImg.contentMode = .scaleAspectFill
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
