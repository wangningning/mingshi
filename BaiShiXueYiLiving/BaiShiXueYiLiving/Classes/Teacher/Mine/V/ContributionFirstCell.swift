//
//  ContributionFirstCell.swift
//  Duluo
//
//  Created by sh-lx on 2017/4/11.
//  Copyright © 2017年 tts. All rights reserved.
//

import UIKit

class ContributionFirstCell: UITableViewCell {

    @IBOutlet weak var userImg: UIImageView!
    
    @IBOutlet weak var sexImg: UIImageView!
    @IBOutlet weak var usernameLab: UILabel!
    
    @IBOutlet weak var gradeBtn: UIButton!
    
    @IBOutlet weak var numLab: UILabel!
    
    var model : ContributionModel!{
        willSet(m){
            
            userImg.kf.setImage(with: URL(string:m.img!))
            if m.sex == "1"
            {
                sexImg.image = #imageLiteral(resourceName: "ssjg_man")
            } else {
                sexImg.image = #imageLiteral(resourceName: "ssjg_woman")
            }
            usernameLab.text = m.username
            numLab.text = m.jewel!
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userImg.layer.cornerRadius = 83 / 2
        userImg.layer.masksToBounds = true
        
        userImg.clipsToBounds = true
        userImg.contentMode = .scaleAspectFill
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
