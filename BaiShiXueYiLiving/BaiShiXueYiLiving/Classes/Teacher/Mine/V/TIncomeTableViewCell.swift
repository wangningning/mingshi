//
//  TIncomeTableViewCell.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class TIncomeTableViewCell: UITableViewCell {

    @IBOutlet weak var incomeLab: UILabel!
    
    @IBOutlet weak var dateLab: UILabel!
    
    @IBOutlet weak var typeLab: UILabel!
    var model:TIncomeModel!{
        willSet(m){
            incomeLab.text = "金额：" + m.earnings!
            dateLab.text = "时间：" + m.date!
            typeLab.text = m.content
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
