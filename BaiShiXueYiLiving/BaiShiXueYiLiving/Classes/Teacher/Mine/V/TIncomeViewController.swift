//
//  TIncomeViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh

class TIncomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var page = 1
    var dataArr: [TIncomeModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "教学收益"
        self.tableView.tableFooterView = UIView()
        self.tableView.register(UINib(nibName:"TIncomeTableViewCell",bundle:nil), forCellReuseIdentifier: "TIncomeTableViewCell")
        
        self.tableView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.page = 1
            self.loadData()
        })
        self.tableView.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: { [unowned self] in
            self.page += 1
            self.loadData()
        })
        self.tableView.mj_header.beginRefreshing()
        // Do any additional setup after loading the view.
    }
    func loadData(){
        NetworkingHandle.fetchNetworkData(url: "/Member/earnings_list", at: self, params: ["p":self.page], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: self.tableView, success: { (response) in
            
            let data = response["data"] as! [String:AnyObject]
            let list = TIncomeModel.modelsWithArray(modelArray: data["list"] as! [[String:AnyObject]]) as! [TIncomeModel]
            
            if self.page == 1{
                self.dataArr.removeAll()
            }
            if list.count == 0{
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
            }
            self.dataArr += list
            self.tableView.reloadData()
        }) {
            
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TIncomeTableViewCell") as! TIncomeTableViewCell
        cell.model = self.dataArr[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArr.count
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
