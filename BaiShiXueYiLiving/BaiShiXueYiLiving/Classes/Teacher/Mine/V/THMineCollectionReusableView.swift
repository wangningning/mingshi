//
//  THMineCollectionReusableView.swift
//  BaiShiXueYiLiving
//
//  Created by apple on 2017/5/18.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class THMineCollectionReusableView: UICollectionReusableView,UICollectionViewDelegate, UICollectionViewDataSource{
    
    @IBOutlet weak var bgImg: UIImageView!

    @IBOutlet weak var rankCollectionView: UICollectionView!
    @IBOutlet weak var rankView: UIView!
    
    @IBOutlet weak var layout: UICollectionViewFlowLayout!
    @IBOutlet weak var markLab: UILabel!
    
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var liveID: UILabel!
    @IBOutlet weak var sexImg: UIImageView!
    @IBOutlet weak var otherNum: UILabel!
    @IBOutlet weak var courseNUm: UILabel!
    @IBOutlet weak var fansNum: UILabel!
    @IBOutlet weak var rankViewWidth: NSLayoutConstraint!
    
    @IBOutlet weak var messageBtn: UIButton!
    var dataArr: [ContributionModel] = []
   
    var hisInfo : PersonInfoModel! {
        willSet(m) {
            
            self.confirmNewMessage()
            
            if m.sex == "1" || m.sex == "0" {
                sexImg.image = #imageLiteral(resourceName: "ssjg_man")
            } else if m.sex == "2" {
                sexImg.image = #imageLiteral(resourceName: "ssjg_woman")
            } 
            
            userName.text = m.username
            fansNum.text = m.fans_count
            otherNum.text = m.follow_count
            courseNUm.text = m.video_count
            if m.id != nil {
                liveID.text = "直播间ID：\(m.id!)"
            }
            if  m.img != nil {
                userImg.kf.setImage(with: URL(string: m.img!))
            }
            if m.mark == "" || m.mark == nil{
                markLab.text = "暂无评分"
            } else {
                markLab.text = "评分：" + m.mark!
            }
            self.dataArr = m.top!
            self.rankViewWidth.constant = CGFloat(35 * self.dataArr.count)
            self.rankCollectionView.reloadData()
          
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        bgImg.contentMode = .scaleAspectFill
        bgImg.clipsToBounds = true
        bgImg.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(THMineCollectionReusableView.jumpToPersonVC)))
        
        self.userImg.layer.cornerRadius = 50/2
        self.userImg.layer.masksToBounds = true
        self.userImg.contentMode = .scaleAspectFill
        self.userImg.clipsToBounds = true
        
        self.layout.minimumLineSpacing = 0
        self.layout.minimumInteritemSpacing = 5
        self.layout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5)
       // let w = (self.rankView.width - 5*3)/3
        self.layout.scrollDirection = .horizontal
        self.layout.itemSize = CGSize(width: 30, height: 30)
        
        rankCollectionView.delegate = self
        rankCollectionView.dataSource = self
        rankCollectionView.isScrollEnabled = false
        rankCollectionView.register(UINib(nibName: "AvatarCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "AvatarCollectionViewCell")
        rankView.isUserInteractionEnabled = true
        rankView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(THMineCollectionReusableView.rankViewClicked)))
    }
    func confirmNewMessage(){
        //私信 客服是否有新的消息
        let conversation = EMClient.shared().chatManager.getAllConversations() as! [EMConversation]
        for m in conversation {
            if m.type == EMConversationTypeChat, m.conversationId == UserDefaults.standard.value(forKey: "service") as! String, m.unreadMessagesCount != 0{
                serviceRD = false
            }
            if m.type == EMConversationTypeChat, m.conversationId != UserDefaults.standard.value(forKey: "service") as? String, m.unreadMessagesCount != 0{
                messageRD = false
            }
        }
        NetworkingHandle.fetchNetworkData(url: "/member/has_read_message", at: self, isShowHUD: false, success: { (response) in
            let data = response["data"] as! String
            if data == "1"{
                notificationRD = false
            }
            if serviceRD && messageRD && notificationRD{
                self.messageBtn.isSelected = false
                self.messageBtn.setImage(#imageLiteral(resourceName: "wd_xx"), for:.highlighted)
            } else{
                self.messageBtn.isSelected = true
                self.messageBtn.setImage(#imageLiteral(resourceName: "ic_m"), for:.highlighted)
            }
            
        })
        
    }

    @IBAction func messageBtnAction(_ sender: UIButton) {
        self.responderViewController()?.navigationController?.pushViewController(MessageCenterViewController(), animated: true)
    }
    func jumpToPersonVC(){
        
        
        let vc = PersonInfoEditViewController()
        vc.model = hisInfo
        vc.updateSuccess = { img in
            self.userImg.image = img
            self.fetchData()
        }
        self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)

        
    }
    // MARK: 获取用户信息
    func fetchData() {
        NetworkingHandle.fetchNetworkData(url: "/Member/index", at: self, success: { (response) in
            let data = response["data"] as! [String: AnyObject]
            self.hisInfo = PersonInfoModel.modelWithDictionary(diction: data)
            
        }) {
            
        }
    }

    func rankViewClicked() {
        
        let vc = ContributionListViewController()
        vc.user_id = DLUserInfoHandler.getIdAndToken()?.id
        self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func focusAction(_ sender: UIButton) {
        self.responderViewController()?.navigationController?.pushViewController(FocusOnCourseViewController(), animated: true)
        print("关注课程")
    }
    
    @IBAction func otherPerson(_ sender: UIButton) {
        let vc = FocusOtherPersonViewController()
        vc.type = "关注的人"
        self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)

    }
    @IBAction func fansAction(_ sender: UIButton) {
        let vc = FocusOtherPersonViewController()
        vc.type = "粉丝"
        self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
    }
}
extension THMineCollectionReusableView{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataArr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AvatarCollectionViewCell", for: indexPath) as! AvatarCollectionViewCell
        cell.model = self.dataArr[indexPath.row]
        return cell

    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("111111")
    }
}
