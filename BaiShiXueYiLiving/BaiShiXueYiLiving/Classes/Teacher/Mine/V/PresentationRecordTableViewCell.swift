//
//  PresentationRecordTableViewCell.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/27.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class PresentationRecordTableViewCell: UITableViewCell {
    @IBOutlet weak var moneyLab: UILabel!

    @IBOutlet weak var expend: UILabel!
    
    @IBOutlet weak var timeLab: UILabel!
    @IBOutlet weak var state: UILabel!
    
    var model: PresentationRecordModel!{
        willSet(m){
            self.moneyLab.text = "可到账金额：" + m.amount!
            
            self.expend.text = "消耗银票：" + m.score!
            if m.status == "1"{
                
                self.state.text = "申请"
                
            }else if m.status == "2"{
                
                self.state.text = "冻结"
                
            }else{
                self.state.text = "成功"
            }
            self.timeLab.text = m.date
        }
    }
    var tmodel: TeacherProfit!{
        willSet(m){
            self.moneyLab.text = "金额：" + m.earnings!
            self.timeLab.text = m.date
            if  m.type == "1" {
                self.state.text = "高级会员"
            }else if m.type == "2"{
                self.state.text = "钻石会员"
            }else{
                self.state.text = "一对一指导"
            }
            self.expend.isHidden = true
        }
    }
    override func awakeFromNib() {
       
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
