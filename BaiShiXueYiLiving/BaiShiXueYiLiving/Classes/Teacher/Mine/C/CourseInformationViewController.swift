//
//  CourseInformationViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/27.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class CourseInformationViewController: UIViewController {

    @IBOutlet weak var textview: UITextView!
    
    @IBOutlet weak var confirmBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.confirmBtn.layer.cornerRadius = 6
        self.confirmBtn.layer.masksToBounds = true
        self.title = "课程通知"
        
        self.textview.becomeFirstResponder()
        // Do any additional setup after loading the view.
    }

    @IBAction func confirmAction(_ sender: UIButton) {
        if self.textview.text.characters.count == 0{
            ProgressHUD.showMessage(message: "请输入您要通知的内容")
            return
        }
        NetworkingHandle.fetchNetworkData(url: "/Member/send_notice", at: self, params: ["content":self.textview.text], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
            ProgressHUD.showSuccess(message: "发送成功")
            self.navigationController?.popViewController(animated: true)
        }) { 
            
        }
        
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
