//
//  TeacherProfitViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/27.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh
class TeacherProfitViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tabelview: UITableView!
    var teacherArr = [TeacherProfit]()
    var page = 1
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "教师收益"
        self.tabelview.tableFooterView = UIView()
        self.tabelview.register(UINib(nibName:"PresentationRecordTableViewCell",bundle:Bundle.main), forCellReuseIdentifier: "PresentationRecordTableViewCell")
        self.tabelview.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.page = 1
            self.loadData()
        })
        self.tabelview.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: { [unowned self] in
            self.page += 1
            self.loadData()
        })
        self.tabelview.mj_header.beginRefreshing()
        // Do any additional setup after loading the view.
    }
    func loadData(){
        NetworkingHandle.fetchNetworkData(url: "/Member/earnings_list", at: self, params: ["p":self.page], hasHeaderRefresh: self.tabelview, success: { (response) in
            let data = response["data"] as! [String:AnyObject]
            let arr =  TeacherProfit.modelsWithArray(modelArray: data["list"] as! [[String:AnyObject]]) as! [TeacherProfit]
            if arr.count == 0{
                self.tabelview.mj_footer.endRefreshingWithNoMoreData()
            }
            if self.page == 1{
                self.teacherArr.removeAll()
            }
            self.teacherArr += arr
            self.tabelview.reloadData()
        }) { 
            
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PresentationRecordTableViewCell") as! PresentationRecordTableViewCell
        cell.selectionStyle = .none
        cell.tmodel = self.teacherArr[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.teacherArr.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
