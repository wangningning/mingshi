//
//  BindingAlipayViewController.swift
//  MoDuLiving
//
//  Created by 曾觉新 on 2017/3/11.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class BindingAlipayViewController: BSBaseViewController, UITextFieldDelegate {
    
    var scrollView: UIScrollView!
    var bgView: UIView!
    var iconImageView: UIImageView!
    var accountTextField: UITextField!
    var nameField: UITextField!
    var bindingButton: UIButton!
    var line: UILabel!
    var phoneNum = ""
    var codeTF: UITextField!
    var codeBtn: UIButton!
   // var type: BindingAlipayType = .binding
    var bindSuccess:(()->())?
    var purseModel: MyPurseModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "提现账户绑定"
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "提现记录", target: self, action: #selector(rightBarButtonItemaAction))
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        
        initViews()
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(resignKeyboard)))
    }
    
    //MARK: 初始化
    func initViews() {
        scrollView = UIScrollView()
        scrollView.isScrollEnabled = true
        scrollView.alwaysBounceVertical = true
        scrollView.backgroundColor = UIColor(hexString:"#f1f5f6")
        scrollView.frame = CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height)
        self.view = scrollView
//        scrollView.snp.makeConstraints { (make) in
//            make.left.right.top.bottom.equalTo(0)
//        }
        
        bgView = UIView()
        bgView.backgroundColor = UIColor.white
        scrollView.addSubview(bgView)
        bgView.snp.makeConstraints { (make) in
            make.top.left.equalTo(0)
            make.width.equalTo(scrollView.snp.width)
            make.height.equalTo(340)
        }
        
        iconImageView = UIImageView()
        iconImageView.image = UIImage(named: "bg_Alipay")
        bgView.addSubview(iconImageView)
        iconImageView.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.bgView.snp.centerX)
            make.top.equalTo(20)
        }
        
        let clueLabel = UILabel()
        clueLabel.text = ""
        clueLabel.font = UIFont.systemFont(ofSize: 10)
        clueLabel.textColor = UIColor(hexString:"#f1f5f6")
        clueLabel.backgroundColor = UIColor(hexString:"#f1f5f6")
        bgView.addSubview(clueLabel)
        clueLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.bgView.snp.centerX)
            make.top.equalTo(self.iconImageView.snp.bottom).offset(20)
        }
        
        let line = UIView()
        line.backgroundColor = UIColor(hexString:"#f1f5f6")
        bgView.addSubview(line)
        line.snp.makeConstraints { (make) in
            make.left.right.equalTo(0)
            make.height.equalTo(10)
            make.top.equalTo(clueLabel.snp.bottom).offset(10)
        }
        
        let accountLabel = UILabel()
        accountLabel.text = "支付宝账号"
        accountLabel.font = UIFont.systemFont(ofSize: 14)
        bgView.addSubview(accountLabel)
        accountLabel.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(line.snp.bottom).offset(0)
            make.height.equalTo(45)
        }
        
        self.accountTextField = UITextField()
        self.accountTextField.delegate = self
        self.accountTextField.font = UIFont.systemFont(ofSize: 12)
        self.accountTextField.placeholder = "请输入支付宝账号"
       // self.accountTextField.borderStyle = .roundedRect
        bgView.addSubview(self.accountTextField)
        self.accountTextField.snp.makeConstraints { (make) in
            make.centerY.equalTo(accountLabel.snp.centerY)
            make.left.equalTo(110)
            make.right.equalTo(-15)
            make.height.equalTo(30)
        }
        
        self.line = UILabel()
        self.line.backgroundColor = UIColor(hexString:"#f1f5f6")
        bgView.addSubview(self.line)
        
        self.line.snp.makeConstraints { (make) in
            make.top.equalTo(self.accountTextField.snp.bottom).offset(5)
            make.left.right.equalTo(0)
            make.height.equalTo(1)
        }
        
        let nameLabel = UILabel()
        nameLabel.text = "真实姓名"
        nameLabel.font = UIFont.systemFont(ofSize: 14)
        bgView.addSubview(nameLabel)
        nameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(accountLabel.snp.left)
            make.centerY.equalTo(accountLabel.snp.centerY).offset(45)
            make.height.equalTo(45)
        }
        
        self.nameField = UITextField()
        self.nameField.delegate = self
        self.nameField.font = UIFont.systemFont(ofSize: 12)
        self.nameField.placeholder = "请输入真实姓名"
      //  self.nameField.borderStyle = .roundedRect
        bgView.addSubview(self.nameField)
        self.nameField.snp.makeConstraints { (make) in
            make.centerY.equalTo(nameLabel.snp.centerY)
            make.left.equalTo(self.accountTextField.snp.left)
            make.right.equalTo(self.accountTextField.snp.right)
            make.height.equalTo(self.accountTextField.snp.height)
        }
        
        let line2 = UILabel()
        line2.backgroundColor = UIColor(hexString:"#f1f5f6")
        bgView.addSubview(line2)
        
        line2.snp.makeConstraints { (make) in
            make.left.right.equalTo(0)
            make.top.equalTo(nameLabel.snp.bottom).offset(0)
            make.height.equalTo(10)
        }
        
        let codeLab = UILabel()
        codeLab.text = "验证码"
        codeLab.font = UIFont.systemFont(ofSize: 14)
        codeLab.textColor = UIColor(hexString:"#333333")
        bgView.addSubview(codeLab)
        
        codeLab.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(line2.snp.bottom)
            make.height.equalTo(45)
        }
        self.codeBtn = UIButton(type: .custom)
        self.codeBtn.setTitle("验证", for: .normal)
        self.codeBtn.setTitleColor(UIColor(hexString:"#00acf2"), for: .normal)
        self.codeBtn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        self.codeBtn.layer.cornerRadius = 12
        self.codeBtn.layer.borderWidth = 1
        self.codeBtn.layer.masksToBounds = true
        self.codeBtn.layer.borderColor = UIColor(hexString:"#00acf2").cgColor
        bgView.addSubview(self.codeBtn)
        self.codeBtn.addTarget(self, action: #selector(self.getCode), for: .touchUpInside)
        self.codeBtn.snp.makeConstraints { (make) in
            make.right.equalTo(-10)
            make.centerY.equalTo(codeLab.snp.centerY)
            make.width.equalTo(60)
            make.height.equalTo(26)
            
            
        }

        self.codeTF = UITextField()
        self.codeTF.placeholder = "请输入您获得的验证码"
        self.codeTF.delegate = self
        self.codeTF.font = UIFont.systemFont(ofSize: 12)
        bgView.addSubview(self.codeTF)
        
        self.codeTF.snp.makeConstraints { (make) in
            make.centerY.equalTo(codeLab.snp.centerY)
            make.left.equalTo(self.nameField.snp.left)
            make.right.equalTo(self.codeBtn.snp.left)
            make.height.equalTo(self.nameField.snp.height)
        }
        
        
        
        self.bindingButton = UIButton(type: .custom)
        self.bindingButton.setTitle("立即绑定", for: .normal)
        self.bindingButton.setTitleColor(UIColor.white, for: .normal)
        self.bindingButton.backgroundColor = UIColor(hexString: "#00acf2")
        self.bindingButton.layer.cornerRadius = 5
        self.bindingButton.layer.masksToBounds = true
        self.scrollView.addSubview(self.bindingButton)
        self.bindingButton.addTarget(self, action: #selector(clickBindingButton(sender:)), for: .touchUpInside)
        self.bindingButton.snp.makeConstraints { (make) in
            make.left.equalTo(30)
            make.width.equalTo(scrollView.width-60)
//            make.right.equalTo(self.view.snp.right).offset(-30)
            make.height.equalTo(40)
            make.top.equalTo(self.bgView.snp.bottom).offset(40)
        }
        
        
        
        
    }
    func getCode(){
        let timer = fetchVerificationCodeCountdown(button: codeBtn, timeOut: 60)
        NetworkingHandle.fetchNetworkData(url: "/Public/sendSMS", at: self, params: ["mobile": DLUserInfoHandler.getUserInfo()?.phone ?? "", "type": "3"], success: { (result) in
        }, failure: { [unowned self] errorCode in
            timer.cancel()
            self.codeBtn.setTitle("验证", for: .normal)
            self.codeBtn.isEnabled = true
        })

    }
    
    
    //MARK: 点击事件
    func rightBarButtonItemaAction() {
//        let vc = PresentationRecordViewController()
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    func clickBindingButton(sender: UIButton) {
        bindingAlipay()
    }
    func resignKeyboard() {
        UIApplication.shared.keyWindow?.endEditing(true)
    }
    
    
    //MARK:  网络请求
    func bindingAlipay() {
       // self.navigationController?.pushViewController(WithdrawViewController(), animated: true)
        if codeTF.text!.characters.count == 0 || (self.nameField.text?.characters.count)! == 0{
            ProgressHUD.showMessage(message: "请完整信息")
            return
        }
      if (self.accountTextField.text?.isPhoneNumber)!{
            
        }else{
            if (self.accountTextField.text?.isEmail)!{
                
            }else{
                ProgressHUD.showMessage(message: "支付宝邮箱格式错误")
                return
            }
        }
        let param = ["bank_card": self.accountTextField.text!,
                     "verify" : self.codeTF.text!,
                     "realname" : self.nameField.text!] as [String : AnyObject]
        let url = "/Member/set_default_withdraw_card"
        NetworkingHandle.fetchNetworkData(url: url, at: self, params: param,  success: { (reponse) in
            self.bindSuccess?()
            self.navigationController?.popViewController(animated: true)
        }) {
            
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
