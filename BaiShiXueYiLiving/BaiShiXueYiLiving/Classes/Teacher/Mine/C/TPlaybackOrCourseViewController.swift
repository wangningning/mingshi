//
//  TPlaybackOrCourseViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh

class TPlaybackOrCourseViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var page = 1
    var dataArr: [Any] = []
    var videoArr = [Any]()
    var count: String!
    var isPlayback:Bool!
    var nav : UINavigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isPlayback{
            self.title = "精彩回放"
        }else{
            self.title = "我的课程"
        }
        
        self.tableView.tableFooterView = UIView()
    self.tableView.register(UINib(nibName:"TPlaybackOrCourseTableViewCell",bundle:nil), forCellReuseIdentifier: "TPlaybackOrCourseTableViewCell")
        self.tableView.register(UINib(nibName:"PhotoIndex1CollectionReusableView",bundle:Bundle.main), forHeaderFooterViewReuseIdentifier: "PhotoIndex1CollectionReusableView")
        self.tableView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.page = 1
            self.loadData()
        })
        self.tableView.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: { [unowned self] in
            self.page += 1
            self.loadData()
        })
        self.tableView.mj_header.beginRefreshing()

        // Do any additional setup after loading the view.
    }
    func loadData(){
        var url = ""
        if isPlayback{
            url = "/Home/live_store"
        }else{
            url = "/Home/tutor_video"
        }
        NetworkingHandle.fetchNetworkData(url: url, at: self, params: ["user_id":DLUserInfoHandler.getIdAndToken()!.id,"p":self.page], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: self.tableView, success: { (response) in
            
            let data = response["data"] as! [String:AnyObject]
            var list = Array<Any>.init()
            if self.isPlayback{
               list = MyLiveList.modelsWithArray(modelArray: data["list"] as! [[String:AnyObject]]) as! [MyLiveList]
            }else{
                list = VideoModel.modelsWithArray(modelArray: data["list"] as! [[String:AnyObject]]) as! [VideoModel]
            }
           
            self.count = data["count"] as! String
            if self.page == 1{
                self.dataArr.removeAll()
                self.videoArr.removeAll()
            }
            if list.count == 0{
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
            }
            if self.isPlayback{
               self.dataArr += list
            }else{
                self.videoArr += list
            }
            
            self.tableView.reloadData()
        }) {
            
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TPlaybackOrCourseTableViewCell") as! TPlaybackOrCourseTableViewCell
        if indexPath.section == 0 {
            cell.date.isHidden = true
            cell.title.textColor = UIColor(hexString: "#999999")
            cell.title.font = UIFont.systemFont(ofSize: 12.0)
            if self.count != nil{
                if isPlayback{
                    cell.title.text = self.count + "个精彩回放"
                }else{
                    cell.title.text = self.count + "个免费视频"
                }

            }
        }else{
            if isPlayback {
                cell.model = self.dataArr[indexPath.row] as! MyLiveList
            }else{
                cell.vmodel = self.videoArr[indexPath.row] as! VideoModel
            }
            
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return self.dataArr.count
        }
       
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 36
        }else{
            return 41
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isPlayback{
            if self.dataArr.count <= 0{
                return
            }
            let vc = PlayVideoViewController()
            vc.username = (dataArr[indexPath.row] as! MyLiveList).title
            vc.play_img = (dataArr[indexPath.row] as! MyLiveList).play_img
            vc.url = (dataArr[indexPath.row] as! MyLiveList).url
            vc.live_store_id = (dataArr[indexPath.row] as! MyLiveList).live_id
            vc.share_url = (dataArr[indexPath.row] as! MyLiveList).share_url
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            if self.videoArr.count <= 0{
                return
            }
            let vc = TCurriculumDetailViewController()
            vc.videoID = (videoArr[indexPath.row] as! VideoModel).video_id
            vc.play_img = (videoArr[indexPath.row] as! VideoModel).video_img
            vc.url = (videoArr[indexPath.row] as! VideoModel).url
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
