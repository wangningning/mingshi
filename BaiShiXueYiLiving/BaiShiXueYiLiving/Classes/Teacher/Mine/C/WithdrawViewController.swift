//
//  WithdrawViewController.swift
//  MoDuLiving
//
//  Created by 曾觉新 on 2017/3/11.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import TTTAttributedLabel

class WithdrawViewController: UIViewController, UITextFieldDelegate,TTTAttributedLabelDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var huoliCountLabel: UILabel!
    @IBOutlet weak var moneyLabel: UILabel!//可兑换收益
    @IBOutlet weak var alipayAccLabel: UILabel!
    @IBOutlet weak var goldTextField: UITextField!
    @IBOutlet weak var arrivalMoneyLabel: UILabel!
    @IBOutlet weak var changeAccButton: UIButton!
    @IBOutlet weak var putInButton: UIButton!
    
    @IBOutlet weak var protocolLab: UILabel!
    
    var withDrawSuccess: (()->())?
    
    var arrivalMoney: Float!
  //  var alipayModel: AlipayWithdrawModel?
    var purseModel: MyPurseModel!
    var ratio: String!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.scrollView.bounces = false
        self.scrollView.alwaysBounceVertical = false
        self.scrollView.alwaysBounceHorizontal = true
        NotificationCenter.default.addObserver(self, selector: #selector(textFieldTextDidChange), name: NSNotification.Name.UITextFieldTextDidEndEditing, object: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "收益提现"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "提现记录", target: self, action: #selector(rightBarButtonItemaAction))
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSForegroundColorAttributeName : UIColor(hexString:"#ec6b1a"),NSFontAttributeName:UIFont.systemFont(ofSize: 16)], for: .normal)
        NotificationCenter.default.addObserver(self, selector: #selector(WithdrawViewController.textFieldTextDidChange(notification:)), name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
        
        
        let attr = NSMutableAttributedString.init(string: "具体提现规则请查看《名师传艺》规定的《银票提现协议》\n且《名师传艺》拥有最终解释权")
        attr.addAttributes([NSForegroundColorAttributeName:UIColor.init(hexString: "#ec6b1a")], range: NSRange.init(location: 18, length: 8))
        self.protocolLab.attributedText = attr
        self.protocolLab.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(WithdrawViewController.searchUserPayRule)))
        self.loadData()
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(resignKeyboard)))
        
        initViews()
        
    }
    
    func loadData(){
        NetworkingHandle.fetchNetworkData(url: "/Member/withdraw_info", at: self, success: { (response) in
            let data = response["data"] as! [String:AnyObject]
            self.huoliCountLabel.text = "\(data["amount"] as! String)银票"
            self.ratio = data["ratio"] as! String
            self.moneyLabel.text = "\(Float(data["amount"] as! String)! * Float(data["ratio"] as! String)!)元"
            self.arrivalMoney = Float(data["amount"] as! String)! * Float(data["ratio"] as! String)!
            if data["card"] == nil{
                self.alipayAccLabel.isUserInteractionEnabled = true
                self.alipayAccLabel.text = "  去绑定  "
                self.alipayAccLabel.layer.cornerRadius = 6
                self.alipayAccLabel.layer.borderColor = UIColor(hexString: "#ec6b1a").cgColor
                self.alipayAccLabel.layer.borderWidth = 1.0
                self.alipayAccLabel.layer.masksToBounds = true
                self.alipayAccLabel.textColor = UIColor(hexString: "#ec6b1a")
                self.alipayAccLabel.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.bindAccount)))
            }else{
                self.purseModel = MyPurseModel.modelWithDictionary(diction: data["card"] as! [String:AnyObject])
                if self.purseModel.bank_id != nil{
                    self.alipayAccLabel.reloadInputViews()
                    self.alipayAccLabel.isUserInteractionEnabled = false
                    self.alipayAccLabel.layer.borderWidth = 0
                    self.alipayAccLabel.text = self.getAlipayAcc(account: self.purseModel.bank_card!)
                    self.alipayAccLabel.textColor = UIColor.init(hexString: "#CDCDCD")
                    
                }else{
                    self.alipayAccLabel.isUserInteractionEnabled = true
                    self.alipayAccLabel.text = "  去绑定  "
                    self.alipayAccLabel.layer.cornerRadius = 6
                    self.alipayAccLabel.layer.borderColor = UIColor(hexString: "#ec6b1a").cgColor
                    self.alipayAccLabel.layer.borderWidth = 1.0
                    self.alipayAccLabel.layer.masksToBounds = true
                    self.alipayAccLabel.textColor = UIColor(hexString: "#ec6b1a")
                    self.alipayAccLabel.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.bindAccount)))
                }
                
                
            }
            
        })
    }
    func bindAccount(){
        let vc = BindingAlipayViewController()
        vc.purseModel = self.purseModel
        vc.bindSuccess = { [unowned self] in
            self.alipayAccLabel.isUserInteractionEnabled = false
            self.view.reloadInputViews()
            self.loadData()
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func initViews() {
        scrollView.backgroundColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.00)
        putInButton.layer.cornerRadius = 5
        putInButton.layer.masksToBounds = true
        putInButton.isUserInteractionEnabled = false
        
    }
    
    //MARK: 点击事件
    func rightBarButtonItemaAction() {
        let vc = PresentationRecordViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    @IBAction func clickPutInButton(_ sender: UIButton) {
        putIn()
    }
    
    //MARK: UITextFieldDelegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if text.characters.count == 0 {
            self.arrivalMoneyLabel.text = "0"
            return true
        }
        if Float(text) == nil {
            return true
        }
        if Float(text)! > Float((self.arrivalMoney)) {
            textField.text = "\(self.arrivalMoney!)"
            getArrivalMoney(huoli: textField.text!)
            return false
        }
        return true
    }
    func textFieldTextDidChange(notification: Notification){
        
        let object: UITextField = notification.object as! UITextField
        let text = object.text
        
       
        if text?.characters.count == 0 {
            
         putInButton.isUserInteractionEnabled = false
            
        } else {
            putInButton.isUserInteractionEnabled = true
            getArrivalMoney(huoli: text!)
        }
    }
    func resignKeyboard() {
        
        self.goldTextField.resignFirstResponder()
    }
    
    //MARK:网络请求
    func putIn() {
        if Float(goldTextField.text!)! > Float(self.arrivalMoney) {
            ProgressHUD.showNoticeOnStatusBar(message: "超出额度")
            return
        }
        let param = ["card_id": self.purseModel.id! as AnyObject,
                     "money" : self.arrivalMoneyLabel.text as AnyObject] as [String : AnyObject]
        let url = "/Member/withdraw"
        NetworkingHandle.fetchNetworkData(url: url, at: self, params: param, success: { (reponse) in
            let message = "您本次申请提现：\(self.arrivalMoneyLabel.text!)元，请耐心等待审核通过后欠款自动汇入账户"
            let alert = UIAlertController(title: "申请已提交", message: message, preferredStyle: .alert)
            
            let action = UIAlertAction(title: "确定", style: .default, handler: { (action) in
                self.withDrawSuccess?()
                self.navigationController?.popViewController(animated: true)

            })
            alert.addAction(action)
            
            self.navigationController?.present(alert, animated: true, completion: nil)

        }) { 
            
        }
    }
    func getArrivalMoney(huoli: String) {//获取到达的金额
        self.arrivalMoneyLabel.text = "\(Float(huoli)! * Float(self.ratio)!)"
    }
    
    
    //MARK: obkect
    func getAlipayAcc(account: String) -> String {

        if  account.characters.count >= 7{
            return (account as NSString).substring(to: 3) + "***" + (account as NSString).substring(from: account.characters.count - 3)
        } else {
            return account
        }
        
    }
    
     func searchUserPayRule() {
        let vc = MDWebViewController()
        vc.url = NetworkingHandle.mainHost + "/Home/xieyi/id/9"
        vc.title = "用户充值协议"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
