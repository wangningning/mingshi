//
//  PresentationRecordViewController.swift
//  MoDuLiving
//
//  Created by 曾觉新 on 2017/3/7.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh

class PresentationRecordViewController: BSBaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var dataArr: [PresentationRecordModel] = []
    var page = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "提现记录"
        
    self.tableView.register(UINib(nibName:"PresentationRecordTableViewCell",bundle:Bundle.main), forCellReuseIdentifier: "PresentationRecordTableViewCell")
        
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor(hexString: "f1f5f6")
        self.tableView.tableFooterView = UIView()
       
        self.tableView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.requestDate(page: 1)
        })
        self.tableView.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: { [unowned self] in
            self.requestDate(page: self.page + 1)
            
        })
        self.tableView.mj_header.beginRefreshing()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArr.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "PresentationRecordTableViewCell", for: indexPath) as! PresentationRecordTableViewCell
        cell.selectionStyle = .none
        cell.model = self.dataArr[indexPath.row]
        return cell
    }
    
    
    //MARK: 请求数据
    func requestDate(page: Int) {
        let params = ["p" : "\(page)"];
        NetworkingHandle.fetchNetworkData(url: "/Member/withdraw_record", at: self, params: params,  hasHeaderRefresh: tableView, success: { (response) in
            self.tableView.mj_header.endRefreshing()
            self.page = page
            
            let data = response["data"] as! [String:AnyObject]
           
            let arr = PresentationRecordModel.modelsWithArray(modelArray: data["list"] as! [[String:AnyObject]])
            if arr.count == 0{
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
            }
            if self.page == 1 {
                

                
                self.dataArr = PresentationRecordModel.modelsWithArray(modelArray: (response["data"] as! [String:Any])["list"] as! [[String : AnyObject]]) as! [PresentationRecordModel]
            } else {
                self.dataArr += PresentationRecordModel.modelsWithArray(modelArray: (response["data"] as! [String:Any])["list"] as! [[String : AnyObject]]) as! [PresentationRecordModel]
            }
            self.tableView.reloadData()
        }) { 
            
        }
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
