//
//  ContributionListViewController.swift
//  Duluo
//
//  Created by 梁毅 on 2017/3/21.
//  Copyright © 2017年 tts. All rights reserved.
//

import UIKit
import MJRefresh

class ContributionListViewController: DLBaseViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var user_id: String?
    var dataArr:[ContributionModel] = []
    var totalNum: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "贡献榜"
        self.navigationController?.navigationBar.isHidden = false
        
        tableView.tableFooterView = UIView()
        tableView.tableHeaderView = UIView()
        
        tableView.register(UINib(nibName: "ContributionFirstCell", bundle: Bundle.main), forCellReuseIdentifier: "FirstCell")
        tableView.register(UINib(nibName: "ContributionSecondTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "SecondCell")
        tableView.register(UINib(nibName: "ContributionThirdTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ThirdCell")
        tableView.separatorStyle = .none
        
        
        tableView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.page = 1
            self.loadData()
        })
        
        let footer = MJRefreshAutoNormalFooter(refreshingBlock: { [unowned self] in
            self.page += 1
            self.loadData()
        })
        footer?.setTitle("没有更多数据了", for: .noMoreData)
        footer?.setTitle("上拉查看更多", for: .idle)
        tableView.mj_footer = footer
//        
        self.tableView.mj_header.beginRefreshing()
    }
    //MARK: FeedBackSegmentedViewDelegate
  

    func loadData() {
        let param = ["user_id":self.user_id!,"p":self.page] as [String : Any]
        let url = "?m=Api&c=Home&a=top"
        NetworkingHandle.fetchNetworkData(url: url, at: self, params: param, isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: self.tableView, success: { (response) in
            let data = response["data"] as! [String:AnyObject]
            self.totalNum = data["count"] as? String
            let list = ContributionModel.modelsWithArray(modelArray: data["list"] as! [[String:AnyObject]]) as! [ContributionModel]
            if list.count == 0{
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
            }
            if self.page == 1{
                self.dataArr.removeAll()
            }
            self.dataArr += list
            
            self.tableView.reloadData()
            
        }) { 
            if self.page > 1{
                self.page -= 1
            }
        }
    }
    
    //MARK: - table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FirstCell") as! ContributionFirstCell
            cell.model = self.dataArr[indexPath.row]
            cell.selectionStyle = .none
            return cell
            
        } else if indexPath.row == 1 
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SecondCell") as! ContributionSecondTableViewCell
            cell.model = self.dataArr[indexPath.row]
            cell.selectionStyle = .none
            cell.type = "1"
            return cell
            
        } else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SecondCell") as! ContributionSecondTableViewCell
            cell.model = self.dataArr[indexPath.row]
            cell.selectionStyle = .none
            cell.type = "2"
            return cell
        }
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ThirdCell") as! ContributionThirdTableViewCell
            cell.model = self.dataArr[indexPath.row]
            cell.rankLab.text = "NO."+"\(indexPath.row + 1)"
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            
            return 221
            
        } else if indexPath.row == 1 || indexPath.row == 2{
            return 116
        }
        return 70
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        
        let header = UILabel()
        if self.totalNum != nil{
            if self.totalNum == "" {
                header.text = "暂无数据"
            }else{
                header.text = self.totalNum! + "贡献"
            }
            
 
        }
        header.textColor = UIColor(red: 227/255, green: 61/255, blue: 65/255, alpha: 1)
        header.font = UIFont.systemFont(ofSize: 18)
        header.textAlignment = .center
        header.backgroundColor = UIColor.white
        header.layer.borderColor = UIColor(hexString: "#edecf2").cgColor
        header.layer.borderWidth = 1
        return header
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        pushToUserInfoCenter(atViewController: self, uId: self.dataArr[indexPath.row].user_id!)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
