//
//  TMineViewController.swift
//  BaiShiXueYiLiving
//
//  Created by 梁毅 on 2017/5/10.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh

class TMineViewController: BSBaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIGestureRecognizerDelegate, UINavigationControllerDelegate{
    
    var dataArr: [VCModel] = []
    var headerView : THMineCollectionReusableView!
    var person = PersonInfoModel()
    @IBOutlet weak var layout: CustomCollectionViewFlowLayout!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        NotificationCenter.default.addObserver(self, selector: #selector(self.userInfoChangeNotifcation(notification:)), name: NSNotification.Name(rawValue: UserInfoChangeNotifcation), object: nil)
        
        //关注人数改变通知
        NotificationCenter.default.addObserver(self, selector: #selector(self.loadData), name: NSNotification.Name(rawValue:focusPersonNumChangeNotification), object: nil)
        
        
        self.navigationController?.delegate = self
        self.loadData()
        
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        layout.sectionInset = UIEdgeInsets(top: 5, left: 0, bottom: 22, right: 0)
        
        layout.headerReferenceSize = CGSize(width:kScreenWidth, height: 200/375.0 * kScreenWidth + 46)
        let w = (kScreenWidth - 2)/3
        layout.itemSize = CGSize(width: w, height: 115/125.0 * w)
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(UINib(nibName:"THMineCollectionReusableView",bundle:Bundle.main), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "THMineCollectionReusableView")
        
        collectionView.register(UINib(nibName:"CollectionHeaderViewCell",bundle:Bundle.main), forCellWithReuseIdentifier: "CollectionHeaderViewCell")
        
        self.dataArr = [VCModel(itemImg: "icon_huifang",  vc: TPlaybackOrCourseViewController.self),
                        VCModel(itemImg: "icon_kecheng2", vc: TPlaybackOrCourseViewController.self),
                        VCModel(itemImg: "icon_zbshouyi", vc: WithdrawViewController.self),
                        VCModel(itemImg: "icon_jxshouyi", vc: TIncomeViewController.self),
                        VCModel(itemImg: "icon_tongzhi",  vc: CourseInformationViewController.self),
                        VCModel(itemImg: "icon_xuesheng", vc: TMyStudentViewController.self),
                        VCModel(itemImg: "icon_shezhi2",  vc: SettingViewController.self),
//                        VCModel(itemImg: "icon_bangzhu-", vc: HelpViewController.self),VCModel(itemImg: "ic_tiezi", vc: MyPostsViewController.self),
                        VCModel(itemImg: "icon_qiehuan2", vc: MineViewController.self)]
    }
    func loadData(){
       
        NetworkingHandle.fetchNetworkData(url: "/Member/index", at: self, isAuthHide: true, isShowHUD: false, isShowError: true, hasHeaderRefresh: self.collectionView, success: { (response) in
            
            let data = response["data"] as! [String: AnyObject]
            self.person = PersonInfoModel.modelWithDictionary(diction: data)
            if self.person.type != DLUserInfoHandler.getUserInfo()?.type{
                ProgressHUD.showMessage(message: "身份发生改变，请重新登录")
                UIApplication.shared.keyWindow?.rootViewController = LoginNavigationController.setup()
                return
            }
            self.headerView.hisInfo = self.person
            self.collectionView.reloadData()
            
        }) { 
            
        }
    }
   
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataArr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionHeaderViewCell", for: indexPath) as! CollectionHeaderViewCell
        cell.itemImg.image = UIImage(named:self.dataArr[indexPath.row].itemImg)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "THMineCollectionReusableView", for: indexPath) as! THMineCollectionReusableView
        return headerView
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = self.dataArr[indexPath.row]
        if model.vc == MineViewController.self {
            isTeacher = false
            let tabar = BaseTabbarController()
            UIApplication.shared.keyWindow?.rootViewController = tabar
            return
        }
        if model.vc == TPlaybackOrCourseViewController.self{
            let vcc = TPlaybackOrCourseViewController()
            if indexPath.row == 0{
                
                vcc.isPlayback = true
            }else{
                
                vcc.isPlayback = false
            }
            self.navigationController?.pushViewController(vcc, animated: true)
            return
            
        }
        let vvc = self.dataArr[indexPath.row].vc.init()
        
        self.navigationController?.pushViewController(vvc, animated: true)
    }
    func userInfoChangeNotifcation(notification: Notification) {
        if notification.object == nil{
            
            self.loadData()
            
        } else {
            
            let personModel = notification.object as? PersonInfoModel
            self.headerView.hisInfo = personModel
            self.collectionView.reloadData()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.navigationController?.navigationBar.change(UIColor.white, with: scrollView, andValue: 200)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.automaticallyAdjustsScrollViewInsets = false
        self.loadData()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.automaticallyAdjustsScrollViewInsets = true
    }
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        let isShow = viewController.isKind(of: TMineViewController.self)
        self.navigationController?.setNavigationBarHidden(isShow, animated: true)
    }
        
}
class CustomCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let collectionView = self.collectionView
        let insets = collectionView?.contentInset
        let offset = collectionView?.contentOffset
        let minY = -((insets?.top)!)
        
        let attributesArray = super.layoutAttributesForElements(in: rect)
        if offset!.y < minY {
            let headerSize = self.headerReferenceSize
            let deltaY = CGFloat(fabsf(Float((offset?.y)! - CGFloat(minY))))
            for attrs: UICollectionViewLayoutAttributes in attributesArray! {
                if attrs.representedElementKind == UICollectionElementKindSectionHeader {
                    var headerRect = attrs.frame
                    headerRect.size.height = max(minY, headerSize.height + deltaY)
                    headerRect.origin.y = headerRect.origin.y - deltaY
                    attrs.frame = headerRect
                    break
                }
            }
        }
        return attributesArray
    }
}

