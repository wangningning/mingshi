
//
//  HisPersonalMemberFanCollectionViewCell.swift
//  DragonVein
//
//  Created by 梁毅 on 2017/4/5.
//  Copyright © 2017年 tts. All rights reserved.
//

import UIKit

class HisPersonalMemberFanCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var contentLabel: UILabel!
    
    @IBOutlet weak var markLab: UILabel!
    
    @IBOutlet weak var sexImg: UIImageView!
    @IBOutlet weak var gradeImg: UIImageView!
    
    var model: FollowListModel! {
        
        willSet(m) {
            avatarImageView.sd_setImage(with: URL(string: (m.img)!))
            nameLabel.text = m.username
            contentLabel.text = m.autograph
            if m.type == "2" {
                markLab.text = "评分："+m.mark!
                gradeImg.image = #imageLiteral(resourceName: "kazhb_V")
            }else {
                markLab.text = ""
                gradeImg.isHidden = true
            }
            if m.sex == "1"{
                sexImg.image = #imageLiteral(resourceName: "ssjg_man")
            } else if m.sex == "2"{
                sexImg.image = #imageLiteral(resourceName: "ssjg_woman")
            }else {
                sexImg.image = #imageLiteral(resourceName: "mshzhd_people")
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initViews()
    }
    
    func initViews() {
        avatarImageView.layer.cornerRadius = 51 / 2
        avatarImageView.layer.masksToBounds = true
        avatarImageView.clipsToBounds = true
        avatarImageView.contentMode = .scaleAspectFill
    }
    
    @IBAction func clickLikeButton(_ sender: UIButton) {
        
        focusOtherPerson(viewResponder: self, other_id: (model?.user_id)!, btn: sender, type: (model?.is_follow!)!) {
            if self.model?.is_follow == "1" {
                self.model?.is_follow = "2"
            } else {
                self.model?.is_follow = "1"
            }
        }
    }

}
