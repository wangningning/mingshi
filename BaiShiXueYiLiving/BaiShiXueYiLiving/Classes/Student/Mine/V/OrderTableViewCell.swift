//
//  OrderTableViewCell.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/17.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell {


    @IBOutlet weak var goodsImg: UIImageView!
    @IBOutlet weak var goodsName: UILabel!
    @IBOutlet weak var goodsType: UILabel!
    @IBOutlet weak var goodsPrice: UILabel!
    @IBOutlet weak var goodsNum: UILabel!
    
    
    
    var model: GoodsCartModel!{
        willSet(m){
           // goodsImg.kf.setImage(with: URL(string: m.img!)!)
            goodsImg.kf.setImage(with: URL(string: m.img!)!, placeholder: #imageLiteral(resourceName: "live_default"), options: nil, progressBlock: nil, completionHandler: nil)
            goodsName.text = m.name
            goodsNum.text = "x" + m.number!
            if m.kinds_detail?.count == 0{
                goodsType.text = "规格：无"
            }else{
                var kinds : NSString = ""
                for str in m.kinds_detail! {
                    kinds = kinds.appending(str.kind_detail! + ",") as NSString
                }
                goodsType.text = "规格: " + kinds.substring(to: kinds.length - 2)
            }
            
            goodsPrice.text = "￥ "+m.sale_price!
            
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.goodsImg.clipsToBounds = true
        self.goodsImg.contentMode = .scaleAspectFill
        // Initialization code
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
