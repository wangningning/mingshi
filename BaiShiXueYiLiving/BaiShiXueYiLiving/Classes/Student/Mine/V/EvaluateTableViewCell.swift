//
//  EvaluateTableViewCell.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/23.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class EvaluateTableViewCell: UITableViewCell,UITextViewDelegate{
    @IBOutlet weak var goodsImage: UIImageView!

    @IBOutlet weak var starView: UIView!
    
    @IBOutlet weak var starNumberLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var addPhotoView: UIView!
    
    @IBOutlet weak var placeHolderLab: UILabel!
    
    var addBtn:UIButton!
    var stars: TggStarEvaluationView!
    var photo: BBSAddPhotoView!
    var imagesData: [UIImage] = []
    
    private let butWidth:Float = 57
    
    let model = SubcommentModel()
    
    var submitModel: ((SubcommentModel)->())?
    var tmodel:GoodsCartModel!{
        willSet(m){
            
            model.goods_id = m.goods_id
            self.goodsImage.kf.setImage(with: URL(string:m.img!)!)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.textView.delegate = self
       
      

    }
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.characters.count == 0{
            self.placeHolderLab.isHidden = false
        }else{
            self.placeHolderLab.isHidden = true
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        self.model.content = textView.text
        self.model.goods_id = self.tmodel.goods_id
        self.submitModel?(model)

    }
    override func layoutSubviews() {
        super.layoutSubviews()
        stars = TggStarEvaluationView()
        stars.starCount = UInt(self.model.goods_mark)
        stars.frame = CGRect(x: 0, y: (60 - 45)/2, width:240, height: 45)
        stars.backgroundColor = UIColor.white
        stars.isTapEnabled = true
        stars.evaluateViewChooseStarBlock = { count in
            self.model.goods_mark = Int(count)
            self.model.goods_id = self.tmodel.goods_id
            self.submitModel?(self.model)
            self.starNumberLabel.text = "\(count)星"
        }
        self.starView.addSubview(stars)
        
        addBtn = UIButton(type: .custom)
        addBtn.setImage(#imageLiteral(resourceName: "tuikuang_shch"), for: .normal)
        addBtn.addTarget(self, action: #selector(addPhotoAction), for: .touchUpInside)
        addBtn.frame = CGRect(x: 16, y: 0, width: Int(butWidth), height: Int(butWidth))
        self.addPhotoView.addSubview(addBtn)
    
        
    }
    func addPhotoAction(){
        
        let action = ZLPhotoActionSheet()
        action.maxSelectCount = 3 - imagesData.count
        action.showPhotoLibrary(withSender: self.responderViewController()!, last: []) { (selectPhotos, selectPhotoModels) in
            self.imagesData = self.imagesData + selectPhotos
            if self.imagesData.count == 0{
                self.model.img = ""
                self.model.thumb = ""
                self.model.goods_id = self.tmodel.goods_id
                self.submitModel?(self.model)
                self.updateFrame()
            } else{
                NetworkingHandle.uploadOneMorePicture(url: "/Tools/upload_comment_picture", atVC: self.responderViewController()!, images: self.imagesData, uploadSuccess: { (response) in
                    let info = response["data"] as! [String:AnyObject]
                    let imgArr = info["img"] as! [String]
                    let thumb = info["thumb"] as! [String]
                    self.model.img = ""
                    self.model.thumb = ""
                    for str in imgArr{
                        self.model.img = self.model.img! + str + ","
                    }
                    for t in thumb{
                        self.model.thumb = self.model.thumb! + t + ","
                    }
                    let index = self.model.img.index(self.model.img.endIndex, offsetBy: -1)
                    let index2 = self.model.thumb?.index((self.model.thumb?.endIndex)!, offsetBy: -1)
                    self.model.img = self.model.img.substring(to: index)
                    self.model.thumb = self.model.thumb?.substring(to: index2!)
                    self.model.goods_id = self.tmodel.goods_id
                    self.submitModel?(self.model)
                    self.updateFrame()
                })
            }
            
        }
    }
    func updateFrame(){
        for v in self.addPhotoView.subviews {
            if ((v as? UIImageView) != nil) {
                v.removeFromSuperview()
            }
        }
        
        let butGap: Float = 5
        imagesData.enumerateKeysAndObjects { (obj, idx) in
            let columnIndex = idx % 4
            let rowIndex = idx / 4
            let imageView = UIImageView(frame: CGRect(x: CGFloat(columnIndex) * CGFloat(butWidth + butGap), y: CGFloat(rowIndex) * CGFloat(butWidth + butGap), width: CGFloat(butWidth), height: CGFloat(butWidth)))
            imageView.image = obj
            imageView.tag = idx
            imageView.isUserInteractionEnabled = true
            self.addPhotoView.addSubview(imageView)
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(clickImage(sender:)))
            imageView.addGestureRecognizer(tap)
        }
        if imagesData.count == 3 {
            addBtn.isHidden = true
        } else {
            addBtn.isHidden = false
        }
        let a = imagesData.count % 3
        let b = imagesData.count / 3
        if imagesData.count == 0 {
            
            addBtn.frame = CGRect(x: 16, y: 0, width: Int(butWidth), height: Int(butWidth))

        }else{
            
            addBtn.frame = CGRect(x: CGFloat(a) * CGFloat(butWidth + butGap), y: CGFloat(b) * CGFloat(butWidth + butGap), width: CGFloat(butWidth), height: CGFloat(butWidth))
        }
        
    }
    func clickImage(sender: UITapGestureRecognizer) {
        let v = sender.view
        imagesData.remove(at: (v?.tag)!)
        if imagesData.count == 0 {
            self.model.img = ""
            self.model.goods_id = self.tmodel.goods_id
            self.submitModel?(self.model)
            self.updateFrame()
            return
        } else{
            NetworkingHandle.uploadOneMorePicture(url: "/Tools/upload_comment_picture", atVC: self.responderViewController()!, images: self.imagesData, uploadSuccess: { (response) in
                let info = response["data"] as! [String:AnyObject]
                let imgArr = info["img"] as! [String]
                let thumb = info["thumb"] as! [String]
                self.model.img = ""
                self.model.thumb = ""
                
                for str in imgArr{
                    self.model.img = self.model.img! + str + ","
                    
                }
                for t in thumb{
                    self.model.thumb = self.model.thumb! + t + ","
                }
                let index = self.model.img.index(self.model.img.endIndex, offsetBy: -1)
                let index2 = self.model.thumb?.index((self.model.thumb?.endIndex)!, offsetBy: -1)
                self.model.img = self.model.img.substring(to: index)
                self.model.thumb = self.model.thumb?.substring(to: index2!)
                self.model.goods_id = self.tmodel.goods_id
                self.submitModel?(self.model)
                self.updateFrame()
            })
        }
        
       
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
class SubcommentModel: KeyValueModel {
    var goods_id : String!
    var goods_mark = 5
    var content: String!
    var img: String!
    var thumb: String?
    
}
