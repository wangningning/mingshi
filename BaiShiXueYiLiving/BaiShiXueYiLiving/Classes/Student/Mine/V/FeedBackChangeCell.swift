//
//  FeedBackChangeCell.swift
//  BaiShiXueYiLiving
//
//  Created by 初程程 on 2018/5/28.
//  Copyright © 2018年 liangyi. All rights reserved.
//

import UIKit

class FeedBackChangeCell: UITableViewCell {
    var contentLabel: UILabel!
    var dateLabel: UILabel!
    var model: FeedbackModel? {
        willSet(m) {
            contentLabel.text = m?.title
            dateLabel.text = m?.intime
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        createCustomView()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    func createCustomView(){
        contentLabel = UILabel()
        contentLabel.numberOfLines = 0
        contentLabel.textColor = UIColor(hexString: "#333333")
        contentLabel.font = UIFont.systemFont(ofSize: 13)
        self.contentView.addSubview(contentLabel)
        
        dateLabel = UILabel()
        dateLabel.textColor = UIColor(hexString: "#333333")
        dateLabel.font = UIFont.systemFont(ofSize: 13)
        dateLabel.text = "20123456"
        self.contentView.addSubview(dateLabel)
        
        contentLabel.snp.makeConstraints { (make) in
            make.left.equalTo(27)
            make.top.equalTo(15)
            //            make.bottom.equalTo(-62)
        }
        
        dateLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.contentLabel.snp.left)
            make.bottom.equalTo(-10)
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
