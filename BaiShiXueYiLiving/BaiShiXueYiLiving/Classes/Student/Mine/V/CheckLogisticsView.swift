//
//  CheckLogisticsView.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/22.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class CheckLogisticsView: UIView {

    @IBOutlet weak var state: UILabel!
    @IBOutlet weak var company: UILabel!
    @IBOutlet weak var number: UILabel!
    var kuadiName: String!{
        willSet{
            self.company.text = newValue
        }
    }
    var model:LogisticsModel!{
        willSet(m){
            if m.State == "3" {
                self.state.text = "已签收"
            }else{
                self.state.text = "配送中"
            }
           // self.company.text = m.EBusinessID
            self.number.text = m.LogisticCode
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        //self.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 150)

    }
    
    class func set()->CheckLogisticsView{
        let view = Bundle.main.loadNibNamed("CheckLogisticsView", owner: nil, options: nil)?.first as! CheckLogisticsView
        view.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 165)
        return view
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
