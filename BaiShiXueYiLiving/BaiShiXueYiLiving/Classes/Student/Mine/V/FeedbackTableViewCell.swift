//
//  FeedbackTableViewCell.swift
//  MoDuLiving
//
//  Created by 曾觉新 on 2017/3/8.
//  Copyright © 2017年 liangyi. All rights reserved.
// 

import UIKit

class FeedbackTableViewCell: UITableViewCell {
    
    var avatarImageView: UIImageView!
    var bubbleImageView: UIImageView!
    var contentLabel: UILabel!
    var dateLabel: UILabel!
    
    
    var model: FeedbackModel? {
        willSet(m) {
            contentLabel.text = m?.summary
            avatarImageView.image = #imageLiteral(resourceName: "logo")
            dateLabel.text = m?.intime
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initViews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func initViews() {
        self.backgroundColor = UIColor.clear
        
        avatarImageView = UIImageView()
        avatarImageView.layer.cornerRadius = 20
        avatarImageView.layer.masksToBounds = true
        avatarImageView.backgroundColor = UIColor.white
        avatarImageView.image = #imageLiteral(resourceName: "logo")
        self.contentView.addSubview(avatarImageView)
        
        
        bubbleImageView = UIImageView()
        bubbleImageView.image = UIImage(named: "Dialog box")?.stretchableImage(withLeftCapWidth: 35, topCapHeight: 33)
        self.contentView.addSubview(bubbleImageView)
        
        contentLabel = UILabel()
        contentLabel.numberOfLines = 0
        contentLabel.textColor = UIColor(hexString: "#333333")
        contentLabel.font = UIFont.systemFont(ofSize: 13)
        self.bubbleImageView.addSubview(contentLabel)
        
        dateLabel = UILabel()
        dateLabel.textColor = UIColor(hexString: "#333333")
        dateLabel.font = UIFont.systemFont(ofSize: 13)
        dateLabel.text = "20123456"
        self.bubbleImageView.addSubview(dateLabel)
        
        updateUI()
    }
    
    func updateUI() {
        
        avatarImageView.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 40, height: 40))
            make.top.equalTo(10);
            make.left.equalTo(15)
        }
        
        
        bubbleImageView.snp.makeConstraints { (make) in
            make.top.equalTo(self.avatarImageView.snp.top)
            make.left.equalTo(67)
            make.right.equalTo(-72)
            make.bottom.equalTo(-10)
        }
        
        contentLabel.snp.makeConstraints { (make) in
            make.left.equalTo(27)
            make.right.equalTo(-20)
            make.top.equalTo(15)
//            make.bottom.equalTo(-62)
        }
        
        dateLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.contentLabel.snp.left)
            make.bottom.equalTo(-10)
        }
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
