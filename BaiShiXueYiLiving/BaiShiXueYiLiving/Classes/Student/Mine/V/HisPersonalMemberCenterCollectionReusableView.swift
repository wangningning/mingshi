
//
//  HisPersonalMemberCenterCollectionReusableView.swift
//  DragonVein
//
//  Created by 梁毅 on 2017/3/28.
//  Copyright © 2017年 tts. All rights reserved.
//

import UIKit

class HisPersonalMemberCenterCollectionReusableView: UICollectionReusableView , UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet weak var backBtn: UIButton!

    @IBOutlet weak var focusNum: UILabel!
  
    @IBOutlet weak var RankCollectionViewWidth: NSLayoutConstraint!
    @IBOutlet weak var isLiveBtn: UIButton!
    @IBOutlet weak var rankView: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var fansLabel: UILabel!
    @IBOutlet weak var mapNum: UILabel!
    @IBOutlet weak var liveNum: UILabel!
    @IBOutlet weak var autographLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var moreBtn: UIButton!
 
    @IBOutlet weak var layout: UICollectionViewFlowLayout!
   
    
    @IBOutlet weak var RankCollectionView: UICollectionView!
    
    @IBOutlet weak var vipImg: UIImageView!
    
    
    @IBOutlet weak var sexImg: UIImageView!
    
    @IBOutlet weak var vipName: UILabel!
  
    @IBOutlet weak var markLab: UILabel!
    
    @IBOutlet weak var IDBtn: UIButton!
    
    @IBOutlet weak var liveDesLab: UILabel!
    
    @IBOutlet weak var videoDesLab: UILabel!
    
    @IBOutlet weak var fansDesLab: UILabel!
    
    @IBOutlet weak var focusDesLab: UILabel!
    
    @IBOutlet weak var liveBtn: UIButton!
    
    @IBOutlet weak var fansBtn: UIButton!
    
    @IBOutlet weak var attentionBtn: UIButton!
    
    @IBOutlet weak var videoBtn: UIButton!
    
    @IBOutlet weak var rankViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var rankViewMargin: NSLayoutConstraint!
    
    var rankArr: [ContributionModel] = []
    var liveModel = LiveList()
    var master_id: String?
    var clickedIndex: ((Int)->())?
    var vcPassed: UIViewController?

    var hisInfo : HisInfo! {
        willSet(m) {
            
            if m.type == "1"{
                
                liveDesLab.text = "关注"
                videoDesLab.text = "粉丝"
                fansDesLab.isHidden = true
                focusDesLab.isHidden = true
                fansBtn.isEnabled = false
                attentionBtn.isEnabled = false
                rankViewHeight.constant = 0
                rankView.isHidden = true
                rankViewMargin.constant = 0
            }
            
            if m.sex == "1" || m.sex == "0"{
                
                sexImg.image = #imageLiteral(resourceName: "ssjg_man")
            } else if m.sex == "2" {
                
                sexImg.image = #imageLiteral(resourceName: "ssjg_woman")
            }
            
            usernameLabel.text = m.username
            if m.type == "1"{
                liveNum.text = m.follow_count
                mapNum.text = m.fans_count
            }else{
                liveNum.text = m.live_count
                fansLabel.text = m.fans_count
                focusNum.text = m.follow_count
                mapNum.text = m.video_count
 
            }
            
            if m.type == "1"{
                IDBtn.isHidden = true
            }else{
                
                if m.id != nil {
                    
                    IDBtn.setTitle("ID：" + m.id!, for: .normal)
                    
                }
            }
            
            if  m.img != nil {
                img.kf.setImage(with: URL(string: m.img!))
            }
            if m.type == "1"{
                vipImg.isHidden = true
                vipName.isHidden = true
                
            }
            if m.type == "1"{
                
                markLab.isHidden = true
            } else{
                if m.mark == "" || m.mark == nil{
                    markLab.text = "暂无评分"
                } else {
                    markLab.text = "评分：" + m.mark!
                }
            }
            
            
            if m.autograph == nil || m.autograph == "" {
               if m.type == "2"{
                     autographLabel.text = "名师还未设置个性签名"
               }else{
                if m.sex == "1"{
                    autographLabel.text = "他还未设置个性签名"
                }else{
                    autographLabel.text = "她还未设置个性签名"
                }
            }
               
            } else {
                autographLabel.text = m.autograph
            }
            
            
            if m.type == "2" {
                if  m.is_live == "2" {
                    isLiveBtn.isHidden = true
                }
                else {
                    isLiveBtn.isHidden = false
                }
            }else{
                isLiveBtn.isHidden = true
            }
            if m.type == "1"{
                
                self.rankArr.removeAll()
                
            }else{
                
                self.rankArr = m.top!
                self.RankCollectionViewWidth.constant = CGFloat(self.rankArr.count * 35)
                self.RankCollectionView.reloadData()
            }
            
           
        }
    }
    @IBAction func moreBtnClicked(_ sender: Any) {
        let action = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let report = UIAlertAction(title: "举报", style: .default) { (alert) in
            NetworkingHandle.fetchNetworkData(url: "/Index/report_why", at: self, params: ["type": 2], success: { (result) in
                let data = result["data"] as! [[String: AnyObject]]
                let list = ReportModel.modelsWithArray(modelArray: data) as! [ReportModel]
                self.reportAlert(arr: list)
            })
        }
        var str = "拉黑"
        if self.hisInfo.shield == "2" {
            str = "取消拉黑"
        }
        let black = UIAlertAction(title: str, style: .default) { (alert) in
            NetworkingHandle.fetchNetworkData(url: "/Index/shield", at: self, params: ["user_id": self.hisInfo.user_id!], success: { (result) in
                let data = result["data"] as! String
                if data == "1"{
                    ProgressHUD.showMessage(message: "已经取消拉黑")
                    self.hisInfo.shield = "1"
                }else{
                    ProgressHUD.showSuccess(message: "拉黑成功")
                    self.hisInfo.shield = "2"
                }
                
            })
        }
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        action.addAction(report)
        action.addAction(black)
        action.addAction(cancel)
        self.responderViewController()?.present(action, animated: true, completion: {
            
        })
    }
    // 举报
    func reportAlert(arr: [ReportModel]) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        for model in arr {
            let alertAction = UIAlertAction(title: model.name!, style: .default) { (action) in
                print(model.name!)
                let param = ["user_id":self.hisInfo.user_id!,"why":model.name!]
                NetworkingHandle.fetchReNetworkData(url: "/Index/report", at: self, params: param,  success: { (response) in
                    ProgressHUD.showSuccess(message: "举报成功")
                }, failure: {
                    
                })
            }
            alert.addAction(alertAction)
        }
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        alert.addAction(cancel)
        self.responderViewController()?.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func copyBtnTitle(_ sender: UIButton) {
        if hisInfo.id != nil{
            UIPasteboard.general.string = hisInfo.id!
            ProgressHUD.showSuccess(message: "复制ID成功")
        }
    }
    @IBAction func backBtnClicked(_ sender: Any) {
        _ = self.responderViewController()?.navigationController?.popViewController(animated: true)
    }

    @IBAction func liveBtnClicked(_ sender: Any) {
        
        let vc = self.responderViewController() as! HisPersonalMemberCenterViewController
        if vc.isHasLive! {
            
            vc.navigationController?.popViewController(animated: true)
        } else {
//            if self.hisInfo.is_vip == "1"{
//                LyAlertView.alert(message: "您还不是该导师的会员，升级为会员后才可观看", ok: "升级", okBlock: {
//
//                    let vc = MembershipDetailViewController()
//                    vc.teacherID = self.hisInfo.id
//                    self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
//
//                })
//                return
//            }
            WatchLiveViewController.toWatch(from: vc, model: liveModel)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        layout.itemSize = CGSize(width: 30, height: 30)
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        layout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
        layout.scrollDirection = .horizontal
        RankCollectionView.delegate = self
        RankCollectionView.dataSource = self
        RankCollectionView.isScrollEnabled = false
        RankCollectionView.register(UINib(nibName: "AvatarCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "collectionCell")
   
        rankView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(rankViewClicked)))
        img.clipsToBounds = true
        img.contentMode = .scaleAspectFill

        img.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickAvater)))
    }
    func clickAvater() {
        if hisInfo != nil {
            let targetVC = PhotoBrowerViewController()
            if let m = img.image {
                targetVC.dataArr = [m]
            } else {
                targetVC.dataArr = [hisInfo.img as Any]
            }
           // targetVC.userID = hisInfo.user_id
            self.responderViewController()?.present(targetVC, animated: true, completion: nil)
        }
        
    }
    func rankViewClicked() {
        if self.hisInfo.type == "1" {
            return
        }else{
            if hisInfo.user_id != nil{
                let vc = ContributionListViewController()
                vc.user_id = hisInfo.user_id
                self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        
    }
    //MARK: UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.rankArr.count
       
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! AvatarCollectionViewCell
        cell.model = self.rankArr[indexPath.row]
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    

    @IBAction func jumpTo(_ sender: UIButton) {
        
    }

    @IBAction func BtnClicked(_ sender: UIButton) {
        if self.hisInfo.type == "1"{
            if sender.tag == 0 {
                self.clickedIndex?(2)
            }else if sender.tag == 3{
                self.clickedIndex?(1)
            }
        }else{
            self.clickedIndex?(sender.tag)
        }
        
    }
}
