//
//  SettingVCFooterView.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/27.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class SettingVCFooterView: UIView {

    @IBOutlet weak var cancelBtn: UIButton!
    var btnClicked: (() -> ())?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cancelBtn.layer.cornerRadius = 6
        self.cancelBtn.layer.masksToBounds = true
        
        
    }
    class func setFooter() -> SettingVCFooterView{
        
        let view = Bundle.main.loadNibNamed("SettingVCFooterView", owner: self, options: nil)?.first as! SettingVCFooterView
        return view
        
    }
    
    @IBAction func cancelLogin(_ sender: UIButton) {
        self.btnClicked?()
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
