//
//  MessageListTableViewCell.swift
//  Duluo
//
//  Created by sh-lx on 2017/4/14.
//  Copyright © 2017年 tts. All rights reserved.
//

import UIKit

class MessageListTableViewCell: UITableViewCell {

    
    @IBOutlet weak var badge: UILabel!
    @IBOutlet weak var BGView: UIView!
    
    @IBOutlet weak var userImg: UIImageView!
    
    @IBOutlet weak var usernameLab: UILabel!
    
    @IBOutlet weak var messageLab: UILabel!
    
    @IBOutlet weak var sexImg: UIImageView!
    
    @IBOutlet weak var gradeBtn: UIButton!
    
    @IBOutlet weak var timeLab: UILabel!
    @IBOutlet weak var gradeImg: UIImageView!
    
    
//    var privateModel: PrivateChatModel!{
//        willSet(m){
//            self.usernameLab.text = m.name
//            self.messageLab.text = EaseConvertToCommonEmoticonsHelper.convert(toSystemEmoticons: m.content)
//            if m.grade_img != "" {
//                gradeImg.kf.setImage(with: URL(string:m.grade_img))
//            }
//            if m.sex == "1" {
//                self.sexImg.image = UIImage(named:"nan")
//                
//            } else {
//                self.sexImg.image = UIImage(named:"nv")
//            }
//            self.userImg.kf.setImage(with: URL(string:m.img))
//            self.timeLab.text = m.time
//            self.gradeBtn.set(grade: m.grade)
//            
//            if m.unreadCount > 0 {
//                badge.isHidden = false
//                badge.text = "\(m.unreadCount)"
//                let size = badge.sizeThatFits(CGSize(width: 100, height: 16))
//                let s = max(size.height, size.width)
//                badge.layer.cornerRadius = s/2
//                badge.layer.masksToBounds = true
//            } else {
//                badge.isHidden = true
//            }
//        }
//    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.userImg.layer.cornerRadius = self.userImg.frame.size.width/2
        self.userImg.layer.masksToBounds = true
        self.userImg.isUserInteractionEnabled = true
        self.userImg.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(jumpToHisPersonVC)))
        
        self.backgroundColor = UIColor(hexString: "#edecf2")
        self.BGView.backgroundColor = UIColor.white
        self.BGView.layer.cornerRadius = 5
        self.BGView.layer.masksToBounds = true
    }

    func jumpToHisPersonVC() {
//        if self.privateModel != nil{
//            let vc = HisPersonalMemberCenterViewController()
//            vc.userId = self.privateModel.id
//            self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
//        }
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
