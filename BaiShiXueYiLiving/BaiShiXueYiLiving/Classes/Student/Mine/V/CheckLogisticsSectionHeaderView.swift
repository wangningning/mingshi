//
//  CheckLogisticsSectionHeaderView.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/7/28.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class CheckLogisticsSectionHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var state: UILabel!

    @IBOutlet weak var company: UILabel!
    
    @IBOutlet weak var number: UILabel!
   
    var kuaidiNode: String!{
        willSet{
            self.number.text = "运单编号：" + newValue
  
        }
    }
    var kuaidiName: String!{
        willSet{
            self.company.text = "快递公司：" + newValue
        }
    }
    var kuaidi_state: String!{
        willSet{
            switch newValue {
            case "1":
                self.state.text = "待发货"
            case "2":
                self.state.text = "已发货"
            case "3":
                self.state.text = "派送中"
            case "4":
                self.state.text = "已签收"
            default:
                self.state.text = ""
            }
        }
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
