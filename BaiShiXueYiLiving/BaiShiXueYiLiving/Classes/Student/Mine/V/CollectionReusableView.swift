//
//  CollectionReusableView.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class CollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var coverImg: UIImageView!

    @IBOutlet weak var sex: UIImageView!
    @IBOutlet weak var fansNum: UILabel!
    @IBOutlet weak var followOtherNum: UILabel!
    @IBOutlet weak var lessonNum: UILabel!
    @IBOutlet weak var phone: UILabel!
    
    @IBOutlet weak var badge1: UILabel!
    
    @IBOutlet weak var badge2: UILabel!
    
    @IBOutlet weak var badge3: UILabel!
    
    @IBOutlet weak var badge4: UILabel!
    
    @IBOutlet weak var vipImg: UIImageView!
    
    @IBOutlet weak var messageBtn: UIButton!
    var hasRead:String!{
        willSet(read){
            if read == "2" {
                self.messageBtn.isSelected = false
                self.messageBtn.setImage(#imageLiteral(resourceName: "wd_xx"), for: .highlighted)
            } else{
                self.messageBtn.isSelected = true
                self.messageBtn.setImage(#imageLiteral(resourceName: "ic_m"), for: .highlighted)
            }
        }
    }
    
    var model: PersonInfoModel!{
        willSet(m){
            
            self.confirmNewMessage()
            
            self.userName.text = m.username
            if m.sex == "1" || m.sex == "0" {
                self.sex.image = #imageLiteral(resourceName: "ssjg_man")
            }else if m.sex == "2" {
                self.sex.image = #imageLiteral(resourceName: "ssjg_woman")
            } 
            if m.type == "1"{
                vipImg.isHidden = true
            }else{
                vipImg.isHidden = false
            }
            self.userImg.kf.setImage(with: URL(string: m.img!)!)
            self.phone.text = m.phone
            self.lessonNum.text = m.video_count
            self.followOtherNum.text = m.follow_count
            self.fansNum.text = m.fans_count
            
            if Int(m.count1!)! > 99 {
                 m.count1 = "99+"
            }
            if Int(m.count1!)! == 0{
                self.badge1.isHidden = true
            }else{
                self.badge1.isHidden = false
            }
            let size1 = self.badge1.sizeThatFits(CGSize(width: 100, height: 16))
            let s1 = max(size1.height, size1.width)
            self.badge1.layer.cornerRadius = s1/2
            self.badge1.layer.masksToBounds = true
            self.badge1.text = m.count1
            
            if Int(m.count2!)! > 99 {
                m.count2 = "99+"
            }
            if Int(m.count2!)! == 0{
                self.badge2.isHidden = true
            }else{
                self.badge2.isHidden = false
            }
            let size2 = self.badge2.sizeThatFits(CGSize(width: 100, height: 16))
            let s2 = max(size2.height, size2.width)
            self.badge2.layer.cornerRadius = s2/2
            self.badge2.layer.masksToBounds = true
            self.badge2.text = m.count2
            
           if Int(m.count3!)! > 99 {
                m.count3 = "99+"
            }
            
            if Int(m.count3!)! == 0{
                self.badge3.isHidden = true
            }else{
                self.badge3.isHidden = false
            }
            
            let size3 = self.badge3.sizeThatFits(CGSize(width: 100, height: 16))
            let s3 = max(size3.height, size3.width)
            self.badge3.layer.cornerRadius = s3/2
            self.badge3.layer.masksToBounds = true
            self.badge3.text = m.count3
            
            
            let size4 = self.badge4.sizeThatFits(CGSize(width: 100, height: 16))
            let s4 = max(size4.height, size4.width)
            self.badge4.layer.cornerRadius = s4/2
            self.badge4.layer.masksToBounds = true
            self.badge4.isHidden = true

            let arr =  EMClient.shared().chatManager.getAllConversations() as! [EMConversation]
            
            for m in arr {
                if m.type == .init(0) {
                    let hx = UserDefaults.standard.value(forKey: "service") as! String
                    if m.conversationId == hx{
                        self.badge4.text = "\(m.unreadMessagesCount)"
                        break
                    }
                }
            }

        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backView.isHidden = true
        self.coverImg.contentMode = .scaleAspectFill
        self.coverImg.clipsToBounds = true
        self.coverImg.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(jumpToPersonInfo)))
        
        
        self.userImg.layer.cornerRadius = self.userImg.frame.size.width/2
        self.userImg.layer.masksToBounds = true
        self.userImg.contentMode = .scaleAspectFill
        self.userImg.clipsToBounds = true
        
        
               // Initialization code
    }
    func confirmNewMessage(){
        //私信 客服是否有新的消息
        let conversation = EMClient.shared().chatManager.getAllConversations() as! [EMConversation]
        for m in conversation {
            if m.type == EMConversationTypeChat, m.conversationId == UserDefaults.standard.value(forKey: "service") as! String, m.unreadMessagesCount != 0{
                serviceRD = false
            }
            if m.type == EMConversationTypeChat, m.conversationId != UserDefaults.standard.value(forKey: "service") as? String, m.unreadMessagesCount != 0{
                messageRD = false
            }
        }
        NetworkingHandle.fetchNetworkData(url: "/member/has_read_message", at: self, isShowHUD: false, success: { (response) in
            let data = response["data"] as! String
            if data == "1"{
                notificationRD = false
            }
            if serviceRD && messageRD && notificationRD{
               self.messageBtn.isSelected = false
                self.messageBtn.setImage(#imageLiteral(resourceName: "wd_xx"), for: .highlighted)
            } else{
                self.messageBtn.isSelected = true
                self.messageBtn.setImage(#imageLiteral(resourceName: "ic_m"), for: .highlighted)
            }
            
        })
        
        //
    }

    // MARK: 获取用户信息
    func fetchData() {
        NetworkingHandle.fetchNetworkData(url: "/Member/index", at: self, success: { (response) in
            let data = response["data"] as! [String: AnyObject]
            self.model = PersonInfoModel.modelWithDictionary(diction: data)
            
        }) {
            
        }
    }
    @IBAction func messageBtnAction(_ sender: UIButton) {
        self.responderViewController()?.navigationController?.pushViewController(MessageCenterViewController(), animated: true)
    }

    func jumpToPersonInfo(){
        let vc = PersonInfoEditViewController()
        vc.model = self.model
        vc.updateSuccess = { img in
            self.userImg.image = img
            self.fetchData()
        }
        self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func attentionLessonesAction(_ sender: UIButton) {
        self.responderViewController()?.navigationController?.pushViewController(FocusOnCourseViewController(), animated: true)
        print("关注课程")
    }
    
    @IBAction func followOtherPersonAction(_ sender: UIButton) {
        let vc = FocusOtherPersonViewController()
        vc.type = "关注的人"
        self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)

        print("关注的人")
    }
    
    @IBAction func fansAction(_ sender: UIButton) {
        let vc = FocusOtherPersonViewController()
        vc.type = "粉丝"
        self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
        print("粉丝")
    }
    
    @IBAction func prepareToPayAction(_ sender: UIButton) {
        print("待付款")
        let vc = OrderViewController()
        vc.type = 1
        self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func prepareToReceive(_ sender: UIButton) {
        print("待收货")
        let vc = OrderViewController()
        vc.type = 2
        self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func prepareToTalk(_ sender: UIButton) {
        let vc = OrderViewController()
        vc.type = 3
        self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
        print("待评价")
    }
    
    @IBAction func afterSale(_ sender: UIButton) {
        print("售后")
        
        ContactUsViewController.contactCustomerServiceVC(from: self.responderViewController()!)

    }
    
    @IBAction func orderFormAction(_ sender: UIButton) {
        print("我的订单")
        let vc = OrderViewController()
        vc.type = 0
        self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
    }
    
}
