//
//  MembershipCell.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/17.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class MembershipCell: UITableViewCell {

    @IBOutlet weak var memberName: UILabel!
    
    @IBOutlet weak var teacherName: UILabel!
    
    @IBOutlet weak var ID: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var memberBtn: UIButton!
    
    var model:MemberShipModel!{
        willSet(m){
            if m.state == "1"{
                self.memberName.text = "高级会员"
            }else{
                self.memberName.text = "钻石会员"
            }
            self.teacherName.text = m.username
            self.ID.text = m.id
            self.date.text = m.date_value
            if m.is_xu == "1"{
                self.date.text = m.date_value! + "(已过期)"
            }
            
            if m.is_xu == "2" {
                memberBtn.setImage(#imageLiteral(resourceName: "btn_shengji2"), for: .normal)
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func recharge(_ sender: UIButton) {

        if self.model.state == "2" && self.model.is_xu == "2"{
            ProgressHUD.showNoticeOnStatusBar(message: "您已经是最高等级")
            return
        }
        let vc = MembershipDetailViewController()
        vc.teacherID = self.model.user_id2!
        self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    /**
     时间戳转时间
     
     
     :param: timeStamp <#timeStamp description#>
     
     :returns: return time
     */
    func timeStampToString(timeStamp:String,format:String)->String {
        
        let string = NSString(string: timeStamp)
        
        let timeSta:TimeInterval = string.doubleValue
        let dfmatter = DateFormatter()
        dfmatter.timeStyle = .short
        dfmatter.dateStyle = .medium
        dfmatter.dateFormat = format
        
        let date = NSDate(timeIntervalSince1970: timeSta)
        
        print(dfmatter.string(from: date as Date))
        return dfmatter.string(from: date as Date)
    }

}
