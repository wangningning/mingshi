

//
//  HisPersonalMemberCenterCollectionViewCell.swift
//  DragonVein
//
//  Created by 梁毅 on 2017/3/28.
//  Copyright © 2017年 tts. All rights reserved.
//

import UIKit

class HisPersonalMemberCenterCollectionViewCell: UICollectionViewCell {
   
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    var myLiveList: MyLiveList! {
        willSet(m) {
            
            dateLabel.text = m.date_value
            if m.title == "" {
                titleLabel.text = "直播回放-\(m.live_id ?? "")"
            }else{
                titleLabel.text = m.title ?? "直播回放-\(m.live_id ?? "")"
            }
            
        }
    }
    var video: VideoModel!{
        willSet(m){
            dateLabel.text = timeStampToString(timeStamp:m.intime!,format:"yyyy-MM-dd")
            titleLabel.text = m.title
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
