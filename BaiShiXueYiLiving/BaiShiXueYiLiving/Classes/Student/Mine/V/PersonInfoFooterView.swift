//
//  PersonInfoFooterView.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/19.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class PersonInfoFooterView: UIView {

    @IBOutlet weak var confirmBtn: UIButton!
    
    @IBOutlet weak var autographView: UIView!
    
    @IBOutlet weak var autographText: UITextView!
    var confirmAction: (()->())?
    var model : PersonInfoModel!{
        willSet(m){
            if m.autograph == ""{
                self.autographText.text = "编辑您的个性签名"
            } else {
                self.autographText.text = m.autograph
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.confirmBtn.layer.cornerRadius = 6
        self.confirmBtn.layer.masksToBounds = true
        autographView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(PersonInfoFooterView.jumpToPersonInfoVC)))
    }
    func jumpToPersonInfoVC(){
        
        let vc = ChangeAutographViewController()
        vc.model = self.model
        vc.updateSuccess = { text in
            self.autographText.text = text
        }
        self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
    }
    class func setup() -> PersonInfoFooterView{
        let view = Bundle.main.loadNibNamed("PersonInfoFooterView", owner: nil, options: nil)!.first as! PersonInfoFooterView
        return view
        
    }
    
    @IBAction func confirmAction(_ sender: UIButton) {
        self.confirmAction?()
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
