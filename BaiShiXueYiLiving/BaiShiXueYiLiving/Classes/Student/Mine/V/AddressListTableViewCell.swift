
//
//  AddressListTableViewCell.swift
//  FKDCClient
//
//  Created by 梁毅 on 2017/2/10.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class AddressListTableViewCell: UITableViewCell {
    @IBOutlet weak var phoneLabel: UILabel!
    var str : String?

    @IBOutlet weak var deleteLabel: IconLabel!
    @IBOutlet weak var editlabel: IconLabel!
    @IBOutlet weak var defaultBtn: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var username: UILabel!
    var addressList : AddressList! {
        willSet(m) {
            phoneLabel.text = m.phone
            username.text = m.name
            addressLabel.text = m.province! + m.city!
            addressLabel.text = addressLabel.text! + m.area!
            addressLabel.text = addressLabel.text! + m.street!
            addressLabel.text = addressLabel.text! + m.address!
            if  m.is_default == "2" {
                defaultBtn.isSelected = true
                
            }
            else {
                defaultBtn.isSelected = false
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()

        editlabel.direction = IconDirection(rawValue: 0)!
        editlabel.set("编辑", with: UIImage(named: "bj"))
        editlabel.frame.origin = CGPoint(x: 0, y: 0)
        
        deleteLabel.direction = IconDirection(rawValue: 0)!
        deleteLabel.set("删除", with: UIImage(named: "sc-2"))
        deleteLabel.frame.origin = CGPoint(x: 0, y: 0)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
