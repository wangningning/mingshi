//
//  OrderDetailTableFooterView.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/6.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class OrderDetailTableFooterView: UIView {

    @IBOutlet weak var billName: UILabel!
    
    @IBOutlet weak var userMessageTF: UITextField!
    
    @IBOutlet weak var postage: UILabel!
    
    @IBOutlet weak var paid: UILabel!
    
    
    @IBOutlet weak var state: UILabel!
    
    @IBOutlet weak var company: UILabel!
    
    @IBOutlet weak var postNumber: UILabel!
    
    @IBOutlet weak var orderNumber: UILabel!
    
    @IBOutlet weak var rightBtn: UIButton!
    
    var kuaidi_state: String?
    var order_no: String?
    var amount: String?
    var kuaidi: String?
    var kuaidi_node: String?
    var kuaidiName: String?
    var goodsArr: [GoodsCartModel] = []
    var order_id: String?
    var sstate: String!{
        willSet{
//            if newValue == "1" {
//                self.rightBtn.isSelected = false
//
//            }else{
//                self.rightBtn.isSelected = true
//
//            }
        }
    }

    class func setup() -> OrderDetailTableFooterView{
        return Bundle.main.loadNibNamed("OrderDetailTableFooterView", owner: nil, options: nil)?.first as! OrderDetailTableFooterView
    }
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    @IBAction func rightButtonAction(_ sender: UIButton) {
        if self.sstate == "1" {
            print("付款")
            let vc = SelectPayWayViewController()
            if self.orderNumber.text != nil{
                vc.order_no = self.order_no
            }
            if  self.paid.text != nil {
                var str : NSString = ""
                str = self.amount! as NSString
                vc.amount = str.substring(from: 1)
            }
            vc.payType = "1"
            self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
        }else if self.sstate == "4"{
            let vc = EvaluateGoodsViewController()
            vc.order_id = order_id
            vc.goodsArr = goodsArr;
        self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            if self.sstate == "2" {
                ProgressHUD.showMessage(message: "请等待商家发货")
                return
            }
            let vc =  CheckLogisticsViewController()
            vc.kuaidi = self.kuaidi
            vc.kuaidiName = self.kuaidiName
            vc.kuaidi_node = self.kuaidi_node
            vc.kuaidistate = self.kuaidi_state
            vc.order_number = self.order_no
            self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
