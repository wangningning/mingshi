//
//  OrderFormSectionHeaderView.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/4.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class OrderFormSectionHeaderView: UITableViewHeaderFooterView {


   
    
    @IBOutlet weak var orderno: UILabel!
    @IBOutlet weak var orderState: UILabel!
    var model : OrderListModel!{
        willSet(m){
            orderno.text = "订单号：" + m.order_no!
            if  m.state == "1" {
                orderState.text = "等待付款"
            }
            else if  m.state == "2" {
                orderState.text = "等待发货"
            }
            else if  m.state == "3" {
                orderState.text = "等待收货"
            }else if  m.state == "4" {
                orderState.text = "等待评价"
            }
            else if  m.state == "5" {
                orderState.text = "交易成功"
            }
            else {
                orderState.text = "已取消"
            }

        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
       // self.contentView.backgroundColor = UIColor.white
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
