//
//  OrderFormSectionFooterView.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/4.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class OrderFormSectionFooterView: UITableViewHeaderFooterView {

    @IBOutlet weak var postage: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var rightBtn: UIButton!
    @IBOutlet weak var middleBtn: UIButton!
    
    @IBOutlet weak var leftBtn: UIButton!
    var cancelOrderForm : ((OrderListModel)->())?
    var deleteOrderForm : ((OrderListModel)->())?
    var confirmReceiveGoods : ((OrderListModel)->())?
    var model : OrderListModel!{
        willSet(m){
            if m.has_postage == "1"{
                postage.text = "免运费"
            }else{
                postage.text = "含运费\(m.postage!)元"
            }
            price.text = "￥ " + m.paid!
            total.text = "共计\(m.item!)件商品"
            //1待支付；2待发货；3待收货；4待评价；5可删除订单
            if  m.state == "1" {
                rightBtn.setImage(#imageLiteral(resourceName: "btn_fukuan2"), for:.normal)
                middleBtn.setImage(#imageLiteral(resourceName: "btn_quxiao"), for: .normal)
                leftBtn.setImage(#imageLiteral(resourceName: "btn_kefu"), for: .normal)
                leftBtn.isHidden = false
            }
            else if  m.state == "2" {
                
                rightBtn.setImage(#imageLiteral(resourceName: "btn_shouhuo"), for: .normal)
                middleBtn.setImage(#imageLiteral(resourceName: "btn_kefu"), for: .normal)
               // leftBtn.setImage(#imageLiteral(resourceName: "btn_kefu"), for: .normal)
                leftBtn.isHidden = true
            }
            else if  m.state == "3" {
                rightBtn.setImage(#imageLiteral(resourceName: "btn_shouhuo"), for: .normal)
                middleBtn.setImage(#imageLiteral(resourceName: "btn_wuliu"), for: .normal)
                leftBtn.setImage(#imageLiteral(resourceName: "btn_kefu"), for: .normal)
                leftBtn.isHidden = false
                
            }
            else if m.state == "4" {
                rightBtn.setImage(#imageLiteral(resourceName: "btn_pingjia2"), for: .normal)
                middleBtn.setImage(#imageLiteral(resourceName: "btn_wuliu"), for: .normal)
                leftBtn.setImage(#imageLiteral(resourceName: "btn_kefu"), for: .normal)
                leftBtn.isHidden = false
                
            } else if m.state == "5" {
                rightBtn.setImage(#imageLiteral(resourceName: "btn_shanchu2"), for: .normal)
                middleBtn.setImage(#imageLiteral(resourceName: "btn_kefu"), for: .normal)
                leftBtn.isHidden = true
            }else{
                leftBtn.isHidden = true
                rightBtn.isHidden = true
                middleBtn.isHidden = true
            }

            
        }
    }
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBAction func rightAction(_ sender: UIButton) {
        
        if model.state == "3" || model.state == "2"{
            if model.state == "2"{
                
                ProgressHUD.showMessage(message: "请等待商家发货")
                return
            }
            NetworkingHandle.fetchNetworkData(url: "/mall/receive_order", at: self, params: ["order_no":model.order_no!], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
                self.confirmReceiveGoods?(self.model)
                ProgressHUD.showSuccess(message: "已确认")
            }, failure: { 
                
            })
        } else if model.state == "1" {
            let vc = SelectPayWayViewController()
            vc.payType = "1"
            vc.amount = model.paid
            vc.order_no = model.order_no
            self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
        } else if model.state == "4" {
            if model.order_detail?.count == 0{
                ProgressHUD.showMessage(message: "商品列表为空")
                return
            }
            
           let vc = EvaluateGoodsViewController()
            vc.order_id = model.order_no
            vc.goodsArr = model.order_detail!
           self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
        } else if model.state == "5" {
          
            print("******" + self.model.order_no!)
            NetworkingHandle.fetchNetworkData(url: "/mall/del_order", at: self, params: ["order_no": self.model.order_no!], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
                self.deleteOrderForm?(self.model)

            }, failure: { 
                
            })

           
        }
    }

    @IBAction func middleAction(_ sender: UIButton) {
        
        if model.state == "1" {
            NetworkingHandle.fetchNetworkData(url: "/mall/cancel_order", at: self, params: ["order_no":model.order_no!], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
                self.model.isCancel = true
                self.cancelOrderForm?(self.model)
            }, failure: { 
                
            })
            
        } else if model.state == "2" || model.state == "3" || model.state == "4"{
            if model.state == "2" {
                ContactUsViewController.contactCustomerServiceVC(from: self.responderViewController()!)
                return
            }
            let vc = CheckLogisticsViewController()
            vc.kuaidi = model.kuaidi
            vc.kuaidi_node = model.kuaidi_node
            vc.kuaidiName = model.kuaidi_name
            vc.kuaidistate = model.kuaidi_state
            vc.order_number = model.order_no
            self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
        } else if model.state == "5" {
            print("客服")
            ContactUsViewController.contactCustomerServiceVC(from: self.responderViewController()!)

        }
    }
    
    @IBAction func leftACtion(_ sender: UIButton) {
        ContactUsViewController.contactCustomerServiceVC(from: self.responderViewController()!)
        print("客服")
    }
    
    
}
