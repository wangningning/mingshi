//
//  PurchaseView.swift
//  FKDCClient
//
//  Created by 梁毅 on 2017/2/9.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
typealias blockActionSheet = (_ index: Int)->()

class PurchaseView: UIView {
    
    
    var block: blockActionSheet?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(handleGesture))
        self.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(quitBtnClicked), name: NSNotification.Name(rawValue: "quitBtnClicked"), object: nil)
        
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "quitBtnClicked"), object: nil)
    }
    func quitBtnClicked() {
        self.cancel()
    }
    func handleGesture(tap: UITapGestureRecognizer) {
        
        if tap.location(in: tap.view).y < self.frame.size.height - self.contentView.frame.size.height {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "handleGesture"), object: nil)
            
            self.cancel()
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //type: 1购物车 2立即抢购
    class func showWithTitle(kindOfShop:[kinds_detailModel],sale_price:String,type:String, goodsName:String, goodsID: String, vc: UIViewController){
        
        UIApplication.shared.keyWindow?.endEditing(true)
        
        let sheet = PurchaseView()
        let window : UIWindow = ((UIApplication.shared.delegate?.window)!)!
        
        sheet.frame = window.bounds
        if (vc.navigationController?.navigationBar.isHidden)! {
            sheet.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
        } else {
            sheet.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight - 64)
        }
        
        sheet.show(kindOfShop: kindOfShop,sale_price: sale_price, type:type, goodsName: goodsName, goodsID: goodsID)
        vc.view.addSubview(sheet)
    }
    func show(kindOfShop:[kinds_detailModel],sale_price:String,type:String, goodsName:String, goodsID: String) {
        
        self.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.2)
        contentView.frame = CGRect(x: 0, y: self.frame.size.height * 2/5, width: kScreenWidth, height: self.frame.size.height * 3/5)
        contentView.kindOfShops = kindOfShop
        contentView.sale_price = sale_price
        contentView.type = type
        contentView.goodsName.text = goodsName
        contentView.goods_id = goodsID
        self.addSubview(contentView)

        
        
        
        
        let frame = self.contentView.frame
        var newframe = frame
        self.alpha = 0.1
        newframe.origin.y = self.frame.size.height
        contentView.frame = newframe
        contentView.collectionview.register(UINib(nibName: "PurchaseColorCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PurchaseColorCollectionViewCell")
        
        if type == "3" {
            
            contentView.finishButton.setTitle("立即购买", for: .normal)
        }
        
        
        UIView.animate(withDuration: 0.25, animations: {
            self.contentView.frame = frame
            self.alpha = 1
        }) { (finished) in
            
        }
        
    }
    
    fileprivate lazy var contentView: PurchaseContentView = {
        let toolBarView = Bundle.main.loadNibNamed("PurchaseContentView", owner: nil
            , options: nil)!.last as! PurchaseContentView
        return toolBarView
    }()
    func cancel() {
        var frame = self.contentView.frame
        frame.origin.y += frame.size.height
        UIView.animate(withDuration: 0.25, animations: {
            self.contentView.frame = frame
            self.alpha = 0.1
        }) { (finished) in
            self.removeFromSuperview()
        }
    }
    
    
    
    
}
extension UIView {
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

class PurchaseContentView: UIView {
    var kindOfShops = [kinds_detailModel]()
    var sale_price: String?
    var type : String?
    var lastKindOne: kindsDetailModel?
    var lastKindTwo: kindsDetailModel?
    var goods_id: String!
    var sectionHeader: PurchaseNumberCollectionViewReusableView?
    
    @IBOutlet weak var layout: UICollectionViewFlowLayout!
    @IBOutlet weak var collectionview: UICollectionView!
    
    
    @IBOutlet weak var goodsName: UILabel!
    
    @IBOutlet weak var finishButton: UIButton!
    
    @IBAction func quitBtnClicked(_ sender: Any) {
        
        
        lastKindOne?.is_Seleted = false
        lastKindTwo?.is_Seleted = false
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "quitBtnClicked"), object: nil)
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let w = (kScreenWidth - 15 * 5)/4
        layout.itemSize = CGSize(width: w, height: w * 23/76)
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 15
        layout.sectionInset = UIEdgeInsetsMake(5, 15, 0, 15)
        
        collectionview.delegate = self
        collectionview.dataSource = self
        
        collectionview.register(UINib(nibName: "PurchaseColorCollectionReusableView", bundle: Bundle.main), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "PurchaseColorCollectionReusableView")
        collectionview.register(UINib(nibName: "PurchaseColorCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PurchaseColorCollectionViewCell")
        
        collectionview.register(UINib(nibName: "PurchaseNumberCollectionViewReusableView", bundle: Bundle.main), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "PurchaseNumberCollectionViewReusableView")
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleGesture), name: NSNotification.Name(rawValue: "handleGesture"), object: nil)
    }
    func handleGesture() {
        lastKindOne?.is_Seleted = false
        lastKindTwo?.is_Seleted = false
    }
  
    
    @IBAction func isSelectedBtnClicked(_ sender: Any) {
        if type != "3" {
            purchase(type: type!)
        } else {
            purchase(type: "3")
        }
    }
    func purchase(type: String) {
        if kindOfShops.count == 1, kindOfShops[0].kind_detail?.count == 0{
            self.gotoBuy(type: type)
            return
        }
//        (kindOfShops.count == 1 && lastKindOne != nil && lastKindOne!.is_Seleted) || (kindOfShops.count == 2 && lastKindOne != nil && lastKindOne!.is_Seleted && lastKindTwo != nil && lastKindTwo!.is_Seleted)
        if  self.checkKinds() {
            var stock = ""
            var param = ["goods_id":lastKindOne!.goods_id!,"kinds_id":lastKindOne!.kind_id!]
            if lastKindTwo?.kind_id ?? "" != "" {
                param["kinds_id"] = (lastKindOne?.kind_id)! + "," + lastKindTwo!.kind_id!
            }
            NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Mall&a=check_stock", at: self, params: param, isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
                let data = response["data"] as! [String: AnyObject]
                stock = data["stock"] as! String
                if stock == "0" {
                    
                    ProgressHUD.showNoticeOnStatusBar(message: "库存不足")
                    return
                }
                else {
                    self.gotoBuy(type: type)
                }

                
            }, failure: {
                
            })
        } else  {
            ProgressHUD.showNoticeOnStatusBar(message: "请选择规格")
        }
    }
    func checkKinds() -> Bool {
        if kindOfShops.count == 1 {
            if kindOfShops[0].kind_detail?.count == 0 {
                return true
            }
            if (lastKindOne?.is_Seleted)! {
                return true
            }
        }
        if kindOfShops.count == 2 {
            if kindOfShops[0].kind_detail?.count == 0 {
                lastKindOne = kindsDetailModel()
                lastKindOne?.is_Seleted = true
            }
            if kindOfShops[1].kind_detail?.count == 0 {
                lastKindTwo = kindsDetailModel()
                lastKindTwo?.is_Seleted = true
            }
            if lastKindOne == nil || lastKindTwo == nil {
                return false
            }
            return lastKindOne!.is_Seleted && lastKindTwo!.is_Seleted
        }
        return false
    }
    func gotoBuy(type: String) {
        
        //无规格商品
        if kindOfShops.count == 1, kindOfShops[0].kind_detail?.count == 0{
            
            
            let param = ["goods_id":self.goods_id!,"number":sectionHeader?.number.text ?? "1","kinds_id": ""]
            
            if  type == "1"{
                NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Mall&a=to_goods_cart", at: self, params: param as [String:AnyObject], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
                    ProgressHUD.showNoticeOnStatusBar(message: "加入购物车成功")
                    self.lastKindOne?.is_Seleted = false
                    self.lastKindTwo?.is_Seleted = false
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "quitBtnClicked"), object: nil)
                    
                }, failure: {
                    
                })
                return
            }else {
                NetworkingHandle.fetchNetworkData(url: "/mall/set_goods_order", at: self, params: param as [String : AnyObject], isAuthHide: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
                    let vc = ConfirmOrderViewController()
                    if  type == "2" {
                        vc.type = "2"
                        
                    } else {
                        vc.type = "3"
                        
                    }
                self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
                    
                }) { (erroCode) in
                    
                }
            }

        }
        //有规格商品
        if (kindOfShops.count == 1 && lastKindOne != nil && lastKindOne!.is_Seleted) || (kindOfShops.count == 2 && lastKindOne != nil && lastKindOne!.is_Seleted && lastKindTwo != nil && lastKindTwo!.is_Seleted) {
            
            var parma = ["goods_id":lastKindOne!.goods_id!,"number":sectionHeader?.number.text ?? "1","kinds_id": lastKindOne!.kind_id!]
            if lastKindTwo != nil &&  lastKindTwo?.kind_id != nil{
                parma["kinds_id"] = lastKindOne!.kind_id! + "," + lastKindTwo!.kind_id!
            }
            
            if  type == "1"{
                NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Mall&a=to_goods_cart", at: self, params: parma as [String:AnyObject], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
                    ProgressHUD.showNoticeOnStatusBar(message: "加入购物车成功")
                    self.lastKindOne?.is_Seleted = false
                    self.lastKindTwo?.is_Seleted = false
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "quitBtnClicked"), object: nil)
                    
                }, failure: {
                    
                })
            }
            else {
                NetworkingHandle.fetchNetworkData(url: "/mall/set_goods_order", at: self, params: parma as [String : AnyObject], isAuthHide: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
                    let vc = ConfirmOrderViewController()
                    if  type == "2" {
                        vc.type = "2"
                      
                    } else {
                        vc.type = "3"
                      
                    }
                self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
                    
                }) { (erroCode) in
                    
                }
            }
        }
    }
    
}
extension PurchaseContentView: UICollectionViewDataSource,UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout,PurchaseColorCollectionViewCellDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return kindOfShops.count + 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == kindOfShops.count {
            return 0
        }
        
        return (kindOfShops[section].kind_detail?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PurchaseColorCollectionViewCell", for: indexPath) as! PurchaseColorCollectionViewCell
        cell.kindDetialOFShop = kindOfShops[indexPath.section].kind_detail?[indexPath.row]
        cell.indexPath = indexPath
        cell.delegate = self
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0 {
            return CGSize(width: kScreenWidth, height: 30)
        }
        return CGSize(width: kScreenWidth, height: 60)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if indexPath.section == kindOfShops.count {
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "PurchaseNumberCollectionViewReusableView", for: indexPath) as! PurchaseNumberCollectionViewReusableView
            
            //一种规格
            if kindOfShops.count == 1, lastKindOne != nil, lastKindOne!.is_Seleted {
                
                headerView.sPrice = Double(lastKindOne!.sale_price!)! + Double(sale_price!)!
                let price = headerView.sPrice * Double(headerView.number.text!)!
                headerView.price.text = "￥ \(String(format: "%.2f", price))"
                headerView.sPrice = price
            //两种规格
            } else if kindOfShops.count == 2, lastKindOne != nil, lastKindTwo != nil, lastKindOne!.is_Seleted, lastKindTwo!.is_Seleted {
                
                headerView.sPrice = Double(lastKindOne!.sale_price ?? "0")! + Double(lastKindTwo!.sale_price ?? "0")! + Double(sale_price ?? "0")!
                let price = headerView.sPrice * Double(headerView.number.text!)!
                headerView.price.text = "￥ \(String(format: "%.2f", price))"
            //无规格
            } else {
               
                headerView.sPrice = Double(self.sale_price!)!
                let price = headerView.sPrice * Double(headerView.number.text!)!
                headerView.price.text = "￥ \(String(format: "%.2f", price))"

            }
            
            headerView.addBtn.addTarget(self, action: #selector(addButtonAction(_:)), for: .touchUpInside)
            headerView.subBtn.addTarget(self, action: #selector(subBtnAction(_:)), for: .touchUpInside)
            sectionHeader = headerView
            return headerView
        }
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "PurchaseColorCollectionReusableView", for: indexPath) as! PurchaseColorCollectionReusableView
        headerView.label.text = kindOfShops[indexPath.section].kind
        if indexPath.section == 0 {
            headerView.line.isHidden = true
        } else {
            headerView.line.isHidden = false
        }
        return headerView
    }
    func subBtnAction(_ sender: Any) {
        if kindOfShops.count == 1, kindOfShops[0].kind_detail?.count == 0{
            
            if  1 < Int((self.sectionHeader?.number.text!)!)! {
                let ii = Int((self.sectionHeader?.number.text!)!)! - 1
                self.sectionHeader?.number.text = String(ii)
                self.collectionview.reloadData()
            }
            else {
                
                ProgressHUD.showNoticeOnStatusBar(message: "商品数量不能少于1")
                
            }
            return

        }
        if (kindOfShops.count == 1 && lastKindOne != nil && lastKindOne!.is_Seleted) || (kindOfShops.count == 2 && lastKindOne != nil && lastKindOne!.is_Seleted && lastKindTwo != nil && lastKindTwo!.is_Seleted) {
            
            if  1 < Int((self.sectionHeader?.number.text!)!)! {
                let ii = Int((self.sectionHeader?.number.text!)!)! - 1
                self.sectionHeader?.number.text = String(ii)
                self.collectionview.reloadData()
            }
            else {
                
                ProgressHUD.showNoticeOnStatusBar(message: "商品数量不能少于1")
                
            }
            
        }else{
            ProgressHUD.showNoticeOnStatusBar(message: "请选择规格")
            
        }

    }
    func addButtonAction(_ sender: Any) {
        
        if kindOfShops.count == 1, kindOfShops[0].kind_detail?.count == 0{
            let ii = Int((self.sectionHeader?.number.text!)!)! + 1
            self.sectionHeader?.number.text = String(ii)
            self.collectionview.reloadData()
            return

        }
//        (kindOfShops.count == 1 && lastKindOne != nil && lastKindOne!.is_Seleted) || (kindOfShops.count == 2 && lastKindOne != nil && lastKindOne!.is_Seleted && lastKindTwo != nil && lastKindTwo!.is_Seleted)
        if  self.checkKinds(){
            var stock = ""
            var param = ["goods_id":lastKindOne!.goods_id!,"kinds_id":lastKindOne!.kind_id!]
            
            if lastKindTwo?.kind_id ?? "" != "" {
                param["kinds_id"] = lastKindOne!.kind_id! + "," + lastKindTwo!.kind_id!
            }
            NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Mall&a=check_stock", at: self, params: param, isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
                let data = response["data"] as! [String: AnyObject]
                stock = data["stock"] as! String
                if  Int(stock)! > Int((self.sectionHeader?.number.text!)!)! {
                    let ii = Int((self.sectionHeader?.number.text!)!)! + 1
                    self.sectionHeader?.number.text = String(ii)
                    self.collectionview.reloadData()
                }
                else {
                    ProgressHUD.showMessage(message: "库存不足")
                }

                
            }, failure: {
                
            })
        }else{
            ProgressHUD.showNoticeOnStatusBar(message: "请选择规格")

        }
        
        
        
        
    }
    
    
    // cell 代理
    func didSelect(model: kindsDetailModel, indexPath: IndexPath) {
        if indexPath.section == 0 {
            if lastKindOne != model {
                model.is_Seleted = true
                lastKindOne?.is_Seleted = false
            } else {
                model.is_Seleted = !model.is_Seleted
            }
            lastKindOne = model
        } else {
            if lastKindTwo != model {
                model.is_Seleted = true
                lastKindTwo?.is_Seleted = false
            } else {
                model.is_Seleted = !model.is_Seleted
            }
            lastKindTwo = model
        }
        self.collectionview.reloadData()
    }
    
}

