//
//  MessageCenterCell.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class MessageCenterCell: UITableViewCell {

    @IBOutlet weak var headImg: UIImageView!
    
    @IBOutlet weak var title: UILabel!
   
    @IBOutlet weak var content: UILabel!
    
    @IBOutlet weak var badge: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.badge.layer.cornerRadius = self.badge.frame.size.width/2
        self.badge.layer.masksToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
