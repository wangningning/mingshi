
//
//  PhotoCollectionViewCell.swift
//  Duluo
//
//  Created by 梁毅 on 2017/4/6.
//  Copyright © 2017年 tts. All rights reserved.
//

import UIKit

protocol PhotoCellDelegate: NSObjectProtocol{
    func photoCell(_ cell: PhotoCollectionViewCell, model: PhotoList)
}
class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var btn: UIButton!
    
    weak var delegate: PhotoCellDelegate?
    
    var isEditing = false
    
    var model: PhotoList! {
        willSet(m) {
            
            btn.isHidden = !isEditing
            btn.isSelected = m.isSelected
            
            guard let url = m.imgs else {
                return
            }
            img.kf.setImage(with: URL(string: url))
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        img.contentMode = .scaleAspectFill
        img.clipsToBounds = true
    }
    
    @IBAction func selectButtonAction(_ sender: UIButton) {
        model.isSelected = !model.isSelected
        sender.isSelected = model.isSelected
        delegate?.photoCell(self, model: model)
    }
}
