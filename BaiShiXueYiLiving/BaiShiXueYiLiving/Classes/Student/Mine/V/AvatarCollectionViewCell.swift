//
//  AvatarCollectionViewCell.swift
//  MoDuLiving
//
//  Created by 曾觉新 on 2017/3/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class AvatarCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var vipImg: UIImageView!
    
    var model: ContributionModel! {
        didSet {
            avatarImageView.kf.setImage(with: URL(string: (model?.img!)!))
            if model.type == "1"{
                vipImg.isHidden = true
            }else{
                vipImg.isHidden = true
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        avatarImageView.layer.cornerRadius = 15
        avatarImageView.layer.masksToBounds = true
        avatarImageView.clipsToBounds = true
        avatarImageView.contentMode = .scaleAspectFill
    }

}
