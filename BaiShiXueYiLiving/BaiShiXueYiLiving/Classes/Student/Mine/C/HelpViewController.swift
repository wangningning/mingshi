//
//  HelpViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/18.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class HelpViewController: BSBaseViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "帮助"
        self.tableview.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableview.tableFooterView = UIView()
        //self.tableview.separatorStyle = .none
        // Do any additional setup after loading the view.
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "cell")!
        
        cell.accessoryType = .disclosureIndicator
        cell.selectionStyle = .none
        if indexPath.row == 0 {
             cell.textLabel?.text = "使用帮助"
        } else if indexPath.row == 1{
            cell.textLabel?.text = "关于名师传艺声明"
        } else {
            cell.textLabel?.text = "APP介绍"
        }
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14.0)
        cell.textLabel?.textColor = UIColor(hexString:"333333")
        
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var param = ""
        let vc = MDWebViewController()
        if indexPath.row == 0 {
            vc.title = "使用帮助"
            param = "6"
        } else if indexPath.row == 1{
            vc.title = "名师传艺声明"
            param = "7"
        } else {
            vc.title = "APP介绍"
            param = "8"
        }
        vc.url = NetworkingHandle.mainHost + "/Home/xieyi/id/" + param
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
