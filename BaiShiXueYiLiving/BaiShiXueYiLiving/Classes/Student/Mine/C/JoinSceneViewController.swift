//
//  JoinSceneViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/11.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh
class JoinSceneViewController:  BSBaseViewController,UITableViewDelegate,UITableViewDataSource {

    var dataArr = [TScenceModel]()
    var page = 1
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "参与现场"
        tableView.register(UINib(nibName: "TSceneTableViewCell", bundle: nil), forCellReuseIdentifier: "TSceneTableViewCell")
        tableView.tableFooterView = UIView()
        tableView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.page = 1
            self.loadData(page:self.page)
        })
        tableView.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: { [unowned self] in
            self.page += 1
            self.loadData(page:self.page)
        })
        tableView.mj_header.beginRefreshing()
        // Do any additional setup after loading the view.
    }
    func loadData(page:Int) {
        
        NetworkingHandle.fetchNetworkData(url: "/Member/tutor_class_list", at: self, params: ["p":page], hasHeaderRefresh: tableView, success: { (response) in
            let data = response["data"] as! [String: AnyObject]
            let list = data["list"] as! [[String: AnyObject]]
            let ll = TScenceModel.modelsWithArray(modelArray: list) as! [TScenceModel]
            if self.page == 1 {
                self.dataArr.removeAll()
            }
            if ll.count < 10 {
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
            }
            self.dataArr += ll
            self.tableView.reloadData()
        }) {
            if self.page > 1 {
                self.page -= 1
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kScreenWidth * 180 / 375
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0.1
        }else{
           return 10
        }
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TSceneTableViewCell", for: indexPath) as! TSceneTableViewCell
        cell.model = dataArr[indexPath.section]
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataArr.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = TSceneDetailViewController()
        vc.id = self.dataArr[indexPath.section].id
        vc.isFromMineVC = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
