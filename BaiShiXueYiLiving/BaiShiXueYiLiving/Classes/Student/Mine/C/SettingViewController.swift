//
//  SettingViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/18.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import Kingfisher

class SettingViewController: BSBaseViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tableview: UITableView!
    var footerBtn = UIButton(type:.custom)
    @IBOutlet weak var cancelLoginBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "设置"
        self.tableview.register(UITableViewCell.self, forCellReuseIdentifier: "SetCell")
      // tableview.tableFooterView = UIView()
        
        let view = SettingVCFooterView.setFooter()
        self.tableview.tableFooterView = view
        view.btnClicked = {
            let alertController = UIAlertController(title: "提示", message: "是否确定退出登录", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
            let okAction = UIAlertAction(title: "确定", style: .default) { (_) in
                DispatchQueue.main.async {
                    DLUserInfoHandler.deleteUserInfo()
                    UIApplication.shared.keyWindow?.rootViewController = LoginNavigationController.setup()
                }
            }
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
        
    }
    
   
    func computeCache() -> String {
        let acache: CGFloat = CGFloat(EMSDImageCache.shared().getSize())
        
        return String(format: "%.1f", acache / 1024 / 1024)  + "MB"
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let  cell = UITableViewCell.init(style: .value1, reuseIdentifier: "SetCell")
        
        if indexPath.section == 0 {
            if indexPath.row == 0{
                cell.textLabel?.text = "黑名单"
            }
        } else if indexPath.section == 1{
            if indexPath.row == 0 {
                cell.textLabel?.text = "清除本地缓存"
                cell.detailTextLabel?.text = computeCache()
            }
        }
        cell.accessoryType = .disclosureIndicator
        cell.selectionStyle = .none
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14.0)
        cell.textLabel?.textColor = UIColor(hexString:"333333")
        cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 14.0)
        cell.detailTextLabel?.textColor = UIColor(hexString:"999999")
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return 1
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else {
            return 5
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return nil
        } else {
            let view = UIView.init(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 5))
            view.backgroundColor = UIColor(hexString:"f1f5f6")
            return view
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            let vc = FocusOtherPersonViewController()
            vc.type = "黑名单"
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            if indexPath.section == 1{
                if indexPath.row == 0{
                    EMSDImageCache.shared().clearDisk()
                    EMSDImageCache.shared().clearMemory()
                    KingfisherManager.shared.cache.clearDiskCache()
                    KingfisherManager.shared.cache.clearMemoryCache()
                    KingfisherManager.shared.cache.cleanExpiredDiskCache()
                    ProgressHUD.showSuccess(message: "清理完成")
                    self.tableview.reloadData()
                } 
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
