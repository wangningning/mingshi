//
//  FeedbackInfoViewController.swift
//  BaiShiXueYiLiving
//
//  Created by 初程程 on 2018/5/28.
//  Copyright © 2018年 liangyi. All rights reserved.
//

import UIKit

class FeedbackInfoViewController: UIViewController {
    var model: FeedbackModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        let titleLabel:UILabel = UILabel.init()
        titleLabel.text = model?.title
        self.view.addSubview(titleLabel)
        let contentLabel:UILabel = UILabel.init()
        contentLabel.text = model?.summary
        self.view.addSubview(contentLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.top.equalToSuperview().offset(20)
        }
        
        contentLabel.snp.makeConstraints { (make) in
            make.left.equalTo(titleLabel.snp.left)
            make.top.equalTo(titleLabel.snp.bottom).offset(10)
            make.width.equalTo(self.view.frame.size.width-40)
            make.height.greaterThanOrEqualTo(10)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
