//
//  FocusOnCourseViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh
class FocusOnCourseViewController: BSBaseViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var layout: UICollectionViewFlowLayout!
    var page = 1
    var videoArray = [VideoModel]()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "关注课程"
    self.collectionView.register(UINib(nibName:"HomePageLiveStoreCollectionViewCell", bundle:Bundle.main), forCellWithReuseIdentifier: "HomePageLiveStoreCollectionViewCell")
        
        layout.minimumLineSpacing = 14
        layout.minimumInteritemSpacing = 21
        layout.sectionInset = UIEdgeInsetsMake(12, 12, 5, 12)
        let w = (kScreenWidth - 12 * 2 -  21) / 2
        layout.itemSize = CGSize(width: w, height: 191 / 166 * w)
        
        self.collectionView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.page = 1
            self.loadData()
        })
         let footer = MJRefreshAutoNormalFooter(refreshingBlock: { [unowned self] in
            self.page += 1
            self.loadData()
            
        })
        footer?.setTitle("", for: .noMoreData)
        footer?.setTitle("", for: .idle)
        self.collectionView.mj_footer = footer
        self.collectionView.mj_header.beginRefreshing()
        // Do any additional setup after loading the view.
    }

    func loadData(){
        NetworkingHandle.fetchNetworkData(url: "/Member/collect_video", at: self, params: ["p":self.page], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: self.collectionView, success: { (response) in
            let data = response["data"] as! [String:AnyObject]
            let video = VideoModel.modelsWithArray(modelArray: data["list"] as! [[String : AnyObject]] ) as! [VideoModel]
            if self.page == 1{
                self.videoArray.removeAll()
            }
            if video.count == 0{
                self.collectionView.mj_footer.endRefreshingWithNoMoreData()
            }
            
            self.videoArray += video
            self.collectionView.reloadData()
            
        }) { 
            if self.page > 1{
                self.page -= 1
            }
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomePageLiveStoreCollectionViewCell", for: indexPath) as! HomePageLiveStoreCollectionViewCell
        cell.model = self.videoArray[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.videoArray.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = TCurriculumDetailViewController()
        vc.url = self.videoArray[indexPath.row].url
        vc.videoID = self.videoArray[indexPath.row].video_id
        vc.play_img = self.videoArray[indexPath.row].video_img
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
