//
//  TeacherListViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/11.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
class MessageList:KeyValueModel{
    var user_id: String?
    var username: String?
    var sex: String?
    var img: String?
    var hx_username: String?
    var hx_password: String?
    var date_value: String?
}
class TeacherListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var listArr:[MessageList] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "学习档案"
        self.tableView.register(UINib(nibName:"PrivateMessageTableViewCell", bundle:Bundle.main), forCellReuseIdentifier: "PrivateMessageTableViewCell")
        self.tableView.tableFooterView = UIView()
        self.loadData()
        // Do any additional setup after loading the view.
    }
    func loadData(){
        NetworkingHandle.fetchNetworkData(url: "/Member/learn_list", at: self, success: { (response) in
            let data = response["data"] as! [String:AnyObject]
            self.listArr.removeAll()
            self.listArr = MessageList.modelsWithArray(modelArray: data["list"] as! [[String:AnyObject]]) as! [MessageList]
            self.tableView.reloadData()
        })
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PrivateMessageTableViewCell") as! PrivateMessageTableViewCell
        cell.model = self.listArr[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listArr.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.listArr[indexPath.row]
        let versionKey = String(kCFBundleVersionKey)
        let currentVersion = Bundle.main.infoDictionary?[versionKey] as! String
        print(" 当前版本号为：\(currentVersion)")
        let param = ["version": currentVersion]
        NetworkingHandle.fetchNetworkData(url: "/Member/is_this", at: self, params: param, isShowHUD: false,  success: { (response) in
            let result = response["data"] as! String
            if result == "1"{
                return
            }else{
                if DLUserInfoHandler.getUserInfo()?.type == "2"{
                    ProgressHUD.showMessage(message: "名师身份暂无权限")
                    return
                }
                NetworkingHandle.fetchNetworkData(url: "/Home/check_teach", at: self, params: ["user_id":model.user_id!], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
                    let data = response["data"] as! String
                    if data == "1"{
                        let vc = DirectChatViewController(conversationChatter: model.hx_username, conversationType: EMConversationTypeChat)
                        vc?.userId = model.user_id
                        vc?.usernameTHEY = model.username
                        vc?.img = model.img
                        vc?.hx_username = model.hx_username
                        vc?.isTeacher = false
                        self.navigationController?.pushViewController(vc!, animated: true)
                        
                    }else if data == "2"{
                        ProgressHUD.showMessage(message: "本次请教需要付费")
                        NetworkingHandle.fetchNetworkData(url: "/Home/setup_teach_order", at: self, params: ["user_id": model.user_id!], isShowHUD: true, isShowError: true, success: { (response) in
                            let vc = SelectPayWayViewController()
                            vc.amount = "10.00"
                            vc.order_no = response["data"] as? String
                            vc.payType = "2"
                            vc.userId = model.user_id
                            vc.hx_username = model.hx_username
                            vc.img = model.img
                            vc.usernameTHEY = model.username
                            self.navigationController?.pushViewController(vc, animated: true)
                        }, failure: {
                            
                        })
                        
                        
                    }else{
                        let vc = DirectChatViewController(conversationChatter: model.hx_username, conversationType: EMConversationTypeChat)
                        vc?.userId = model.user_id
                        vc?.usernameTHEY = model.username
                        vc?.img = model.img
                        vc?.hx_username = model.hx_username
                        vc?.isTeacher = false
                        vc?.isTeachBefore = true
                        self.navigationController?.pushViewController(vc!, animated: true)
                        
                    }
                    
                }, failure: {
                    
                })
                
 
            }
            
        }) { 
            
        }
        
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
