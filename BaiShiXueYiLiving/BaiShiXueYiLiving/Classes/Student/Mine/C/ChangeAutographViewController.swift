//
//  ChangeAutographViewController.swift
//  MoDuLiving
//
//  Created by 曾觉新 on 2017/3/6.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class ChangeAutographViewController: UIViewController, UITextViewDelegate {
    @IBOutlet weak var numberLabel: UILabel!
    
    var model: PersonInfoModel!
    var updateSuccess:((String)->())?
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.00)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "保存", target: self, action: #selector(clickRightBar))
        
        textView.text = model?.autograph
        let autograph = model?.autograph ?? ""
        let textString = autograph as NSString
        let count = 60-textString.length
        numberLabel.text = "\(count)"
        textView.becomeFirstResponder()
        NotificationCenter.default.addObserver(self, selector: #selector(ChangeAutographViewController.textViewNotifitionAction(userInfo:)), name: NSNotification.Name.UITextViewTextDidChange, object: nil)
    }
    func textViewNotifitionAction(userInfo: NSNotification){
        let textVStr = textView.text as NSString
        numberLabel.text = "\(60-textVStr.length)"
        if (textVStr.length > 60) {
            let str = textVStr.substring(to: 60)
            textView.text = str
            numberLabel.text = "0"
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: 按键点击事件
    func clickRightBar() {
        changeAutograph(text: self.textView.text)
    }
    
    //MARK:
    func changeAutograph(text: String){
        textView.resignFirstResponder()
        let params = ["autograph" : text,"username":self.model!.username!,"phone":self.model!.phone!,"sex":self.model!.sex!]
        NetworkingHandle.fetchNetworkData(url: "/Member/change_user", at: self, params: params, success: { (response) in
            self.model?.autograph = text
            self.updateSuccess?(text)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: UserInfoChangeNotifcation), object: self.model);
            _ = self.navigationController?.popViewController(animated: true)
        })
       
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
