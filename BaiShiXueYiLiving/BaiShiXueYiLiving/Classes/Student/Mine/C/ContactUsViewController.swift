//
//  ContactUsViewController.swift
//  BaiDui
//
//  Created by sh-lx on 2017/5/3.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class ContactUsViewController: EaseMessageViewController ,EaseMessageViewControllerDataSource,EaseMessageViewControllerDelegate {

    var img: String? = nil
    var username: String? = nil
    var smodel: ServiceModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = username
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = UIColor.white
        self.tableView.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight - 64)
        self.dataSource = self
        self.delegate = self
        self.title = self.smodel.username
        
        self.tableView.reloadData()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    override func messagesDidReceive(_ aMessages: [Any]!) {
        let messate = aMessages as NSArray
        for messageNew in messate {
            
            if  self.shouldMarkMessageAsRead() == true {
                let newMessage = messageNew as! EMMessage
                self.conversation.markMessageAsRead(withId: newMessage.messageId, error: nil)
                
            }
        }
        
    }

    func shouldMarkMessageAsRead() -> Bool {
        var iSMark = true
        if  dataSource != nil {
            if  dataSource.responds(to: #selector(messageViewControllerShouldMarkMessages(asRead:))) != true {
                iSMark = dataSource.messageViewControllerShouldMarkMessages!(asRead: self)
            }
            else {
                
            }
        }
            
        else {
            if UIApplication.shared.applicationState == UIApplicationState.background || self.isViewDidAppear {
                iSMark = false
            }
        }
        
        return iSMark;
        
    }
    func messageViewControllerShouldMarkMessages(asRead viewController: EaseMessageViewController!) -> Bool {
        return true
    }
    
    func messageViewController(_ viewController: EaseMessageViewController!, modelFor message: EMMessage!) -> IMessageModel! {
        let model = EaseMessageModel(message: message)
        if  model?.isSender == true {
            model?.avatarURLPath = DLUserInfoHandler.getUserInfo()?.img
            model?.nickname = DLUserInfoHandler.getUserInfo()?.username
            
        }else {
            model?.avatarURLPath = self.smodel.img
            model?.nickname = self.smodel.username
        }
        return model
    }
    override func sendTextMessage(_ text: String!, withExt ext: [AnyHashable : Any]!){
        super.sendTextMessage(text, withExt: ["username":DLUserInfoHandler.getUserInfo()?.username ?? "","newsTime":getCurrentTime()])
    }
    func getCurrentTime()-> String{
        let date = DateFormatter.init()
        date.dateFormat = "YYYY-MM-dd HH:mm:ss"
        
        let now = Date.init()
        
        let nowStr = date.string(from: now)
        return nowStr
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    class func contactCustomerServiceVC(from vc: UIViewController){
        NetworkingHandle.fetchNetworkData(url: "/Home/kefu", at: vc, success: { (response) in
            let data = response["data"] as! [String:AnyObject]
            let m = ServiceModel.modelWithDictionary(diction: data)
            UserDefaults.standard.set(m.hx_username, forKey: "service")
            serviceRD = true
            let contactVC = ContactUsViewController.init(conversationChatter: m.hx_username, conversationType: EMConversationTypeChat)
            contactVC?.smodel = m
            contactVC?.hidesBottomBarWhenPushed = true
            vc.navigationController?.pushViewController(contactVC!, animated: true)
        })
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
class ServiceModel:KeyValueModel{
    var user_id: String?
    var username: String?
    var sex: String?
    var img: String?
    var id: String?
    var hx_username: String?
    var hx_password: String?
    var alias: String?
}
