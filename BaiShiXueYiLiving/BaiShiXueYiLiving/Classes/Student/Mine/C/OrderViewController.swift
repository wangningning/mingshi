//
//  OrderViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/17.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class OrderViewController: PageViewController {

    
    @IBOutlet weak var allOrderBtn: UIButton!
    @IBOutlet weak var prepareToPayBtn: UIButton!
    @IBOutlet weak var prepareToReceiveBtn: UIButton!
    
    @IBOutlet weak var prepareToTalkBtn: UIButton!
    
    @IBOutlet weak var bottomLine: UILabel!
    var sliderLine : UILabel!
    var selectedBtn: UIButton!
    var type: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let allOrder = GlobalOrderViewController()
        let prepareToPay = GlobalOrderViewController()
        let prepareToReceive = GlobalOrderViewController()
        let prepareToTalk = GlobalOrderViewController()
       
        allOrder.doType = 0
        prepareToPay.doType = 1
        prepareToReceive.doType = 2
        prepareToTalk.doType = 3
        
        self.viewControllerArray = [allOrder, prepareToPay, prepareToReceive, prepareToTalk]
        
        self.buttonArray = [allOrderBtn, prepareToPayBtn, prepareToReceiveBtn, prepareToTalkBtn]
        
        var frame = self.view.bounds
        frame.origin.y = 49
        frame.size.height -= 49
        pageViewController.view.frame = frame
        pageViewController.setViewControllers([self.viewControllerArray[type]], direction: .forward, animated: true, completion: nil)
        
       
        
        
        if self.type == 0{
            
            self.allOrderBtn.isSelected = true
            selectedBtn = self.allOrderBtn
            self.title = "全部"
            
        } else if self.type == 1 {
            
            self.prepareToPayBtn.isSelected = true
            selectedBtn = self.prepareToPayBtn
            self.title = "待付款"
            
        } else if self.type == 2 {
            self.prepareToReceiveBtn.isSelected = true
            selectedBtn = self.prepareToReceiveBtn
            self.title = "待收货"
            
        } else if self.type == 3{
            self.prepareToTalkBtn.isSelected = true
            selectedBtn = self.prepareToTalkBtn
            self.title = "待评价"
            
        }
        self.sliderLine = UILabel()
        self.sliderLine.frame = CGRect(x: kScreenWidth/4 * CGFloat(self.type), y: 45, width: kScreenWidth/4, height: 3)
        self.sliderLine.backgroundColor = UIColor(hexString: "EC6B1A")
        self.view.addSubview(self.sliderLine)

        // Do any additional setup after loading the view.
    }

    @IBAction func btnCliked(_ sender: UIButton) {
        if sender.tag == 0 {
            self.title = "全部"
        }
        else if sender.tag == 1 {
            self.title = "待付款"
        }
        else if sender.tag == 2 {
            self.title = "待发货"
        }
        else if sender.tag == 3 {
            self.title = "待评价"
        }
        UIView.animate(withDuration: 0.1) {
            self.sliderLine.frame = CGRect(x: sender.frame.origin.x, y: 45, width: sender.frame.size.width, height: 3)
        }
        
        self.selectedViewController(atButton: sender )
        
    }
    // 重写父类刷新按钮方法
    override func viewControllerTransition(toIndex index: Int) {
        
        let btn = self.buttonArray[index]
        UIView.animate(withDuration: 0.1) {
            self.sliderLine.frame = CGRect(x: btn.frame.origin.x, y: 45, width: btn.frame.size.width, height: 3)
        }
        self.title = btn.titleLabel?.text

        if selectedBtn === btn {
            
            return
        }
        selectedBtn.isSelected = false
        btn.isSelected = true
        selectedBtn = btn
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
