//
//  ChangeUserNameViewController.swift
//  MoDuLiving
//
//  Created by 曾觉新 on 2017/3/6.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class ChangeUserNameViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var textField: UITextField!
    
    var model: PersonInfoModel!
    var successSubmit:(()->())?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.00)
        let buttonItem = UIBarButtonItem(title: "保存", target: self, action: #selector(clickRightBar))

        self.navigationItem.rightBarButtonItem = buttonItem
        self.textField.returnKeyType = .send
        NotificationCenter.default.addObserver(self, selector: #selector(textFieldNotifition(noti:)), name: NSNotification.Name.UITextFieldTextDidChange, object: textField)
        textField.text = model!.username
        textField.becomeFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func textFieldNotifition(noti: Notification) {
        let tf = noti.object as? UITextField
        if tf == textField  {
            let textVStr = self.textField.text!.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines) as NSString
            if (textVStr.length > 9) {
                let str = textVStr.substring(to: 9)
                self.textField.text = str
            }
        }
    }
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        changeUserName(name: textField.text!)
        return true
    }
    //    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    //
    //        var text = ((textField.text)! as NSString).replacingCharacters(in: range, with: string)
    //        if text.characters.count <= 16 {
    //            return true
    //        }
    //        return false
    //    }
    func clickRightBar(){
        self.changeUserName(name: textField.text!)
    }
    //MARK: 修改昵称
    func changeUserName(name: String){
        textField.resignFirstResponder()
        if textField.text?.characters.count == 0{
            ProgressHUD.showMessage(message: "用户名不能为空")
            return
        }
         let params = ["autograph" : self.model!.autograph,"username":name,"phone":self.model!.phone!,"sex":self.model!.sex!]
        NetworkingHandle.fetchNetworkData(url: "/Member/change_user", at: self, params: params as [String:AnyObject], success: { (response) in
            self.model!.username = name
            self.successSubmit?()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: UserInfoChangeNotifcation), object: self.model);
            DLUserInfoHandler.update(name: name, img: nil, grade: nil)
            _ = self.navigationController?.popViewController(animated: true)
        })
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
