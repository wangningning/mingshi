//
//  PrivateViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/19.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh

class PrivateViewController: UIViewController {
    @IBOutlet weak var tableview: UITableView!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "私信"
        
        self.tableview.separatorStyle = .none
        self.tableview.backgroundColor = UIColor(red: 237/255, green: 236/255, blue: 242/255, alpha: 1)
        
        self.tableview.register(UINib(nibName: "MessageListTableViewCell", bundle: nil), forCellReuseIdentifier: "MessageCell")
        self.tableview.mj_header = MJRefreshNormalHeader(refreshingBlock: {
            self.loadData()
        })
        //        let footer = MJRefreshAutoNormalFooter(refreshingBlock: {
        //            self.loadData()
        //        })
        //        footer?.setTitle("", for: .idle)
        //        footer?.setTitle("", for: .noMoreData)
        //        self.tableView.mj_footer = footer
        
        self.tableview.mj_header.beginRefreshing()
        // Do any additional setup after loading the view.
    }
    func loadData() {
        func refresh() {
            tableview.mj_header.endRefreshing()
          //  chatDataArr.sort(by: {$0.timestamp > $1.timestamp})
            tableview.reloadData()
        }
        func getContent(m: EMConversation) -> String {
            if m.latestMessage == nil {
                return "有新消息"
            }
            let type = m.latestMessage.body.type
            var content = "有新消息"
            if type  == EMMessageBodyTypeText {
                let messageBody = m.latestMessage.body as! EMTextMessageBody
                content = messageBody.text
            } else if type  == EMMessageBodyTypeImage {
                content = "图片消息"
            } else if type  == EMMessageBodyTypeVoice {
                content = "语音消息"
            } else if type  == EMMessageBodyTypeVideo {
                content = "视频信息"
            } else if type  == EMMessageBodyTypeLocation {
                content = "位置信息"
            }
            return content
        }
        let list = EMClient.shared().chatManager.getAllConversations() as! [EMConversation]
      //  chatDataArr.removeAll()
        
        var i = 0
        if list.count == 0 {
            refresh()
        }
        for m in list {
            if m.latestMessage != nil, m.type == EMConversationTypeChat {
                NetworkingHandle.fetchNetworkData(url: "/Index/get_user_info", at: self, params: ["hx_username":getUserHxIdFrom(model: m)], success: { (result) in
                    let data = result["data"] as! [String: String]
                    
                    let name = data["username"] ?? ""
                    let id = data["user_id"] ?? ""
//
//                    let mdoel = PrivateChatModel(name: name, img: data["img"]!, content: getContent(m: m), time: self.timeStampToString(timeStamp: TimeInterval(m.latestMessage.timestamp/1000)), id: id, chatId: m.conversationId, sex: data["sex"]!, grade_img: "")
//                    data["grade_img"] ?? ""
//                    mdoel.unreadCount = Int(m.unreadMessagesCount)
//                    mdoel.timestamp = m.latestMessage.timestamp - 1488439927871
//                    self.chatDataArr.append(mdoel)
                    
                    i += 1
                    if i == list.count {
                        refresh()
                    }
                }, failure: {
                    i += 1
                    if i == list.count {
                        refresh()
                    }
                })
            } else {
                i += 1
                if i == list.count {
                    refresh()
                }
            }
        }
    }
    func getUserHxIdFrom(model: EMConversation) -> String {
        if model.latestMessage.direction == EMMessageDirectionSend {
            return model.latestMessage.to
        }
        return model.latestMessage.from
    }
    func timeStampToString(timeStamp: TimeInterval) -> String {
        let nowTimeInterval = Date().timeIntervalSince1970
        let interval = nowTimeInterval - timeStamp
        if interval < 60 {
            return "刚刚"
        }
        if interval < 60*60 {
            return "\(Int(floor(interval/60)))分钟前"
        }
        if interval < 60*60*24 {
            return "\(Int(floor(interval/60/60)))小时前"
        }
        if interval < 60*60*24*30  {
            return "\(Int(floor(interval/60/60/24)))天前"
        }
        if interval < 60*60*24*30*12  {
            return "\(Int(floor(interval/60/60/24/30)))个月前"
        }
        return "一年前"
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell") as! MessageListTableViewCell
      //  cell.privateModel = self.chatDataArr[indexPath.row]
        cell.selectionStyle = .none
        return cell
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return self.chatDataArr.count
        return 2
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let model = chatDataArr[indexPath.row]
//        
//        print("聊天室")
//        model.unreadCount = 0
//        tableView.reloadRows(at: [indexPath], with: .automatic)
//        let chatController = DirectChatViewController(conversationChatter: model.chatId, conversationType: EMConversationTypeChat)
//        chatController?.img = model.img
//        chatController?.usernameTHEY = model.name
//        chatController?.userId = model.id
//        self.navigationController?.pushViewController(chatController!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "删除") { (action, index) in
            self.deleteBlackList(indexPath: index)
        }
        return [deleteAction]
    }
    // 从黑名单中删除
    func deleteBlackList(indexPath: IndexPath) {
      //  let model = chatDataArr[indexPath.row]
//        EMClient.shared().chatManager.deleteConversation(model.chatId, isDeleteMessages: true, completion: { [unowned self] (s, e) in
//            if e == nil {
//                self.chatDataArr.remove(at: indexPath.row)
//                self.tableView.reloadData()
//            }
//        })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
