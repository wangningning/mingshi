//
//  GlobalOrderViewController.swift
//  FKDCClient
//
//  Created by 梁毅 on 2017/2/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh
class GlobalOrderViewController: BSBaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var page = 1
    var doType = 0
    var vcType: String?
    var orderList = [OrderListModel]()
    var isReloadMark = false
    var emptyLabel:UILabel!
    override func viewWillAppear(_ animated: Bool) {
        if  vcType != nil {
            self.navigationController?.navigationBar.isHidden = false
            
        }
        if self.doType == 3 {
            isReloadMark = UserDefaults.standard.object(forKey: "isReloadMark") as? Bool ?? false
            if isReloadMark {
                    isReloadMark = false
                 UserDefaults.standard.set(self.isReloadMark, forKey: "isReloadMark")
                 self.tableView.mj_header.beginRefreshing()
            }
           

        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        emptyLabel = UILabel.init()
        emptyLabel.frame = CGRect(x: tableView.frame.width/2-50, y: tableView.frame.height/2-15, width: 100, height: 30)
        emptyLabel.textAlignment = .center
        emptyLabel.text = "暂无数据"
        
        tableView.addSubview(emptyLabel)
    
        NotificationCenter.default.addObserver(self, selector: #selector(GlobalOrderViewController.loadData), name: NSNotification.Name.init(rawValue:EvaluateGoodsSuccessNotification), object: nil)

        tableView.register(UINib(nibName: "OrderTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderTableViewCell")
        tableView.register(UINib(nibName:"OrderFormSectionHeaderView", bundle:nil), forHeaderFooterViewReuseIdentifier: "OrderFormSectionHeaderView")
        tableView.register(UINib(nibName:"OrderFormSectionFooterView", bundle:nil), forHeaderFooterViewReuseIdentifier: "OrderFormSectionFooterView")
        tableView.separatorStyle = .none
        tableView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.page = 1
            self.loadData()
        })
        let footer = MJRefreshAutoNormalFooter(refreshingBlock: { [unowned self] in
            self.page += 1
            self.loadData()
        })
        footer?.setTitle("", for: .noMoreData)
        footer?.setTitle("", for: .idle)
        tableView.mj_footer = footer

        self.tableView.mj_header.beginRefreshing()
    }
    func loadData() {
        var parma = [String:String]()
        if doType == 0 {
            parma = ["p":String(page)]
        }
        else {
            parma = ["p":String(page),"state":String(doType)]
        }
        NetworkingHandle.fetchNetworkData(url: "/mall/mall_order", at: self, params: parma, isAuthHide: true, isShowHUD: true, isShowError: true,hasHeaderRefresh: self.tableView, success: { (response) in
            let data = response["data"] as! [String:AnyObject]
            
            let list = OrderListModel.modelsWithArray(modelArray: data["list"] as! [[String : AnyObject]]) as! [OrderListModel]
            if self.page == 1{
                self.orderList.removeAll()
            }

            if list.count == 0{
            self.tableView.mj_footer.endRefreshingWithNoMoreData()
            }
            self.orderList += list
            if self.orderList.count > 0 {
                self.emptyLabel.isHidden = true
            }else{
                self.emptyLabel.isHidden = false
            }
            self.tableView.reloadData()
            
        }) {
            
            if self.page > 1{
                self.page -= 1
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 41
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 87
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return orderList.count
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "OrderFormSectionHeaderView") as! OrderFormSectionHeaderView
        view.model = orderList[section]
        return view
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "OrderFormSectionFooterView") as! OrderFormSectionFooterView
        view.cancelOrderForm = { model in
            for m in self.orderList{
                if m.order_no == model.order_no {
                    m.isCancel = model.isCancel
                    break
                }
            }
            for (index,model) in self.orderList.enumerated() {
                if model.isCancel == true {
                    self.orderList.remove(at: index)
                    break
                }
            }
            self.loadData()
        }
        view.deleteOrderForm = { model in
            for (index,m) in self.orderList.enumerated() {
                if model.order_no == m.order_no {
                    self.orderList.remove(at: index)
                    break
                }
            }
            self.loadData()
        }
        view.confirmReceiveGoods = { [weak self] model in
            
                self?.page = 1
                self?.loadData()
            self?.isReloadMark = true
            UserDefaults.standard.set(self?.isReloadMark, forKey: "isReloadMark")
        }
        view.model = orderList[section]

        return view
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (orderList[section].order_detail?.count)!
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderTableViewCell") as! OrderTableViewCell
        cell.model = (orderList[indexPath.section].order_detail?[indexPath.row])!
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 99
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = OrderDetailViewController()
        vc.order_no = orderList[indexPath.section].order_no
     vc.order_detail = orderList[indexPath.section].order_detail
        self.navigationController?.pushViewController(vc, animated: true)
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}
