//
//  YSHUserAddressEditViewController.swift
//  Yesho
//
//  Created by Luiz on 2016/12/4.
//  Copyright © 2016年 luiz. All rights reserved.
//

import UIKit

class YSHUserAddressEditViewController: UIViewController, UITextFieldDelegate {
    
    
   

    
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var name: UITextField!
    
//    @IBOutlet weak var street: UITextField!
    
    @IBOutlet weak var proviceCityDistrict: UITextField!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var detailsAddress: UITextField!
    
    var addressModel: AddressList?
    
    var editComplete: (() -> ())?
    
    var provice, city, district: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.confirmBtn.layer.cornerRadius = 6
        self.confirmBtn.layer.masksToBounds = true
        
        if  let model = self.addressModel {
            self.name.text = model.name
            self.phoneNumber.text = model.phone
            
            self.proviceCityDistrict.text = "\(model.province!)-\(model.city!)-\(model.area!)"
            self.detailsAddress.text = model.address
            if  model.address != nil {

            }
            provice = model.province
            city = model.city
            district = model.area
            if provice == city {
                self.proviceCityDistrict.text =  city! + district!
                
            } else {
                self.proviceCityDistrict.text = provice! + city!
                self.proviceCityDistrict.text = self.proviceCityDistrict.text! + district!
            }
        }
        
        let viewGesture = UITapGestureRecognizer(target: self, action: #selector(resignTextField))
        self.view.addGestureRecognizer(viewGesture)
        
        NotificationCenter.default.addObserver(self, selector: #selector(textFieldDidChangeValue(noti:)), name: NSNotification.Name.UITextFieldTextDidChange, object: [self.name,self.phoneNumber,self.proviceCityDistrict])
        
        
    }
    /// text field text 改变的监听
    func textFieldDidChangeValue(noti: Notification?) -> () {
//        self.confirmBtn.isEnabled = !(self.name.text?.isEmpty)! && !(self.phoneNumber.text?.isEmpty)! && !(self.proviceCityDistrict.text?.isEmpty)! && !(self.detailsAddress.text?.isEmpty)!
    }
    /// 收起键盘
    func resignTextField() -> () {
        UIApplication.shared.keyWindow?.endEditing(true)
    }
    // MARK: - text field 代理
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        resignTextField()
        let v = ProvinceCityDistrictChoice(frame: self.view.bounds)
        v.show(choiced: { [unowned self] (p, c, d) in
            self.provice = p
            self.city = c
            self.district = d
            self.proviceCityDistrict.text = "\(p)-\(c)-\(d)"
            self.textFieldDidChangeValue(noti: nil)
        })
        self.view.addSubview(v)
        return false
    }
    @IBAction func confirmAction(_ sender: AnyObject) {
        if (name.text?.isEmpty)! {
         
            ProgressHUD.showNoticeOnStatusBar(message: "请填写收货人姓名")

            return
        }
        if !(self.phoneNumber.text!.isPhoneNumber) {
         
            ProgressHUD.showNoticeOnStatusBar(message: "请填写正确的手机号")

            return
        }
        if provice == nil {
           
            ProgressHUD.showNoticeOnStatusBar(message: "请选择地区")

            return
        }

        if (detailsAddress.text?.isEmpty)! {
            ProgressHUD.showNoticeOnStatusBar(message: "请填写详细地址")
            return
        }
        var param = ["name":name.text!, "phone":phoneNumber.text!,"province":provice!,"city":city!,"area":district!,"address":detailsAddress.text!] as [String : Any]
        var URLStr = ""
        if let model = self.addressModel {
            URLStr = "?m=Api&c=Mall&a=edit_receive_address"
            param["id"] = model.id!
        }
        else {
            URLStr = "?m=Api&c=Mall&a=add_receive_address"
        }
        NetworkingHandle.fetchNetworkData(url: URLStr, at: self, params: param as [String:AnyObject], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
            
            self.editComplete?()
            self.navigationController?.popViewController(animated: true)
        }) { 
            
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
// 收获地址
class YSHUserAddressInfoModel: KeyValueModel {
    var addr_id: Any?
    var addr_name: Any?
    var addr_tel: Any?
    var addr_zipcode: Any?
    var addr_prce: Any?
    var addr_city: Any?
    var addr_district: Any?
    var address: Any?
    var isdefault: Any?
}
