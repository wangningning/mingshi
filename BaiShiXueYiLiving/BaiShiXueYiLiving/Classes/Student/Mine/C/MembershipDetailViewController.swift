//
//  MembershipDetailViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/17.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class MembershipDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableview: UITableView!
    var model: UpdateMemberShipModel!
    var teacherID: String? // 导师ID
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "会员升级"
        self.tableview.register(UINib(nibName:"MemberLevelTableViewCell",bundle:Bundle.main), forCellReuseIdentifier: "MemberLevelTableViewCell")
        
        // Do any additional setup after loading the view.
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MemberLevelTableViewCell") as! MemberLevelTableViewCell
        if indexPath.row == 0 {
            cell.memberImg.image = #imageLiteral(resourceName: "zuansh")
        } else if indexPath.row == 1 {
            cell.memberImg.image = #imageLiteral(resourceName: "gaoji")
        } else {
            cell.memberImg.image = #imageLiteral(resourceName: "putong")
        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (kScreenWidth - 20) * 160/355 + 10
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var param = ""
        if indexPath.row == 0{
            param = "2"
        } else if indexPath.row == 1{
            param = "3"
        } else {
            param = "4"
        }
        let vc = MDWebViewController()
        vc.title = "权限"
        vc.url = NetworkingHandle.mainHost + "/Home/xieyi/id/" + param
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func upgrade(_ sender: UIButton) {
        
        self.loadData()
      
    }
    func loadData(){
        NetworkingHandle.fetchNetworkData(url: "/Home/upgrade_show", at: self, params: ["user_id":self.teacherID!], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (respose) in
            
           
            self.model = UpdateMemberShipModel.modelWithDictionary(diction: respose["data"] as! [String:AnyObject])
            if self.model.status == "3"{
                ProgressHUD.showNoticeOnStatusBar(message: "您已经是该导师最高等级的会员")
                return
            }
            _ =  upgradeView.show(atView: self.view, model:self.model, userID: self.teacherID!)
            
            
        }) { 
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
