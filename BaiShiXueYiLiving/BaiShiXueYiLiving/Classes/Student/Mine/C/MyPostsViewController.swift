//
//  MyPostsViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/8/29.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh

class MyPostsViewController: UIViewController, UITableViewDelegate,UITableViewDataSource, BBSListTableViewCellDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var page = 1
    
    var moduleArr = [BBSModuleModel]()
    var listArr = [BBSListModel]()
    var tempCell: BBSListTableViewCell!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "我的帖子"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "发帖", style: .done, target: self, action: #selector(MyPostsViewController.postMessageAction))
        
        tempCell = BBSListTableViewCell(style: .default, reuseIdentifier: "BBSListTableViewCell")
        self.tableView.register(BBSListTableViewCell.self, forCellReuseIdentifier: "BBSListTableViewCell")
        tableView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.requestBBSListData(p: 1)
        })
        let footer = MJRefreshAutoNormalFooter(refreshingBlock: { [unowned self] in
            self.requestBBSListData(p: self.page + 1)
        })
        
        footer?.setTitle("", for: .noMoreData)
        footer?.setTitle("", for: .idle)
        tableView.mj_footer = footer
        self.tableView.mj_header.beginRefreshing()

        
        // Do any additional setup after loading the view.
    }
    func postMessageAction() {
        let vc = BBSPostMessageViewController()
        vc.successPost = { [unowned self] in
            self.tableView.mj_header.beginRefreshing()
        }
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

    //MARK: 请求数据
    func requestBBSListData(p: Int) {
        NetworkingHandle.fetchNetworkData(url: "/Topical/my_posts", at: self, params: ["p":p], isAuthHide: false, isShowHUD: false, isShowError: false, hasHeaderRefresh: self.tableView, success: { (response) in
            let data = response["data"] as! [String: AnyObject]
            let list = data["list"] as! [[String: AnyObject]]
            
            let listData = BBSListModel.modelsWithArray(modelArray: list) as! [BBSListModel]
            if p == 1 {
                self.page = 1
                self.listArr.removeAll()
                self.listArr += listData
                
            } else {
                self.page += 1
                
                self.listArr += listData
                
            }
            self.tableView.reloadData()
        }) { 
            if self.page > 1{
                self.page -= 1
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BBSListTableViewCell") as! BBSListTableViewCell
        cell.model = self.listArr[indexPath.row]
        cell.selectionStyle = .none
        cell.isMinePostVC = true
        cell.myDelegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listArr.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = self.listArr[indexPath.row]
        if model.cellHeight == nil {
            tempCell.model = model
            model.cellHeight = tempCell.getCellHeight()
        }
        return model.cellHeight!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = BBSDetailsViewController()
        vc.deleteComplete = {
            self.requestBBSListData(p: 1)
        }
        vc.updateBBSList = {(model) in
            let bbsModel = self.listArr[indexPath.row]
            bbsModel.zan = model.zan
            bbsModel.ping = model.ping
            bbsModel.is_zan = model.is_zan
            self.tableView.reloadData()
        }
        vc.bbsModel = self.listArr[indexPath.row]
        
        self.navigationController?.pushViewController(vc, animated: true)

    }
    func bbsListTableViewCell(cell: BBSListTableViewCell, eventFo tag: Int, model: BBSListModel) {
        switch tag {
        case 1:
            
            let vc = BBSDetailsViewController()
            vc.deleteComplete = {
                self.requestBBSListData(p: 1)
            }
            vc.updateBBSList = {(bbsModel) in
                model.zan = bbsModel.zan
                model.ping = bbsModel.ping
                model.is_zan = bbsModel.is_zan
                self.tableView.reloadData()
            }
            vc.isDirectlyReply = true
            vc.bbsModel = model
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            print("跳转到详情页面")
            
            break
        case 2:
            
            likePost(model: model)
            break
        case 3:
            print("删除")
            LyAlertView.alert(atVC: self, title: "提示", message: "确定删除？", cancel: "取消", ok: "确定", okBlock: {
                NetworkingHandle.fetchNetworkData(url: "/Topical/del_posts", at: self, params: ["post_id":model.post_id!], isAuthHide: false, isShowHUD: false, isShowError: true, success: { (response) in
                    for (index,m) in self.listArr.enumerated(){
                        if m.post_id == model.post_id{
                            self.listArr.remove(at: index)
                            self.tableView.reloadData()
                            ProgressHUD.showSuccess(message: "删除成功")
                        }
                    }
                    
                }, failure: {
                    
                })
                
            }, cancelBlock: { 
                
            })
           
            break
        default:
            break
        }
    }
    //MARK: 帖子点赞
    func likePost(model: BBSListModel) {
        let params = ["post_id" : model.post_id as AnyObject,
                      ] as [String : AnyObject]
        
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Topical&a=zan_posts", at: self, params: params as [String : AnyObject], isAuthHide: true, isShowHUD: false, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
            let data = response["data"] as! String
            let zan = Int(model.zan!)
            if data == "2" {//取消点赞
                ProgressHUD.showMessage(message: "取消成功")
                model.is_zan = "2"
                model.zan = "\(zan! - 1)"
            } else {
                ProgressHUD.showMessage(message: "点赞成功")
                model.is_zan = "1"
                model.zan = "\(zan! + 1)"
            }
            self.tableView.reloadData()
        }) { (errorCode) in
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
