//
//  OrderDetailViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/5.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class OrderDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var tableview: UITableView!
    var orderform:orderFormDetailModel!
    var order_no: String?
    var order_detailArr = [ConfirmOrder]()
    var footerView: OrderDetailTableFooterView!
    var order_detail: [GoodsCartModel]?
    override func viewWillAppear(_ animated: Bool) {
        var vcArr = self.navigationController?.viewControllers
        
        for (index, vc) in (vcArr?.enumerated())! {
            
            if vc.isKind(of: SelectPayWayViewController.self){
                vcArr?.remove(at: index)
            }
            if vc.isKind(of: ConfirmOrderViewController.self){
                vcArr?.remove(at: index)
            }
            
        }
       self.navigationController?.setViewControllers(vcArr!, animated: false)
       super.viewWillAppear(animated)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "订单详情"
        
        self.tableview.register(UINib(nibName:"ConfirmOrderHeaderView",bundle:Bundle.main), forHeaderFooterViewReuseIdentifier: "ConfirmOrderHeaderView")
        self.tableview.register(UINib(nibName:"ConfirmOrderTableViewCell",bundle:Bundle.main), forCellReuseIdentifier: "ConfirmOrderTableViewCell")

        self.footerView = OrderDetailTableFooterView.setup()
        self.tableview.tableFooterView = self.footerView
//        self.tableview.sectionFooterHeight = 280
        self.loadData()
        // Do any additional setup after loading the view.
    }

    func loadData(){
        
        NetworkingHandle.fetchNetworkData(url: "/mall/order_detail", at: self, params: ["order_no":self.order_no!], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
            
            self.orderform = orderFormDetailModel.modelWithDictionary(diction: response["data"] as! [String:AnyObject])
            
            
            self.footerView.userMessageTF.text = self.orderform.remark!
            self.footerView.order_no = self.orderform.order_no
            self.footerView.amount = "￥" + String.init(format: "%.2f", Double(self.orderform.paid!)!)
            
            self.footerView.goodsArr = self.order_detail!
            
            self.footerView.kuaidi_state = self.orderform.kuaidi_state!
            self.footerView.kuaidi = self.orderform.kuaidi
            self.footerView.kuaidiName = self.orderform.kuaidi_name
            self.footerView.kuaidi_node = self.orderform.kuaidi_node
            self.footerView.sstate = self.orderform.state
            
            if self.orderform.send_bill == "1"{
                self.footerView.billName.text = "无相关信息"
            } else {
                self.footerView.billName.text = self.orderform.bill_name
            }
            self.footerView.paid.text = "￥" + String.init(format: "%.2f", Double(self.orderform.paid!)!)
            if self.orderform.has_postage == "1"{
                self.footerView.postage.text = "免运费"
            }else {
                self.footerView.postage.text = "含运费\(self.orderform.postage!)元 实收金额 "
            }
            
            if self.orderform.state == "4"{
                self.footerView.state.text = "待评价"
//                self.footerView.rightBtn.setImage(#imageLiteral(resourceName: "btn_pingjia2"), for: .normal)
            self.footerView.rightBtn.setImage(UIImage.init(named: "btn_pingjia2"), for: .normal)
            } else if self.orderform.state == "2"{
                if self.orderform.kuaidi_state == "1" {
                    self.footerView.state.text = "待发货"
                    self.footerView.rightBtn.setImage(#imageLiteral(resourceName: "btn_wuliu2"), for: .normal)
                }
                if self.orderform.kuaidi_state == "2" {
                    self.footerView.state.text = "已发货"
                    self.footerView.rightBtn.setImage(#imageLiteral(resourceName: "btn_wuliu2"), for: .normal)
                }
                if self.orderform.kuaidi_state == "3"{
                    self.footerView.state.text = "派送中"
                    self.footerView.rightBtn.setImage(#imageLiteral(resourceName: "btn_wuliu2"), for: .normal)
                }
                if self.orderform.kuaidi_state == "4" {
                    self.footerView.state.text = "已签收"
                    self.footerView.rightBtn.setImage(#imageLiteral(resourceName: "btn_wuliu2"), for: .normal)
                }
                
            } else if self.orderform.state == "1" {
                self.footerView.state.text = "待付款"
                self.footerView.rightBtn.setImage(#imageLiteral(resourceName: "btn_fukuan2"), for: .normal)
            }else if self.orderform.state == "3" {
                self.footerView.state.text = "待收货"
                self.footerView.rightBtn.setImage(#imageLiteral(resourceName: "btn_fukuan2"), for: .normal)
                self.footerView.rightBtn.isHidden = true
            }else if self.orderform.state == "5" {
                self.footerView.state.text = "已评价"
                self.footerView.rightBtn.setImage(#imageLiteral(resourceName: "btn_fukuan2"), for: .normal)
                self.footerView.rightBtn.isHidden = true
            }else if self.orderform.state == "6" {
                self.footerView.state.text = "已取消"
                self.footerView.rightBtn.setImage(#imageLiteral(resourceName: "btn_fukuan2"), for: .normal)
                self.footerView.rightBtn.isHidden = true
            }
            if self.orderform.kuaidi_name! == "" {
                self.footerView.company.text = ""
            }else{
                self.footerView.company.text =  "物流公司："  + self.orderform.kuaidi_name!
            }
            if self.orderform.kuaidi! == "" {
                self.footerView.postNumber.text = ""
            }else{
                self.footerView.postNumber.text = "运单编号：" + self.orderform.kuaidi!
            }

            self.footerView.orderNumber.text = "订单编号：" + self.orderform.order_no!

            
            let data = response["data"] as! [String:AnyObject]
            self.order_detailArr.removeAll()
            let dataArr = ConfirmOrder.modelsWithArray(modelArray: data["order_detail"] as! [[String : AnyObject]] ) as! [ConfirmOrder]
            self.order_detailArr += dataArr
//            self.footerView.goodsArr = self.order_detailArr
            self.tableview.reloadData()
            
        }) { 
            
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "ConfirmOrderTableViewCell") as! ConfirmOrderTableViewCell
        cell.confirmOrder = self.order_detailArr[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.order_detailArr.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 99
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ConfirmOrderHeaderView") as! ConfirmOrderHeaderView
        if self.orderform != nil {
            view.detailImg.isHidden = true
            view.payStateHeight.constant = 0
            view.payStateView.isHidden = true
            view.addressLabel.text = "收货地址: " + self.orderform.address!
            view.namelabel.text = "收货人: " + self.orderform.name!
            view.phoneLabel.text = self.orderform.phone
        }
       
        
        return view
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 87
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
