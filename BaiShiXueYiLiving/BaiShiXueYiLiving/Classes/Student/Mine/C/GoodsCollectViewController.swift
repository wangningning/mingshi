//
//  GoodsCollectViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/17.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh

class GoodsCollectViewController: BSBaseViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var layout: UICollectionViewFlowLayout!
    var collection = [GoodsCollectionModel]()
    var page = 1
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "收藏"
        self.collectionView.register(UINib(nibName:"GoodsCollectCell",bundle:Bundle.main), forCellWithReuseIdentifier: "GoodsCollectCell")
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        let w = kScreenWidth/2
        layout.itemSize = CGSize(width: w, height: 199/181 * w-10)
        
        
        
        self.collectionView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.page = 1
            self.loadData()
        })
        self.collectionView.mj_footer = MJRefreshAutoFooter(refreshingBlock: { [unowned self] in
            self.page += 1
            self.loadData()
        })
        self.collectionView.mj_header.beginRefreshing()
        // Do any additional setup after loading the view.
    }
    func loadData(){
        
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Mall&a=collect", at: self, params: ["p":self.page], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: self.collectionView, success: { (response) in
            let data = response["data"] as! [String:AnyObject]
            let collectionData = GoodsCollectionModel.modelsWithArray(modelArray: data["list"] as! [[String:AnyObject]]) as! [GoodsCollectionModel]
            if self.page == 1{
                self.collection.removeAll()
            }
            if collectionData.count == 0{
                self.collectionView.mj_footer.endRefreshingWithNoMoreData()
            }
            self.collection += collectionData
            self.collectionView.reloadData()
            
        }) { 
            
        }
        
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GoodsCollectCell", for: indexPath) as! GoodsCollectCell
        cell.collectionModel = self.collection[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collection.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = ShopDetailViewController()
        vc.goods_id = self.collection[indexPath.row].goods_id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
