//
//  PrivateMessageViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/17.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh

class PrivateMessageViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!

    var dataArr: [PrivateChatModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(UINib(nibName:"PrivateChatAnchorTableViewCell", bundle:nil), forCellReuseIdentifier: "PrivateChatAnchorTableViewCell")
        self.tableView.tableFooterView = UIView()
        
        self.tableView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.loadData()
        })
        self.tableView.mj_header.beginRefreshing()
        // Do any additional setup after loading the view.
    }

    func loadData(){
        self.tableView.mj_header.endRefreshing()
        let list = EMClient.shared().chatManager.getAllConversations() as! [EMConversation]
        self.dataArr.removeAll()
       
        for (index,m) in list.enumerated() {
            if m.latestMessage != nil, m.type == EMConversationTypeChat {
                if m.latestMessage.chatType == EMChatTypeChat{
               
                    if let otherMessage = m.lastReceivedMessage(){
                        
                        if otherMessage.ext["user_id"] as? String != nil{
                            let model = PrivateChatModel(name: otherMessage.ext["username"]! as! String, img: otherMessage.ext["img"] as! String, content: getContent(m: m), time:timeStampToString(timeStamp: TimeInterval(m.latestMessage.timestamp/1000)), id:otherMessage.ext["user_id"] as! String, chatId: m.conversationId, state: otherMessage.ext["state"] as! String)
                            model.unreadCount = Int(m.unreadMessagesCount)
                            model.timestamp = m.latestMessage.timestamp - 1488439927871
                            if m.latestMessage.conversationId == UserDefaults.standard.value(forKey: "service") as! String {
                                
                            } else{
                                
                                self.dataArr.append(model)
                                if index == list.count - 1 {
                                    dataArr.sort(by: {$0.timestamp > $1.timestamp})
                                }
                                self.tableView.reloadData()
                            }

                        }
                        
                    } else{
                        NetworkingHandle.fetchNetworkData(url: "/Index/get_user_info", at: self, params: ["hx_username":getUserHxIdFrom(model: m)], success: { (result) in
                            let data = result["data"] as! [String: String]
                            
                            let name = data["username"] ?? ""
                            let id = data["user_id"] ?? ""
                            
                            let model = PrivateChatModel(name: name, img: data["img"]!, content: self.getContent(m: m), time: self.timeStampToString(timeStamp: TimeInterval(m.latestMessage.timestamp/1000)), id: id, chatId: m.conversationId, state: "1")
                            model.unreadCount = Int(m.unreadMessagesCount)
                            model.timestamp = m.latestMessage.timestamp - 1488439927871
                            
                            if m.latestMessage.conversationId == UserDefaults.standard.value(forKey: "service") as! String {
                                
                            } else{
                                self.dataArr.append(model)
                                if index == list.count - 1 {
                                    self.dataArr.sort(by: {$0.timestamp > $1.timestamp})
                                }
                                self.tableView.reloadData()
                            }
                            
                        })

                        
                    }
                    
                }
            }
           
        }
        
    }
    func getUserHxIdFrom(model: EMConversation) -> String {
        if model.latestMessage.direction == EMMessageDirectionSend {
            return model.latestMessage.to
        }
        return model.latestMessage.from
    }
    func getContent(m: EMConversation) -> String {
        if m.latestMessage == nil {
            return "有新消息"
        }
        let type = m.latestMessage.body.type
        var content = "有新消息"
        if type  == EMMessageBodyTypeText {
            let messageBody = m.latestMessage.body as! EMTextMessageBody
            content = messageBody.text
        } else if type  == EMMessageBodyTypeImage {
            content = "视频消息"
        } else if type  == EMMessageBodyTypeVoice {
            content = "语音消息"
        } else if type  == EMMessageBodyTypeVideo {
            content = "视频信息"
        } else if type  == EMMessageBodyTypeLocation {
            content = "位置信息"
        }
        return content
    }
    func timeStampToString(timeStamp: TimeInterval) -> String {
        let nowTimeInterval = Date().timeIntervalSince1970
        let interval = nowTimeInterval - timeStamp
        if interval < 60 {
            return "刚刚"
        }
        if interval < 60*60 {
            return "\(Int(floor(interval/60)))分钟前"
        }
        if interval < 60*60*24 {
            return "\(Int(floor(interval/60/60)))小时前"
        }
        if interval < 60*60*24*30  {
            return "\(Int(floor(interval/60/60/24)))天前"
        }
        if interval < 60*60*24*30*12  {
            return "\(Int(floor(interval/60/60/24/30)))个月前"
        }
        return "一年前"
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PrivateChatAnchorTableViewCell") as! PrivateChatAnchorTableViewCell
        cell.pmodel = self.dataArr[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.dataArr.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.dataArr[indexPath.row]
        model.unreadCount = 0
        tableView.reloadRows(at: [indexPath], with: .automatic)
        print("指点状态在这里显示" + model.state)
        
        let versionKey = String(kCFBundleVersionKey)
        let currentVersion = Bundle.main.infoDictionary?[versionKey] as! String
        print(" 当前版本号为：\(currentVersion)")
        let param = ["version": currentVersion]
        NetworkingHandle.fetchNetworkData(url: "/Member/is_this", at: self, params: param, isShowHUD: false, success: { (response) in
            let result = response["data"] as! String
            if result == "1"{
                let chatController = DirectChatViewController(conversationChatter: model.chatId, conversationType: EMConversationTypeChat)
                chatController?.img = model.img
                chatController?.usernameTHEY = model.name
                chatController?.userId = model.id
                chatController?.hx_username = model.chatId
                chatController?.isAudit = true
                chatController?.reloadMeassage = { [unowned self] in
                    self.loadData()
                }
                self.navigationController?.pushViewController(chatController!, animated: true)
            }else{
                NetworkingHandle.fetchNetworkData(url: "/Home/check_teach", at: self, params: ["user_id":model.id], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
                    let data = response["data"] as! String
                    
                    if data == "1"{
                        if DLUserInfoHandler.getUserInfo()?.type == "1"{
                            self.jumpToDirectChatVC(isTeacher: false, isTeachBefore: false, state: "1", model: model)
                        }else{
                            self.jumpToDirectChatVC(isTeacher: true, isTeachBefore: false, state: "1", model: model)
                        }
                    } else if data == "2"{
                        if DLUserInfoHandler.getUserInfo()?.type == "1"{
                            NetworkingHandle.fetchNetworkData(url: "/Home/teach_fee", at: self,params: ["user_id": model.id], success: { (response) in
                                let data = response["data"] as! String
                                NetworkingHandle.fetchNetworkData(url: "/Home/setup_teach_order", at: self, params: ["user_id": model.id], isShowHUD: true, isShowError: true, success: { (response) in
                                    ProgressHUD.showMessage(message: "本次指点需要付费")
                                    
                                    let vc = SelectPayWayViewController()
                                    vc.order_no = response["data"] as? String
                                    vc.payType = "2"
                                    vc.userId = model.id
                                    vc.usernameTHEY = model.name
                                    vc.img = model.img
                                    vc.hx_username = model.chatId
                                    vc.amount = data
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }, failure: {
                                    
                                })
                                
                            })
                        }else{
                            self.jumpToDirectChatVC(isTeacher: false, isTeachBefore: true, state: "2", model: model)
                        }
                        
                    } else if data == "3"{
                        if DLUserInfoHandler.getUserInfo()?.type == "1"{
                            self.jumpToDirectChatVC(isTeacher: false, isTeachBefore: true, state: "2", model: model)
                        }else{
                            self.jumpToDirectChatVC(isTeacher: false, isTeachBefore: true, state: "2", model: model)
                        }
                    }
                }, failure: {
                    
                })
 
            }
        }) { 
            
        }
        
        
    }
    func jumpToDirectChatVC(isTeacher t: Bool, isTeachBefore b: Bool, state s: String, model m:PrivateChatModel){
        
        let chatController = DirectChatViewController(conversationChatter: m.chatId, conversationType: EMConversationTypeChat)
        chatController?.img = m.img
        chatController?.usernameTHEY = m.name
        chatController?.userId = m.id
        chatController?.isTeacher = t
        chatController?.isTeachBefore = b
        chatController?.state = s
        chatController?.hx_username = m.chatId
        chatController?.reloadMeassage = { [unowned self] in
            self.loadData()
        }
        self.navigationController?.pushViewController(chatController!, animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

