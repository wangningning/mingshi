//
//  FocusOtherPersonViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh

class FocusOtherPersonViewController: BSBaseViewController,UITableViewDataSource,UITableViewDelegate {

    var type: String!
    var page = 1
    var dataArr: [FollowListModel]=[]
    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.type != "" {
            self.title = self.type

        }
        self.tableview.register(UINib(nibName:"FensiTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "FensiTableViewCell")
        self.tableview.separatorStyle = .none
        
        self.tableview.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.page = 1
            self.loadData()
        })
        
        let footer = MJRefreshAutoNormalFooter { [unowned self] in
            self.page += 1
            self.loadData()
        }
        footer?.setTitle("没有更多数据了", for: .noMoreData)
        self.tableview.mj_footer = footer
        
        self.tableview.mj_header.beginRefreshing()
        
        // Do any additional setup after loading the view.
    }
    func loadData(){
        var url = ""
        if self.type == "关注的人" {
            url = "/Member/my_follow"
        } else if self.type == "粉丝"{
            url = "/Member/my_fans"
        } else if self.type == "黑名单"{
            url = "/Index/shield_list"
        }
        let param = ["pagesize":"10","p":self.page] as [String:AnyObject]
        NetworkingHandle.fetchNetworkData(url: url, at: self, params: param, isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: self.tableview, success: { (response) in
            let data = response["data"] as! [String:AnyObject]
            let arr = FollowListModel.modelsWithArray(modelArray: data["list"] as! [[String:AnyObject]]) as! [FollowListModel]
            if self.page == 1{
                
                self.dataArr.removeAll()
            }
            if arr.count == 0{
                
                self.tableview.mj_footer.endRefreshingWithNoMoreData()
                
            }
            self.dataArr += arr
            self.tableview.reloadData()

        }) { 
            if self.page > 1{
                self.page -= 1
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "FensiTableViewCell") as! FensiTableViewCell
        cell.model = self.dataArr[indexPath.row]
        cell.type = self.type
        cell.cancelSuccess = { [unowned self] in
            self.tableview.mj_header.beginRefreshing()
        }
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArr.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 81
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.type == "黑名单"{
            return
        }
        let model = self.dataArr[indexPath.row]
        pushToUserInfoCenter(atViewController: self, uId: model.user_id!)
      
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
