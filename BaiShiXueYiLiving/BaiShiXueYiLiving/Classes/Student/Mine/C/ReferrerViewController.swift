//
//  ReferrerViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/18.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
class ReferrerViewController: UIViewController {

    @IBOutlet weak var confirmBtn: UIButton!
    
    @IBOutlet weak var phoneTF: UITextField!
    var successSubmit: (()->())?
    var referrerPhone: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "推荐人"
        
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        self.confirmBtn.layer.cornerRadius = 6
        self.confirmBtn.layer.masksToBounds = true
        NotificationCenter.default.addObserver(self, selector: #selector(ReferrerViewController.submit(noti:)), name: NSNotification.Name.UITextFieldTextDidEndEditing, object: self.phoneTF)
        self.phoneTF.isEnabled = self.referrerPhone.characters.count == 0 ? true : false
        if !self.phoneTF.isEnabled {
            self.phoneTF.text = self.referrerPhone
            self.confirmBtn.isHidden = true
        }
        print("~~~~~~~~~~~\(self.referrerPhone.characters.count)")
        // Do any additional setup after loading the view.
    }

    func submit(noti:Notification){
        let tf = noti.object as! UITextField
        if tf.text?.characters.count == 0{
            ProgressHUD.showMessage(message: "手机号码不能为空")
            resignKeyboard()
            return
        }
        if !(tf.text?.isPhoneNumber)! {
            resignKeyboard()
            ProgressHUD.showMessage(message: "手机号码格式不正确")
            return
        }
        
        
        
    }
    func resignKeyboard(){
        if self.phoneTF.canBecomeFirstResponder{
            self.phoneTF.resignFirstResponder()
        }
    }
    @IBAction func confirmAction(_ sender: UIButton) {
        
        NetworkingHandle.fetchNetworkData(url: "/Member/referee", at: self, params: ["phone":self.phoneTF.text!], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
            ProgressHUD.showNoticeOnStatusBar(message: "绑定成功")
            self.navigationController?.popViewController(animated: true)
        }) { 
            
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
