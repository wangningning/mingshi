//
//  SystemNotificationViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/25.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh

class SystemNotificationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    var feedbackArr: [FeedbackModel] = []
    var page = 1
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.tableView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.page = 1
            self.loadData()
        })
        let footer = MJRefreshAutoNormalFooter(refreshingBlock: { [unowned self] in
            self.page += 1
            self.loadData()
            
        })
        footer?.setTitle("", for: .idle)
        footer?.setTitle("", for: .noMoreData)
        self.tableView.mj_footer = footer
        self.tableView.mj_header.beginRefreshing()
        // Do any additional setup after loading the view.
    }
    func loadData(){
        NetworkingHandle.fetchNetworkData(url: "/Member/notice", at: self, params: ["p":self.page], hasHeaderRefresh: self.tableView, success: { (response) in
            let data = response["data"] as! [String:AnyObject]
            let arr = FeedbackModel.modelsWithArray(modelArray: data["data"] as! [[String:AnyObject]]) as! [FeedbackModel]
            if arr.count == 0{
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
            }
            if self.page == 1{
                self.feedbackArr.removeAll()
            }
            self.feedbackArr += arr
            self.tableView.reloadData()

        }) { 
            
        }
       
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "FeedbackTableViewCell") as? FeedBackChangeCell
        if cell == nil {
            
             cell =  FeedBackChangeCell(style: .default, reuseIdentifier: "FeedbackTableViewCell")
//
        }
        cell?.selectionStyle = .none
        cell?.model = self.feedbackArr[indexPath.row]
        return cell!
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.feedbackArr.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        let row = indexPath.row
//        let model = feedbackArr[row]
//        //        let text = model.content as! NSString
//        let text: NSString = NSString(cString: (model.summary?.cString(using: String.Encoding.utf8))!, encoding: String.Encoding.utf8.rawValue)!
//        let sizeTmp: CGSize = CGSize(width: kScreenWidth - 94 - 92, height: 10000)
//        let size = text.boundingRect(with: sizeTmp, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)], context: nil)
        
        
        return  97
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        let model = feedbackArr[row]
        let con = FeedbackInfoViewController()
        con.title = model.title
        con.model = model
        self.navigationController?.pushViewController(con, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
