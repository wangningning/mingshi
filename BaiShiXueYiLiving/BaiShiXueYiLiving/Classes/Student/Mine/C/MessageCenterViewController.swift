//
//  MessageCenterViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
var serviceRD = true
var messageRD = true
var notificationRD = true

class MessageVCModel: NSObject {
    var title: String
    var headImg: String
    var vc: UIViewController.Type
    var content: String
    var isHidden: Bool
    init(title: String, headImg:String, vc: UIViewController.Type, content: String, isHidden: Bool) {
        
        self.title = title
        self.headImg = headImg
        self.vc = vc
        self.content = content
        self.isHidden = isHidden
    }
}
class MessageCenterViewController: BSBaseViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableview: UITableView!
    
    var content1: String! = ""
    var content2: String! = ""
    var content4: String! = ""
    
    var dataArr: [[MessageVCModel]] = [[]]
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "消息中心"
        self.tableview.register(UINib(nibName:"MessageCenterCell",bundle:Bundle.main), forCellReuseIdentifier: "MessageCenterCell")
        
        self.getContent()
        
                self.tableview.separatorStyle = .none
        // Do any additional setup after loading the view.
    }
    func getContent(){
        if !serviceRD {
            let arr = EMClient.shared().chatManager.getAllConversations() as! [EMConversation]
            for m in arr{
                if m.type == EMConversationTypeChat, m.conversationId == UserDefaults.standard.value(forKey: "service") as! String {
                    let type = m.latestMessage.body.type
                    if type == EMMessageBodyTypeText {
                        let messageBody = m.latestMessage.body as! EMTextMessageBody
//                        messageBody.text
                        self.content1 = "点击查看您与客服的沟通记录"
                    }
                    
                }
            }
        }
        
        if !messageRD {
            content2 = "您收到新的私信消息"
        }
        if !notificationRD {
            content4 = "您收到新的系统通知"
        }

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArr[section].count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "MessageCenterCell") as! MessageCenterCell
        cell.headImg.image = UIImage(named: self.dataArr[indexPath.section][indexPath.row].headImg)
        cell.title.text = self.dataArr[indexPath.section][indexPath.row].title
        cell.badge.isHidden = self.dataArr[indexPath.section][indexPath.row].isHidden
        cell.content.text = self.dataArr[indexPath.section][indexPath.row].content
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 61
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if  indexPath.row == 0 {
            serviceRD = true
            ContactUsViewController.contactCustomerServiceVC(from: self)
            return
        }
        if indexPath.row == 1 {
            messageRD = true
        }
        if indexPath.row == 2 {
            notificationRD = true
        }
        self.tableview.reloadData()
        let model = self.dataArr[indexPath.section][indexPath.row]
        let vc = model.vc.init()
        vc.title = model.title
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.confirmNewMessage()
    }
    func confirmNewMessage(){
        //私信 客服是否有新的消息
        let conversation = EMClient.shared().chatManager.getAllConversations() as! [EMConversation]
        for m in conversation {
            if m.type == EMConversationTypeChat, m.conversationId == UserDefaults.standard.value(forKey: "service") as! String, m.unreadMessagesCount != 0{
                serviceRD = false
            }else{
                serviceRD = true
            }
            if m.type == EMConversationTypeChat, m.conversationId != UserDefaults.standard.value(forKey: "service") as? String, m.unreadMessagesCount != 0{
                messageRD = false
            }else{
                messageRD = true
            }
        }
        NetworkingHandle.fetchNetworkData(url: "/member/has_read_message", at: self, success: { (response) in
            let data = response["data"] as! String
            if data == "1"{
                notificationRD = false
            }else{
                notificationRD = true
            }
            self.setData()
            self.tableview.reloadData()
        })
        
    }
    func setData(){
        self.dataArr.removeAll()
        self.dataArr = [[MessageVCModel(title:"客服",headImg:"xxzhx_kefu",vc:ContactUsViewController.self,content:content1,isHidden:serviceRD),MessageVCModel(title:"私信",headImg:"xxzhx_pinglun",vc:PrivateMessageViewController.self,content:content2,isHidden:messageRD),MessageVCModel(title:"系统公告",headImg:"xxzhx_xttzh",vc:SystemNotificationViewController.self,content:content4,isHidden:notificationRD)]]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
