//
//  MineViewController.swift
//  BaiShiXueYiLiving
//
//  Created by 梁毅 on 2017/5/8.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
var isAuthen = false
let UserInfoChangeNotifcation = "UserInfoChangeNotifcation"//用户信息发生变化

class VCModel: NSObject {
    var itemImg: String
    var vc: UIViewController.Type
    
    
    init(itemImg: String, vc: UIViewController.Type) {
        self.itemImg = itemImg
        self.vc = vc
    }
}
class MineViewController: BSBaseViewController,UICollectionViewDataSource,UICollectionViewDelegate, UINavigationControllerDelegate{

    var dataArr: [VCModel] = []
    var personInfo : PersonInfoModel!
    var headView : CollectionReusableView!
    var hasRead = "2"
    @IBOutlet weak var layout: CustomCollectionViewFlowLayout!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.delegate = self
       
        NotificationCenter.default.addObserver(self, selector: #selector(MineViewController.userInfoChangeNotifcation(notification:)), name: NSNotification.Name(rawValue: UserInfoChangeNotifcation), object: nil)
        
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        layout.sectionInset = UIEdgeInsets(top: 5, left: 0, bottom: 14, right: 0)
        
        layout.headerReferenceSize = CGSize(width:kScreenWidth, height: 200/375.0 * kScreenWidth + 110)
        let w = (kScreenWidth - 2 ) / 3
        layout.itemSize = CGSize(width: w, height: 115/125.0 * w)
        
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(UINib(nibName:"CollectionReusableView",bundle:Bundle.main), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "CollectionReusableView")
        collectionView.register(UINib(nibName:"CollectionHeaderViewCell",bundle:Bundle.main), forCellWithReuseIdentifier: "CollectionHeaderViewCell")
        if DLUserInfoHandler.getUserInfo()?.type == "1" {
            let versionKey = String(kCFBundleVersionKey)
            let currentVersion = Bundle.main.infoDictionary?[versionKey] as! String
            print(" 当前版本号为：\(currentVersion)")
            let param = ["version": currentVersion]
            NetworkingHandle.fetchNetworkData(url: "/Member/is_this", at: self, params: param,isShowHUD: false, success: { (response) in
                let result = response["data"] as! String
                if result == "1"{
                    self.dataArr = [VCModel(itemImg: "icon_dangan", vc: TeacherListViewController.self),
                                    VCModel(itemImg: "icon_zhanghu", vc: AccountViewController.self),
                                    VCModel(itemImg: "icon_xianchang", vc: JoinSceneViewController.self),
//                                    VCModel(itemImg: "icon_shoucang", vc: GoodsCollectViewController.self),
//                                    VCModel(itemImg: "icon_address", vc: AddressListViewController.self),
                                    VCModel(itemImg: "tuijianren-1", vc: ReferrerViewController.self),
                                    VCModel(itemImg: "icon_bangzhu-", vc: HelpViewController.self),
                                    VCModel(itemImg: "icon_shezhi", vc: SettingViewController.self)]
                    
                }else{
                    self.dataArr = [VCModel(itemImg: "icon_dangan", vc: TeacherListViewController.self),
                                    VCModel(itemImg: "icon_zhanghu", vc: AccountViewController.self),
                                    VCModel(itemImg: "icon_xianchang", vc: JoinSceneViewController.self),
//                                    VCModel(itemImg: "icon_shoucang", vc: GoodsCollectViewController.self),
//                                    VCModel(itemImg: "icon_address", vc: AddressListViewController.self),
                                    VCModel(itemImg: "icon_shengji", vc: MembershipViewController.self),
                                    VCModel(itemImg: "tuijianren-1", vc: ReferrerViewController.self),
                                    VCModel(itemImg: "icon_bangzhu-", vc: HelpViewController.self),
                                    VCModel(itemImg: "icon_shezhi", vc: SettingViewController.self), VCModel(itemImg: "ic_tiezi", vc: MyPostsViewController.self)]
                }
                self.collectionView.reloadData()
            }, failure: {
                
            })
            
        }else{
            self.dataArr = [VCModel(itemImg: "icon_dangan", vc: TeacherListViewController.self),
                            VCModel(itemImg: "icon_zhanghu", vc: AccountViewController.self),
//                            VCModel(itemImg: "icon_xianchang", vc: JoinSceneViewController.self),
//                            VCModel(itemImg: "icon_shoucang", vc: GoodsCollectViewController.self),
//                            VCModel(itemImg: "icon_address", vc: AddressListViewController.self),
                            VCModel(itemImg: "icon_shengji", vc: MembershipViewController.self),
                            VCModel(itemImg: "tuijianren-1", vc: ReferrerViewController.self),
                            VCModel(itemImg: "icon_bangzhu-", vc: HelpViewController.self),
                            VCModel(itemImg: "icon_shezhi", vc: SettingViewController.self), VCModel(itemImg: "ic_tiezi", vc: MyPostsViewController.self),
                            VCModel(itemImg: "icon_qiehuan-1", vc: TMineViewController.self)]
            self.collectionView.reloadData()
            
        }
        
        
        
    }
    func userInfoChangeNotifcation(notification: Notification) {
        if notification.object == nil{
            self.loadData()
        } else {
            let personModel = notification.object as? PersonInfoModel
            self.headView.model = personModel
            self.collectionView.reloadData()
        }
        
    }

    func loadData(){
        NetworkingHandle.fetchNetworkData(url: "/Member/index", at: self, isShowHUD: false, success: { (response) in
            
            let data = response["data"] as! [String:AnyObject]
            self.personInfo = PersonInfoModel.modelWithDictionary(diction: data)
            if self.personInfo.type != DLUserInfoHandler.getUserInfo()?.type{
                ProgressHUD.showMessage(message: "身份发生改变，请重新登录")
                UIApplication.shared.keyWindow?.rootViewController = LoginNavigationController.setup()
                return
            }
            DLUserInfoHandler.update(name: self.personInfo.username, img: self.personInfo.img, grade: "")
            self.headView.model = self.personInfo
            self.collectionView.reloadData()
        })
        
        NetworkingHandle.fetchNetworkData(url: "/member/has_read_message", at: self, success:{ (response) in
            let data = response["data"] as! String
            self.hasRead = data
            self.collectionView.reloadData()
        })
    }
    func rightBarButtonItemAction() {
        
        self.navigationController?.pushViewController(MessageCenterViewController(), animated: true)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataArr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionHeaderViewCell", for: indexPath) as! CollectionHeaderViewCell
        cell.itemImg.image = UIImage(named:self.dataArr[indexPath.row].itemImg)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        self.headView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CollectionReusableView", for: indexPath) as! CollectionReusableView
        self.headView.hasRead = hasRead
        return self.headView
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.dataArr[indexPath.row].vc.init()
        let model = self.dataArr[indexPath.row]
        if model.vc == TMineViewController.self {
            isTeacher = true
            let tabar = BaseTabbarController()
            UIApplication.shared.keyWindow?.rootViewController = tabar
            return
        }
        if model.vc == FocusOnCourseViewController.self{
            if DLUserInfoHandler.getUserInfo()?.type == "2" {
                ProgressHUD.showMessage(message: "名师身份暂无学习档案")
                return
            }
        }
        if model.vc == ReferrerViewController.self {
            
            NetworkingHandle.fetchNetworkData(url: "/Member/show_referee", at: self, success: { (response) in
                let vc = ReferrerViewController()
                vc.referrerPhone = response["data"] as! String
                self.navigationController?.pushViewController(vc, animated: true)
                
            })
            return
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.automaticallyAdjustsScrollViewInsets = false
        self.loadData()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.automaticallyAdjustsScrollViewInsets = true
    }
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        
        let isShow = viewController.isKind(of: MineViewController.self)
        
        self.navigationController?.setNavigationBarHidden(isShow, animated: true)
        
    }
        

}
