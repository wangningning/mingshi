

//
//  HisPersonalMemberCenterViewController.swift
//  DragonVein
//
//  Created by 梁毅 on 2017/3/28.
//  Copyright © 2017年 tts. All rights reserved.
//

import UIKit
import MJRefresh
class HisPersonalMemberCenterViewController: DLBaseViewController,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,UINavigationControllerDelegate{
    
    @IBOutlet weak var messageBtn: UIButton!
    @IBOutlet weak var focusOnBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var layout: UICollectionViewFlowLayout!
    
    @IBOutlet weak var line: UILabel!
    
    @IBOutlet weak var upgradeBtn: UIButton!
    var userId: String?
    var videoArr = [VideoModel]()
    var live_storeArr = [live_store]()
    var hisInfo = HisInfo()
    var liveModel = LiveList()
    var countStr: String?
    var dataArr: [FollowListModel] = []
    var attentionArr: [FollowListModel] = []
    var isHasLive: Bool?
    var isCurrentUserLiving: Bool?
    
    var myLiveList = [MyLiveList]()
    var type : Int!
   
    var sFocusBtn = UIButton.init(type: .custom)
    var tUpdateGradeBtn = UIButton.init(type: .custom)
    var result = ""
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
        if #available(iOS 11, *) {
            self.collectionView.contentInsetAdjustmentBehavior = .never
        }else{
            self.automaticallyAdjustsScrollViewInsets = false
        }
        self.navigationController?.navigationBar.isHidden = true
       
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11, *){
            self.collectionView.contentInsetAdjustmentBehavior = .always
        }else{
            self.automaticallyAdjustsScrollViewInsets = true
        }
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        collectionView.register(UINib(nibName: "HisPersonalMemberCenterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HisPersonalMemberCenterCollectionViewCell")
        collectionView.register(UINib(nibName: "HisPersonalMemberFanCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HisPersonalMemberFanCollectionViewCell")
        collectionView.register(UINib(nibName: "HisPersonalMemberCenterCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HisPersonalMemberCenterCollectionReusableView")

        collectionView.register(UINib.init(nibName: "PhotoIndex1CollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "PhotoIndex1CollectionReusableView")
        
        let versionKey = String(kCFBundleVersionKey)
        let currentVersion = Bundle.main.infoDictionary?[versionKey] as! String
        print(" 当前版本号为：\(currentVersion)")
        let param = ["version": currentVersion]
        NetworkingHandle.fetchNetworkData(url: "/Member/is_this", at: self, params: param, isShowHUD: false, success: { (response) in
            self.result = response["data"] as! String
            if self.result == "2"{
                self.upgradeBtn.setTitle("会员升级", for: .normal)
            }
            self.reloadOtherPersonCenterData()
        }) {
            
        }

        
        
        
        collectionView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.page = 1
            guard self.type != nil else{
                return
            }
            self.loadData(type: self.type)
            
        })
        let footer = MJRefreshAutoNormalFooter(refreshingBlock: { [unowned self] in
            self.page += 1
            self.loadData(type: self.type)
        })
        footer?.setTitle("没有更多了", for: .noMoreData)
        collectionView.mj_footer = footer
    }
    
    func reloadOtherPersonCenterData() {
        let param = ["user_id": userId!]
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Home&a=tutor_detail", at: self, params: param, isShowHUD: true,hasHeaderRefresh: collectionView, success: { (response) in
            
            let data = response["data"] as! [String: AnyObject]
            self.hisInfo = HisInfo.modelWithDictionary(diction: data)
            
            //学生
            if self.hisInfo.type == "1"{
                
                self.type = 2
                self.layout.itemSize = CGSize(width: kScreenWidth, height: 81)
                self.updateBottomView()
                
            }else{
                
                self.type = 0
                if DLUserInfoHandler.getUserInfo()?.type == "2"{
                    self.updateBottomView()
                }
                self.layout.itemSize = CGSize(width: kScreenWidth, height: 41)
                if self.hisInfo.is_live == "1"{
                    
                    self.liveModel = LiveList.modelWithDictionary(diction: data["live"] as! [String : AnyObject])
                }
            }
            self.collectionView.mj_header.beginRefreshing()

           
            
            if  self.hisInfo.is_follow == "2" {
                self.focusOnBtn.isSelected = false
                self.sFocusBtn.isSelected = false
            }else {
                self.focusOnBtn.isSelected = true
                self.sFocusBtn.isSelected = true
            }
            self.collectionView.reloadData()
        }) {
        }

    }
    func updateBottomView(){
        
        self.line.isHidden = true
        self.messageBtn.isHidden = true
        self.focusOnBtn.isHidden = true
        self.upgradeBtn.isHidden = true
        self.messageBtn.isEnabled = false
        self.focusOnBtn.isEnabled = false
        self.upgradeBtn.isEnabled = false
        
        
        
        
        
        self.sFocusBtn.setTitle("关注", for: .normal)
        self.sFocusBtn.setTitle("已关注", for: .selected)
        self.sFocusBtn.setTitleColor(UIColor(hexString:"#ec6b1a"), for: .normal)
        self.sFocusBtn.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        self.sFocusBtn.backgroundColor = UIColor.white
        self.sFocusBtn.addTarget(self, action: #selector(HisPersonalMemberCenterViewController.FocusBtnClicked(_:)), for: .touchUpInside)
        self.view.addSubview(self.sFocusBtn)
        
        if self.hisInfo.type == "1"{
            self.sFocusBtn.snp.makeConstraints { (make) in
                make.bottom.equalTo(0)
                make.height.equalTo(40)
                make.left.equalTo(0)
                make.width.equalTo(kScreenWidth)
            }
            
        } else if DLUserInfoHandler.getUserInfo()?.type == "2" && self.hisInfo.type == "2"{
            
            if self.result == "1"{
                self.tUpdateGradeBtn.setTitle("点赞", for:.normal)
            }else{
                self.tUpdateGradeBtn.setTitle("会员升级", for: .normal)
                
            }
            self.tUpdateGradeBtn.setTitleColor(UIColor.white, for: .normal)
            self.tUpdateGradeBtn.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            self.tUpdateGradeBtn.backgroundColor = UIColor(hexString: "#ec6b1a")
            self.tUpdateGradeBtn.addTarget(self, action: #selector(HisPersonalMemberCenterViewController.upgradeMemeber(_:)), for: .touchUpInside)
            self.view.addSubview(self.tUpdateGradeBtn)
            self.sFocusBtn.snp.makeConstraints { (make) in
                make.bottom.equalTo(0)
                make.height.equalTo(40)
                make.left.equalTo(0)
                make.width.equalTo(kScreenWidth/2)
            }
            self.tUpdateGradeBtn.snp.makeConstraints({ (make) in
                make.bottom.equalTo(0)
                make.height.equalTo(40)
                make.left.equalTo(self.sFocusBtn.snp.right)
                make.width.equalTo(kScreenWidth/2)
            })
            

        }
        
        
        
    }
    

    func loadData(type: Int) {
        
        func LoadFocusData() {
            let params = ["user_id":userId!,"p":page] as [String : Any]
            var url = ""
            if type == 1{
                url = "/Member/user_fans"
            } else if type == 2{
                url = "/Member/user_follow"
            }
            NetworkingHandle.fetchNetworkData(url: url, at: self, params: params, hasHeaderRefresh: collectionView, success: { (response) in
                let data = response["data"] as! [String:AnyObject]
                let list = FollowListModel.modelsWithArray(modelArray: data["list"] as! [[String : AnyObject]]) as! [FollowListModel]
                if list.count == 0{
                    self.collectionView.mj_footer.endRefreshingWithNoMoreData()
                }
                if type == 1{
                    if self.page == 1 {
                        self.dataArr = list
                    } else {
                        self.dataArr += list
                    }
                } else if type == 2 {
                    if self.page == 1 {
                        self.attentionArr = list
                    } else {
                        self.attentionArr += list
                    }
                }
                
                self.collectionView.reloadData()
            }) {
                
            }
        }
        
        if  type == 0 {
            let params = ["user_id":userId!,"p":page] as [String : Any]
            NetworkingHandle.fetchNetworkData(url: "/Home/live_store", at: self, params: params, hasHeaderRefresh: collectionView, success: { (response) in
                let data = response["data"] as! [String: AnyObject]
                let dataArr = MyLiveList.modelsWithArray(modelArray: data["list"] as! [[String:AnyObject]]) as! [MyLiveList]
                if self.page == 1 {
                    self.myLiveList.removeAll()
                }
                if dataArr.count == 0 {
                    self.collectionView.mj_footer.endRefreshingWithNoMoreData()
                }
                self.myLiveList += dataArr
                
                self.collectionView.reloadData()
            }) {
                if self.page > 1 {
                    self.page -= 1
                }
            }
            
        }else if type == 1 {
            LoadFocusData()
        }else if type == 2{
            LoadFocusData()
        }else {
            let params = ["user_id":userId!,"p":page] as [String : Any]
            NetworkingHandle.fetchNetworkData(url: "/Home/tutor_video", at: self, params: params, hasHeaderRefresh: collectionView, success: { (response) in
                let data = response["data"] as! [String: AnyObject]
                let datas = data["list"] as! [[String: AnyObject]]
                let list = VideoModel.modelsWithArray(modelArray: datas) as! [VideoModel]
                if self.page == 1 {
                    self.videoArr.removeAll()
                }
                if list.count == 0 {
                    self.collectionView.mj_footer.endRefreshingWithNoMoreData()
                }
                
                self.videoArr += list
                if  self.videoArr.count == 0 {
                    
                } else if self.videoArr.count == 1 {
                    
                } else {
                    
                }
                self.collectionView.reloadData()
            }) {
                if self.page > 1 {
                    self.page -= 1
                }
            }

        }
        
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if type == nil{
            return 0
        }else{
            if  type == 3 || type == 0 {
                return 2
            }else {
                return 1
            }  
        }
        
        
        
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if indexPath.section == 0 {
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HisPersonalMemberCenterCollectionReusableView", for: indexPath) as! HisPersonalMemberCenterCollectionReusableView
            view.vcPassed = self
            view.clickedIndex = { indexes in
                self.layout.minimumLineSpacing = 0
                self.layout.minimumInteritemSpacing = 0
                if indexes == 3 || indexes == 0 {
                    let itemW = kScreenWidth
                    self.layout.itemSize = CGSize(width: itemW, height: 41)
                }else {
                    self.layout.itemSize = CGSize(width: kScreenWidth, height: 81)
                }
                self.page = 1
                self.collectionView.reloadData()
                self.type = indexes
                self.loadData(type: self.type)
            }
            if hisInfo.user_id != nil {
                view.hisInfo = hisInfo
                view.liveModel = liveModel
            }
            return view
            
        } else {
            
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "PhotoIndex1CollectionReusableView", for: indexPath) as! PhotoIndex1CollectionReusableView
            if hisInfo.user_id != nil{
                if type == 0{
                    view.dateLabel.text = hisInfo.live_count! + "个精彩回放"
                }else if type == 3{
                    view.dateLabel.text = hisInfo.video_count! + "个免费视频"
                }
            }
            return view
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if type == 3 || type == 0 {
            if section == 0 {
                
                return CGSize(width: kScreenWidth, height: 127.0/125 * kScreenWidth + 46*1 + 12 + 50)
            }
            return CGSize(width: kScreenWidth, height: 36)
        }
        if self.hisInfo.type == "1"{
            
             return CGSize(width: kScreenWidth, height: 127.0/125 * kScreenWidth + 12 + 50)
        }else{
            return CGSize(width: kScreenWidth, height: 127.0/125 * kScreenWidth + 46 * 1 + 12 + 50)
        }
       
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if  type == 0 {
            if section == 0{
                return 0
            }
            return myLiveList.count
        }else if type == 3 {
            if section == 0 {
                return 0
            }
            return videoArr.count
        }
        else if type == 1{
           
            return dataArr.count
            
        } else {
            
            return attentionArr.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if  type == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HisPersonalMemberCenterCollectionViewCell", for: indexPath) as! HisPersonalMemberCenterCollectionViewCell
            cell.myLiveList = myLiveList[indexPath.row]
            return cell
        }else if type == 3 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HisPersonalMemberCenterCollectionViewCell", for: indexPath) as! HisPersonalMemberCenterCollectionViewCell
            cell.video = videoArr[indexPath.row]
            return cell
        }else if type == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HisPersonalMemberFanCollectionViewCell", for: indexPath) as! HisPersonalMemberFanCollectionViewCell
            cell.model = dataArr[indexPath.row]
            return cell
        } else  {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HisPersonalMemberFanCollectionViewCell", for: indexPath) as! HisPersonalMemberFanCollectionViewCell
            cell.model = attentionArr[indexPath.row]
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.type == 0 {
            print("直播直播")
            
            if  isHasLive == true {
                if isCurrentUserLiving == true {
                    ProgressHUD.showMessage(message: "您当前正在开直播，请关闭直播后再进行观看")
                    return
                }
                ProgressHUD.showMessage(message: "您当前正在观看直播，请关闭后再进行观看")
                return
            }
            if self.result == "1"{
                let vc = PlayVideoViewController()
                vc.username = myLiveList[indexPath.row].title
                vc.play_img = myLiveList[indexPath.row].play_img
                vc.url = myLiveList[indexPath.row].url
                vc.live_store_id = myLiveList[indexPath.row].live_id
                vc.share_url = myLiveList[indexPath.row].share_url
                self.navigationController?.pushViewController(vc, animated: true)
                return
            }
//            if self.hisInfo.is_vip == "1"{
//                LyAlertView.alert(message: "您还不是该导师的会员，升级为会员后才可观看", ok: "升级", okBlock: {
//
//                    let vc = MembershipDetailViewController()
//                    vc.teacherID = self.userId
//                    self.navigationController?.pushViewController(vc, animated: true)
//
//                })
//            }else{
                let vc = PlayVideoViewController()
                vc.username = myLiveList[indexPath.row].title
                vc.play_img = myLiveList[indexPath.row].play_img
                vc.url = myLiveList[indexPath.row].url
                vc.live_store_id = myLiveList[indexPath.row].live_id
                vc.share_url = myLiveList[indexPath.row].share_url
                self.navigationController?.pushViewController(vc, animated: true)
                return
//            }
            
        } else if type == 3 {
            let vc = TCurriculumDetailViewController()
            vc.videoID = videoArr[indexPath.row].video_id
            vc.play_img = videoArr[indexPath.row].video_img
            vc.url = videoArr[indexPath.row].url
            self.navigationController?.pushViewController(vc, animated: true)

        }else {
            if type == 1{
                pushToUserInfoCenter(atViewController: self, uId: dataArr[indexPath.row].user_id!)
            } else {
                pushToUserInfoCenter(atViewController: self, uId: attentionArr[indexPath.row].user_id!)
            }
            
        }
    }
    @IBAction func FocusBtnClicked(_ sender: UIButton) {
        focusOtherPerson(viewResponder: self, other_id: hisInfo.user_id!, btn: sender, type: hisInfo.is_follow!) { 
            if self.hisInfo.is_follow == "1" {
                
                self.hisInfo.is_follow = "2"
                
            } else {
                
                self.hisInfo.is_follow = "1"
            }
            self.reloadOtherPersonCenterData()
        }
    }
    @IBAction func messageClicked(_ sender: UIButton) {
        
        
            if result == "1"{
                let vc = DirectChatViewController(conversationChatter: self.hisInfo.hx_username, conversationType: EMConversationTypeChat)
                vc?.userId = self.hisInfo.user_id
                vc?.usernameTHEY = self.hisInfo.username
                vc?.img = self.hisInfo.img
                vc?.hx_username = self.hisInfo.hx_username
                vc?.isAudit = true
                self.navigationController?.pushViewController(vc!, animated: true)
            }else{
                if DLUserInfoHandler.getUserInfo()?.type == "2"{
                    
                    ProgressHUD.showMessage(message: "抱歉，名师身份没有开启指点的权限")
                    return
                }
                NetworkingHandle.fetchNetworkData(url: "/Home/check_teach", at: self, params: ["user_id":self.hisInfo.user_id!], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
                    let data = response["data"] as! String
                    if data == "1"{
                        let vc = DirectChatViewController(conversationChatter: self.hisInfo.hx_username, conversationType: EMConversationTypeChat)
                        vc?.userId = self.hisInfo.user_id
                        vc?.usernameTHEY = self.hisInfo.username
                        vc?.img = self.hisInfo.img
                        vc?.isCustomNav = false
                        vc?.isTeacher = false
                        vc?.hx_username = self.hisInfo.hx_username
                        self.navigationController?.pushViewController(vc!, animated: true)
                        
                    }else if data == "2"{
                        NetworkingHandle.fetchNetworkData(url: "/Home/teach_fee", at: self, params: ["user_id": self.hisInfo.user_id!], success: { (response) in
                            let data = response["data"] as! String
                            NetworkingHandle.fetchNetworkData(url: "/Home/setup_teach_order", at: self, params: ["user_id": self.hisInfo.user_id!], isShowHUD: true, isShowError: true, success: { (response) in
                                ProgressHUD.showMessage(message: "本次指点需要付费")
                                let vc = SelectPayWayViewController()
                                vc.order_no = response["data"] as? String
                                vc.payType = "2"
                                vc.userId = self.hisInfo.user_id
                                vc.usernameTHEY = self.hisInfo.username
                                vc.img = self.hisInfo.img
                                vc.hx_username = self.hisInfo.hx_username
                                vc.amount = data
                                
                                self.navigationController?.pushViewController(vc, animated: true)
                            }, failure: {
                                
                            })
                            
                        })
                    } else if data == "3" {
                        
                        let chatController = DirectChatViewController(conversationChatter: self.hisInfo.hx_username, conversationType: EMConversationTypeChat)
                        chatController?.img = self.hisInfo.img
                        chatController?.usernameTHEY = self.hisInfo.username
                        chatController?.userId = self.hisInfo.user_id
                        chatController?.hx_username = self.hisInfo.hx_username
                        chatController?.isTeachBefore = true
                        chatController?.state = "2"
                        self.navigationController?.pushViewController(chatController!, animated: true)
                        
                    }
                    
                }, failure: {
                    
                })

                
            }
        
    }
    
    @IBAction func upgradeMemeber(_ sender: UIButton) {
        if self.result == "1"{
            ProgressHUD.showSuccess(message: "点赞成功")
        }else{
            let vc = MembershipDetailViewController()
            vc.teacherID = self.hisInfo.user_id
            self.navigationController?.pushViewController(vc, animated: true)

        }
    }
}
