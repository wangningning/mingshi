//
//  CheckLogisticsViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/6.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import Alamofire

class CheckLogisticsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var tableView: UITableView!
    var kuaidi_node: String?
    var kuaidi: String?
    var kuaidiName: String?
    var kuaidistate: String?
    var model: LogisticsModel!
    var tracesArr: [TracesModel] = []
    var order_number: String!
    var goods_number: String!
    var header: CheckLogisticsView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "查看物流"
       
        
        self.tableView.register(UINib(nibName:"CheckLogisticsTableViewCell",bundle:nil), forCellReuseIdentifier: "CheckLogisticsTableViewCell")
        self.tableView.register(UINib.init(nibName: "CheckLogisticsSectionHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: CheckLogisticsSectionHeaderView.description())
        self.tableView.tableFooterView = UIView()
        self.loadData()
        // Do any additional setup after loading the view.
    }
    func loadData(){
        
        ProgressHUD.showLoading(toView: self.view)
        if self.kuaidi == ""{
            
            self.kuaidi = "YTO"
            self.kuaidi_node = "884926968025883403"
            
        }
        NetworkingHandle.fetchNetworkData(url: "/KdApi/getTracesByJson", at: self, params: ["kuaidi": self.kuaidi ?? "YTO","kuaidi_node": self.kuaidi_node ?? "884926968025883403"], isShowHUD: true, isShowError: true, hasHeaderRefresh: self.tableView, success: { (response) in
            
            ProgressHUD.hideLoading(toView: self.view)
            let data = response["data"] as! [String:AnyObject]
            self.model = LogisticsModel.modelWithDictionary(diction: data)
            self.tracesArr += self.model!.Traces!
            if self.tracesArr.count > 0{
                self.tracesArr.last?.isCurrentState = true
                
            }
            self.tableView.reloadData()

        }) {
            
        }
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckLogisticsTableViewCell") as! CheckLogisticsTableViewCell
        cell.model = self.tracesArr[self.tracesArr.count - indexPath.row - 1]
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tracesArr.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: CheckLogisticsSectionHeaderView.description()) as! CheckLogisticsSectionHeaderView
       
        if self.model != nil && self.kuaidiName != nil && self.kuaidistate != nil{
            header.kuaidiNode = self.model.LogisticCode
            header.kuaidiName = self.kuaidiName
            header.kuaidi_state = self.kuaidistate

        }
        return header
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 95
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
