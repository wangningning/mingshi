//
//  ChangeSexViewController.swift
//  MoDuLiving
//
//  Created by 曾觉新 on 2017/3/6.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class ChangeSexViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var model: PersonInfoModel!
     var successSubmit:(()->())?
    
    @IBOutlet weak var tableView: UITableView!
    
    let titles = ["男", "女"]
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "性别"
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "cell")
        
        cell.selectionStyle = .none
        cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
        cell.textLabel?.textColor = UIColor(hexString: "#222222")
        cell.textLabel?.text = titles[indexPath.row]
        
        if (model?.sex == "1" && indexPath.row == 0) || (model?.sex == "2" && indexPath.row == 1) {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIAlertController(title:"提示", message:"您选择了"+"\(titles[indexPath.row])生，"+"确定吗?",preferredStyle:.alert)
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        let confirm = UIAlertAction(title: "确定", style: .default){(alert) in
            self.changeSex(sex: "\(indexPath.row + 1)")
        }
        
        
        vc.addAction(cancel)
        vc.addAction(confirm)
        
        self.present(vc, animated: true, completion: nil)

    }
    
    
    //MARK: 
    func changeSex(sex: String){
        
        let params = ["autograph" : self.model!.autograph,"username":self.model!.username!,"phone":self.model!.phone!,"sex":sex] as! [String : String]
        NetworkingHandle.fetchNetworkData(url: "/Member/change_user", at: self, params: params, success: { (response) in
            self.model?.sex = sex
            self.successSubmit?()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: UserInfoChangeNotifcation), object: self.model);
            _ = self.navigationController?.popViewController(animated: true)
        }) { 
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
