//
//  BBSCommentCell.swift
//  FKDCClient
//
//  Created by 曾觉新 on 2017/2/26.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class BBSCommentCell: UITableViewCell {

    var nameLabel: UILabel!
    var commentLabel: UILabel!
    var customSize: CGSize!
    private var _model: BBSResponseModel!
    var model: BBSResponseModel! {
        willSet(m){
            
            let a = m.content?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
            let  string = "\(m.username!)：\(a!)"
           
            let attrstring:NSMutableAttributedString = NSMutableAttributedString(string:string)
            attrstring.addAttribute(NSForegroundColorAttributeName, value: themeColor, range: NSRange.init(location: 0, length: (m.username?.characters.count)! + 1))
            
            
            commentLabel.attributedText = attrstring

        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        commentLabel.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.left.equalTo(0)
            make.right.equalTo(0)
        }

       
        
    }
    
    
    
    func initViews() {
       
        
        commentLabel = UILabel()
        commentLabel.font = UIFont.systemFont(ofSize: 13)
        commentLabel.textColor = UIColor(hexString: "#727171")
        commentLabel.numberOfLines = 0
        commentLabel.lineBreakMode = .byCharWrapping
        self.contentView.addSubview(commentLabel)
        
        
    }
    func getCellheight() -> CGFloat{
        
        super.layoutIfNeeded()
        commentLabel.superview?.layoutIfNeeded()
        return commentLabel.height + commentLabel.frame.origin.y + 13
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
