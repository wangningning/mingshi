//
//  BBSDetailsReplyView.swift
//  FKDCClient
//
//  Created by 曾觉新 on 2017/2/26.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit


protocol BBSDetailsReplyViewDelegate {
    func detailsReplyView(replyView: BBSDetailsReplyView, index: Int)
}

class BBSDetailsReplyView: UIView {
    
    var replyCountButton: UIButton!
    var likeCountButton: UIButton!
    
    var commentButton: UILabel!
    
    
    var myDelegate: BBSDetailsReplyViewDelegate?
    
    private var _model: BBSListModel!
    var model: BBSListModel! {
        willSet(m){
            replyCountButton.setTitle(" " + m.ping!, for: .normal)
            
            likeCountButton.setTitle(" " + m.zan!, for: .normal)
            
            if m.is_zan == "1"{
                likeCountButton.setImage(#imageLiteral(resourceName: "shc_xiao_shc"), for: .normal)
            }else {
                likeCountButton.setImage(#imageLiteral(resourceName: "shc_xiao_wshc"), for: .normal)
            }
        }

    }
    var tmodel: TCurriculumDetailModel!{
        willSet(m){
            
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor(hexString: "#f4f4f4")
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        replyCountButton.snp.makeConstraints { (mark) in
            mark.centerY.equalTo(self.snp.centerY)
            mark.left.equalTo(self.snp.right).offset(-115)
        }
        
        likeCountButton.snp.makeConstraints { (mark) in
            mark.centerY.equalTo(self.snp.centerY)
            mark.left.equalTo(self.snp.right).offset(-57)
        }
        
        
        
        commentButton.snp.makeConstraints { (mark) in
            mark.top.equalTo(0)
            mark.bottom.equalTo(0)
            mark.left.equalTo(25)
//            mark.width.equalTo(110)
            mark.right.equalTo(self.replyCountButton.snp.left)
        }
    }
    
    func initViews() {
        
        replyCountButton = UIButton()
        replyCountButton.tag = 2
        replyCountButton.setTitleColor(UIColor(hexString: "#333333"), for: .normal)
        replyCountButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        replyCountButton.addTarget(self, action: #selector(clickButton(sender:)), for: .touchUpInside)
        replyCountButton.setImage(UIImage(named: "yylt_pl"), for: .normal)
        self.addSubview(replyCountButton)
        
        likeCountButton = UIButton()
        likeCountButton.tag = 3
        likeCountButton.setTitleColor(UIColor(hexString: "#333333"), for: .normal)
      //  likeCountButton.setImage(UIImage(named: "shc_xiao_wshc"), for: .normal)
        likeCountButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        likeCountButton.addTarget(self, action: #selector(clickButton(sender:)), for: .touchUpInside)
        self.addSubview(likeCountButton)
        
        commentButton = UILabel()
        commentButton.tag = 1;
        commentButton.text = "说点什么吧"
        commentButton.isUserInteractionEnabled = true
        commentButton.textColor = UIColor(hexString: "#595757")
        commentButton.textAlignment = .left
        let tap = UITapGestureRecognizer(target: self, action: #selector(clickComment(sender:)))
        commentButton.addGestureRecognizer(tap);
//        commentButton.setTitle("说点什么吧", for: .normal)
//        commentButton.setTitleColor(UIColor(hexString: "#595757"), for: .normal)
//        commentButton.titleLabel?.font = UIFont.systemFont(ofSize: 13)
//        commentButton.addTarget(self, action: #selector(clickButton(sender:)), for: .touchUpInside)
        self.addSubview(commentButton)
    }
    
    func clickButton(sender: UIButton) {
        myDelegate?.detailsReplyView(replyView: self, index: sender.tag)
    }
    
    
    func clickComment(sender: UITapGestureRecognizer) {
        myDelegate?.detailsReplyView(replyView: self, index: 1)
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
