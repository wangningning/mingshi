//
//  BBSHeaderView.swift
//  FKDCClient
//
//  Created by 曾觉新 on 2017/2/24.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit


class BBSHeaderView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, CyclePictureViewDelegate{
    let screenProportion = kScreenWidth / 375
    
    var cyclePictureView: CyclePictureView!
    var titleLabel: UILabel!
    var lineView: UIView!
    var clickedItem : ((BBSModuleModel)->())?
    var cycleClickItem:((Int)->())?
    
    //var myDelegate: BBSHeaderViewDelegate?
    
    private var _bannerArr: [BannerModel] = []
    var bannerArr: [BannerModel]! {
        set {
            _bannerArr = newValue
            
            var imageArr: [String] = []
            for bannerModel in _bannerArr {
                imageArr.append(bannerModel.b_img!)
            }
            cyclePictureView.imageURLArray = imageArr
        }
        get {
            return _bannerArr
        }
    }
    
    private var _moduleArr: [BBSModuleModel] = []
    var moduleArr: [BBSModuleModel]! {
        set {
            _moduleArr = newValue
            
            self.myCollectionView.reloadData()
        }
        get {
            return _moduleArr
        }
    }
    
    fileprivate lazy var myCollectionView: UICollectionView = {
        

        let layout = UICollectionViewFlowLayout()
        var w = (kScreenWidth - 24 * 2 - 54 * 2)/3
        if kScreenWidth == 320 {
            w = (kScreenWidth - 10 * 2 - 20 * 2)/3
        }
        layout.itemSize = CGSize(width: w, height:  w)
        print(w)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 54
        if kScreenWidth == 320{
            layout.minimumLineSpacing = 20
        }
        layout.scrollDirection = .horizontal
        
        layout.sectionInset = UIEdgeInsetsMake(0, 24, 0, 24)
        if kScreenWidth == 320{
            layout.sectionInset = UIEdgeInsetsMake(0, 10, 0, 10)
        }
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = UIColor.white
        collectionView.register(BBSModuleCollectionViewCell.self, forCellWithReuseIdentifier: "CollectionViewCell")
        return collectionView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        cyclePictureView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(kScreenWidth * 201/375)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.cyclePictureView.snp.bottom).offset(16)
            make.centerX.equalTo(self.snp.centerX)
        }
        
        myCollectionView.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(self.titleLabel.snp.bottom).offset(21)
            make.height.equalTo((85 * kScreenWidth / 375))
        }
        
        lineView.snp.makeConstraints { (make) in
            make.bottom.equalTo(0)
            make.left.right.equalTo(0)
            make.height.equalTo(10)
        }
        
    }
    
    func initViews() {
        self.backgroundColor = UIColor.white
        cyclePictureView = CyclePictureView(frame: CGRect(), imageURLArray: nil)
        cyclePictureView.delegate = self
        cyclePictureView.autoScroll = true
        cyclePictureView.placeholderImage = #imageLiteral(resourceName: "live_default")
        cyclePictureView.timeInterval = 3.6
        
        cyclePictureView.detailLableBackgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5)
        self.addSubview(cyclePictureView)
        
        titleLabel = UILabel()
        titleLabel.font = UIFont.systemFont(ofSize: 14)
        titleLabel.textColor = UIColor(hexString: "#111111")
        titleLabel.text = "热门话题"
        self.addSubview(titleLabel)
        
        self.addSubview(self.myCollectionView)
        
        lineView = UIView()
        lineView.backgroundColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.00)
        self.addSubview(lineView)
    }
    func cyclePictureView(_ cyclePictureView: CyclePictureView, didSelectItemAtIndexPath indexPath: IndexPath) {
        self.cycleClickItem?(indexPath.row)
    }
    
    //MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! BBSModuleCollectionViewCell
        cell.moduleModle = self.moduleArr[indexPath.row]
        return cell;
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.moduleArr.count;
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = self.moduleArr[indexPath.row]
        self.clickedItem?(model)
        
    }
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
