//
//  BBSListPhotoView.swift
//  FKDCClient
//
//  Created by 曾觉新 on 2017/2/26.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class BBSListPhotoView: UIView {
    
    var viewHeight:CGFloat! = 0
    
    private var _contentImg: [ImgModel] = []
    private var _imgsArr: [BBSImageModel] = []
    var contentImg: [ImgModel]! {
        set{
            _contentImg = newValue
            for v in self.subviews {
                v.removeFromSuperview()
            }
            let itemW = (kScreenWidth - 30 - 10 * 3.0) / 4
            var itemH = (kScreenWidth - 50 - 10 * 3.0) / 4
            if _contentImg.count == 1 {
                let imgW: CGFloat = CGFloat(75)
                
                let imgH: CGFloat = CGFloat(75)
                itemH = imgH / imgW * itemW;
            }
            let butGap: CGFloat = 5
            _contentImg.enumerateKeysAndObjects { (obj, idx) in
                let columnIndex = idx % 4
                let rowIndex = idx / 4
                let imageView = UIImageView(frame: CGRect(x: CGFloat(columnIndex) * (itemW + butGap), y: CGFloat(rowIndex) * (itemH + butGap), width: itemW, height: itemH))
                //imageView.yy_imageURL = URL(string: obj.img!)
                imageView.kf.setImage(with: URL(string: obj.img!))
                imageView.tag = idx
                imageView.isUserInteractionEnabled = true
                self.addSubview(imageView)
                imageView.clipsToBounds = true
                imageView.contentMode = .scaleAspectFill
                self.viewHeight = CGFloat(imageView.frame.size.height) + CGFloat(imageView.frame.origin.y)
                //                let tap = UITapGestureRecognizer(target: self, action: #selector(clickImage(sender:)))
                //                imageView.addGestureRecognizer(tap)
            }

        }
        get{
            return _contentImg
        }
    }
    var imgsArr: [BBSImageModel]! {
        set {
            _imgsArr = newValue
            
            for v in self.subviews {
                v.removeFromSuperview()
            }
            
            
            
            
            let itemW = (kScreenWidth - 50 - 5 * 2.0) / 3
            var itemH = (kScreenWidth - 50 - 5 * 2.0) / 3
            
            if _imgsArr.count == 1 {
                let imgW: CGFloat = CGFloat(atof(_imgsArr.last?.width))
            
                let imgH: CGFloat = CGFloat(atof(_imgsArr.last?.height))
                itemH = imgH / imgW * itemW;
            }
            
            let butGap: CGFloat = 5
            
            _imgsArr.enumerateKeysAndObjects { (obj, idx) in
                let columnIndex = idx % 3
                let rowIndex = idx / 3
                let imageView = UIImageView(frame: CGRect(x: CGFloat(columnIndex) * (itemW + butGap), y: CGFloat(rowIndex) * (itemH + butGap), width: itemW, height: itemH))
                //imageView.yy_imageURL = URL(string: obj.img!)
                imageView.kf.setImage(with: URL(string: obj.img!))
                imageView.tag = idx
                imageView.isUserInteractionEnabled = true
                self.addSubview(imageView)
                imageView.clipsToBounds = true
                imageView.contentMode = .scaleAspectFill
                self.viewHeight = CGFloat(imageView.frame.size.height) + CGFloat(imageView.frame.origin.y)
//                let tap = UITapGestureRecognizer(target: self, action: #selector(clickImage(sender:)))
//                imageView.addGestureRecognizer(tap)
            }
        }
        get {
            return _imgsArr
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    func initViews() {
        
    }


    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
