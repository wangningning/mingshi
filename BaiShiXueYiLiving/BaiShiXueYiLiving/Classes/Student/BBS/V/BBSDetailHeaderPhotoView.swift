//
//  BBSDetailHeaderPhotoView.swift
//  FKDCClient
//
//  Created by 曾觉新 on 2017/2/26.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class BBSDetailHeaderPhotoView: UIView, SDPhotoBrowserDelegate {
    
    
    private var thumbImageArr: Array<BBSImageModel>!//缩略图
    private var imageArr: Array<BBSImageModel>! //原图
    
    var viewHeight: CGFloat = 0
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func initViews() {
        
    }
    
    func setImageArr(thumbImageArr: [BBSImageModel], imageArr: [BBSImageModel]){
        viewHeight = 0
        self.thumbImageArr = thumbImageArr
        self.imageArr = imageArr
        
        for v in self.subviews {
            v.removeFromSuperview()
        }
        
        
        let itemW = kScreenWidth - 30
        let gap: CGFloat = 10
        
        self.imageArr.enumerateKeysAndObjects { (model, index) in
            let imageW = CGFloat(atof(model.width))
            let imageH = CGFloat(atof(model.height))
            
            var height: CGFloat = 0.0
            if imageW != 0 && imageH != 0 { //防止图片404出现的闪退
                height = itemW / imageW * imageH
            }
            
            let imageView = UIImageView(frame: CGRect(x: 0, y: viewHeight, width: itemW, height: height))
            imageView.tag = index
            imageView.isUserInteractionEnabled = true
           // imageView.yy_imageURL = URL(string: model.img!)
            imageView.kf.setImage(with: URL(string: model.img!))
            self.addSubview(imageView)
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(clickImage(sender:)))
            imageView.addGestureRecognizer(tap)
            
            viewHeight = imageView.frame.size.height + imageView.frame.origin.y + gap
        }
    }
    
    
    func clickImage(sender: UITapGestureRecognizer) {
        let v = sender.view as! UIImageView
//
        let browser = SDPhotoBrowser()
        browser.delegate = self
        browser.imageCount = self.imageArr.count
        browser.currentImageIndex = v.tag
        browser.sourceImagesContainerView = self
        self.responderViewController()?.view.endEditing(true)
        browser.show()
    }
    //MARK: - SDPhotoBrowserDelegate
    func photoBrowser(_ browser: SDPhotoBrowser!, highQualityImageURLFor index: Int) -> URL! {
        return URL(string: self.imageArr[index].img!)
    }
    func photoBrowser(_ browser: SDPhotoBrowser!, placeholderImageFor index: Int) -> UIImage! {
        return UIImage()
    }
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
