//
//  BBSDetailReplyCell.swift
//  FKDCClient
//
//  Created by 曾觉新 on 2017/2/26.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

protocol BBSDetailReplyCellDelegate {
    //index: 1,举报，2，点赞， 3，评论
    func bbsDetailReplyCell(cell: BBSDetailReplyCell, clickButton index: Int, model: BBSResponseModel)
}


class BBSDetailReplyCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {
    
    var myDelegate: BBSDetailReplyCellDelegate?
    
    var userIconImageView: UIImageView!
    var userNameLabel: UILabel!
    var dateLabel: UILabel!
    var sexImageView: UIImageView!
    var contentLabel: UILabel!
    var commentBgView: UIView!
    var commentTableView: UITableView!
    
    var reportButton: UIButton!//举报
    var likeButton: UIButton!//点赞
    var replyButton: UIButton!
    var tcell: BBSCommentCell!
    
    
    private var _model: BBSResponseModel!
    var model: BBSResponseModel! {
        set {
            _model = newValue
            
          
//            userIconImageView.kf.setImage(with: URL(string: _model.img!))
            userIconImageView.sd_setImage(with: URL(string: _model.img!), placeholderImage: #imageLiteral(resourceName: "kb_shchfm"))
            userNameLabel.text = _model.username
            contentLabel.text = _model.content
            dateLabel.text = _model.intime

                if _model.sex == "1" {
                    sexImageView.image = #imageLiteral(resourceName: "ssjg_man")
                } else if _model.sex == "2"{
                    sexImageView.image = #imageLiteral(resourceName: "ssjg_woman")
                } else {
                    sexImageView.image = #imageLiteral(resourceName: "mshzhd_people")
                }
            
            if _model.response == nil || _model.response?.count == 0 {
                commentBgView.snp.updateConstraints({ (make) in
                    make.top.equalTo(self.dateLabel.snp.bottom)
                    make.height.equalTo(0)
                })
            } else {
                var height : CGFloat = 0
                for (index,m) in (_model.response?.enumerated())! {
                    tcell.model = m
                    height += tcell.getCellheight()
                }
                
                commentBgView.snp.updateConstraints({ (make) in
                    make.top.equalTo(self.dateLabel.snp.bottom).offset(14)
                    make.height.equalTo(height + 15)
                })
            }
            commentTableView.reloadData()
            
        }
        get {
            return _model
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateLayout()
    }
    
    func initViews() {
        userIconImageView = UIImageView()
        userIconImageView.layer.cornerRadius = 30 / 2
        userIconImageView.layer.masksToBounds = true
        userIconImageView.clipsToBounds = true
        userIconImageView.contentMode = .scaleAspectFill
        self.contentView.addSubview(userIconImageView)
        
        userNameLabel = UILabel()
        userNameLabel.font = UIFont.systemFont(ofSize: 14)
        userNameLabel.textColor = UIColor(hexString: "#333333")
        
        self.contentView.addSubview(userNameLabel)
        
        dateLabel = UILabel()
        dateLabel.font = UIFont.systemFont(ofSize: 12)
        dateLabel.textColor = UIColor(hexString: "#666666")
        self.contentView.addSubview(dateLabel)
        
        sexImageView = UIImageView()
        self.contentView.addSubview(sexImageView)
        
        
        contentLabel = UILabel()
        contentLabel.font = UIFont.systemFont(ofSize: 14)
        contentLabel.textColor = UIColor(hexString: "#333333")
        contentLabel.numberOfLines = 0
        self.contentView.addSubview(contentLabel)
        
        
        replyButton = UIButton(type: .custom)
        replyButton.tag = 3
        replyButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        replyButton.setTitle("回复", for: .normal)
        replyButton.setTitleColor(UIColor(hexString: "#727171"), for: .normal)
        replyButton.addTarget(self, action: #selector(clickButton(sender:)), for: .touchUpInside)
        self.contentView.addSubview(replyButton)
        
        commentBgView = UIView()
        commentBgView.backgroundColor = UIColor(hexString: "#f6f6f6")
        self.contentView.addSubview(commentBgView)
        
        tcell = BBSCommentCell.init(style: .default, reuseIdentifier: "comCell")
        commentTableView = UITableView(frame: CGRect(), style: .plain)
        commentTableView.register(BBSCommentCell.self, forCellReuseIdentifier: "comCell")
        commentTableView.backgroundColor = UIColor.clear
        commentTableView.separatorColor = UIColor.clear
        commentTableView.dataSource = self
        commentTableView.delegate = self
        commentTableView.isUserInteractionEnabled = false
        commentBgView.addSubview(commentTableView)
        
    }
    
    func updateLayout() {
        
        userIconImageView.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.left.equalTo(15)
            make.size.equalTo(CGSize(width: 30, height: 30))
        }
        
        
        userNameLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(userIconImageView.snp.centerY)
            make.left.equalTo(self.userIconImageView.snp.right).offset(12)
        }
        
        contentLabel.snp.makeConstraints { (make) in
            make.left.equalTo(30)
            make.top.equalTo(userNameLabel.snp.bottom).offset(25)
            make.right.equalTo(self.contentView.snp.right).offset(-5)
        }
        
        dateLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentLabel.snp.bottom).offset(18)
            make.left.equalTo(self.contentLabel)
        }
        
        sexImageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.userNameLabel.snp.centerY)
            make.left.equalTo(self.userNameLabel.snp.right).offset(10)
           
        }
        
        
        replyButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.dateLabel.snp.centerY)
            make.right.equalTo(-25)
        }
        
        commentBgView.snp.makeConstraints { (make) in
            make.left.equalTo(self.contentLabel)
            make.right.equalTo(-14)
        }
        
        commentTableView.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.right.equalTo(-15)
            make.top.equalTo(9)
            make.bottom.equalTo(9)
        }
    }
    
    func getCellHeight() -> CGFloat{
        
        updateLayout()
        commentBgView.superview?.layoutIfNeeded()
        print("\(commentBgView)")
        return commentBgView.frame.size.height + commentBgView.frame.origin.y + 10
    }
    
    
    //MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "comCell", for: indexPath) as! BBSCommentCell
        cell.backgroundColor = UIColor.clear
        cell.model = _model.response?[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if _model == nil || _model.response == nil || _model.response?.count == 0 {
            return 0
        } else {
            return (_model.response?.count)!
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let m = _model.response?[indexPath.row]
        if m?.cellHeight == nil {
            tcell.model = m
        }
        return tcell.getCellheight()
    }
    
    
    //MARK: -点击事件
    func clickButton(sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 1://举报
            
            break
            
        case 2://点赞
            
            break
            
        case 3://回复
            break
            
        default:
            break
        }
        
        myDelegate?.bbsDetailReplyCell(cell: self, clickButton: tag, model: model)
    }
    
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
