//
//  BBSDetailHeaderView.swift
//  FKDCClient
//
//  Created by 曾觉新 on 2017/2/26.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class BBSDetailHeaderView: UIView {
    
    var userIconImageView: UIImageView!
    var userNameLabel: UILabel!
    var dateLabel: UILabel!
    var sexImageView: UIImageView!
    var titleLabel: UILabel!
    var contentLabel: UILabel!
    var allReplyLab: UILabel!
    var lineView: UIView!
    var photoView: BBSDetailHeaderPhotoView!
    
    var viewHeight: CGFloat = 0
    
//    private var _model: BBSListModel!
    var model: BBSListModel! {
        willSet(m) {
            if  m.head_img != nil {
               userIconImageView.kf.setImage(with: URL(string: m.head_img!))
                userNameLabel.text = m.username
                dateLabel.text = m.date_value
                titleLabel.text = m.title
                contentLabel.text = m.content?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
                
             
                if m.sex == "1" || m.sex == "0" {
                    sexImageView.image = UIImage(named: "ssjg_man")
                } else if m.sex == "2" {
                    sexImageView.image = UIImage(named: "ssjg_woman")
                }
                
                
                if m.content != nil && m.content?.characters.count != 0 {
                    contentLabel.snp.updateConstraints({ (make) in
                        make.top.equalTo(self.titleLabel.snp.bottom).offset(10)
                    })
                } else {
                    contentLabel.snp.updateConstraints({ (make) in
                        make.top.equalTo(self.titleLabel.snp.bottom)
                    })
                }
                
                
                if m.img != nil && m.img?.count != 0{
                    photoView.setImageArr(thumbImageArr: m.thumb!, imageArr: m.img!)
                    photoView.snp.updateConstraints({ (make) in
                        make.top.equalTo(self.contentLabel.snp.bottom).offset(10)
                        make.height.equalTo(photoView.viewHeight)
                    })
                } else {
                    photoView.snp.updateConstraints({ (make) in
                        make.top.equalTo(self.contentLabel.snp.bottom)
                        make.height.equalTo(0)
                    })
                }
                
                photoView.superview?.layoutIfNeeded()
                if photoView.frame.size.height == 0 {
                    viewHeight = 40 + photoView.frame.origin.y
                } else {
                    viewHeight = 30 + photoView.frame.size.height + photoView.frame.origin.y
                }
            }
        }
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        userIconImageView.snp.makeConstraints { (make) in
            make.top.equalTo(16)
            make.left.equalTo(15)
            make.size.equalTo(CGSize(width: 30, height: 30))
        }
        
        userNameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.userIconImageView)
            make.left.equalTo(self.userIconImageView.snp.right).offset(12)
        }
        
        dateLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.userIconImageView)
            make.left.equalTo(self.userNameLabel)
        }
        
        sexImageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.userNameLabel.snp.centerY)
            make.left.equalTo(self.userNameLabel.snp.right).offset(10)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.userIconImageView.snp.bottom).offset(10)
            make.left.equalTo(self.userIconImageView)
            make.right.equalTo(-22)
        }
        
        
        contentLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.userIconImageView)
            make.top.equalTo(self.titleLabel.snp.bottom).offset(5)
            make.right.equalTo(-5)
        }
        
        photoView.snp.makeConstraints{(make) in
            make.left.equalTo(15)
            make.right.equalTo(-15)
        }
        
        allReplyLab.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(10)
        }
        lineView.snp.makeConstraints { (make) in
            make.bottom.equalTo(0)
            make.left.right.equalTo(0)
            make.height.equalTo(33)
        }
        
    }
    
    
    func initViews() {
        
        userIconImageView = UIImageView()
        userIconImageView.layer.cornerRadius = 30 / 2
        userIconImageView.layer.masksToBounds = true
        
        self.addSubview(userIconImageView)
        
        userNameLabel = UILabel()
        userNameLabel.font = UIFont.systemFont(ofSize: 14)
        userNameLabel.textColor = UIColor(hexString: "#595757")
        self.addSubview(userNameLabel)
        
        dateLabel = UILabel()
        dateLabel.font = UIFont.systemFont(ofSize: 12)
        dateLabel.textColor = UIColor(hexString: "#999999")
        self.addSubview(dateLabel)
        
        sexImageView = UIImageView()
        self.addSubview(sexImageView)
        
        titleLabel = UILabel()
        titleLabel.font = UIFont.systemFont(ofSize: 13)
        titleLabel.textColor = UIColor(hexString: "#3e3a39")
        titleLabel.numberOfLines = 0
        self.addSubview(titleLabel)
        
        contentLabel = UILabel()
        contentLabel.font = UIFont.systemFont(ofSize: 14)
        contentLabel.textColor = UIColor(hexString: "#3e3a39")
        contentLabel.numberOfLines = 0
        contentLabel.lineBreakMode = .byCharWrapping
        self.addSubview(contentLabel)
        
        photoView = BBSDetailHeaderPhotoView()
        self.addSubview(photoView)
        lineView = UIView()
        lineView.backgroundColor = UIColor(hexString:"f1f5f6")
        self.addSubview(lineView)
        
        allReplyLab = UILabel()
        allReplyLab.text = "全部回复"
        allReplyLab.textColor = UIColor(hexString: "#333333")
        allReplyLab.font = UIFont.systemFont(ofSize: 14)
        self.lineView.addSubview(allReplyLab)
        
        
        
    }
    
    
    

    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
