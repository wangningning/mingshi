//
//  BBSModuleCollectionViewCell.swift
//  FKDCClient
//
//  Created by 曾觉新 on 2017/2/25.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class BBSModuleCollectionViewCell: UICollectionViewCell {
    
    var bgImageView: UIImageView!
    var titleLabel: UILabel!
    var tuijianImageView: UIImageView!
    var postCountLabel: UILabel!
    
    
    private var _moduleModle: BBSModuleModel = BBSModuleModel()
    var moduleModle: BBSModuleModel! {
        set {
            _moduleModle = newValue
           // bgImageView.yy_imageURL = URL(string: _moduleModle.picture!)
            bgImageView.kf.setImage(with: URL(string: _moduleModle.picture!))
           
//            titleLabel.text = _moduleModle.title!
//            postCountLabel.text = "今日：" + _moduleModle.post_count!
            if _moduleModle.is_tuijian == "1" {
                tuijianImageView.isHidden = true
            } else {
                tuijianImageView.isHidden = false
            }
            
        }
        get {
            return _moduleModle
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func initViews() {
        bgImageView = UIImageView()
        self.addSubview(bgImageView)
        
        titleLabel = UILabel()
        titleLabel.font = UIFont.boldSystemFont(ofSize: 12)
        titleLabel.numberOfLines = 2
        titleLabel.textColor = UIColor.white
        self.addSubview(titleLabel)
        
        postCountLabel = UILabel()
        postCountLabel.font = UIFont.boldSystemFont(ofSize: 9)
        postCountLabel.textColor = UIColor.white
        self.addSubview(postCountLabel)
        
        tuijianImageView = UIImageView()
        self.addSubview(tuijianImageView)
        
        
        bgImageView.snp.makeConstraints { (make) in
            make.top.bottom.left.right.equalTo(0)
        }
        titleLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(29)
            make.centerX.equalTo(self.bgImageView.snp.centerX)
            make.left.equalTo(9)
            make.right.equalTo(-9)
        }
        postCountLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.bgImageView.snp.centerX)
            make.top.equalTo(self.titleLabel.snp.bottom).offset(13)
        }
        tuijianImageView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.right.equalTo(0)
        }
        
    }
    
    
    
    
    
    
}
