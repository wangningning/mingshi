//
//  BBSListTableViewCell.swift
//  FKDCClient
//
//  Created by 曾觉新 on 2017/2/25.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

protocol BBSListTableViewCellDelegate {
    //tag: 1,评论， 2，点赞， 3，删除，4，下拉键
    func bbsListTableViewCell(cell: BBSListTableViewCell, eventFo tag: Int, model: BBSListModel)
    
}

class BBSListTableViewCell: UITableViewCell {
    
    var userAvatarImageView: UIImageView!
    var useNameLabel: UILabel!
    var titleLabel: UILabel!
    var sexImageView: UIImageView!
    var aboutLabel: UILabel!
    var eventButton: UIButton!//事件
    var replysButton: UIButton!//回复数
    var likeButton: UIButton!//点赞
    var messageButton: UIButton! //私信
    var dateLabel: UILabel!
    var photoView: BBSListPhotoView!//显示图片
    var line: UIView!
    var reportBtn = UIButton.init(type: .custom)
    var myDelegate: BBSListTableViewCellDelegate?
    
    var isMinePostVC : Bool!{
        willSet{
            if newValue {
                self.reportBtn.setTitle("删除", for: .normal)
            }else{
                self.reportBtn.setTitle("举报", for: .normal)
                self.reportBtn.isHidden = true
            }
        }
    }
   
    var model: BBSListModel! {
        willSet(m) {

            
            
            if m.head_img != nil {
               
                userAvatarImageView.kf.setImage(with: URL(string: m.head_img!))
                useNameLabel.text = m.username
                titleLabel.text = m.title
                replysButton.setTitle("  " + m.ping!, for: .normal)
                likeButton.setTitle("  " + m.zan!, for: .normal)
                dateLabel.text = m.intime
                if m.sex == "1" || m.sex == "0" {
                    sexImageView.image = #imageLiteral(resourceName: "ssjg_man")
                }else if m.sex == "2"{
                    sexImageView.image = #imageLiteral(resourceName: "ssjg_woman")
                }
                if m.is_zan == "1"{
                    likeButton.isSelected = true
                } else {
                    likeButton.isSelected = false
                }
               
                if m.title == nil || m.title?.characters.count == 0{
                    aboutLabel.snp.updateConstraints({ (make) in
                    make.top.equalTo(self.titleLabel.snp.bottom).offset(-10)
                        make.left.equalTo(13)
                        make.right.equalTo(-13)
                    })
                } else {
                    aboutLabel.snp.updateConstraints({ (make) in
                        make.top.equalTo(self.titleLabel.snp.bottom).offset(10)
                        make.left.equalTo(13)
                        make.right.equalTo(-13)
                    })
                }
                if m.content == nil || m.content?.characters.count == 0 {
                    aboutLabel.text = ""
                    aboutLabel.snp.updateConstraints({ (make) in
                        make.top.equalTo(self.titleLabel.snp.bottom)
                        make.left.equalTo(13)
                        make.right.equalTo(-13)
                    })
                } else {
                    aboutLabel.text = m.content?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
                    aboutLabel.snp.updateConstraints({ (make) in
                        make.top.equalTo(self.titleLabel.snp.bottom).offset(10)
                        make.left.equalTo(13)
                        make.right.equalTo(-13)
                    })
                    
                }
                
                if m.content == nil || m.thumb?.count == 0 {
                    photoView.imgsArr = []
                    photoView.snp.updateConstraints({ (make) in
                        make.height.equalTo(0)
                        make.top.equalTo(self.aboutLabel.snp.bottom)
                    })
                } else {
                    
                    
                    photoView.imgsArr = m.thumb
                    
                    photoView.snp.updateConstraints({ (make) in
                        make.top.equalTo(self.aboutLabel.snp.bottom).offset(15)
                        make.height.equalTo(self.photoView.viewHeight)
                    })
                }
                
            }
            
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutUI()
    }
    
    func initViews() {
        
        userAvatarImageView = UIImageView()
        userAvatarImageView.layer.cornerRadius = 30/2
        userAvatarImageView.layer.masksToBounds = true
        self.contentView.addSubview(userAvatarImageView)
       
        useNameLabel = UILabel()
        useNameLabel.font = UIFont.systemFont(ofSize: 13)
        useNameLabel.textColor = UIColor(hexString: "#333333")
        
        self.contentView.addSubview(useNameLabel)
        
        titleLabel = UILabel()
        titleLabel.numberOfLines = 2;
        titleLabel.font = UIFont.systemFont(ofSize: 17)
        titleLabel.textColor = UIColor(hexString: "#333333")
        
        self.contentView.addSubview(titleLabel)
        
        sexImageView = UIImageView()
        self.contentView.addSubview(sexImageView)
        
        aboutLabel = UILabel()
        aboutLabel.numberOfLines = 0
        aboutLabel.font = UIFont.systemFont(ofSize: 13)
        aboutLabel.textColor = UIColor(hexString: "#333333")
        
        self.contentView.addSubview(aboutLabel)
        
        eventButton = UIButton(type: .custom)
        eventButton.setImage(UIImage(named: "xiala"), for: .normal)
        eventButton.tag = 4
        eventButton.addTarget(self, action: #selector(clickButton(sender:)), for: .touchUpInside)
        self.contentView.addSubview(eventButton)
        
        replysButton = getButton(image: UIImage(named: "yylt_pl")!)
        replysButton.tag = 1
        self.contentView.addSubview(replysButton)
        
        likeButton = getButton(image: UIImage(named: "shc_xiao_wshc")!)
        likeButton.setImage(#imageLiteral(resourceName: "shc_xiao_shc"), for: .selected)
        likeButton.isSelected = false
        likeButton.tag = 2
        self.contentView.addSubview(likeButton)
        
       
        
        self.reportBtn.setTitleColor(UIColor.init(hexString: "#666666"), for: .normal)
        self.reportBtn.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        self.reportBtn.addTarget(self, action: #selector(BBSListTableViewCell.reportPost), for:.touchUpInside)
        self.contentView.addSubview(self.reportBtn)
        
        dateLabel = UILabel()
        dateLabel.font = UIFont.systemFont(ofSize: 12)
        dateLabel.textColor = UIColor(hexString: "#666666")
        self.contentView.addSubview(dateLabel)
        
        
        
        photoView = BBSListPhotoView()
        self.contentView.addSubview(photoView)
        photoView.contentMode = .scaleAspectFill
        photoView.clipsToBounds = true
        line = UIView()
        line.backgroundColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.00)
        self.contentView.addSubview(line)
    }
    
    func getButton(image: UIImage) ->UIButton {
    
        let button = UIButton(type: .custom)
        button.setImage(image, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        button.setTitleColor(UIColor(hexString: "#666666"), for: .normal)
        button.addTarget(self, action: #selector(clickButton(sender: )), for: .touchUpInside)
        return button

    }
    //MARK: 点击事件
    func clickButton(sender: UIButton) {
       
        myDelegate?.bbsListTableViewCell(cell: self, eventFo: sender.tag, model: model)
    }
    
    //MARK: 布局
    func layoutUI() {
        userAvatarImageView.snp.makeConstraints { (make) in
            make.top.equalTo(16)
            make.left.equalTo(12)
            make.size.equalTo(CGSize(width: 30, height: 30))
        }
        
        useNameLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.userAvatarImageView.snp.centerY)
            make.left.equalTo(self.userAvatarImageView.snp.right).offset(12)
        }
        
        sexImageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.userAvatarImageView.snp.centerY)
            make.left.equalTo(self.useNameLabel.snp.right).offset(12)
        }
        

        
        
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.userAvatarImageView.snp.left)
            make.top.equalTo(self.userAvatarImageView.snp.bottom).offset(17)
            make.right.equalTo(-15)
        }
        
        
        photoView.snp.makeConstraints { (make) in
            
            make.left.equalTo(12)
            make.right.equalTo(-12)
        }
        
        replysButton.snp.makeConstraints { (make) in
           
            make.bottom.equalTo(-15)
           // make.top.equalTo(photoView.snp.bottom)
            make.left.equalTo(12)
            make.height.equalTo(20)
            
        }
        
        likeButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.replysButton.snp.centerY)
            make.left.equalTo(self.replysButton.snp.left).offset(36 + 14)
            make.height.equalTo(20)
        }
        
        
        
        dateLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.replysButton.snp.centerY)
            make.right.equalTo(-12)
        }
        
        reportBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.replysButton.snp.centerY)
            make.right.equalTo(self.dateLabel.snp.left).offset(-15)
           
        }
        
        line.snp.makeConstraints { (make) in
            make.bottom.left.right.equalTo(0);
            make.height.equalTo(5)
        }
        
    }
    
    func getCellHeight() -> CGFloat {
        layoutUI()
        photoView.superview?.layoutIfNeeded()
        return photoView.frame.size.height + photoView.frame.origin.y + 48
        
    }
    func reportPost(){
        if isMinePostVC{
            myDelegate?.bbsListTableViewCell(cell: self, eventFo: 3, model: self.model)
        }else{
            let vc = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            let titleArr = ["淫秽色情","侮辱他人","恶意营销","言语低俗","广告诈骗"]
            
            for str in titleArr {
                
                let ac = UIAlertAction.init(title: str, style: .default) { (alert) in
                    ProgressHUD.showSuccess(message: "举报成功")
                }
                vc.addAction(ac)
            }
            let cancel = UIAlertAction.init(title: "取消", style: .cancel) { (alert) in
                
            }
            
            vc.addAction(cancel)
            self.responderViewController()?.present(vc, animated: true, completion: nil)
        }
        
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
