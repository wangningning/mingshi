//
//  BBSDetailsViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/29.
//  Copyright © 2017年 liangyi. All rights reserved.
//王志刚

import UIKit
import IQKeyboardManagerSwift

class BBSDetailsViewController: UIViewController,STInputBarDelegate,BBSDetailsReplyViewDelegate,UITableViewDelegate,UITableViewDataSource,BBSDetailReplyCellDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var bbsModel: BBSListModel!
    var share = ShareView()
    var commentList: [BBSResponseModel] = []
    var deleteComplete: (()->())?
    var updateBBSList:((_ model: BBSListModel)->())?
    
    var isSelected = false
    
    var tempCell: BBSDetailReplyCell!
    var detailReplyView: BBSDetailsReplyView!
    var headerView: BBSDetailHeaderView!
    var inputView1: STInputBar!
    let placeHolder = "说点什么吧"
    
    var isDirectlyReply = false  //开启页面直接回复
    var isCommentReply: Bool = false//是否为回复评论
    var commentModel: BBSResponseModel?//被回复人的模型
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().enable = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "帖子详情"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "tzxq_fx"), style: .done, target: self, action: #selector(shareAction))
        requestDetailData()
        tempCell = BBSDetailReplyCell(style: .default, reuseIdentifier: "cell")
        
        self.tableView.keyboardDismissMode = .onDrag
        self.tableView.separatorColor = UIColor.clear
        self.tableView.register(BBSDetailReplyCell.self, forCellReuseIdentifier: "BBSDetailReplyCell")
        
        inputView1 = STInputBar(frame: CGRect(x: 0, y: kScreenHeight - 45 - 64, width: self.view.frame.size.width, height: 45))
        inputView1.fitWhenKeyboardShowOrHide = true
        inputView1.myDelegate = self;
        inputView1.setDidSendClicked { (text) in  //点击发送
            self.clickSend(text: text!)
        }
        self.view.addSubview(inputView1)
        
        detailReplyView = BBSDetailsReplyView(frame: CGRect(x: 0, y: kScreenHeight - 64 - 45, width: kScreenWidth, height: 45))
        detailReplyView.myDelegate = self
        detailReplyView.model = bbsModel
        self.view.addSubview(detailReplyView)
        
        headerView = BBSDetailHeaderView()
        tableView.tableHeaderView = headerView
        headerView.model = bbsModel
        headerView.superview?.layoutIfNeeded()
        headerView.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: headerView.viewHeight)
       
        tableView.reloadData()
        
        if isDirectlyReply {
            inputView1.textView.becomeFirstResponder()
            inputView1.placeHolder = placeHolder
        }
        
        // Do any additional setup after loading the view.
    }
    func inputBarShouldReturn(_ inputView: STInputBar!) {
        self.clickSend(text: inputView.textView.text!)
    }
    func shareAction(){
        isSelected = !isSelected
        if isSelected {
            share = ShareView.show(atView: self.view, url: bbsModel.share_url!, avatar: bbsModel.head_img!, username: bbsModel.username!, type: "2")
        } else{
            share.dismiss()
        }
        share.dismissBlock = { [unowned self] in
            self.isSelected = !self.isSelected
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BBSDetailReplyCell") as! BBSDetailReplyCell
        cell.selectionStyle = .none
        cell.model = self.commentList[indexPath.row]
        cell.myDelegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.commentList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = self.commentList[indexPath.row]
        if model.cellHeight == nil {
            tempCell.model = model
            model.cellHeight = tempCell.getCellHeight()
        }
        return model.cellHeight!
    }
    //MARK: BBSDetailReplyCellDelegate
    func bbsDetailReplyCell(cell: BBSDetailReplyCell, clickButton index: Int, model: BBSResponseModel)
    {
        switch index {
            case 3://回复
            inputView1.textView.becomeFirstResponder()
            inputView1.placeHolder = "回复@" + model.username!
            isCommentReply = true
            commentModel = model
            break
        default:
            break
        }
    }
    
    //MARK: -点击发送
    func clickSend(text: String) {
        
        if self.isCommentReply && self.commentModel != nil{
            self.sendReply(text: text, response_id: (self.commentModel?.comment_id!)!)
        } else {
            self.sendReply(text: text, response_id: "")
        }
        self.inputView1.textView.resignFirstResponder()
    }
    //MARK: - 回复
    func sendReply(text: String, response_id: String) {
        
                let params = ["post_id" : bbsModel.post_id as AnyObject,
                              "content" : text as AnyObject,
                              "response_id" : response_id as AnyObject
                    ,"type":"1"] as [String : AnyObject]
        
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Topical&a=comment_posts", at: self, params: params, isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
            ProgressHUD.showNoticeOnStatusBar(message: "回复成功")
            self.requestDetailData()
        }) { 
            
        }

    }
    func detailsReplyView(replyView: BBSDetailsReplyView, index: Int) {
        if index == 1 ||  index == 2{
            inputView1.textView.becomeFirstResponder()
            inputView1.placeHolder = placeHolder
            isCommentReply = false
        } else {
            likePost()
        }
    }
    //MARK: 帖子点赞
    func likePost() {
        let params = ["post_id" : bbsModel.post_id as AnyObject] as [String : AnyObject]
        
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Topical&a=zan_posts", at: self, params: params as [String : AnyObject], isAuthHide: true, isShowHUD: false, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
            let data = response["data"] as! String
            let zan = Int(self.bbsModel.zan!)
            if data == "2" {//取消点赞
                ProgressHUD.showMessage(message: "取消成功")
                self.bbsModel.is_zan = "2"
                self.bbsModel.zan = "\(zan! - 1)"
            } else {
                ProgressHUD.showMessage(message: "点赞成功")
                self.bbsModel.is_zan = "1"
                self.bbsModel.zan = "\(zan! + 1)"
            }
            self.requestDetailData()
            }) { (errorCode) in
            
            }
        
        
    }
    //MARK: - 获取详情页数据
    func requestDetailData() {
        let params = ["post_id" : bbsModel.post_id!]
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Topical&a=posts_view", at: self, params: params, hasHeaderRefresh: nil, success: { (response) in
            let data = response["data"]
            if data != nil {
                self.bbsModel = BBSListModel.modelWithDictionary(diction: data as! [String : AnyObject])
                if self.updateBBSList != nil {
                    self.updateBBSList!(self.bbsModel);
                }
                self.detailReplyView.model = self.bbsModel
                if self.bbsModel.comment != nil {
                    self.commentList = self.bbsModel.comment!
                }
            }
            self.tableView.reloadData()
        }) {
            
        }
        
    }
    
    
    //MARK: 评论点赞
    func likeReply(model: BBSResponseModel) {
        
        //        let params = [
        //            "post_id" : bbsModel.post_id as AnyObject,
        //            "comment_id" : model.comment_id as AnyObject
        //            ] as [String : AnyObject]
        //
        //        LYNetWorkRequest.fetchNetworkData(url: "/api.php?m=Api&c=Topical&a=zan_comment", params: params as [String : AnyObject], isAutoHide: false, isShowOrNot: "2", hasHeaderRefresh: nil, success: { (response) in
        //
        //            let data = response["data"] as! NSNumber
        //
        //            if data == 1 {//取消点赞
        //                ProgressHUDManager.showSuccessWithStatus(string: "取消成功")
        //            } else {
        //                ProgressHUDManager.showSuccessWithStatus(string: "点赞成功")
        //            }
        //            self.requestDetailData()//评论成功过更新数据
        //        }) { (errorCode) in
        //
        //        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
