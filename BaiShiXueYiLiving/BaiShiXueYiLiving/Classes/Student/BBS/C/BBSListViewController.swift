//
//  BBSListViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/30.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh

class BBSListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,BBSListTableViewCellDelegate{

    @IBOutlet weak var tableView: UITableView!
    var model: BBSModuleModel!
    var listArr: [BBSListModel] = []
    var page = 1
    var tempCell: BBSListTableViewCell!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = model.title
        
        tempCell = BBSListTableViewCell(style: .default, reuseIdentifier: "BBSListTableViewCell")
        self.tableView.register(BBSListTableViewCell.self, forCellReuseIdentifier: "BBSListTableViewCell")
        self.tableView.mj_header = MJRefreshNormalHeader(refreshingBlock: {[unowned self] in
            self.page = 1
            self.requestBBSListData(p: self.page)
        })
        let footer = MJRefreshAutoNormalFooter(refreshingBlock: { [unowned self] in
            self.page += 1
            self.requestBBSListData(p: self.page)
        })
        footer?.setTitle("", for: .idle)
        footer?.setTitle("", for: .noMoreData)
        self.tableView.mj_footer = footer
        self.tableView.mj_header.beginRefreshing()
        // Do any additional setup after loading the view.
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BBSListTableViewCell") as! BBSListTableViewCell
        cell.myDelegate = self
        cell.model = self.listArr[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listArr.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = self.listArr[indexPath.row]
        if model.cellHeight == nil {
            tempCell.model = model
            model.cellHeight = tempCell.getCellHeight()
        }
        return model.cellHeight!
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = BBSDetailsViewController()
        vc.bbsModel = self.listArr[indexPath.row]
        vc.updateBBSList = { model in
            let bbsModel = self.listArr[indexPath.row]
            bbsModel.zan = model.zan
            bbsModel.ping = model.ping
            self.tableView.reloadData()
        }
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func bbsListTableViewCell(cell: BBSListTableViewCell, eventFo tag: Int, model: BBSListModel) {
        print("cell代理\(tag)")
    }
    //MARK: 请求数据
    func requestBBSListData(p: Int) {
        let params = [
            "p" : "\(p)",
            "module_id": model.module_id
        ] as [String:AnyObject]
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Topical&a=module_posts", at: self, params: params, isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: self.tableView, success: { (response) in
            let data = response["data"] as! [String: AnyObject]
            let list = BBSListModel.modelsWithArray(modelArray: data["list"] as! [[String: AnyObject]]) as! [BBSListModel]
            if p == 1{
                
                self.listArr.removeAll()
            }
            if list.count == 0{
                
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
            }
            self.listArr += list
            self.tableView.reloadData()

        }) {
            
        }
}

    //MARK: 帖子点赞
    func likePost(model: BBSListModel) {
//        let params = ["post_id" : model.post_id as AnyObject,
//                      ] as [String : AnyObject]
//        
//        LYNetWorkRequest.fetchNetworkData(url: "/api.php?m=Api&c=Topical&a=zan_post", params: params as [String : AnyObject], isAutoHide: false, isShowOrNot: "2", hasHeaderRefresh: nil, success: { (response) in
//            let data = response["data"] as! NSNumber
//            let zan = Int(model.zan!)
//            if data == 1 {//取消点赞
//                ProgressHUDManager.showSuccessWithStatus(string: "取消成功")
//                model.zan = "\(zan! - 1)"
//            } else {
//                ProgressHUDManager.showSuccessWithStatus(string: "点赞成功")
//                model.zan = "\(zan! + 1)"
//            }
//            self.tableView.reloadData()
//        }) { (errorCode) in
//            
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
