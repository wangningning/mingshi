//
//  BBSPostMessageViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/29.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class BBSPostMessageViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, UITextViewDelegate,BBSAddPhotoViewDelegate, UITextFieldDelegate{
    @IBOutlet weak var selectTopicTF: UITextField!
    @IBOutlet weak var content: UITextView!
    @IBOutlet weak var photoView: UIView!
    @IBOutlet weak var photoViewHeight: NSLayoutConstraint!
    @IBOutlet weak var submitBtn: UIButton!
    
    @IBOutlet weak var tipsLab: UILabel!
    var rightBtn : UIButton!
    var topicForm: UITableView!
    var addPhotoView: BBSAddPhotoView!
    var plateData: [BBSModuleModel] = []
    var postModel: BBSModuleModel?
    var moubleid: String!
    var successPost: (()->())?
    private let butWidth = (kScreenWidth - 30 - 5 * 3.0) / 4
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "发帖"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "提交", style:.done, target: self, action: #selector(BBSPostMessageViewController.submitAction(_:)))
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSForegroundColorAttributeName : UIColor(hexString:"#333333"), NSFontAttributeName : UIFont.systemFont(ofSize: 16)], for: .normal)
        NotificationCenter.default.addObserver(self, selector: #selector(BBSPostMessageViewController.txtViewDidChange(noti:)), name: NSNotification.Name.UITextViewTextDidChange, object: self.content)
        
        self.photoViewHeight.constant = butWidth + 107 + 15
        
        
        
        self.selectTopicTF.delegate = self
        
        self.content.layer.cornerRadius = 6
        self.content.layer.masksToBounds = true
        self.content.delegate = self
       
        
        
        self.selectTopicTF.layer.cornerRadius = 6
        self.selectTopicTF.layer.borderColor = UIColor(hexString: "#666666").cgColor
        self.selectTopicTF.layer.borderWidth = 1
        self.selectTopicTF.layer.masksToBounds = true
        
        self.rightBtn = UIButton(type:.custom)
        self.rightBtn.setImage(#imageLiteral(resourceName: "ft_xl"), for: .normal)
        self.rightBtn.frame = CGRect(x: 0, y: 0, width: 33, height: 33)
       // self.rightBtn.addTarget(self, action: #selector(showTopicForm(sender:)), for:.touchUpInside)
        self.rightBtn.isSelected = true
        self.selectTopicTF.rightView = self.rightBtn
        self.selectTopicTF.rightViewMode = .always
        
        
        
        addPhotoView = BBSAddPhotoView()
        addPhotoView.myDelegate = self
        self.photoView.addSubview(addPhotoView)
        
        addPhotoView.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.width.equalTo(kScreenWidth - 30)
            make.top.equalTo(self.content.snp.bottom).offset(15)
            make.height.equalTo(200)
        }

        
       
        self.submitBtn.layer.cornerRadius = 6
        self.submitBtn.layer.masksToBounds = true
        self.submitBtn.isHidden = true
        loadData()
        // Do any additional setup after loading the view.
    }
    func txtViewDidChange(noti: Notification){
        
        
        let tv = noti.object as! UITextView
        self.tipsLab.isHidden = tv.text.characters.count == 0 ? false : true
        if (tv.text?.characters.count)! > 60 {

            let str = tv.text! as NSString
            tv.text = str.substring(to: 60)
        }
    }
    func loadData() {
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Topical&a=topical_module", at: self,success: { (response) in
                        self.plateData = BBSModuleModel.modelsWithArray(modelArray: response["data"] as! [[String : AnyObject]]) as! [BBSModuleModel]
                        self.postModel = self.plateData.first
                        self.moubleid = self.postModel?.module_id
        self.selectTopicTF.text = "  " + (self.postModel?.title)!
            
        }) {
            
        }

    }
        //MARK: - BBSAddPhotoViewDelegate
    func bbsAddPhotoView(addPhotoView: BBSAddPhotoView, clickButton index:Int)
    {
        if index == 100 {
            
            let actionSheet = ZLPhotoActionSheet()
            actionSheet.maxSelectCount = 9 - addPhotoView.imagesData.count
            
            
            actionSheet.showPhotoLibrary(withSender: self, last: [], completion: {(selectPhotos, selectPhotoModels) in
                
                self.addPhotoView.imagesData = self.addPhotoView.imagesData + selectPhotos
                self.photoViewHeight.constant = self.butWidth * CGFloat((self.addPhotoView.imagesData.count / 4) + 1) + 30
                self.addPhotoView.updateLayout()
            })
            
            
            
        }
    }

//    func showTopicForm(sender:UIButton) {
//        if sender.isSelected {
//            print("展现")
//            setFormView()
//            sender.isSelected = false
//        } else {
//            print("消失")
//            remove()
//            sender.isSelected = true
//        }
//    }

    @IBAction func showOrhideTopicForm(_ sender: UIButton) {
        if self.rightBtn.isSelected {
            
            setFormView()
            self.rightBtn.isSelected = false
        } else {
            
            remove()
            self.rightBtn.isSelected = true
        }
    }
    func setFormView(){
        self.topicForm = UITableView()
        self.topicForm.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        self.topicForm.delegate = self
        self.topicForm.dataSource = self
        self.view.addSubview(self.topicForm)
        self.topicForm.register(UITableViewCell.self, forCellReuseIdentifier: "topicFormCell")
        
        self.topicForm.snp.makeConstraints { (make) in
            make.top.equalTo(self.selectTopicTF.bottom)
            make.left.right.equalTo(self.selectTopicTF)
            make.height.equalTo(self.plateData.count * 44)
        }
        
        
    }
    func remove() {
        if self.topicForm != nil {
            self.topicForm.removeFromSuperview()
        }
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        if self.selectTopicTF.text?.characters.count == 0{
        ProgressHUD.showMessage(message: "请选择话题")
            return
        }
        if self.content.text == "  填写帖子内容" || self.content.text.characters.count == 0{
            ProgressHUD.showMessage(message: "请输入帖子内容")
            return
        }
        if self.addPhotoView.imagesData.count > 0{
            NetworkingHandle.uploadOneMorePicture(url: "?m=Api&c=Topical&a=set_topical_post", atVC: self, images: self.addPhotoView.imagesData, params: ["module_id":self.moubleid,"state":"1","content":self.content.text], isAuthHide: true, uploadSuccess: { (response) in
                ProgressHUD.showSuccess(message: "发帖成功！")
                self.successPost?()
                self.navigationController?.popViewController(animated: true)
            }) {
                
            }

        } else {
            NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Topical&a=set_topical_post", at: self, params: ["module_id":self.moubleid,"content":self.content.text], isAuthHide: true, isShowHUD: true, isShowError: true, success: { (response) in
                ProgressHUD.showSuccess(message: "发帖成功！")
                self.successPost?()
                self.navigationController?.popViewController(animated: true)
            }, failure: {
                
            })
        }
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.plateData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "topicFormCell")!
        cell.textLabel?.text = self.plateData[indexPath.row].title
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let str = self.plateData[indexPath.row]
        self.selectTopicTF.text = "  " + str.title!
        self.moubleid = str.module_id
        self.rightBtn.isSelected = true
        remove()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        var point = touches.first?.location(in: self.view)
        if self.topicForm != nil {
            point = self.topicForm.layer.convert(point!, from: self.view.layer)
            if self.topicForm.layer.contains(point!){
                
            }else{
                self.rightBtn.isSelected = true
                remove()
            }
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UIScrollView{
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.next?.touchesBegan(touches, with: event)
        super.touchesBegan(touches, with: event)
    }
}
