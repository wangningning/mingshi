//
//  CategoryListViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/24.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh

class CategoryListViewController: BSBaseViewController, UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate {

    @IBOutlet weak var tableView: UITableView!
    var searchBar: UISearchBar!
    var resultArr = [SearchResultModel]()
    var page = 1
    var kindsid : String!
    var goodsList = [SearchResultModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.titleView = titleView()

        self.tableView.register(UINib(nibName:"CategoryListViewCell", bundle:Bundle.main), forCellReuseIdentifier: "CategoryListViewCell")
        self.tableView.mj_header = MJRefreshHeader(refreshingBlock: { [unowned self] in
            self.page = 1
            self.loadData()
        })
        self.tableView.mj_footer = MJRefreshAutoFooter(refreshingBlock: { [unowned self] in
            self.page += 1
            self.loadData()
        })
        self.tableView.mj_header.beginRefreshing()
        // Do any additional setup after loading the view.
    }
    func loadData() {
        
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Mall&a=goods_list", at: self, params: ["id":self.kindsid,"p":self.page,"pagesize":10], isShowHUD: true, hasHeaderRefresh: self.tableView, success: { (response) in
            let data = response["data"] as! [String:AnyObject]
            let arr = SearchResultModel.modelsWithArray(modelArray: data["goods"] as! [[String : AnyObject]]) as! [SearchResultModel]
            if arr.count == 0{
                
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
            }
            if self.page == 1{
                
                self.goodsList.removeAll()
            }
            self.goodsList += arr
            self.tableView.reloadData()
        }) {
            
        }
        
    }


    func numberOfSections(in tableView: UITableView) -> Int {
        if self.resultArr.count == 0 {
            return self.goodsList.count
        }else{
            return self.resultArr.count
 
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryListViewCell") as! CategoryListViewCell
        if self.resultArr.count == 0 {

            cell.model = self.goodsList[indexPath.section]
           
        } else{
            
            cell.model = self.resultArr[indexPath.section]
        }
         return cell
        
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let tf = searchBar.value(forKey: "_searchField") as? UITextField
        if tf?.text == "" || tf?.text?.characters.count == 0 {
            ProgressHUD.showNoticeOnStatusBar(message: "请输入关键字进行搜索")
            return
        }
      //  let tf = searchBar.value(forKey: "_searchField") as? UITextField
        
        let params = ["name":tf?.text ?? "","p":self.page] as [String : Any]
        
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Mall&a=search_goods", at: self, params: params, isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
            
            self.tableView.mj_header.endRefreshing()
            self.searchBar.resignFirstResponder()
            let data = response["data"] as! [String:AnyObject]
            let goods = SearchResultModel.modelsWithArray(modelArray: data["goods"] as! [[String : AnyObject]]) as! [SearchResultModel]
            if self.page == 1{
                
                self.resultArr.removeAll()
            }
            if goods.count == 0{
                
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
            }
            self.resultArr = goods
            if self.resultArr.count == 0{
                ProgressHUD.showNoticeOnStatusBar(message: "没有符合关键字的商品，请重新搜索")
            }
            self.tableView.reloadData()
        }) {
            
        }

     //  self.tableView.mj_header.beginRefreshing()
        
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        return view
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = ShopDetailViewController()
        if self.resultArr.count == 0{
             vc.goods_id = self.goodsList[indexPath.section].goods_id
        }else{
            vc.goods_id = self.resultArr[indexPath.section].goods_id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func titleView() -> UIView {
        let tView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
        searchBar = UISearchBar()
        searchBar.returnKeyType = .search
        searchBar.delegate = self
        searchBar.placeholder = "请输入关键字进行搜索"
        searchBar.searchBarStyle = .minimal
        searchBar.showsCancelButton = true
        
        searchBar.setSearchFieldBackgroundImage(getImage(color: UIColor(hexString:"#f1f3f5"), height: 29), for: .normal)//        searchBar.setImage(UIImage(named: "sousuo2"), for: .search, state: .normal)
        searchBar.enablesReturnKeyAutomatically = true
        tView.addSubview(searchBar)
        
        let tf = searchBar.value(forKey: "_searchField") as? UITextField
        tf?.font = defaultFont(size: 13)
        tf?.tintColor = themeColor
        tf?.textColor = UIColor(hexString: "#999999")
        tf?.layer.cornerRadius = 15
        tf?.layer.masksToBounds = true
        searchBar.frame = CGRect(x: 40, y: 0, width: 270, height: 40)
//        searchBar.snp.makeConstraints { (make) in
//            make.left.equalTo(-35)
//            make.right.equalTo(0)
//            make.centerY.equalTo(22)
//            make.height.equalTo(40)
//        }
        return tView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
