//
//  ShopViewController.swift
//  BaiShiXueYiLiving
//
//  Created by 梁毅 on 2017/5/10.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh
class ShopViewController: BSBaseViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    @IBOutlet weak var collectionview: UICollectionView!
    
    @IBOutlet weak var layout: UICollectionViewFlowLayout!
    var shop = [ShopList]()
    var bannerArr = [BannerModel]()
    var page = 1
    var categoryArr = [ShopCategory]()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "商城"
        if #available(iOS 11, *) {
            self.collectionview.contentInsetAdjustmentBehavior = .never
        }else{
            self.automaticallyAdjustsScrollViewInsets = false
        }
        self.navigationItem.titleView = titleView()
        
        self.layout.minimumLineSpacing = 0
        self.layout.minimumInteritemSpacing = 0
        self.layout.headerReferenceSize = CGSize(width: kScreenWidth, height: 336/375 * kScreenWidth + 56)
        self.layout.itemSize = CGSize(width: kScreenWidth/2, height: kScreenWidth/2)

        
        collectionview.register(UINib(nibName: "ShoppingCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "ShoppingCollectionReusableView")
        collectionview.register(UINib(nibName: "RecommendCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "RecommendCollectionReusableView")
        collectionview.register(UINib(nibName:"GoodsCollectCell", bundle:Bundle.main), forCellWithReuseIdentifier: "GoodsCollectCell")
        collectionview.register(UINib(nibName: "ShoppingKindCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ShoppingKindCollectionViewCell")
        
        collectionview.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.page = 1
            self.loadData(page:self.page)
        })
//        let footer = MJRefreshAutoNormalFooter(refreshingBlock: { [unowned self] in
//            self.page += 1
//            self.loadData(page:self.page)
//        })
//        footer?.setTitle("", for: .noMoreData)
//        footer?.setTitle("", for: .idle)
//        collectionview.mj_footer = footer
        self.collectionview.mj_header.beginRefreshing()
    }
    func loadData(page:Int) {
        let params = ["p": page, "pagesize": 10]
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Mall&a=index", at: self, params: params,isShowHUD: true,hasHeaderRefresh: collectionview, success: { (response) in
            let data = response["data"] as! [String: AnyObject]
            let category = data["category"] as! [[String: AnyObject]]
            self.categoryArr = ShopCategory.modelsWithArray(modelArray: category) as! [ShopCategory]
            let goods = data["goods"] as! [[String: AnyObject]]
            let dataArr = ShopList.modelsWithArray(modelArray: goods) as! [ShopList]
            self.bannerArr = BannerModel.modelsWithArray(modelArray: data["banner"] as! [[String: AnyObject]]) as! [BannerModel]
            if self.page == 1 {
                self.shop.removeAll()
            }
            if dataArr.count < 10 {
                self.collectionview.mj_footer.endRefreshingWithNoMoreData()
            }
            self.shop += dataArr
            self.collectionview.reloadData()
        })
    }
    
    func titleView() -> UIView {
        let views = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 44))
        views.backgroundColor = UIColor.clear
        let button = UIButton()
        let img = #imageLiteral(resourceName: "sousuo2")
        button.setImage(img, for: .normal)
        button.frame = CGRect(x: (kScreenWidth-img.size.width)/2, y: 0, width: img.size.width, height: 44)
        button.addTarget(self, action: #selector(ShopViewController.jumpToSearchVC), for: .touchUpInside)
        views.addSubview(button)
        return views
    }

    func jumpToSearchVC(){
        self.navigationController?.pushViewController(CategoryListViewController(), animated: true)
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        return shop.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

            let cell = collectionview.dequeueReusableCell(withReuseIdentifier: "GoodsCollectCell", for: indexPath) as! GoodsCollectCell
            cell.shopModel = shop[indexPath.row]
            return cell
     
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ShoppingCollectionReusableView", for: indexPath) as! ShoppingCollectionReusableView
            headerView.bannerDataArr = bannerArr
            headerView.category = categoryArr
            headerView.kindsCollectionView.reloadData()
            headerView.selectIndex = { [unowned self] index in
                //1不跳转；2web链接;3个人中心；4商品
                let type = self.bannerArr[index].b_type
                if type == "1"{
                    
                }else if type == "2"{
                    let vc = MDWebViewController()
                    vc.url = self.bannerArr[index].url
                    vc.title = self.bannerArr[index].title
                    self.navigationController?.pushViewController(vc, animated: true)
                } else if type == "3"{
                    pushToUserInfoCenter(atViewController: self, uId: (self.bannerArr[index].jump)!)
                } else if type == "4"{
                    let vc = ShopDetailViewController()
                    vc.goods_id = self.bannerArr[index].jump
                    self.navigationController?.pushViewController(vc, animated: true)
                }

            }
            return headerView
 
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if shop[indexPath.row].goods_id == nil{
            return
        }
            let vc = ShopDetailViewController()
            vc.goods_id = shop[indexPath.row].goods_id
            self.navigationController?.pushViewController(vc, animated: true)
  
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.navigationController?.navigationBar.change(UIColor(hexString: "EC6B1A"), with: scrollView, andValue: 200)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.star()
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.navigationBar.isTranslucent = true
        scrollViewDidScroll(collectionview)
    }
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.reset()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
