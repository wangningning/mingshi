//
//  SearchViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/24.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController,UISearchBarDelegate{

    var searchBar: UISearchBar!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.titleView = titleView()
        // Do any additional setup after loading the view.
    }

    func titleView() -> UIView {
        let tView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
        searchBar = UISearchBar()
        searchBar.returnKeyType = .search
        searchBar.delegate = self
        searchBar.placeholder = "请输入关键字进行搜索"
        searchBar.searchBarStyle = .minimal
        searchBar.showsCancelButton = true
        searchBar.setSearchFieldBackgroundImage(getImage(color: UIColor(hexString:"#f1f3f5"), height: 29), for: .normal)
        
        //searchBar.setImage(UIImage(named: "sousuo2"), for: .search, state: .normal)
        //searchBar.backgroundColor = UIColor(hexString:"#f1f3f5")
        searchBar.enablesReturnKeyAutomatically = true
        tView.addSubview(searchBar)
        
        let tf = searchBar.value(forKey: "_searchField") as? UITextField
        tf?.font = defaultFont(size: 13)
        tf?.backgroundColor = UIColor(hexString:"#f1f3f5")
        tf?.textColor = UIColor(hexString: "#999999")
        tf?.layer.cornerRadius = 15
        tf?.layer.masksToBounds = true
        
        searchBar.snp.makeConstraints { (make) in
            make.left.equalTo(-35)
            make.right.equalTo(0)
            make.centerY.equalTo(22)
            make.height.equalTo(40)
        }
        return tView
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("!!!!!!")
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        let tf = searchBar.value(forKey: "_searchField") as? UITextField
        if (tf?.canResignFirstResponder)!{
            tf?.resignFirstResponder()
        }
 
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
