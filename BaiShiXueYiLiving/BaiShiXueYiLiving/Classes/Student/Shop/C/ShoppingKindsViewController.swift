//
//  ShoppingKindsViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/23.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh

class ShoppingKindsViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var collectionView: UICollectionView!
    
    var dataArr = [String]()
    var secondCategoryArr = [SecondCategorysModel]()
    var categoryArr = [ShopCategory]()
    var bannerDataArr = [BannerCategorysModel]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var layout: UICollectionViewFlowLayout!
    var index : Int!
    var page = 1
    var id: String!
    var shopKindsView: ShopKindsView?
    override func viewDidLoad() {
        super.viewDidLoad()

        //self.title = "分类"
        self.navigationItem.titleView = titleView()
        
    
        self.tableView.register(UINib(nibName:"ShoppingKindsTableViewCell", bundle:Bundle.main), forCellReuseIdentifier: "ShoppingKindsTableViewCell")
        self.tableView.selectRow(at: IndexPath.init(item: self.index, section: 0), animated: false, scrollPosition: .none)
        
        self.layout.minimumLineSpacing = 16
        self.layout.minimumInteritemSpacing = 11
        let w =  (kScreenWidth-100-11*2-15*2)/3
        self.layout.itemSize = CGSize(width:w, height:w * 97/75)
        self.collectionView.register(UINib(nibName:"ShopKindsCollectionViewCell", bundle:Bundle.main), forCellWithReuseIdentifier: "ShopKindsCollectionViewCell")
        self.collectionView.register(UINib(nibName:"ShoppingKindsCollectionReusableView", bundle:Bundle.main), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "ShoppingKindsCollectionReusableView")
        self.collectionView.register(UINib(nibName:"ShoppingKindsSectionCollectionReusableView", bundle:Bundle.main), forSupplementaryViewOfKind:UICollectionElementKindSectionHeader, withReuseIdentifier: "ShoppingKindsSectionCollectionReusableView")
        
        self.loadData(id: self.id)
    
        // Do any additional setup after loading the view.
        
    }



    func titleView() -> UIView {
        let views = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 44))
        views.backgroundColor = UIColor.clear
        let button = UIButton()
        let img = #imageLiteral(resourceName: "sousuo2")
        button.setImage(img, for: .normal)
        button.frame = CGRect(x: (kScreenWidth-img.size.width)/2 - 44, y: 0, width: img.size.width, height: 44)
        button.addTarget(self, action: #selector(ShoppingKindsViewController.jumpToSearchVC), for: .touchUpInside)
        views.addSubview(button)
        return views
    }
    func jumpToSearchVC(){
        if self.secondCategoryArr.count >= 0{
            let vc = CategoryListViewController()
            vc.kindsid = self.secondCategoryArr.first?.id ?? ""
           self.navigationController?.pushViewController(CategoryListViewController(), animated: true)
        }
        
    }

    func loadData(id : String) {
        
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Mall&a=second_category", at: self, params: ["id":id], isAuthHide: true, isShowHUD: false, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
            
            let data = response["data"] as! [String:AnyObject]
            let second = SecondCategorysModel.modelsWithArray(modelArray: data["second"] as! [[String : AnyObject]]) as! [SecondCategorysModel]
            let banner = BannerCategorysModel.modelsWithArray(modelArray: data["category"] as! [[String : AnyObject]]) as! [BannerCategorysModel]
            self.secondCategoryArr.removeAll()
            self.secondCategoryArr += second
            self.bannerDataArr.removeAll()
            self.bannerDataArr += banner
            self.collectionView.reloadData()
            
        }) {
            
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShoppingKindsTableViewCell") as! ShoppingKindsTableViewCell
        cell.title.text = self.categoryArr[indexPath.row].category
        if indexPath.row == 0{
            cell.isSelected = true
        }
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categoryArr.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("=========\(indexPath.row)")
        self.index = indexPath.row
        let idd = self.categoryArr[indexPath.row].id!
        if id != nil {
            self.loadData(id: idd)
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShopKindsCollectionViewCell", for: indexPath) as! ShopKindsCollectionViewCell
        cell.model = self.secondCategoryArr[indexPath.row]
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            return 0
        }
        return self.secondCategoryArr.count
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if indexPath.section == 0 {
            let view = collectionView.dequeueReusableSupplementaryView(ofKind:kind, withReuseIdentifier: "ShoppingKindsCollectionReusableView", for: indexPath) as! ShoppingKindsCollectionReusableView
          //  view.bannerDataArr = self.bannerDataArr
            if self.bannerDataArr.count > 0 {
                view.bannerImg.kf.setImage(with: URL(string:self.bannerDataArr[index].banner_img!)!)
            }
            
            return view
        }
        let section = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ShoppingKindsSectionCollectionReusableView", for: indexPath) as! ShoppingKindsSectionCollectionReusableView
        section.sectionTitle.text = self.categoryArr[index].category
        return section
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        let w = kScreenWidth - 100
        if section == 0{
            return CGSize(width: w, height: w * 1/3)
        } else {
            return CGSize(width: w, height: 30)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 0{
            return UIEdgeInsetsMake(0, 10, 0, 10)
        } else {
            return UIEdgeInsetsMake(10, 10, 5, 15)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = CategoryListViewController()
        vc.kindsid = self.secondCategoryArr[indexPath.row].id
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
