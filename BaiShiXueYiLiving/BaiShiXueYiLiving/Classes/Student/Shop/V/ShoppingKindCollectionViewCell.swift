//
//  ShoppingKindCollectionViewCell.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/23.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class ShoppingKindCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var img: UIImageView!
    var model: ShopCategory! {
        willSet(m) {
           // img.kf.setImage(with: URL(string: m.picture!))
            img.kf.setImage(with: URL(string: m.picture!), placeholder: #imageLiteral(resourceName: "icon_liuxing"), options: nil, progressBlock: nil, completionHandler: nil)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
