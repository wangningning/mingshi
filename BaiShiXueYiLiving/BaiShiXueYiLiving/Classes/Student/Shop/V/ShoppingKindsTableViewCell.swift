//
//  ShoppingKindsTableViewCell.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/30.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class ShoppingKindsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var leftLine: UILabel!
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var bottomLine: UILabel!
    @IBOutlet weak var rightLine: UILabel!
    
//    var model: kindsModel!{
//        willSet(m){
//            if m.isSelected == true{
//                self.leftLine.backgroundColor = UIColor(hexString:"#ec6b1a")
//                self.contentView.backgroundColor = UIColor.white
//                self.rightLine.backgroundColor = UIColor.white
//                self.title.textColor = UIColor(hexString:"#ec6b1a")
//            } else {
//                
//                self.leftLine.backgroundColor = UIColor(hexString:"#f1f5f6")
//                self.title.textColor = UIColor(hexString:"#333333")
//                self.rightLine.backgroundColor = UIColor(hexString:"#cdcdcd")
//                self.contentView.backgroundColor = UIColor(hexString:"f1f5f6")
//            }
//            self.title.text = m.title
//        }
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if self.isSelected{
            
            self.leftLine.backgroundColor = UIColor(hexString:"#ec6b1a")
            self.contentView.backgroundColor = UIColor.white
            self.rightLine.backgroundColor = UIColor.white
            self.title.textColor = UIColor(hexString:"#ec6b1a")
        }
        
       // self.bottomLine.backgroundColor = UIColor(hexString:"#cdcdcd")
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected == true {
            
            self.leftLine.backgroundColor = UIColor(hexString:"#ec6b1a")
            self.contentView.backgroundColor = UIColor.white
            self.rightLine.backgroundColor = UIColor.white
            self.title.textColor = UIColor(hexString:"#ec6b1a")
            
        } else {
            self.leftLine.backgroundColor = UIColor(hexString:"#f1f5f6")
            self.title.textColor = UIColor(hexString:"#333333")
            self.rightLine.backgroundColor = UIColor(hexString:"#cdcdcd")
            self.contentView.backgroundColor = UIColor(hexString:"f1f5f6")
        }
        // Configure the view for the selected state
    }
    
}
