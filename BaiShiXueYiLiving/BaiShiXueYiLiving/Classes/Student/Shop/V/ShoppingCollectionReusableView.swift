//
//  ShoppingCollectionReusableView.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/23.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class ShoppingCollectionReusableView: UICollectionReusableView,UICollectionViewDelegate,UICollectionViewDataSource{

    @IBOutlet weak var bannerView: UIView!
    
    @IBOutlet weak var kindsCollectionView: UICollectionView!
    
    @IBOutlet weak var layout: UICollectionViewFlowLayout!
    var pageScrollView: CarouselView?
    var selectIndex: ((Int)->())?
    var bannerDataArr: [BannerModel]! {
        willSet(m) {
            pageScrollView?.fetchImageUrl = { index in
                return m[index].b_img!
            }
            pageScrollView?.totalPages = m.count
        }
    }
    var category = [ShopCategory]()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.layout.scrollDirection = .horizontal
        self.layout.minimumLineSpacing = 45
        self.layout.sectionInset = UIEdgeInsetsMake(25, 15, 25, 15)
        let w = (kScreenWidth - 15 * 2 - 45 * 2)/3
        self.layout.itemSize = CGSize(width: w, height: w)
        kindsCollectionView.delegate = self
        kindsCollectionView.dataSource = self
        kindsCollectionView.showsVerticalScrollIndicator = false
        kindsCollectionView.showsHorizontalScrollIndicator = false
        kindsCollectionView.register(UINib.init(nibName: "ShoppingKindCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ShoppingKindCollectionViewCell")
        
        self.pageScrollView = CarouselView(frame: CGRect(x:0, y:0, width:kScreenWidth, height: 200/375 * kScreenWidth), animationDuration: 3.6, didSelect: { [unowned self] index in
            self.selectIndex?(index)
        })
        bannerView.addSubview(self.pageScrollView!)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShoppingKindCollectionViewCell", for: indexPath) as! ShoppingKindCollectionViewCell
        cell.model = self.category[indexPath.row]
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return category.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = ShoppingKindsViewController()
        vc.id = self.category[indexPath.row].id
        vc.categoryArr = self.category
        vc.index = indexPath.row
        self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)

    }
    
}
