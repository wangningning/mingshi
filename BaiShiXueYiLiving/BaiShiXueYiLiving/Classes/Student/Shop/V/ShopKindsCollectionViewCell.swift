//
//  ShopKindsCollectionViewCell.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/23.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class ShopKindsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var goodsImg: UIImageView!
    
    @IBOutlet weak var goodsName: UILabel!
    
    var model : SecondCategorysModel! {
        willSet(m){
            goodsImg.kf.setImage(with: URL(string:m.picture!)!)
            goodsName.text = m.category
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
