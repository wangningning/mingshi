//
//  CategoryListViewCell.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/24.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class CategoryListViewCell: UITableViewCell {

    @IBOutlet weak var goodsImage: UIImageView!
    
    @IBOutlet weak var goodsName: UILabel!
    
    @IBOutlet weak var goodsPrice: UILabel!
    
    var model: SearchResultModel!{
        willSet(m){
            goodsImage.kf.setImage(with: URL(string:m.thumb!)!)
            goodsName.text = m.name
            goodsPrice.text =  "RMB " + m.sale_price!
        }
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
