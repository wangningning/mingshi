//
//  ShopKindsView.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/23.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class ShopKindsView: UIView {

    @IBOutlet weak var kindOneBtn: UIButton!
    @IBOutlet weak var kindOneLab: UILabel!
    @IBOutlet weak var kindTwoBtn: UIButton!
    @IBOutlet weak var kindTwoLab: UILabel!
    @IBOutlet weak var kindThreeBtn: UIButton!
    @IBOutlet weak var kindThreeLab: UILabel!
    @IBOutlet weak var topLayout: NSLayoutConstraint!
    
    var selectIndex: ((Int) -> ())?
    var selectBtn = UIButton()
    var index: Int! {
        willSet{
            topLayout.constant = CGFloat(self.kindOneBtn.frame.size.height * CGFloat(newValue))
            if newValue == 0 {
                self.kindOneBtn.isSelected = true
                self.kindOneBtn.backgroundColor = UIColor.white
                self.kindOneLab.isHidden = true
                selectBtn = self.kindOneBtn
            } else if newValue == 1 {
                self.kindTwoBtn.isSelected = true
                self.kindTwoBtn.backgroundColor = UIColor.white
                self.kindTwoLab.isHidden = true
                selectBtn = self.kindTwoBtn
            } else if newValue == 2 {
                self.kindThreeBtn.isSelected = true
                self.kindThreeBtn.backgroundColor = UIColor.white
                self.kindThreeLab.isHidden = true
                selectBtn = self.kindThreeBtn
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
        
        
    }
    class func setView() -> ShopKindsView{
        
        let view = Bundle.main.loadNibNamed("ShopKindsView", owner: nil, options: nil)?.first as! ShopKindsView
        return view
    
    }
    @IBAction func selectKindAction(_ sender: UIButton) {
        
        if sender == selectBtn {
            return
        }
        topLayout.constant = CGFloat(self.kindOneBtn.frame.size.height * CGFloat(sender.tag))
        selectBtn.isSelected = false
        sender.isSelected = true
        selectBtn = sender
        if sender.tag == 0{
            self.kindOneBtn.backgroundColor = UIColor.white
            self.kindTwoBtn.backgroundColor = UIColor(hexString: "#f1f5f6")
            self.kindThreeBtn.backgroundColor = UIColor(hexString: "#f1f5f6")
            self.kindOneLab.isHidden = true
            self.kindTwoLab.isHidden = false
            self.kindThreeLab.isHidden = false
        } else if sender.tag == 1{
            self.kindOneBtn.backgroundColor = UIColor(hexString: "#f1f5f6")
            self.kindTwoBtn.backgroundColor = UIColor.white
            self.kindThreeBtn.backgroundColor = UIColor(hexString: "#f1f5f6")
            self.kindOneLab.isHidden = false
            self.kindTwoLab.isHidden = true
            self.kindThreeLab.isHidden = false
        } else {
            self.kindOneBtn.backgroundColor = UIColor(hexString: "#f1f5f6")
            self.kindTwoBtn.backgroundColor = UIColor(hexString: "#f1f5f6")

            self.kindThreeBtn.backgroundColor = UIColor.white
            self.kindOneLab.isHidden = false
            self.kindTwoLab.isHidden = false
            self.kindThreeLab.isHidden = true
        }
        self.selectIndex?(sender.tag)
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
