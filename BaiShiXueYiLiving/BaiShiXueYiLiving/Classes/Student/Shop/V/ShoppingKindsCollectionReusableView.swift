//
//  ShoppingKindsCollectionReusableView.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/23.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class ShoppingKindsCollectionReusableView: UICollectionReusableView {

    var selectImage: ((Int)->())?
    
    
    @IBOutlet weak var bannerView: UIView!
   
    @IBOutlet weak var bannerImg: UIImageView!
    
    var pageScrollView: CarouselView?
    
    var bannerDataArr: [BannerCategorysModel]! {
        willSet(m) {
            pageScrollView?.fetchImageUrl = { index in
                return m[index].banner_img!
            }
            pageScrollView?.totalPages = m.count
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.pageScrollView = CarouselView(frame: CGRect(x:10, y:0, width:kScreenWidth - 20, height: 85/250 * kScreenWidth), animationDuration: 3.6, didSelect: { [unowned self] index in
//            self.selectImage?(index)
//        })
       // bannerView.addSubview(self.pageScrollView!)
        
    }
//    func clickedImage(){
//        self.selectImage?()
//    }
    
    
}
