
//
//  HomePageViewController.swift
//  BaiShiXueYiLiving
//
//  Created by 梁毅 on 2017/5/8.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh
import Alamofire
let RefreshLoadStateNotification = "RefreshLoadStateNotification"
class HomePageViewController: BSBaseViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var layout: YSHClassifyCollectionViewLayout!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var page = 1
    var livingType = 1
    var bannerDataArr = [BannerModel]()
    var hotLivingData: [LiveList] = []
    var thLivingData: [LiveList] = []
    var videoData: [VideoModel] = []
    var isLoading = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(hexString: "f1f5f4")
        if #available(iOS 11, *) {
            self.collectionView.contentInsetAdjustmentBehavior = .never
        }else{
            self.automaticallyAdjustsScrollViewInsets = false
        }
        
        //NotificationCenter.default.addObserver(self, selector: #selector(HomePageViewController.uploadFreshLoad(noti:)), name: NSNotification.Name(rawValue:RefreshLoadStateNotification), object: nil)
        
        
        layout.colsForSections = [1, 2]
        layout.rowMargins = [0, 14]
        layout.colMargins = [1, 21]
        layout.headerHeight = [323/375 * kScreenWidth + 59,75]
        
        layout.sectionInsets = [UIEdgeInsetsMake(0, 0, 0, 0), UIEdgeInsetsMake(0, 12, 10, 12)]
        layout.itemWAndHRelation = [(.height,   kScreenWidth + 70),   (.rate, CGFloat(191 / 166))]
        
        collectionView.register(UINib(nibName: "HomePageCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HomePageCollectionReusableView")
        collectionView.register(UINib(nibName: "HomePageLiveStoreCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HomePageLiveStoreCollectionReusableView")
        
        collectionView.register(UINib(nibName: "HomePageLiveCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomePageLiveCollectionViewCell")
        collectionView.register(UINib(nibName: "HomePageLiveStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomePageLiveStoreCollectionViewCell")
        self.navigationItem.titleView = titleView()
        
        collectionView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [weak self] in
            self?.page = 1
            self?.loadBannerData()
            self?.fetchLivingList()
            self?.fetchVideoList()
        })
        collectionView.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: { [unowned self] in
            self.page += 1
            self.fetchVideoList()
        })
        self.collectionView.mj_header.beginRefreshing()
    }
    func uploadFreshLoad(noti: Notification){
        self.isLoading = false
        print("~~~~~~~~~~~~~~~~~~~~接收到通知刷新状态")
        self.collectionView.reloadData()
    }
    func fetchLivingList() {
        let params = ["p": self.page, "pagesize": 10, "type": livingType]
        NetworkingHandle.fetchNetworkData(url: "/Home/live_list", at: self, params: params,isShowHUD: true, success: { (response) in
            let data = response["data"] as! [String: AnyObject]
            let list = data["list"] as! [[String: AnyObject]]
            let ll = LiveList.modelsWithArray(modelArray: list) as! [LiveList]
            if self.livingType == 1 {
                self.hotLivingData = ll
            } else {
                self.thLivingData = ll
            }
            self.collectionView.reloadData()
        })
    }
    func fetchVideoList() {
        let params = ["p": page, "pagesize": 10]
        NetworkingHandle.fetchNetworkData(url: "/Home/video", at: self, params: params, isShowHUD: false, hasHeaderRefresh: collectionView, success: { (response) in
            let data = response["data"] as! [String: AnyObject]
            let list = data["list"] as! [[String: AnyObject]]
            let ll = VideoModel.modelsWithArray(modelArray: list) as! [VideoModel]
            if self.page == 1 {
                self.videoData.removeAll()
            }
            if ll.count == 0 {
                self.collectionView.mj_footer.endRefreshingWithNoMoreData()
            }
            self.videoData += ll
            self.collectionView.reloadData()
        }) {
            if self.page > 1 {
                self.page -= 1
            }
        }
    }
    
    // BANNER
    func loadBannerData() {
        NetworkingHandle.fetchNetworkData(url: "/Home/banner_list", at: self,isShowHUD: false, success: { (response) in
            let data = response["data"] as! [[String: AnyObject]]
            self.bannerDataArr = BannerModel.modelsWithArray(modelArray: data) as! [BannerModel]
            self.collectionView.reloadData()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func titleView() -> UIView {
        let views = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 44))
        views.backgroundColor = UIColor.clear
        let button = UIButton()
        let img = #imageLiteral(resourceName: "sousuo2")
        button.setImage(img, for: .normal)
        button.frame = CGRect(x: (kScreenWidth-img.size.width)/2, y: 0, width: img.size.width, height: 44)
        button.addTarget(self, action: #selector(HomePageViewController.jumpToSearchVC), for: .touchUpInside)
        views.addSubview(button)
        return views
    }
    func jumpToSearchVC(){
        self.navigationController?.pushViewController(TSearchViewController(), animated: true)
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            if livingType == 1 {
                return hotLivingData.count
            }
            return thLivingData.count
        }
        return videoData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomePageLiveCollectionViewCell", for: indexPath) as! HomePageLiveCollectionViewCell
            if livingType == 1 {
                cell.model = hotLivingData[indexPath.row]
            } else {
                cell.model = thLivingData[indexPath.row]
            }
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomePageLiveStoreCollectionViewCell", for: indexPath) as! HomePageLiveStoreCollectionViewCell
        cell.model = videoData[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        if indexPath.section == 0 {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HomePageCollectionReusableView", for: indexPath) as! HomePageCollectionReusableView
            headerView.selectedBlock = { [unowned self] type in
                
                
                self.page = 1
                self.livingType = type
                self.fetchLivingList()
            }
            headerView.selectedImgBlock = { [weak self] index in
                //1不跳转；2web链接;3个人中心；4商品
                let type = self?.bannerDataArr[index].b_type
                if type == "1"{
                    
                }else if type == "2"{
                    DispatchQueue.main.async { [weak self] in
                        let vc = MDWebViewController()
                        vc.url = self?.bannerDataArr[index].url
                        vc.title = self?.bannerDataArr[index].title
                        self?.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                } else if type == "3"{
                    DispatchQueue.main.async { [weak self] in
                        pushToUserInfoCenter(atViewController: self!, uId: (self?.bannerDataArr[index].jump)!)
                    }
                    
                } else if type == "4"{
                    DispatchQueue.main.async { [weak self] in
                        let vc = ShopDetailViewController()
                        vc.goods_id = self?.bannerDataArr[index].jump
                        self?.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                }
                
            }
            headerView.bannerDataArr = bannerDataArr
            return headerView
        }
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HomePageLiveStoreCollectionReusableView", for: indexPath) as! HomePageLiveStoreCollectionReusableView
        return headerView
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.section == 0{
           
            
            var params : Dictionary<String,Any> = [:]
            
            if let m = DLUserInfoHandler.getIdAndToken() {
                params["uid"] = m.id
                params["token"] = m.token
                if self.livingType == 1{
                    params["live_id"] = self.hotLivingData[indexPath.row].live_id
                }else{
                    params["live_id"] = self.thLivingData[indexPath.row].live_id
                }
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                Alamofire.request(NetworkingHandle.mainHost + "/index/check_into_live", method: .post, parameters: params).responseJSON(completionHandler: { (dataResponse) in
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    if let error = dataResponse.result.error{
                        
                        if let status = dataResponse.response?.statusCode{
                            if status != 200{
                                
                                
                                ProgressHUD.showNoticeOnStatusBar(message: "\(status)-服务器访问失败")
                            }
                            
                        }else{
                            
                            
                            ProgressHUD.showNoticeOnStatusBar(message: error.localizedDescription)
                        }
                    }
                    guard let json = dataResponse.result.value else {
                        ProgressHUD.showNoticeOnStatusBar(message: "未知错误")
                        
                        return
                    }
                    let result: Dictionary<String, Any> = json as! Dictionary
                    let code: String = (result["status"] as? String)!
                    if code == "ok" {
                        
                        if self.livingType == 1{
                            
                            WatchLiveViewController.toWatch(from: self, model: self.hotLivingData[indexPath.row])
                            
                        }else {
                            WatchLiveViewController.toWatch(from: self, model: self.thLivingData[indexPath.row])
                        }
                    } else {
                        switch code {
                        case "pending":
                            ProgressHUD.showNoticeOnStatusBar(message: result["data"] as! String)
                            DLUserInfoHandler.deleteUserInfo()
                            EMClient.shared().logout(true)
                            UIApplication.shared.keyWindow?.rootViewController = LoginNavigationController.setup()
                        default:
                            
                            LyAlertView.alert(message: result["data"] as! String, ok: "升级", okBlock: {
                                
                                let vc = MembershipDetailViewController()
                                if self.livingType == 1{
                                    vc.teacherID = self.hotLivingData[indexPath.row].user_id
                                }else{
                                    vc.teacherID = self.thLivingData[indexPath.row].user_id
                                }
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                            })
                            
                            
                        }
                    }
                    
                })
                
            }
            
            
        } else{
            
            let vc = TCurriculumDetailViewController()
            vc.videoID = videoData[indexPath.row].video_id
            vc.play_img = videoData[indexPath.row].video_img
            vc.url = videoData[indexPath.row].url
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.navigationController?.navigationBar.change(UIColor(hexString: "EC6B1A"), with: scrollView, andValue: 200)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.star()
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.navigationBar.isTranslucent = true
        scrollViewDidScroll(collectionView)
    }
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.reset()
    }
    
}
