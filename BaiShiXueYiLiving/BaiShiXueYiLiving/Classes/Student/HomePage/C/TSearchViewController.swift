//
//  TSearchViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/7/23.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh

class TSearchViewController: UIViewController, UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource{
   
    
    @IBOutlet weak var tableView: UITableView!
    var searchBar: UISearchBar!
    var searchResult: [ListTPointingModel] = []
    var page = 1
    override func viewWillDisappear(_ animated: Bool) {
        self.searchBar.resignFirstResponder()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = themeColor
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: UITableViewCell.description())
        self.tableView.register(UINib.init(nibName: "TPointingTableViewCell", bundle: nil), forCellReuseIdentifier: TPointingTableViewCell.description())
        self.tableView.register(UINib.init(nibName: "TSearchSectionFooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: TSearchSectionFooterView.description())
        self.tableView.tableFooterView = UIView()
        self.navigationItem.titleView = titleView()
        let header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.page = 1
            self.loadData()
        })
        if self.historyArr.count == 0 {
            header?.setTitle("搜索记录为空", for: .idle)
            header?.setTitle("搜索记录为空", for: .noMoreData)
        }
        self.tableView.mj_header = header
        let footer = MJRefreshAutoNormalFooter(refreshingBlock: {  [unowned self] in
            self.page += 1
            self.loadData()
        })
        footer?.setTitle("", for: .idle)
        self.tableView.mj_footer = footer
        // Do any additional setup after loading the view.
    }
    func loadData()
    {
        if searchBar.text?.characters.count == 0 {
            self.tableView.mj_header.endRefreshing()
            self.tableView.mj_footer.endRefreshingWithNoMoreData()
            return
        }
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Home&a=search", at: self, params: ["name":searchBar.text!,"p":self.page], isShowError: true, hasHeaderRefresh: self.tableView, success: { (response) in
            let data = response["data"] as! [String:AnyObject]
            let arr = ListTPointingModel.modelsWithArray(modelArray:data["list"] as! [[String : AnyObject]]) as! [ListTPointingModel]
            if self.page == 1{
                self.searchResult.removeAll()
            }
            if arr.count == 0{
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
            }
            self.searchResult += arr
            self.tableView.reloadData()
            
        }) {
            
        }
    }
    fileprivate var historyArr:[String] = {
        let data = UserDefaults.standard.value(forKey: "history") as? [String] ?? []
        return data
    }()
    
    func titleView() -> UIView {
        let tView = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 44))
        searchBar = UISearchBar()
        searchBar.frame = CGRect.init(x: 0, y: 0, width: kScreenWidth - 70, height: 44)
        searchBar.returnKeyType = .search
        searchBar.delegate = self
        searchBar.placeholder = "请输入关键字或直播ID搜索"
        searchBar.searchBarStyle = .minimal
        searchBar.showsCancelButton = true
        searchBar.setSearchFieldBackgroundImage(getImage(color: UIColor.white, height: 29), for: .normal)
       // searchBar.setImage(UIImage(named: "sousuo2"), for: .search, state: .normal)
        searchBar.enablesReturnKeyAutomatically = true
        tView.addSubview(searchBar)
        
        let tf = searchBar.value(forKey: "_searchField") as? UITextField
        tf?.font = defaultFont(size: 13)
        tf?.tintColor = themeColor
        tf?.textColor = UIColor(hexString: "#999999")
        tf?.layer.cornerRadius = 15
        tf?.layer.masksToBounds = true
        
        
        return tView
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text?.characters.count == 0 {
            ProgressHUD.showMessage(message: "请输入想要查找的导师")
            return
        }
        self.page = 1
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Home&a=search", at: self, params: ["name":searchBar.text!,"p":self.page], isShowError: true, hasHeaderRefresh: self.tableView, success: { (response) in
            let data = response["data"] as! [String:AnyObject]
            let arr = ListTPointingModel.modelsWithArray(modelArray:data["list"] as! [[String : AnyObject]]) as! [ListTPointingModel]
            if self.page == 1{
                self.searchResult.removeAll()
            }
            if arr.count == 0{
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
            }
            self.searchResult += arr
            if self.searchResult.count == 0{
                ProgressHUD.showMessage(message: "暂无符合条件的搜索结果")
            }
            self.tableView.reloadData()
            
        }) {
            
        }
        
        for str in self.historyArr {
            if str == searchBar.text! {
                return
            }
        }
        if self.historyArr.count > 30 {
            self.historyArr.removeFirst()
        }
        self.historyArr.append(searchBar.text!)
        UserDefaults.standard.set(self.historyArr, forKey: "history")
        
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        self.tableView.isHidden = false
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        let tf = searchBar.value(forKey: "_searchField") as! UITextField
        if tf.text == "" {
            self.navigationController?.popViewController(animated: true)
        }else{
            tf.text = ""
            tf.resignFirstResponder()
            self.tableView.isHidden = true
            self.searchResult.removeAll()
            self.tableView.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.searchResult.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: UITableViewCell.description())!
            cell.textLabel?.text = self.historyArr[indexPath.row]
            cell.selectionStyle = .none
            return cell
        } else{
            let cell = tableView.dequeueReusableCell(withIdentifier: TPointingTableViewCell.description()) as! TPointingTableViewCell
            cell.model = self.searchResult[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.searchResult.count == 0 {
            return self.historyArr.count
        }else{
            return self.searchResult.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.searchResult.count == 0 {
            return 45
        }else{
            return kScreenWidth * 81 / 375
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.searchResult.count == 0 {
            searchBar.text = self.historyArr[indexPath.row]
            self.loadData()
            return
        }
        searchBar.resignFirstResponder()
        pushToUserInfoCenter(atViewController: self, uId:self.searchResult[indexPath.row].user_id!)
       // let vc = HisPersonalMemberCenterViewController()
       // vc.userId = self.searchResult[indexPath.row].user_id
       // self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: TSearchSectionFooterView.description()) as! TSearchSectionFooterView
        footer.cleanRecord = { [unowned self] in
            self.cleanAllHistory()
        }
        return footer
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if self.searchResult.count == 0 {
            if self.historyArr.count == 0 {
                return 0
            }
           return 80
        }else{
            return 0
        }
        
    }
    func cleanAllHistory() {
        self.historyArr.removeAll()
        UserDefaults.standard.set(self.historyArr, forKey: "history")
        self.tableView.reloadData()
        self.tableView.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
