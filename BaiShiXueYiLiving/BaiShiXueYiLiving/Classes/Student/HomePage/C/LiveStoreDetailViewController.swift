//
//  LiveStoreDetailViewController.swift
//  BaiShiXueYiLiving
//
//  Created by 梁毅 on 2017/5/18.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class LiveStoreDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource ,BBSDetailsReplyViewDelegate, STInputBarDelegate {
    var tempCell: BBSDetailReplyCell!
    @IBOutlet weak var tableView: UITableView!
    var inputView1: STInputBar!
    var detailReplyView: BBSDetailsReplyView!
    var headerView: BBSDetailHeaderView!
    let placeHolder = "说点什么吧"
    var commentList: [BBSResponseModel] = []
    
    var isDirectlyReply = false  //开启页面直接回复
    var isCommentReply: Bool = false//是否为回复评论
    var commentModel: BBSResponseModel?//被回复人的模型
    var bbsModel: BBSListModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.keyboardDismissMode = .onDrag
        tableView.separatorColor = UIColor.clear
        
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 40
        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        tableView.estimatedSectionHeaderHeight = 40
        tableView.register(UINib(nibName: "LiveStoreDetailCommentViewCell", bundle: nil), forCellReuseIdentifier: "LiveStoreDetailCommentViewCell")
        tableView.register(UINib(nibName: "LiveStoreDetailHeaderViewCell", bundle: nil), forCellReuseIdentifier: "LiveStoreDetailHeaderViewCell")
        tableView.register(UINib(nibName: "LiveStoreDetailVideoHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "LiveStoreDetailVideoHeaderView")
        tableView.register(UINib(nibName: "LiveStoreDetailCommentHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "LiveStoreDetailCommentHeaderView")
        inputView1 = STInputBar(frame: CGRect(x: 0, y: kScreenHeight - 45 - 64, width: self.view.frame.size.width, height: 45))
        inputView1.fitWhenKeyboardShowOrHide = true
        inputView1.myDelegate = self;
        inputView1.setDidSendClicked { (text) in  //点击发送
            self.clickSend(text: text!)
        }
        self.view.addSubview(inputView1)
        
        detailReplyView = BBSDetailsReplyView(frame: CGRect(x: 0, y: kScreenHeight - 64 - 45, width: kScreenWidth, height: 45))
        detailReplyView.myDelegate = self
        detailReplyView.model = bbsModel
        self.view.addSubview(detailReplyView)
        
        tableView.reloadData()
        
        if isDirectlyReply {
            inputView1.textView.becomeFirstResponder()
            inputView1.placeHolder = placeHolder
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LiveStoreDetailHeaderViewCell", for: indexPath) as! LiveStoreDetailHeaderViewCell
            cell.selectionStyle = .none
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "LiveStoreDetailCommentViewCell", for: indexPath) as! LiveStoreDetailCommentViewCell
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "LiveStoreDetailVideoHeaderView") as! LiveStoreDetailVideoHeaderView
            return view
        }
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "LiveStoreDetailCommentHeaderView") as! LiveStoreDetailCommentHeaderView
        return view
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return 2
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    //MARK: - STInputBarDelegate
    func inputBarShouldReturn(_ inputView: STInputBar!) {
        self.clickSend(text: inputView.textView.text!)
    }
    //MARK: -点击发送
    func clickSend(text: String) {
        
        if self.isCommentReply && self.commentModel != nil{
            self.sendReply(text: text, response_id: (self.commentModel?.comment_id!)!)
        } else {
            self.sendReply(text: text, response_id: "")
        }
        self.inputView1.textView.resignFirstResponder()
    }
    //MARK: - 回复
    func sendReply(text: String, response_id: String) {
        
        let params = ["post_id" : bbsModel.post_id as AnyObject,
                      "content" : text as AnyObject,
                      "response_id" : response_id as AnyObject
            ] as [String : AnyObject]
        
        //        LYNetWorkRequest.fetchNetworkData(url: "/api.php?m=Api&c=Topical&a=comment_post", params: params as [String : AnyObject], isAutoHide: false, isShowOrNot: "2", hasHeaderRefresh: nil, success: { (response) in
        //            ProgressHUDManager.showSuccessWithStatus(string: "回复成功")
        //            self.requestDetailData()//评论成功过更新数据
        //        }) { (errorCode) in
        //
        //        }
    }
    //MARK: - BBSDetailsReplyViewDelegate
    func detailsReplyView(replyView: BBSDetailsReplyView, index: Int) {
        if index == 1 ||  index == 2{
            inputView1.textView.becomeFirstResponder()
            inputView1.placeHolder = placeHolder
            isCommentReply = false
        } else {
            //            likePost()
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
