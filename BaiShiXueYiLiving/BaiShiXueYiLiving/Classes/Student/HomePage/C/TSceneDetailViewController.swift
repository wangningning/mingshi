//
//  TSceneDetailViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/27.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

let navHeight:CGFloat = 64
var bottomViewHeight:CGFloat = 61

class TSceneDetailViewController: BSBaseViewController,UIScrollViewDelegate,UIWebViewDelegate{

    var headView: TSceneDetailHeaderView!
    var id: String!
    var dmodel: TSceneDetailModel!
    var isFromMineVC = false
    var result = ""
    @IBOutlet weak var applyBtn: UIButton!
    @IBOutlet weak var priceLab: UILabel!
    @IBOutlet weak var bottomViewH: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!
    
    var applyNumLab = UILabel()
    var totalSumLab = UILabel()
    var bgView = UIView()
    var applyNum = UILabel()
    var totalSum = UILabel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "名师现场"
        
        self.headView = TSceneDetailHeaderView.setup()
        self.headView.priceChange = { [unowned self] text in
            self.priceLab.text = text
        }
        bottomViewHeight = 61
        if isFromMineVC {
            
            bottomViewHeight = 0
            bottomViewH.constant = 0
            bottomView.isHidden = true
            
            self.headView.applyNumView.isHidden = true
            self.headView.applyNumH.constant = 0
            self.headView.priceView.isHidden = true
            self.headView.priceH.constant = 0
            
            self.bgView.backgroundColor = UIColor.white
            self.headView.addSubview(self.bgView)
            self.bgView.snp.makeConstraints({ (make) in
                make.left.equalTo(0)
                make.top.equalTo(317)
                make.size.equalTo(CGSize.init(width: kScreenWidth, height: 100))
            })
            
            self.applyNumLab.text = "报名数量"
            self.applyNumLab.textColor = UIColor(hexString: "#333333")
            self.applyNumLab.font = UIFont.systemFont(ofSize: 15)
            self.applyNumLab.backgroundColor = UIColor.white
            self.bgView.addSubview(self.applyNumLab)
            self.applyNumLab.snp.makeConstraints({ (make) in
                make.left.equalTo(13)
                make.top.equalTo(0)
                make.size.equalTo(CGSize.init(width: 70, height: 50))
            })
            
            self.applyNum.textColor = UIColor(hexString: "#333333")
            self.applyNum.font = UIFont.systemFont(ofSize: 15)
            self.applyNum.backgroundColor = UIColor.white
            self.bgView.addSubview(self.applyNum)
            self.applyNum.snp.makeConstraints({ (make) in
                make.left.equalTo(self.applyNumLab.snp.right)
                make.top.equalTo(0)
                make.height.equalTo(50)
                make.right.equalTo(0)
            })
            self.totalSumLab.text = "合计"
            self.totalSumLab.textColor = UIColor(hexString: "#333333")
            self.totalSumLab.font = UIFont.systemFont(ofSize: 15)
            self.totalSumLab.backgroundColor = UIColor.white
            self.bgView.addSubview(self.totalSumLab)
            self.totalSumLab.snp.makeConstraints({ (make) in
                make.left.equalTo(13)
                make.top.equalTo(self.applyNumLab.snp.bottom)
                make.size.equalTo(CGSize.init(width: 70, height: 50))
            })
            
            self.totalSum.textColor = UIColor(hexString: "#ec6b1a")
            self.totalSum.font = UIFont.systemFont(ofSize: 15)
            self.totalSum.backgroundColor = UIColor.white
            self.bgView.addSubview(self.totalSum)
            self.totalSum.snp.makeConstraints({ (make) in
                make.left.equalTo(self.totalSumLab.snp.right)
                make.top.equalTo(self.totalSumLab.snp.top)
                make.right.equalTo(0)
                make.height.equalTo(50)
            })

            
           
            
        }
        
        let versionKey = String(kCFBundleVersionKey)
        let currentVersion = Bundle.main.infoDictionary?[versionKey] as! String
        print(" 当前版本号为：\(currentVersion)")
        let param = ["version": currentVersion]
        NetworkingHandle.fetchNetworkData(url: "/Member/is_this", at: self, params: param, isShowHUD: false, isShowError: false, success: { (response) in
            self.result = response["data"] as! String
            if self.result == "1"{
                self.headView.price.isHidden = true
                self.headView.saletxt.isHidden = true
                self.applyBtn.setTitle("免费报名", for: .normal)
                self.priceLab.isHidden = true
                
            }else{
                self.headView.price.isHidden = false
                self.headView.saletxt.isHidden = false
                self.applyBtn.setTitle("立即报名", for: .normal)
                self.priceLab.isHidden = false
            }
        }) {
            
        }
        self.mailScrollView.addSubview(headView)
        self.mailScrollView.addSubview(self.webview)
        self.view.addSubview(self.mailScrollView)
        self.loadData()
       
       
        // Do any additional setup after loading the view.
    }
    func loadData(){
        
        self.webview.loadRequest(URLRequest.init(url: URL.init(string: NetworkingHandle.mainHost + "/home/tutorWebView/id/" + self.id)!))
        
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Home&a=tutor_scene_detail", at: self, params: ["id":self.id], isShowError: true, success: { (response) in
            let data = response["data"] as! [String:AnyObject]
            self.dmodel = TSceneDetailModel.modelWithDictionary(diction: data)
            self.priceLab.text = "¥" + self.dmodel.price!
            self.headView.model = self.dmodel
            if self.isFromMineVC{
                if self.result == "1"{
                    self.totalSumLab.isHidden = true
                    self.totalSum.isHidden = true
                    return
                }
                self.applyNum.text = self.dmodel.number
                self.totalSum.text = "¥" + self.dmodel.amount!
            }
            self.headView.reloadInputViews()
        }) {
            
        }
    }
    
    @IBAction func applyAction(_ sender: UIButton) {
        
        if self.result == "1"{
            
            ProgressHUD.showSuccess(message: "报名成功")
            self.navigationController?.popViewController(animated: true)
            
        }else{
            self.applyBtn.isEnabled = false
            NetworkingHandle.fetchNetworkData(url: "/Home/tutor_scene_order_show", at: self, params: ["id": self.id,"number":self.headView.numBtn.titleLabel!.text ?? ""], isShowHUD: true, isShowError: true, success: { (response) in
                let amount = response["data"] as! String
                NetworkingHandle.fetchNetworkData(url: "/Home/add_class_order", at: self, params: ["id": self.id,"amount":amount,"number":self.headView.numBtn.titleLabel?.text ?? "1"], isShowHUD: true, isShowError: true, success: { (response) in
                    let data = response["data"] as! String
                    let vc = SelectPayWayViewController()
                    vc.payType = "4"
                    vc.order_no = data
                    vc.amount = amount
                    vc.successPay = { [unowned self] in
                        self.loadData()
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }) {
                    self.applyBtn.isEnabled = true
                }
                
                
            }) { 
                self.applyBtn.isEnabled = true
            }

        }
        
        
    }
    
    fileprivate lazy var mailScrollView:UIScrollView = {
        let scroll = UIScrollView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight - navHeight - bottomViewHeight))
        scroll.delegate = self
        scroll.contentSize = CGSize(width: kScreenWidth, height: 491 + kScreenHeight - navHeight - bottomViewHeight)
        scroll.showsHorizontalScrollIndicator = false
        scroll.showsVerticalScrollIndicator = false
        scroll.isPagingEnabled = true
        return scroll
    }()
    fileprivate lazy var webview: UIWebView={
       let view = UIWebView()
        view.delegate = self
        view.scrollView.delegate = self
        view.frame = CGRect(x: 0, y: 491, width: kScreenWidth, height: kScreenHeight - navHeight - bottomViewHeight)
        return view
    }()
    //MARK: - webViewDelegate
    func webViewDidStartLoad(_ webView: UIWebView) {
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
    }
    //MARK: - ScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    
            if scrollView.contentOffset.y > 491 {
                
                UIView.animate(withDuration: 0.4, animations: {
                   
                    scrollView.setContentOffset(CGPoint(x: 0, y: 491), animated: false)
                })
            }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    deinit {
        
        self.result = ""
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
