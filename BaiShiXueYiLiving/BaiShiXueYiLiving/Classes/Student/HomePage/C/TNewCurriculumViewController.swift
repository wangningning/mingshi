//
//  TNewCurriculumViewController.swift
//  BaiShiXueYiLiving
//
//  Created by 王宁 on 2018/5/31.
//  Copyright © 2018年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh

class TNewCurriculumTypeCell: UICollectionViewCell {
    var btnBlock: ((_ index:Int)->())?
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configDataSource(typeDataArr:[TypeListModel]!) -> Void {

        for v in self.contentView.subviews {
//            if v.isKind(of: UIButton.classForCoder()) {
//                v.removeFromSuperview()
//            }
            v.removeFromSuperview()
        }
        if typeDataArr.count == 0 {
            return
        }
        
        let orgView = UIView()
        orgView.backgroundColor = UIColor(red: 236/255, green: 107/255, blue: 26/255, alpha: 1)
        orgView.frame = CGRect(x: 10, y: 10, width: 4, height: 16);
        self.contentView.addSubview(orgView)
        
        let lb = UILabel()
        lb.text = "点击进入更多分类"
        lb.sizeToFit()
        lb.font = UIFont.systemFont(ofSize: 15)
        lb.textColor = UIColor.black
        lb.frame = CGRect(x: orgView.x+4+5, y: 10, width: lb.bounds.size.width, height: lb.bounds.size.height)
        self.contentView.addSubview(lb)
        
        let num : CGFloat = 4
        var totalY : CGFloat = 0
        for index in 0...typeDataArr.count - 1 {
            let typeModel = typeDataArr[index]
            let btn = UIButton()
            btn.setTitle(typeModel.name!, for: .normal)
            btn.layer.borderWidth = 1
            btn.layer.cornerRadius = 4
            btn.layer.borderColor = UIColor(red: 236/255, green: 107/255, blue: 26/255, alpha: 1).cgColor
            btn.setTitleColor(UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1), for: .normal)
            btn.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            btn.tag = index
            btn.addTarget(self, action: #selector(tapAction), for: .touchUpInside)
            let space : CGFloat = 10
            let w : CGFloat = (kScreenWidth - space * 2 - space * (num - 1))/num
            let h : CGFloat = 30
            var y : CGFloat = 0
            y = CGFloat(index / 4) * (h + 10) + 36 + 10
            var x : CGFloat = 0
            x = (CGFloat(index%4) * (w + 10))
            btn.frame = CGRect(x: x + 10, y: y, width: w, height: h)
            totalY = y + h;
            self.contentView.addSubview(btn)
        }
        let line = UIView()
        line.backgroundColor = UIColor.gray;
        line.frame = CGRect(x: 10, y: totalY - 0.5 + 10, width: kScreenWidth - 10 * 2, height: 0.5)
        self.contentView.addSubview(line);
    }
    
    func tapAction(btn:UIButton) -> Void {
        self.btnBlock!(btn.tag)
    }
}

class TNewCurriculumViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var layout: UICollectionViewFlowLayout!
    @IBOutlet weak var collectionView: UICollectionView!
    var dataArr:[CurriculumDateModel] = []
    var typeDataArr:[TypeListModel] = []
    var page = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "sousuo"), style: .done, target: self, action: #selector(TNewCurriculumViewController.searchItemClicked))
        let w = (kScreenWidth - 24 - 21) / 2
        layout.itemSize = CGSize(width: w, height: w * 191 / 166)
        layout.minimumLineSpacing = 14
        
        layout.minimumInteritemSpacing = 21
        layout.sectionInset = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
        collectionView.register(UINib(nibName: "HomePageLiveStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomePageLiveStoreCollectionViewCell")
        //TNewCurriculumTypeCell
        collectionView.register(TNewCurriculumTypeCell.classForCoder(), forCellWithReuseIdentifier: "TNewCurriculumTypeCell")
        collectionView.register(UINib(nibName: "CurriculumSectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "CurriculumSectionReusableView")
        self.collectionView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.page = 1
            self.loadData()
            
        })
//        self.collectionView.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: { [unowned self] in
//            self.page += 1
//            self.loadData()
//        })
        self.collectionView.mj_header.beginRefreshing()
        
    }
    func searchItemClicked() {
        self.navigationController?.pushViewController(TSearchViewController(), animated: true)
    }
    
    func loadData(){
        NetworkingHandle.fetchNetworkData(url: "/Videobytype/videoWithType", at: self, params: ["page":self.page], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: self.collectionView, success: { (response) in
            let data = response["data"] as! [String:AnyObject]
            
            let listData = data["list"] as! [String:AnyObject]
            let videoListData = listData["videoList"]
            let typeListData = listData["typeList"]
            let typeListModels = TypeListModel.mj_objectArray(withKeyValuesArray: typeListData) as! [TypeListModel]
            self.typeDataArr = typeListModels;
            let videoListModels = CurriculumDateModel.mj_objectArray(withKeyValuesArray: videoListData) as! [CurriculumDateModel]
            //            let list = CurriculumDateModel.mj_objectArray(withKeyValuesArray: listData) as! [CurriculumDateModel]
//            if self.page == 1{
//                self.dataArr.removeAll()
//            }
            if videoListModels.count == 0{
                self.collectionView.mj_footer.endRefreshingWithNoMoreData()
            }
            self.dataArr = videoListModels
            self.collectionView.reloadData()
        }) {
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            let curricuModel:CurriculumDateModel = self.dataArr[section - 1];
            return curricuModel.data!.count
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.dataArr.count + 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            var y : CGFloat = 0;
//            var testData = [TypeListModel]()
//            for _ in 0...2 {
//                testData += self.typeDataArr
//            }
            if self.typeDataArr.count <= 0 {
                return CGSize(width: kScreenWidth, height: 0)
            }
            for index in 0...self.typeDataArr.count - 1 {
                let h : CGFloat = 30
                y = CGFloat(index / 4) * (h + 10)
            }
            return CGSize(width: kScreenWidth, height: y + 40 + 10 + 36 + 10)
        } else {
            let w = (kScreenWidth - 24 - 21) / 2
            return CGSize(width: w, height: w * 191 / 166)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0  {
            //TNewCurriculumTypeCell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TNewCurriculumTypeCell", for: indexPath) as! TNewCurriculumTypeCell
//            var testData = [TypeListModel]()
//            for _ in 0...2 {
//                testData += self.typeDataArr
//            }
            cell.configDataSource(typeDataArr: self.typeDataArr)
            cell.btnBlock = { indx in
//
                let model = self.typeDataArr[indx]
                if model.haveSubType == 0 {
                    let vc = TCurriculumTypeViewController()
                    vc.type = model.type_id
                    vc.title = model.name
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    let vc = TCurriculumViewController()
                    vc.typeid = model.type_id
                    vc.title = model.name
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomePageLiveStoreCollectionViewCell", for: indexPath) as! HomePageLiveStoreCollectionViewCell
            cell.ocModel = self.dataArr[indexPath.section - 1].data[indexPath.row]
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        var h : CGFloat = 0.0001;
        if section == 0 {
            h = 0.0001
        }else {
            h = 47.0
        }
        return CGSize(width: kScreenWidth, height: h)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CurriculumSectionReusableView", for: indexPath) as! CurriculumSectionReusableView
        var model = CurriculumDateModel();
        if indexPath.section == 0 {
            model = CurriculumDateModel()
        }
        else {
            model = self.dataArr[indexPath.section - 1];
            headerView.configDataSource(title: model.name)
            headerView.index = indexPath.section - 1
        }
        headerView.moreAction = {[weak self](idx:Int) in
            let model:CurriculumDateModel = self!.dataArr[idx]
            let vc = TCurriculumTypeViewController()
            vc.type = model.type_id
            vc.title = model.name
            self!.navigationController?.pushViewController(vc, animated: true)
        }
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section >= 1 {
            let model = self.dataArr[indexPath.section - 1].data![indexPath.row]
            let vc = TCurriculumDetailViewController()
            vc.url = model.url
            vc.play_img = model.video_img
            vc.username = model.userName
            vc.videoID = model.video_id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
