//
//  TPointingViewController.swift
//  BaiShiXueYiLiving
//
//  Created by 梁毅 on 2017/5/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh
class TPointingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var bannerArr = [BannerTPointingModel]()
    var dataArr = [ListTPointingModel]()
    var recommendedOrNot = false
    var page = 1
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if recommendedOrNot {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "sousuo"), style: .done, target: self, action: #selector(TPointingViewController.searchItemClicked))
        }
        
        
        tableView.register(UINib(nibName: "TPointingTableViewCell", bundle: nil), forCellReuseIdentifier: "TPointingTableViewCell")
        if  recommendedOrNot == false {
            tableView.tableHeaderView = TPointingHeaderView.setup()
        }
        
        tableView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.page = 1
            self.loadData()
        })
        tableView.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: { [unowned self] in
            self.page += 1
            self.loadData()
        })
        tableView.mj_header.beginRefreshing()
        tableView.tableFooterView = UIView()
    }
    func searchItemClicked() {
        self.navigationController?.pushViewController(TSearchViewController(), animated: true)
    }
    func loadData() {
        let param = ["pagesize":10,"p":page] as [String : Any]
        var url = ""
        if  recommendedOrNot == false {
            url = "?m=Api&c=Home&a=tutor_teach"
        }else {
            url = "?m=Api&c=Home&a=tutor_tui"
        }
        NetworkingHandle.fetchNetworkData(url: url, at: self, params: param, hasHeaderRefresh: tableView, success: { (response) in
            let data = response["data"] as! [String: AnyObject]
            let list = data["list"] as! [[String: AnyObject]]
            let ll = ListTPointingModel.modelsWithArray(modelArray: list) as! [ListTPointingModel]
            if  !self.recommendedOrNot {
                self.bannerArr = BannerTPointingModel.modelsWithArray(modelArray:  data["banner"] as! [[String: AnyObject]]) as! [BannerTPointingModel]
                let viewss = self.tableView.tableHeaderView as! TPointingHeaderView
                if self.bannerArr.count > 0 {
                    viewss.img.kf.setImage(with: URL(string: self.bannerArr[0].b_img ?? ""))
                }else{
                    viewss.img.image = UIImage.init(named: "kb_shchfm")
                }
                            }
            if self.page == 1 {
                self.dataArr.removeAll()
            }
            if ll.count == 0 {
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
            }
            self.dataArr += ll
            self.tableView.reloadData()
        }) {
            if self.page > 1 {
                self.page -= 1
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if DLUserInfoHandler.getUserInfo()?.type == "2"{
            ProgressHUD.showMessage(message: "教师没有权限")
            return
        }

        let model = self.dataArr[indexPath.row]
        if recommendedOrNot {
            
            pushToUserInfoCenter(atViewController: self, uId: model.user_id!)
            
        }else{
            let versionKey = String(kCFBundleVersionKey)
            let currentVersion = Bundle.main.infoDictionary?[versionKey] as! String
            print(" 当前版本号为：\(currentVersion)")
            let param = ["version": currentVersion]
            NetworkingHandle.fetchNetworkData(url: "/Member/is_this", at: self, params: param, isShowHUD: false, success: { (response) in
                let result = response["data"] as! String
                if result == "1"{
                    let vc = DirectChatViewController(conversationChatter: model.hx_username, conversationType: EMConversationTypeChat)
                    vc?.userId = model.user_id
                    vc?.usernameTHEY = model.username
                    vc?.img = model.img
                    vc?.hx_username = model.hx_username
                    vc?.isAudit = true
                    self.navigationController?.pushViewController(vc!, animated: true)
                    return
                }else{
                    if DLUserInfoHandler.getUserInfo()?.type == "2"{
                        ProgressHUD.showMessage(message: "抱歉，名师身份没有发起指点的权限")
                        return
                    }
                    
                    NetworkingHandle.fetchNetworkData(url: "/Home/check_teach", at: self, params: ["user_id":model.user_id!], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
                        let data = response["data"] as! String
                        if data == "1"{
                            let vc = DirectChatViewController(conversationChatter: model.hx_username, conversationType: EMConversationTypeChat)
                            vc?.userId = model.user_id
                            vc?.usernameTHEY = model.username
                            vc?.img = model.img
                            vc?.hx_username = model.hx_username
                            self.navigationController?.pushViewController(vc!, animated: true)
                            
                        }else if data == "2"{
                            
                            ProgressHUD.showMessage(message: "本次请教需要付费")
                            NetworkingHandle.fetchNetworkData(url: "/Home/setup_teach_order", at: self, params: ["user_id": model.user_id!], isShowHUD: true, isShowError: true, success: { (response) in
                                let vc = SelectPayWayViewController()
                                vc.amount = model.fee
                                vc.order_no = response["data"] as? String
                                vc.payType = "2"
                                vc.userId = model.user_id
                                vc.hx_username = model.hx_username
                                vc.img = model.img
                                vc.usernameTHEY = model.username
                                self.navigationController?.pushViewController(vc, animated: true)
                            }, failure: {
                                
                            })
                        }else if data == "3"{
                            let chatController = DirectChatViewController(conversationChatter: model.hx_username, conversationType: EMConversationTypeChat)
                            chatController?.img = model.img
                            chatController?.usernameTHEY = model.username
                            chatController?.userId = model.user_id
                            chatController?.hx_username = model.hx_username
                            chatController?.isTeachBefore = true
                            chatController?.state = "2"
                            self.navigationController?.pushViewController(chatController!, animated: true)
                        }
                        
                    }, failure: {
                        
                    })
                    
                    
                }
            }, failure: {
                
            })
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kScreenWidth * 81 / 375
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TPointingTableViewCell", for: indexPath) as! TPointingTableViewCell
        cell.model = dataArr[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArr.count
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        print(111)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
