//
//  ChangeCityViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/22.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class ChangeCityViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var tableView: UITableView!
    var dataArr: [CityModel] = []
    var selectCity:((String)->())?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "名师现场"
        let label = UILabel()
        label.text = "    \(LocationManager.shared.detialArea)"
        label.textColor = UIColor(hexString: "#333333")
        label.font = UIFont.systemFont(ofSize: 14)
        label.backgroundColor = UIColor.white
        label.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 45)
        self.tableView.tableHeaderView = label
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "ChangeCityCell")
        self.tableView.tableFooterView = UIView()
        self.loadData()
        // Do any additional setup after loading the view.
    }
    func loadData(){
        NetworkingHandle.fetchNetworkData(url: "/Home/city_list", at: self,success: { (response) in
            let data = response["data"] as! [[String:AnyObject]]
            self.dataArr = CityModel.modelsWithArray(modelArray: data) as! [CityModel]
            self.tableView.reloadData()
        })
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChangeCityCell")!
        cell.textLabel?.textColor = UIColor(hexString: "#333333")
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
        cell.textLabel?.text = self.dataArr[indexPath.section].list?[indexPath.row].city
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.dataArr[section].list?.count)!
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.dataArr.count
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "    \(self.dataArr[section].shouzimu!)"
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectCity?((self.dataArr[indexPath.section].list?[indexPath.row].city)!)
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
