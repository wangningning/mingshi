//
//  TRequestClassViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/7.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class TRequestClassViewController: UIViewController, MyDatePickerViewDelegate, UITextFieldDelegate{

    @IBOutlet weak var addressTF: UITextField!
    @IBOutlet weak var dateTF: UITextField!
    
    @IBOutlet weak var submitBtn: UIButton!
    
    @IBOutlet weak var selectDateBtn: UIButton!
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "我要开班"
        self.submitBtn.layer.cornerRadius = 6
        self.submitBtn.layer.masksToBounds = true
        IQKeyboardManager.sharedManager().shouldShowTextFieldPlaceholder = true
        IQKeyboardManager.sharedManager().enable = true
        self.addressTF.delegate = self
        // Do any additional setup after loading the view.
    }

    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == self.addressTF {
            if (self.addressTF.text?.characters.count)! > 0 {
                return true
            }else{
                let v = ProvinceCityDistrictChoice(frame: self.view.bounds)
                v.show(choiced: { [unowned self] (p, c, d) in
                    if p == c{
                        self.addressTF.text = p + "-" + d
                    }else{
                        self.addressTF.text = p+"-"+c+"-"+d
                    }
                    
                })
                self.view.addSubview(v)
                return false
            }
        }
        return true
    }
    @IBAction func submitAction(_ sender: UIButton) {
        
        if self.addressTF.text == "" || self.addressTF.text?.characters.count == 0 || self.dateTF.text == "" || self.dateTF.text?.characters.count == 0{
            ProgressHUD.showMessage(message: "请填写完整信息")
            return
        }
        let userValue = DLUserInfoHandler.getIdAndToken()
        
        NetworkingHandle.fetchNetworkData(url: "/home/tutor_class_ask", at: self, params: ["address":self.addressTF.text!,"ask_time":self.dateTF.text!,"uid":userValue?.id ?? "","token":userValue?.token ?? ""], isShowHUD: true, success: { (response) in
            ProgressHUD.showSuccess(message: "申请成功")
            self.navigationController?.popViewController(animated: true)
        }) { 
            
        }
    }
    
    @IBAction func selectDateAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let datePicker = MyDatePickerView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 300))
        datePicker.delegate = self;
        datePicker.show()

    }
    
    //MARK: -MyDatePickerdelegate
   
    func myDatePickerView(_ pickView: MyDatePickerView!, date: Date!) {
        let dateformat = DateFormatter.init()
        dateformat.dateFormat = "yyyy-MM-dd"
        self.dateTF.text = dateformat.string(from: date)
        pickView.dismiss()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
