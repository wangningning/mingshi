
//
//  TSceneViewController.swift
//  BaiShiXueYiLiving
//
//  Created by 梁毅 on 2017/5/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh
class TSceneViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var dataArr = [TScenceModel]()
    var page = 1
    var headerView: TSceneHeaderView!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "我请开班", target: self, action: #selector(TSceneViewController.offerCourse))
        tableView.register(UINib(nibName: "TSceneTableViewCell", bundle: nil), forCellReuseIdentifier: "TSceneTableViewCell")
        
        tableView.tableFooterView = UIView()
        tableView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.page = 1
            self.loadData(city: LocationManager.shared.city)
        })
        tableView.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: { [unowned self] in
            self.page += 1
            self.loadData(city: LocationManager.shared.city)
        })
        
    }
    override func viewWillAppear(_ animated: Bool) {
        tableView.mj_header.beginRefreshing()
    }
    func jumpCityDetailVC(){
        let vc = ChangeCityViewController()
        vc.selectCity = { [unowned self] city in
           print(city)
            LocationManager.shared.city = city
            LocationManager.shared.detialArea = city
            
            self.headerView.btn.setTitle(city, for: .normal)
            self.page = 1
            self.loadData(city: city)
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func loadData(city:String) {
        let param = ["city":city,"pagesize":"10","p":self.page] as [String : AnyObject]
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Home&a=tutor_scene", at: self, params: param, hasHeaderRefresh: tableView, success: { (response) in
            let data = response["data"] as! [String: AnyObject]
            let list = data["list"] as! [[String: AnyObject]]
            let ll = TScenceModel.modelsWithArray(modelArray: list) as! [TScenceModel]
            if self.page == 1 {
                self.dataArr.removeAll()
            }
            if ll.count == 0 {
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
            }
            self.dataArr += ll
            self.tableView.reloadData()
        }) { 
            if self.page > 1 {
                self.page -= 1
            }
        }
    }
    func offerCourse() {
        let vc = TRequestClassViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = TSceneDetailViewController()
        vc.id = self.dataArr[indexPath.section].id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kScreenWidth * 180 / 375
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "TSceneTableViewCell", for: indexPath) as! TSceneTableViewCell
                    cell.model = dataArr[indexPath.section ]
                    return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataArr.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
         return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
