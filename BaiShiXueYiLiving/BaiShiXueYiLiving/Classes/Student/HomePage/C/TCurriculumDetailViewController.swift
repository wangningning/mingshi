//
//  TCurriculumDetailViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/17.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Alamofire
import LYPlayer
import SnapKit
class TCurriculumDetailViewController: UIViewController,PLPlayerDelegate,UITableViewDelegate,UITableViewDataSource,BBSDetailReplyCellDelegate, LYPlayerViewDelegate{

    @IBOutlet weak var tableView: UITableView!
    
    var player: PLPlayer? = nil
    var playTimer: Timer?
    var reconnectCount : Int = 0
    var url : String?
    var play_img: String?
    var live_store_id: String?
    var username: String?
    var previewImg: UIImage?
    var moreButton = UIButton()
    var fullScreenBtn = UIButton()
    var stopBtn = UIButton()
    var isFirst = true
    var videoID: String!
    var detailModel: TCurriculumDetailModel!
    var commentList: [BBSResponseModel] = []
    var tempCell: BBSDetailReplyCell!
    var shareButton = UIButton()
    var isSelected = false
    var share = ShareView()
    
    var playerSlider = UISlider()
    var countdown = UILabel()
    
    let placeHolder = "说点什么吧"
    var chatInputView: TInputView?
    var backBtn = UIButton()
    var isDirectlyReply = false         //开启页面直接回复
    var isCommentReply: Bool = false    //是否为回复评论
    var commentModel: BBSResponseModel? //被回复人的模型
    
    
    
    @IBOutlet weak var replayView: UIView!
    @IBOutlet weak var repalyHeight: NSLayoutConstraint!
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        IQKeyboardManager.sharedManager().enable = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.isHidden = false
    }
    func playerView(playerView: LYPlayerView, didClickFillScreen button: UIButton) {
        if button.isSelected {
            self.replayView.isHidden = true
            self.repalyHeight.constant = 0
        }else{
            self.replayView.isHidden = false
            self.repalyHeight.constant = 50
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        self.automaticallyAdjustsScrollViewInsets = false
        
                
        
        NotificationCenter.default.addObserver(self, selector: #selector(TCurriculumDetailViewController.keyboardWillHide(noti:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(TCurriculumDetailViewController.keyboardWillShow(noti:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(TCurriculumDetailViewController.textFieldChange(noti:)), name: Notification.Name.UITextFieldTextDidChange, object: self.chatInputView?.textField)

        
        
        self.requestDetailData()
        
        let player = LYPlayerView.init(url: URL.init(string: self.url!))
        player.assetName = ""
        player.delegate = self
        self.view.addSubview(player)
        player.snp.makeConstraints { (make) in
            make.top.equalTo(view)
            make.left.right.equalTo(view)
            make.height.equalTo(player.snp.width).multipliedBy(9.0/16.0).priority(750)
        }
        self.tableView.snp.makeConstraints { (make) in
            make.top.equalTo(player.snp.bottom)
            make.left.right.equalTo(view)
            make.bottom.equalTo(self.replayView.snp.top)
        }
//        //播放器
//        let option = PLPlayerOption.default()
//        option.setOptionValue(10, forKey: PLPlayerOptionKeyTimeoutIntervalForMediaPackets)
//        option.setOptionValue(true, forKey: PLPlayerOptionKeyVODFFmpegEnable)
//        player = PLPlayer(url: NSURL(string: self.url!) as URL?, option: option)
//        player?.delegate = self
//        player?.delegateQueue = DispatchQueue.main
//        player?.isBackgroundPlayEnable = true
//
//        if previewImg == nil {
//            player?.launchView?.kf.setImage(with: URL(string: play_img!))
//        } else {
//            player?.launchView?.image = previewImg
//        }
//        player?.playerView?.backgroundColor = UIColor(hexString: "#f1f3f5")
//        player?.launchView?.clipsToBounds = true
//        player?.launchView?.contentMode = .scaleAspectFill
//        player?.playerView?.contentMode = .scaleAspectFill
//        player!.playerView?.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 221/375 * kScreenWidth)
//        player!.playerView?.isUserInteractionEnabled = true
//        self.tableView.tableHeaderView = player!.playerView
//
//        //全屏按钮
//        self.tableView.addSubview(fullScreenBtn)
//        fullScreenBtn.setImage(#imageLiteral(resourceName: "ic_full-screen"), for: .normal)
//        fullScreenBtn.addTarget(self, action: #selector(fullScreenAction(sender:)), for: .touchUpInside)
//        fullScreenBtn.snp.makeConstraints { (make) in
//            make.left.equalTo(kScreenWidth - 50)
//            make.top.equalTo(221/375 * kScreenWidth - 10 - 40)
//            make.height.width.equalTo(35)
//        }
//
//        //停止／播放 按钮
//        self.tableView.addSubview(stopBtn)
//        stopBtn.setImage(UIImage(named: "stop"), for: .normal)
//        stopBtn.setImage(UIImage(named: "paly"), for: .selected)
//        stopBtn.addTarget(self, action: #selector(stopBtnClicked(btn:)), for: .touchUpInside)
//        stopBtn.isSelected = true
//
//        stopBtn.snp.makeConstraints { (make) in
//            make.left.equalTo(15)
//            make.top.equalTo(221/375 * kScreenWidth - 10 - 40)
//            make.height.width.equalTo(35)
//        }
//
//        //返回按钮
//        self.tableView.addSubview(backBtn)
//        backBtn.setImage(#imageLiteral(resourceName: "fanhui-1"), for: .normal)
//        backBtn.backgroundColor = UIColor.init(hexString: "#333333").withAlphaComponent(0.3)
//        backBtn.addTarget(self, action: #selector(TCurriculumDetailViewController.back), for: .touchUpInside)
//        backBtn.layer.cornerRadius = 35/2
//        backBtn.layer.masksToBounds = true
//        backBtn.snp.makeConstraints { (make) in
//            make.left.top.equalTo(15)
//            make.width.height.equalTo(35)
//        }
//
//        //举报按钮
//        moreButton.setTitle("举报", for: .normal)
//        moreButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
//        moreButton.setTitleColor(UIColor.white, for: .normal)
//        moreButton.addTarget(self, action: #selector(reportVedio), for: .touchUpInside)
//        moreButton.backgroundColor = UIColor(hexString:"#000000").withAlphaComponent(0.2)
//        moreButton.layer.cornerRadius = 17.5
//        moreButton.layer.masksToBounds = true
//        self.tableView.addSubview(moreButton)
//
//        moreButton.snp.makeConstraints { (make) in
//            make.centerY.equalTo(backBtn.snp.centerY)
//            make.left.equalTo(kScreenWidth - 52)
//            make.width.height.equalTo(35)
//
//        }
//
////        //分享按钮
////        shareButton.setImage(#imageLiteral(resourceName: "ic_sh"), for: .normal)
////        shareButton.addTarget(self, action: #selector(TCurriculumDetailViewController.shareVideo), for: .touchUpInside)
////       // shareButton.backgroundColor = UIColor.black.withAlphaComponent(0.5)
////        self.tableView.addSubview(shareButton)
////
////        shareButton.snp.makeConstraints { (make) in
////            make.left.equalTo(kScreenWidth - 85)
////            make.top.equalTo(221/375 * kScreenWidth - 10 - 40)
////            make.width.height.equalTo(35)
////        }
//
//        //进度条
//        playerSlider.minimumValue=0  //最小值
//        playerSlider.maximumValue=1  //最大值
//        self.tableView.addSubview(playerSlider)
//
//        playerSlider.snp.makeConstraints { (make) in
//            make.left.equalTo(stopBtn.snp.right).offset(10)
//            make.centerY.equalTo(stopBtn.snp.centerY)
//            make.height.equalTo(30)
//        }
//        countdown.font = UIFont.systemFont(ofSize: 12)
//        countdown.textColor = UIColor.gray
//        self.tableView.addSubview(countdown)
//
//        countdown.snp.makeConstraints { (make) in
//            make.right.equalTo(fullScreenBtn.snp.left).offset(-5)
//            make.centerY.equalTo(playerSlider.snp.centerY)
//            make.left.equalTo(playerSlider.snp.right).inset(-6)
//        }
//        countdown.text = countdownComputer()
//
//        playerSlider.setThumbImage(UIImage(named: "播放按钮"), for: .normal)
//
//        playerSlider.isContinuous = true  //滑块滑动停止后才触发ValueChanged事件
//        playerSlider.addTarget(self, action: #selector(playerSliderChanger(slider:)), for: .valueChanged)
//        playerSlider.minimumTrackTintColor = UIColor(red:0.24, green:0.69, blue:0.94, alpha:1.00)  //左边槽的颜色
//        playerSlider.maximumTrackTintColor = UIColor.gray //右边槽的颜色

        //MARK: - 评论相关
        tempCell = BBSDetailReplyCell(style: .default, reuseIdentifier: "cell")
        
        self.tableView.keyboardDismissMode = .onDrag
        self.tableView.separatorColor = UIColor.clear
        self.tableView.register(BBSDetailReplyCell.self, forCellReuseIdentifier: "BBSDetailReplyCell")
        self.tableView.register(UINib(nibName:"TCurriculumDetailSectionHeaderView",bundle:nil), forHeaderFooterViewReuseIdentifier: "TCurriculumDetailSectionHeaderView")
        tableView.reloadData()
        
        
        self.replayView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(TCurriculumDetailViewController.a)))
        

        // Do any additional setup after loading the view.
    }
    func shareVideo(){
        
        isSelected = !isSelected
        if isSelected {
            share = ShareView.show(atView: self.view, url: self.detailModel.share_url!, avatar: self.detailModel.img!, username: self.detailModel.username!, type: "3")
        } else{
            share.dismiss()
        }
        share.dismissBlock = { [unowned self] in
            self.isSelected = !self.isSelected
        }
    }
    func reportVedio(){
        
        let vc = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let titleArr = ["淫秽色情","侮辱他人","恶意营销","言语低俗","广告诈骗"]
        
        for str in titleArr {
            
            let ac = UIAlertAction.init(title: str, style: .default) { (alert) in
                ProgressHUD.showSuccess(message: "举报成功")
            }
            vc.addAction(ac)
        }
        let cancel = UIAlertAction.init(title: "取消", style: .cancel) { (alert) in
            
        }
        vc.addAction(cancel)
        self.present(vc, animated: true, completion: nil)
        
    }
    func back(){
        
        self.player?.stop()
        self.navigationController?.popViewController(animated: true)
    }
    func stopBtnClicked(btn: UIButton) {
        if isFirst {
            self.startPlayer()
            isFirst = false
        }
        
        btn.isSelected = !btn.isSelected
        if  btn.isSelected {
            self.player?.pause()
        } else {
            self.player?.resume()
        }
    }
    //全屏控制
    func fullScreenAction(sender:UIButton){
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            
            UIApplication.shared.isStatusBarHidden = true
            self.tableView.bounces = false
            self.tableView.isScrollEnabled = false
            self.tableView.reloadData()
            self.tableView.tableHeaderView?.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi/2))
            self.tableView.frame = CGRect(x: 0, y: 0, width: kScreenHeight, height: kScreenWidth)
            
            
            player?.playerView?.contentMode = .scaleAspectFill
            player?.playerView?.frame = CGRect(x: 0, y: 0, width:kScreenWidth, height: kScreenHeight)
            
            self.shareButton.isHidden = true
            self.replayView.isHidden = true
            self.repalyHeight.constant = 0
            self.backBtn.isHidden = true
            self.moreButton.isHidden = true
            self.fullScreenBtn.snp.removeConstraints()
            
            self.stopBtn.snp.removeConstraints()
            self.playerSlider.snp.removeConstraints()
            self.countdown.snp.removeConstraints()
            self.view.addSubview(stopBtn)
            stopBtn.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi/2))
            playerSlider.layer.anchorPoint = CGPoint(x: 1, y: 0)
            playerSlider.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi/2))
            countdown.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi/2))
            self.view.addSubview(fullScreenBtn)
            self.view.addSubview(playerSlider)
            self.view.addSubview(countdown)
            fullScreenBtn.snp.makeConstraints({ (make) in
                make.left.equalTo(10)
                make.bottom.equalTo(-10)
                make.height.width.equalTo(40)
            })
            stopBtn.snp.makeConstraints({ (make) in
                make.left.equalTo(10)
                make.top.equalTo(20)
                make.height.width.equalTo(35)
            })
            
            playerSlider.snp.makeConstraints { (make) in
                make.bottom.equalTo(-95)
                make.right.equalTo(-85)
                make.width.equalTo(kScreenHeight - 10 - 40 - 10 - 35 - 80)
                make.height.equalTo(30)
                
            }
            countdown.snp.makeConstraints { (make) in
                make.left.equalTo(-10)
                make.bottom.equalTo(-50)
                make.width.equalTo(80)
                make.height.equalTo(30)
            }
            

        }else{
            UIApplication.shared.isStatusBarHidden = false
            self.tableView.bounces = true
            self.tableView.isScrollEnabled = true
            self.tableView.tableHeaderView?.transform = CGAffineTransform.identity
            
            self.tableView.isHidden = false
            self.shareButton.isHidden = false
            self.replayView.isHidden = false
            self.backBtn.isHidden = false
            self.moreButton.isHidden = false
            
            self.repalyHeight.constant = 51
            self.player?.playerView?.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 221/375 * kScreenWidth)
            fullScreenBtn.snp.removeConstraints()
            self.tableView.addSubview(fullScreenBtn)
            stopBtn.snp.removeConstraints()
            playerSlider.snp.removeConstraints()
            countdown.snp.removeConstraints()
            
            stopBtn.transform = CGAffineTransform.identity
            playerSlider.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            playerSlider.transform = CGAffineTransform.identity
            countdown.transform = CGAffineTransform.identity
            self.tableView.addSubview(stopBtn)
            self.tableView.addSubview(playerSlider)
            self.tableView.addSubview(countdown)
            fullScreenBtn.snp.makeConstraints { (make) in
                make.left.equalTo(kScreenWidth - 40 - 10)
                make.top.equalTo(221/375 * kScreenWidth - 10 - 40)
                make.height.width.equalTo(40)
            }
            
            stopBtn.snp.makeConstraints { (make) in
                make.left.equalTo(15)
                make.top.equalTo(221/375 * kScreenWidth - 10 - 40)
                make.height.width.equalTo(35)
            }
            
            playerSlider.snp.makeConstraints { (make) in
                make.left.equalTo(stopBtn.snp.right).offset(10)
                make.centerY.equalTo(stopBtn.snp.centerY)
                make.height.equalTo(30)
            }
            
            countdown.snp.makeConstraints { (make) in
                make.right.equalTo(fullScreenBtn.snp.left).offset(-5)
                make.centerY.equalTo(playerSlider.snp.centerY)
                make.left.equalTo(playerSlider.snp.right).inset(-6)
            }

            self.tableView.reloadData()
           
        }
    }

    func a(){
        chatInputView = TInputView.show(atView: self.view, send: { (text) in
            if text.characters.count == 0 || text == ""{
                ProgressHUD.showNoticeOnStatusBar(message: "请输入要发送的内容")
                return
            }
            self.isCommentReply = false
            self.clickSend(text: text)
        })
        self.chatInputView?.inputBgView.isHidden = false
        self.chatInputView?.textField.becomeFirstResponder()
        self.chatInputView?.textField.placeholder = placeHolder
    }
    //MARK: -开始播放
    func startPlayer() {
        
        self.playTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.playTimerFunc), userInfo: nil, repeats: true)
        
        
        Alamofire.request(self.url!, method: .post, parameters: [:]).responseJSON { (dataResponse) in
           
            if dataResponse.result.error != nil {
                if let status = dataResponse.response?.statusCode {
                    if status == 404{
                        
                        ProgressHUD.showMessage(message: "该视频无法播放")
                        self.isFirst = true
                        self.stopBtn.isSelected = true
                    }else{
                        
                       
                        self.player?.play()
                        
                    }
                } else {
                    self.player?.play()
                }
               
            }else{
                self.player?.play()
            }
            
        }
        
        
        
    }
    //MARK: - 进度相关
    func countdownComputer() -> String {
        let time = (player?.totalDuration.seconds)! - (player?.currentTime.seconds)!
        if time <= 0 || time.isNaN {
            return "00:00:00"
        }
        let interval = Int(time)
        let hour = interval / (60 * 60)
        let min = interval % (60 * 60) / 60
        let seconds = interval % (60 * 60) % 60
        
        return "-" + String(format: "%02d", hour) + ":" + String(format: "%02d", min) + ":" + String(format: "%02d", seconds)
    }
    func playTimerFunc() {
        
        countdown.text = countdownComputer()
        playerSlider.value = Float(CMTimeGetSeconds((player?.currentTime)!) / CMTimeGetSeconds((player?.totalDuration)!))
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\(playerSlider.value)")
    }
    func playerSliderChanger(slider: UISlider) {
        let seconds = Float64(slider.value) * CMTimeGetSeconds((player?.totalDuration)!)
        player?.seek(to: CMTimeMakeWithSeconds(seconds, 1))
    }

    //MARK: - 播放状态代理
    func player(_ player: PLPlayer, statusDidChange state: PLPlayerStatus) {
        
        
        if .statusStopped == state {
            
            isFirst = true
            self.player?.stop()
            self.stopBtn.isSelected = true
            playTimer?.invalidate()
            
        } else if state == .statusPlaying {
            
            self.stopBtn.isSelected = false
          
        } else if state == .statusPreparing {
            
            print("statusPreparing")
           
        }else if state == .statusError{
            print("statusError")
           
        }else if state == .statusReady{
            self.playTimer?.fire()
            print("statusReady")
        }else if state == .stateAutoReconnecting{
            
            print("stateAutoReconnecting")
        }
    }
    //MARK: -重连
    func player(_ player: PLPlayer, stoppedWithError error: Error?) {
        self.tryReconnect()
    }
    
    
    func tryReconnect() {
        if  self.reconnectCount < 1 {
            reconnectCount += 1
            self.player?.play()
        }
        UIApplication.shared.isIdleTimerDisabled = true
    }

    //MARK: - 键盘监听
    func keyboardWillHide(noti: Notification) {
        let kbInfo = noti.userInfo
        let duration = kbInfo?[UIKeyboardAnimationDurationUserInfoKey] as! Double
        
        var frame = chatInputView?.inputBgView.frame
        UIView.animate(withDuration: duration, animations: {
            frame?.origin.y = kScreenHeight
            self.chatInputView?.inputBgView.frame = frame!
            self.chatInputView?.dismissAction()
        })
    }
    func keyboardWillShow(noti: Notification) {
        if let tf = chatInputView?.textField, tf.isFirstResponder {
            let kbInfo = noti.userInfo
            let kbRect = (kbInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            let duration = kbInfo?[UIKeyboardAnimationDurationUserInfoKey] as! Double
            var frame = chatInputView?.inputBgView.frame
            
            UIView.animate(withDuration: duration, animations: {
                frame?.origin.y = kScreenHeight - kbRect.height - TInputView.inputViewHeight
                self.chatInputView?.inputBgView.frame = frame!
            })
        }
    }
    func textFieldChange(noti: Notification){
        let tf = noti.object as! UITextField
        if (tf.text?.characters.count)! > 60 {
            let str = tf.text! as NSString
            tf.text = str.substring(to: 60) as String
        }
        
    }

    //MARK: -点击发送
    func clickSend(text: String) {
        
        if self.isCommentReply && self.commentModel != nil{
            self.sendReply(text: text, response_id: (self.commentModel?.comment_id!)!)
        } else {
            self.sendReply(text: text, response_id: "")
        }
        self.chatInputView?.textField.resignFirstResponder()
    }
    //MARK: - 回复
    func sendReply(text: String, response_id: String) {
        
        let params = ["post_id" : self.videoID as AnyObject,
                      "content" : text as AnyObject,
                      "response_id" : response_id as AnyObject,"type":"2"
            ] as [String : AnyObject]
        
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Topical&a=comment_posts", at: self, params: params, isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
            ProgressHUD.showNoticeOnStatusBar(message: "回复成功")
            self.requestDetailData()
        }) {
            
        }
        
    }
    //MARK: - 获取详情页数据
    func requestDetailData() {
        let params = ["video_id" : self.videoID!]
        NetworkingHandle.fetchGetNetworkData(url: "/Home/video_detail", at: self, params: params, hasHeaderRefresh: nil, success: { (response) in
            let data = response["data"]
            if data != nil {
                self.detailModel = TCurriculumDetailModel.modelWithDictionary(diction: data as! [String : AnyObject])
           
                if self.detailModel.comment!.count > 0 {
                    self.commentList = self.detailModel.comment!
                }
                self.tableView.reloadData()
            }
            
            
        }) {
            
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BBSDetailReplyCell") as! BBSDetailReplyCell
        cell.selectionStyle = .none
        cell.model = self.commentList[indexPath.row]
        cell.myDelegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if fullScreenBtn.isSelected{
            return 0
        }else{
            return self.commentList.count
        }
       
    }
    //MARK: BBSDetailReplyCellDelegate
    func bbsDetailReplyCell(cell: BBSDetailReplyCell, clickButton index: Int, model: BBSResponseModel)
    {
        switch index {
        case 3://回复
            chatInputView = TInputView.show(atView: self.view, send: { (text) in
                if text.characters.count == 0 || text == ""{
                    ProgressHUD.showNoticeOnStatusBar(message: "请输入要发送的内容")
                    return
                }
                self.clickSend(text: text)
            })
            self.chatInputView?.inputBgView.isHidden = false
            self.chatInputView?.textField.becomeFirstResponder()
            self.chatInputView?.textField.placeholder = "回复" + model.username!
            
            self.isCommentReply = true
            commentModel = model
            break
        default:
            break
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = self.commentList[indexPath.row]
        if model.cellHeight == nil {
            tempCell.model = model
            model.cellHeight = tempCell.getCellHeight()
        }
        return model.cellHeight!
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if fullScreenBtn.isSelected{
            return nil
        }else{
            if section == 0 {
                
                let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TCurriculumDetailSectionHeaderView") as! TCurriculumDetailSectionHeaderView
                if self.detailModel != nil{
                    view.model = self.detailModel
                }
                view.shareBlock = { [unowned self] in
                    self.shareVideo()
                }
                view.avatarBlock = { [unowned self] in
//                    pushToUserInfoCenter(atViewController: self, uId: self.detailModel.user_id!)
                }
                return view
            }else{
                return UIView()
            }
            

        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if fullScreenBtn.isSelected{
             return 0.1
        }else{
             return kScreenWidth * 209/375
        }
       
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
//    func detailsReplyView(replyView: BBSDetailsReplyView, index: Int) {
//        if index == 1 ||  index == 2{
//            inputView1.textView.becomeFirstResponder()
//            inputView1.placeHolder = placeHolder
//            isCommentReply = false
//        } else {
//            //likePost()
//        }
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
