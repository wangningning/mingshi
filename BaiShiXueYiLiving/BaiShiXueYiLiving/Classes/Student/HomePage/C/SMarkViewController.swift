//
//  SMarkViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/21.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class SMarkViewController: UIViewController {

    var topLab: UILabel!
    var lab1: UILabel!
    var lab2: UILabel!
    var lab3: UILabel!
    var bgView: UIView!
    var mark1: TggStarEvaluationView!
    var mark2: TggStarEvaluationView!
    var mark3: TggStarEvaluationView!
    var confirmBtn: UIButton!
    var point1 = 5
    var point2 = 5
    var point3 = 5
    var user_id: String! //教师id
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "评分"
        
        bgView = UIView()
        bgView.backgroundColor = UIColor.white
        self.view.addSubview(bgView)
        bgView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(0)
            make.height.equalTo(206)
        }
        
        topLab = UILabel()
        topLab.text = "请对本次指点进行评分"
        topLab.font = UIFont.systemFont(ofSize: 12.0)
        topLab.textColor = UIColor(hexString: "#666666")
        bgView.addSubview(topLab)
        topLab.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(17)
        }
        
        lab1 = UILabel()
        lab1.text = "讲解清晰："
        lab1.textColor = UIColor(hexString: "#333333")
        lab1.font = UIFont.systemFont(ofSize: 14)
        bgView.addSubview(lab1)
        lab1.snp.makeConstraints { (make) in
            make.left.equalTo(topLab.snp.left)
            make.top.equalTo(topLab.snp.bottom).offset(30)
            make.size.equalTo(CGSize(width: 75, height: 13))
        }
        
        mark1 = TggStarEvaluationView()
        mark1.isTapEnabled = true
        mark1.backgroundColor = UIColor.white
        mark1.starCount = 5
        mark1.evaluateViewChooseStarBlock = { count in
            print("\(count)")
            self.point1 = Int(count)
        }
        bgView.addSubview(mark1)
        mark1.snp.makeConstraints { (make) in
            make.left.equalTo(lab1.snp.right).offset(31)
            make.centerY.equalTo(lab1.snp.centerY)
            make.size.equalTo(CGSize(width: 23*8, height: 45))
        }
        
        lab2 = UILabel()
        lab2.text = "指点水平："
        lab2.textColor = UIColor(hexString: "#333333")
        lab2.font = UIFont.systemFont(ofSize: 14)
        bgView.addSubview(lab2)
        lab2.snp.makeConstraints { (make) in
            make.left.equalTo(topLab.snp.left)
            make.top.equalTo(lab1.snp.bottom).offset(33)
            make.size.equalTo(lab1.snp.size)
        }
        
        mark2 = TggStarEvaluationView()
        mark2.isTapEnabled = true
        mark2.backgroundColor = UIColor.white
        mark2.starCount = 5
        mark2.evaluateViewChooseStarBlock = { count in
            print("\(count)")
            self.point2 = Int(count)
        }
        bgView.addSubview(mark2)
        
        mark2.snp.makeConstraints { (make) in
            make.left.equalTo(lab2.snp.right).offset(31)
            make.centerY.equalTo(lab2.snp.centerY)
            make.size.equalTo(mark1.snp.size)
        }
        
        lab3 = UILabel()
        lab3.text = "教学态度："
        lab3.textColor = UIColor(hexString: "#333333")
        lab3.font = UIFont.systemFont(ofSize: 14)
        bgView.addSubview(lab3)
        lab3.snp.makeConstraints { (make) in
            make.left.equalTo(topLab.snp.left)
            make.top.equalTo(lab2.snp.bottom).offset(33)
            make.size.equalTo(lab1.snp.size)
        }
        
        mark3 = TggStarEvaluationView()
        mark3.isTapEnabled = true
        mark3.backgroundColor = UIColor.white
        mark3.starCount = 5
        mark3.evaluateViewChooseStarBlock = { count in
            print("\(count)")
            self.point3 = Int(count)
        }
        bgView.addSubview(mark3)
        
        mark3.snp.makeConstraints { (make) in
            make.left.equalTo(lab3.snp.right).offset(31)
            make.centerY.equalTo(lab3.snp.centerY)
            make.size.equalTo(mark1.snp.size)
        }
        
        confirmBtn = UIButton()
        confirmBtn.setTitle("确定", for: .normal)
        confirmBtn.backgroundColor = UIColor(hexString: "#ec6b1a")
        confirmBtn.layer.cornerRadius = 6
        confirmBtn.layer.masksToBounds = true
        confirmBtn.addTarget(self, action: #selector(SMarkViewController.confirmAction), for: .touchUpInside)
        self.view.addSubview(confirmBtn)
        
        confirmBtn.snp.makeConstraints { (make) in
            make.left.equalTo(48)
            make.right.equalTo(-48)
            make.top.equalTo(bgView.snp.bottom).offset(121)
            make.height.equalTo(45)
        }
        
        // Do any additional setup after loading the view.
    }
    func confirmAction(){
        

        NetworkingHandle.fetchNetworkData(url: "/Home/tutor_mark", at: self, params: ["mark1":self.point1,"mark2":self.point2,"mark3":self.point3,"user_id":self.user_id], success: { (response) in
            ProgressHUD.showSuccess(message: "评分成功")
            for vc in (self.navigationController?.viewControllers)! {
                if vc.isKind(of: TPointingViewController.self){
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            }
            
        }) {
            
        }
    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
