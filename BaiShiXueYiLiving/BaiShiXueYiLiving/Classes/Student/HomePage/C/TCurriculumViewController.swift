
//
//  TCurriculumViewController.swift
//  BaiShiXueYiLiving
//
//  Created by 梁毅 on 2017/5/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh

class TCurriculumViewController: BSBaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var layout: UICollectionViewFlowLayout!
    @IBOutlet weak var collectionView: UICollectionView!
    var typeid : NSInteger!
    var dataArr:[CurriculumDateModel] = []
    var page = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "sousuo"), style: .done, target: self, action: #selector(TCurriculumViewController.searchItemClicked))
        let w = (kScreenWidth - 24 - 21) / 2
        layout.itemSize = CGSize(width: w, height: w * 191 / 166)
        layout.minimumLineSpacing = 14
        
        layout.minimumInteritemSpacing = 21
        layout.sectionInset = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
        collectionView.register(UINib(nibName: "HomePageLiveStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomePageLiveStoreCollectionViewCell")
        collectionView.register(UINib(nibName: "CurriculumSectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "CurriculumSectionReusableView")
        self.collectionView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.page = 1
            self.loadData()
            
        })
//        self.collectionView.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: { [unowned self] in
//            self.page += 1
//            self.loadData()
//        })
        self.collectionView.mj_header.beginRefreshing()
        
    }
    func searchItemClicked() {
        self.navigationController?.pushViewController(TSearchViewController(), animated: true)
    }
    
    func loadData(){
        NetworkingHandle.fetchNetworkData(url: "/Videobytype/getVideoByType", at: self, params: ["page":self.page,"typeId":self.typeid], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: self.collectionView, success: { (response) in
            let data = response["data"] as! [String:AnyObject]
            print("data -------- \(data)")
            let listData = data["list"] as! [CurriculumDateModel]
//            let videoListData = listData["videoList"]
//            let typeListData = listData["typeList"]
//            let typeListModels = TypeListModel.mj_objectArray(withKeyValuesArray: typeListData) as! [TypeListModel]
            let videoListModels = CurriculumDateModel.mj_objectArray(withKeyValuesArray: listData) as! [CurriculumDateModel]
////            let list = CurriculumDateModel.mj_objectArray(withKeyValuesArray: listData) as! [CurriculumDateModel]
            if self.page == 1{
                self.dataArr.removeAll()
            }
//            if videoListModels.count == 0{
//                self.collectionView.mj_footer.endRefreshingWithNoMoreData()
//            }
            self.dataArr += videoListModels
            self.collectionView.reloadData()
        }) {
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let curricuModel:CurriculumDateModel = self.dataArr[section];
        return curricuModel.data!.count
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.dataArr.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomePageLiveStoreCollectionViewCell", for: indexPath) as! HomePageLiveStoreCollectionViewCell
        cell.ocModel = self.dataArr[indexPath.section].data[indexPath.row]
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: kScreenWidth, height: 47)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CurriculumSectionReusableView", for: indexPath) as! CurriculumSectionReusableView
        let model = self.dataArr[indexPath.section];
        headerView.configDataSource(title: model.name)
        headerView.index = indexPath.section
        
        headerView.moreAction = {[weak self](idx:Int) in
            let model:CurriculumDateModel = self!.dataArr[idx]
            let vc = TCurriculumTypeViewController()
            vc.type = model.type_id
            vc.title = model.name
            self!.navigationController?.pushViewController(vc, animated: true)
        }
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = self.dataArr[indexPath.section].data![indexPath.row]
        let vc = TCurriculumDetailViewController()
        vc.url = model.url
        vc.play_img = model.video_img
        vc.username = model.userName
        vc.videoID = model.video_id
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
