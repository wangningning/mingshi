//
//  TCurriculumTypeViewController.swift
//  BaiShiXueYiLiving
//
//  Created by 王宁 on 2018/5/23.
//  Copyright © 2018年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh

class TCurriculumTypeViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet var layout: UICollectionViewFlowLayout!
    @IBOutlet var collectionView: UICollectionView!
    
    var dataArr:[CurriculumVideoModel] = []
    var page = 1
    var type:Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "sousuo"), style: .done, target: self, action: #selector(TCurriculumViewController.searchItemClicked))
        let w = (kScreenWidth - 24 - 21) / 2
        layout.itemSize = CGSize(width: w, height: w * 191 / 166)
        layout.minimumLineSpacing = 14
        
        layout.minimumInteritemSpacing = 21
        layout.sectionInset = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
        collectionView.register(UINib(nibName: "HomePageLiveStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomePageLiveStoreCollectionViewCell")
        self.collectionView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.page = 1
            self.loadData()
            
        })
        self.collectionView.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: { [unowned self] in
            self.page += 1
            self.loadData()
        })
        self.collectionView.mj_header.beginRefreshing()
        
    }
    func searchItemClicked() {
        self.navigationController?.pushViewController(TSearchViewController(), animated: true)
    }
    
    func loadData(){
        NetworkingHandle.fetchNetworkData(url: "/Videobytype/getVideoByType", at: self, params: ["page":self.page,"typeId":self.type], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: self.collectionView, success: { (response) in
            let data = response["data"] as! [String:AnyObject]
            print("data ------ \(data)")
            let listData = data["list"]
            let list = CurriculumVideoModel.mj_objectArray(withKeyValuesArray: listData) as! [CurriculumVideoModel]
            print("list ------ \(list)")
            if self.page == 1{
                self.dataArr.removeAll()
            }
            if list.count == 0{
                self.collectionView.mj_footer.endRefreshingWithNoMoreData()
            }
            self.dataArr += list
            self.collectionView.reloadData()
        }) {
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataArr.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomePageLiveStoreCollectionViewCell", for: indexPath) as! HomePageLiveStoreCollectionViewCell
        cell.ocModel = self.dataArr[indexPath.row]
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = self.dataArr[indexPath.row]
        let vc = TCurriculumDetailViewController()
        vc.url = model.url
        vc.play_img = model.video_img
        vc.username = model.userName
        vc.videoID = model.video_id
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
