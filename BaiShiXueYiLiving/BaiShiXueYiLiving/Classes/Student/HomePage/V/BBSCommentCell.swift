//
//  BBSCommentCell.swift
//  FKDCClient
//
//  Created by 曾觉新 on 2017/2/26.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class BBSCommentCell: UITableViewCell {

    var nameLabel: UILabel!
    var commentLabel: UILabel!
    
    private var _model: BBSResponseModel!
    var model: BBSResponseModel! {
        set {
            _model = newValue
            
            nameLabel.text = _model.username! + "："
            commentLabel.text = _model.content
            
        }
        get {
            return _model
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        nameLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.left.equalTo(10)
        }
        commentLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.left.equalTo(self.nameLabel.snp.right).offset(10)
        }
        
    }
    
    func initViews() {
        nameLabel = UILabel()
        nameLabel.font = UIFont.systemFont(ofSize: 13)
        nameLabel.textColor = UIColor(hexString: "#3e3a39")
        self.contentView.addSubview(nameLabel)
        
        commentLabel = UILabel()
        commentLabel.font = UIFont.systemFont(ofSize: 13)
        commentLabel.textColor = UIColor(hexString: "#727171")
        self.contentView.addSubview(commentLabel)
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
