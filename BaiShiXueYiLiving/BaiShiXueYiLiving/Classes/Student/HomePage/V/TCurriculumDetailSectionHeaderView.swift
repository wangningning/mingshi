//
//  TCurriculumDetailSectionHeaderView.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/17.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class TCurriculumDetailSectionHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var videoName: UILabel!
    @IBOutlet weak var content: UILabel!

    @IBOutlet weak var sexImg: UIImageView!
    @IBOutlet weak var zanBtn: UIButton!
    @IBOutlet weak var playBtn: UIButton!
    
    @IBOutlet weak var usetImg: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var markLab: UILabel!
    
    @IBOutlet weak var attentionBTn: UIButton!
    var shareBlock: (()->())?
    var avatarBlock: (()->())?
    var model: TCurriculumDetailModel!{
        willSet(m){
            self.videoName.text = m.title
            self.content.text = m.content?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
            if m.sex == "1"{
                self.sexImg.image = #imageLiteral(resourceName: "ssjg_man")
            }else{
                self.sexImg.image = #imageLiteral(resourceName: "ssjg_woman")
            }
            if m.is_collect == "1"{
                self.zanBtn.isSelected = true
            }else{
                self.zanBtn.isSelected = false
            }
            if m.is_follow == "1"{
                self.attentionBTn.isSelected = true
            } else {
                self.attentionBTn.isSelected = false
            }
            self.zanBtn.setTitle(m.collect_nums!, for: .normal)
            self.playBtn.setTitle(m.watch_nums!, for: .normal)
            self.markLab.text = m.mark
            self.userName.text = m.username
            self.usetImg.kf.setImage(with: URL(string:m.img!))
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.usetImg.layer.cornerRadius = 38/2
        self.usetImg.contentMode = .scaleAspectFill
        self.usetImg.layer.masksToBounds = true
    }
    
    
    @IBAction func attentionAction(_ sender: UIButton) {
        focusOtherPerson(viewResponder: self.responderViewController()!, other_id: self.model.user_id!, btn: self.attentionBTn, type: self.model.is_follow!) {
            if self.model.is_follow == "1"{
                self.attentionBTn.isSelected = false
                self.model.is_follow = "2"
            }else{
                self.attentionBTn.isSelected = true
                self.model.is_follow = "1"
            }
        }
    }
    @IBAction func zan(_ sender: UIButton) {
        NetworkingHandle.fetchNetworkData(url: "/Home/collect_video", at: self, params: ["video_id":self.model.video_id!], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
            if self.model.is_collect == "1"{
                self.model.is_collect = "2"
                self.zanBtn.isSelected = false
                
                
                self.zanBtn.setTitle("\(Int(self.model.collect_nums!)! - 1)", for: .normal)
                self.model.collect_nums = "\(Int(self.model.collect_nums!)! - 1)"
            }else{
                self.model.is_collect = "1"
                self.zanBtn.isSelected = true

                self.zanBtn.setTitle("\(Int(self.model.collect_nums!)! + 1)", for: .normal)
                self.model.collect_nums = "\(Int(self.model.collect_nums!)! + 1)"
            }
        }) {
            
        }

    }
    @IBAction func shareAction(_ sender: UIButton) {
        shareBlock?()
    }
    
    @IBAction func avatarClickAction(_ sender: Any) {
        avatarBlock?()
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
