
//
//  TPointingTableViewCell.swift
//  BaiShiXueYiLiving
//
//  Created by 梁毅 on 2017/5/25.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class TPointingTableViewCell: UITableViewCell {
    @IBOutlet weak var genderB: UIButton!
    @IBOutlet weak var autographL: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var avartImg: UIImageView!
    @IBOutlet weak var rankL: UILabel!
    
    @IBOutlet weak var vipImage: UIImageView!
    var model: ListTPointingModel! {
        willSet(m) {
            genderB.setImage(UIImage(named: m.sex == "1" ? "icon_gx_man" : "icon_gx_woman"), for: .normal)
            if m.autograph == "" {
                autographL.text = "未设置签名"
            }else{
                autographL.text = m.autograph
            }
            
            titleLabel.text = m.username
            avartImg.kf.setImage(with: URL(string: m.img!))
            rankL.text = "评分：" + (m.mark ?? "")
           
        }
    }
    override func awakeFromNib() {
        
        super.awakeFromNib()
        avartImg.layer.cornerRadius = 51 / 2
        avartImg.layer.masksToBounds = true
        avartImg.clipsToBounds = true
        avartImg.contentMode = .scaleToFill
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
