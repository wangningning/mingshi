//
//  HomePageLiveCollectionViewCell.swift
//  BaiShiXueYiLiving
//
//  Created by 梁毅 on 2017/5/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class HomePageLiveCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var backImg: UIImageView!
    @IBOutlet weak var vipImg: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var numsLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    var model: LiveList! {
        willSet(m) {
           // backImg.kf.setImage(with: URL(string: m.play_img!))
            backImg.kf.setImage(with: URL(string: m.play_img!), placeholder: #imageLiteral(resourceName: "live_default"), options: nil, progressBlock: nil, completionHandler: nil)
            titleLabel.text = m.title
            numsLabel.text = m.watch_nums! + "人"
            usernameLabel.text = m.username
            img.kf.setImage(with: URL(string: m.img!))
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        img.layer.cornerRadius = 51 / 2
        img.layer.masksToBounds = true
        
        backImg.contentMode = .scaleAspectFill
        backImg.clipsToBounds = true
        img.contentMode = .scaleAspectFill
        img.clipsToBounds = true
    }

}
