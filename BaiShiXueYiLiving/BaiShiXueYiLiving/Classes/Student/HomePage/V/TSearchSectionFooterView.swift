//
//  TSearchSectionFooterView.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/7/24.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class TSearchSectionFooterView: UITableViewHeaderFooterView {

    @IBOutlet weak var searchBtn: UIButton!
    var cleanRecord: (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.searchBtn.layer.cornerRadius = 6
        self.searchBtn.layer.borderColor = UIColor.init(hexString: "#999999").cgColor
        self.searchBtn.layer.borderWidth = 0.5
        self.searchBtn.layer.masksToBounds = true
    }
    
    @IBAction func cleanHistoryAction(_ sender: UIButton) {
        self.cleanRecord?()
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
