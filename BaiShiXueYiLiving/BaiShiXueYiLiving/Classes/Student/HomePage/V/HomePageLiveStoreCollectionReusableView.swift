//
//  HomePageLiveStoreCollectionReusableView.swift
//  BaiShiXueYiLiving
//
//  Created by 梁毅 on 2017/5/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class HomePageLiveStoreCollectionReusableView: UICollectionReusableView {

    @IBAction func moreBtnClicked(_ sender: UIButton) {
        let vc = TNewCurriculumViewController()
        vc.title = "名师课程"
        self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
