//
//  HomePageLiveStoreCollectionViewCell.swift
//  BaiShiXueYiLiving
//
//  Created by 梁毅 on 2017/5/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class HomePageLiveStoreCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var playNumsBtn: UIButton!
    @IBOutlet weak var titlelabel: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var backImg: UIImageView!
    
    var model: VideoModel! {
        willSet(m) {
            titlelabel.text = m.title
           backImg.kf.setImage(with: URL(string: m.video_img!), placeholder: #imageLiteral(resourceName: "live_default"), options: nil, progressBlock: nil, completionHandler: nil)
            usernameLabel.text = m.username
            playNumsBtn.setTitle(m.watch_nums, for: .normal)
        }
    }
    var ocModel: CurriculumVideoModel! {
        willSet(m) {
            titlelabel.text = m.title
            backImg.kf.setImage(with: URL(string: m.video_img!), placeholder: #imageLiteral(resourceName: "live_default"), options: nil, progressBlock: nil, completionHandler: nil)
            usernameLabel.text = m.userName
            playNumsBtn.setTitle(m.watch_nums, for: .normal)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        backView.layer.cornerRadius = 6
        backImg.clipsToBounds = true
        backImg.contentMode = .scaleAspectFill
    }

}
