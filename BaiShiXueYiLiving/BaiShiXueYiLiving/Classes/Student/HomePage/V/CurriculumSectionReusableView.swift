//
//  CurriculumSectionReusableView.swift
//  BaiShiXueYiLiving
//
//  Created by 王宁 on 2018/5/20.
//  Copyright © 2018年 liangyi. All rights reserved.
//

import UIKit

class CurriculumSectionReusableView: UICollectionReusableView {

    @IBOutlet var titleLabel: UILabel!
    var index:Int!
    var moreAction: ((_ index:Int) -> ())!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    
    func configDataSource(title:String) -> Void {
        self.titleLabel.text = title
    }
    
    @IBAction func moreBtnAction(_ sender: Any) {
        self.moreAction(self.index);
    }
    
}
