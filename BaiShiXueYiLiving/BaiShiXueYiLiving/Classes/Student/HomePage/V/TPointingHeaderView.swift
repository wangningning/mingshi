//
//  TPointingHeaderView.swift
//  BaiShiXueYiLiving
//
//  Created by 梁毅 on 2017/5/25.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class TPointingHeaderView: UIView {

    @IBOutlet weak var img: UIImageView!
    class func setup() -> TPointingHeaderView {
        let view = Bundle.main.loadNibNamed("TPointingHeaderView", owner: nil, options: nil)?.first as! TPointingHeaderView
        return view
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 135 / 350 * kScreenWidth)
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
