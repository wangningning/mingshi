//
//  TSceneDetailHeaderView.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/27.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class TSceneDetailHeaderView: UIView {

    @IBOutlet weak var headImage: UIImageView!
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var address: UILabel!
    
    
    @IBOutlet weak var introduce: UILabel!
    
    @IBOutlet weak var totalNum: UILabel!
    
    @IBOutlet weak var presentNum: UILabel!
    
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var countView: UIView!
    
    @IBOutlet weak var numBtn: UIButton!
    
    @IBOutlet weak var saletxt: UILabel!
    
    @IBOutlet weak var applyNumView: UIView!
    
    @IBOutlet weak var applyNumH: NSLayoutConstraint!
    
    @IBOutlet weak var priceView: UIView!
    
    @IBOutlet weak var priceH: NSLayoutConstraint!
    var reportName:((String)->())?
    var priceChange: ((String)->())?
    var model: TSceneDetailModel!{
        willSet(m){
            self.headImage.kf.setImage(with: URL(string:m.img!)!)
            name.text = m.name
            address.text = m.address
            introduce.text = m.intro
            presentNum.text = "已报名" + m.value! + "人"
            totalNum.text =  "满" + m.limit_value! + "人开班"
            if m.is_vip == "1"{
                price.text =  "¥" + m.vip_price!
            } else{
                price.text =  "¥" + m.price!
            }
            
            
        }
    }
    class func setup() -> TSceneDetailHeaderView{
        let view = Bundle.main.loadNibNamed("TSceneDetailHeaderView", owner: nil, options: nil)?.first as! TSceneDetailHeaderView
        view.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 491)
        return view
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.countView.layer.cornerRadius = 6
        self.countView.layer.masksToBounds = true
    }
    
    @IBAction func subNumAction(_ sender: UIButton) {
        if Int((self.numBtn.titleLabel?.text!)!)! > 1{
            self.numBtn.setTitle("\(Int((self.numBtn.titleLabel?.text!)!)! - 1)", for: .normal)
            self.price.text = "¥\((Double((self.numBtn.titleLabel?.text!)!)!) * Double(self.model.price!)!)"
            self.priceChange?(self.price.text!)
            return
        }
        ProgressHUD.showMessage(message: "报名人数不能小于1")
        
    }
    
    @IBAction func addNumAction(_ sender: UIButton) {

        self.numBtn.setTitle("\(Int((self.numBtn.titleLabel?.text!)!)! + 1)", for: .normal)
        self.price.text = "¥\((Double((self.numBtn.titleLabel?.text!)!)!) * Double(self.model.price!)!)"
        self.priceChange?(self.price.text!)
        

    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
