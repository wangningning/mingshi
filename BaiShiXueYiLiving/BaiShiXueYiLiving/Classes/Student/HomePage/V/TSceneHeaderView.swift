
//
//  TSceneHeaderView.swift
//  BaiShiXueYiLiving
//
//  Created by 梁毅 on 2017/5/25.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class TSceneHeaderView: UIView {
    @IBOutlet weak var btn: UIButton!
    class func setup() -> TSceneHeaderView {
        let view = Bundle.main.loadNibNamed("TSceneHeaderView", owner: nil, options: nil)?.first as! TSceneHeaderView
        view.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 45)

        return view
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        btn.setTitle(LocationManager.shared.detialArea, for: .normal)
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
