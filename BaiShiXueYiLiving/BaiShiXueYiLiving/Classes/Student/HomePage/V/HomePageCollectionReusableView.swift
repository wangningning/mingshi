//
//
//  HomePageCollectionReusableView.swift
//  BaiShiXueYiLiving
//
//  Created by 梁毅 on 2017/5/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class HomePageCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var scrollLabel: UILabel!
    @IBOutlet weak var teacherBtn: UIButton!
    @IBOutlet weak var hotBtn: UIButton!
    
    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var lineLeftConstraint: NSLayoutConstraint!
    
    var tagType = 0
    var selectedBlock: ((Int)->())?
    var selectedImgBlock: ((Int)->())?
    var pageScrollView: CarouselView?
    var imageView:UIImageView?
    var bannerDataArr: [BannerModel]! {
        willSet(m) {
            pageScrollView?.fetchImageUrl = { index in
                return m[index].b_img!
            }
            pageScrollView?.totalPages = m.count
            
            if m.count > 0 {
                imageView?.isHidden = true
            }else{
                imageView?.isHidden = false
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        hotBtn.isSelected = true
        teacherBtn.isSelected = false
        
        self.pageScrollView = CarouselView(frame: CGRect(x:0, y:0, width:kScreenWidth, height: 200/375 * kScreenWidth), animationDuration: 3.6, didSelect: { [unowned self] index in
            self.selectedImgBlock?(index)
        })
        bannerView.addSubview(self.pageScrollView!)
        self.imageView = UIImageView(frame: CGRect(x:0, y:0, width:kScreenWidth, height: 200/375 * kScreenWidth))
        
        self.imageView?.image = #imageLiteral(resourceName: "kb_shchfm")
        
        bannerView.addSubview(self.imageView!)
    }
    @IBAction func hotBtnClicked(_ sender: UIButton) {
        
        if tagType == sender.tag {
            return
        }
        if  sender.tag == 1 {
            hotBtn.isSelected = true
            teacherBtn.isSelected = false
            lineLeftConstraint.constant = 0
        } else {
            hotBtn.isSelected = false
            teacherBtn.isSelected = true
            lineLeftConstraint.constant = kScreenWidth/2
        }
        tagType = sender.tag
        self.selectedBlock?(sender.tag)
    }
    @IBAction func TBtnClicked(_ sender: UIButton) {
        if  sender.tag == 0 {
            let vc = TNewCurriculumViewController()
            vc.title = "名师课程"
            self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
        }else if sender.tag == 1 {
            let vc = TSceneViewController()
            vc.title = "名师现场"
             self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
        }else if sender.tag == 2 {
            let vc = TPointingViewController()
            vc.title = "名师指点"
            self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = TPointingViewController()
            vc.recommendedOrNot = true
            vc.title = "名师推荐"
            self.responderViewController()?.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
