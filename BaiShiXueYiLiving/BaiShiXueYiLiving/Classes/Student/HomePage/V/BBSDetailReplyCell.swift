//
//  BBSDetailReplyCell.swift
//  FKDCClient
//
//  Created by 曾觉新 on 2017/2/26.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

protocol BBSDetailReplyCellDelegate {
    //index: 1,举报，2，点赞， 3，评论
    func bbsDetailReplyCell(cell: BBSDetailReplyCell, clickButton index: Int, model: BBSResponseModel)
}


class BBSDetailReplyCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {
    
    var myDelegate: BBSDetailReplyCellDelegate?
    
    var userIconImageView: UIImageView!
    var userNameLabel: UILabel!
    var dateLabel: UILabel!
    var sexImageView: UIImageView!
    var contentLabel: UILabel!
    var commentBgView: UIView!
    var commentTableView: UITableView!
    
    var reportButton: UIButton!//举报
    var likeButton: UIButton!//点赞
    var replyButton: UIButton!
    
    
    
    private var _model: BBSResponseModel!
    var model: BBSResponseModel! {
        set {
//            _model = newValue
//            
//            userIconImageView.yy_imageURL = URL(string: _model.img!)
//            userNameLabel.text = _model.username
//            contentLabel.text = _model.content
//            dateLabel.text = _model.intime
//            likeButton.setTitle(_model.zan, for: .normal)
//            
//            if _model.user_id == uid {
//                reportButton.isHidden = true
//            } else {
//                reportButton.isHidden = false
//            }
//            
//            if _model.sex != nil &&  _model.sex != "3"{
//                if _model.sex == "1" {
//                    sexImageView.image = UIImage(named: "nansheng")
//                } else {
//                    sexImageView.image = UIImage(named: "nvsheng")
//                }
//            } else {
//                sexImageView.image = nil
//            }
//            
//            if _model.response == nil || _model.response?.count == 0 {
//                commentBgView.snp.updateConstraints({ (make) in
//                    make.top.equalTo(self.dateLabel.snp.bottom)
//                    make.height.equalTo(0)
//                })
//            } else {
//                
//                let height = (_model.response?.count)! * 19 + 20
//                commentBgView.snp.updateConstraints({ (make) in
//                    make.top.equalTo(self.dateLabel.snp.bottom).offset(14)
//                    make.height.equalTo(height)
//                })
//            }
//            commentTableView.reloadData()
            
        }
        get {
            return _model
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateLayout()
    }
    
    func initViews() {
        userIconImageView = UIImageView()
        userIconImageView.layer.cornerRadius = 30 / 2
        userIconImageView.layer.masksToBounds = true
        self.contentView.addSubview(userIconImageView)
        
        userNameLabel = UILabel()
        userNameLabel.font = UIFont.systemFont(ofSize: 13)
        userNameLabel.textColor = UIColor(hexString: "#595757")
        self.contentView.addSubview(userNameLabel)
        
        dateLabel = UILabel()
        dateLabel.font = UIFont.systemFont(ofSize: 9)
        dateLabel.textColor = UIColor(hexString: "#999999")
        self.contentView.addSubview(dateLabel)
        
        sexImageView = UIImageView()
        self.contentView.addSubview(sexImageView)
        
        
        contentLabel = UILabel()
        contentLabel.font = UIFont.systemFont(ofSize: 14)
        contentLabel.textColor = UIColor(hexString: "#595757")
        contentLabel.numberOfLines = 0
        contentLabel.text = "32"
        self.contentView.addSubview(contentLabel)
        
        reportButton = UIButton(type: .custom)
        reportButton.tag = 1
        reportButton.titleLabel?.font = UIFont.systemFont(ofSize: 9)
        reportButton.setTitle("举报", for: .normal)
        reportButton.setTitleColor(UIColor(hexString: "#727171"), for: .normal)
        reportButton.addTarget(self, action: #selector(clickButton(sender:)), for: .touchUpInside)
        self.contentView.addSubview(reportButton)
        
        likeButton = UIButton(type: .custom)
        likeButton.tag = 2
        likeButton.titleLabel?.font = UIFont.systemFont(ofSize: 9)
        likeButton.setTitleColor(UIColor(hexString: "#727171"), for: .normal)
        likeButton.setImage(UIImage(named: "dianzan"), for: .normal)
        likeButton.addTarget(self, action: #selector(clickButton(sender:)), for: .touchUpInside)
        self.contentView.addSubview(likeButton)
        
        replyButton = UIButton(type: .custom)
        replyButton.tag = 3
        replyButton.titleLabel?.font = UIFont.systemFont(ofSize: 9)
        replyButton.setTitle("回复", for: .normal)
        replyButton.setTitleColor(UIColor(hexString: "#727171"), for: .normal)
        replyButton.addTarget(self, action: #selector(clickButton(sender:)), for: .touchUpInside)
        self.contentView.addSubview(replyButton)
        
        commentBgView = UIView()
        commentBgView.backgroundColor = UIColor(hexString: "#f6f6f6")
        self.contentView.addSubview(commentBgView)
        
        commentTableView = UITableView(frame: CGRect(), style: .plain)
        commentTableView.register(BBSCommentCell.self, forCellReuseIdentifier: "comCell")
        commentTableView.backgroundColor = UIColor.clear
        commentTableView.separatorColor = UIColor.clear
        commentTableView.dataSource = self
        commentTableView.delegate = self
        commentTableView.isUserInteractionEnabled = false
        commentBgView.addSubview(commentTableView)
        
    }
    
    func updateLayout() {
        
        userIconImageView.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.left.equalTo(15)
            make.size.equalTo(CGSize(width: 30, height: 30))
        }
        
        
        userNameLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(userIconImageView.snp.centerY)
            make.left.equalTo(self.userIconImageView.snp.right).offset(7)
        }
        
        contentLabel.snp.makeConstraints { (make) in
            make.left.equalTo(44)
            make.top.equalTo(userIconImageView.snp.bottom).offset(10)
        }
        
        dateLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentLabel.snp.bottom).offset(10)
            make.left.equalTo(self.contentLabel)
        }
        
        sexImageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.userNameLabel.snp.centerY)
            make.left.equalTo(self.userNameLabel.snp.right).offset(5)
            make.size.equalTo(CGSize(width: 20, height: 20))
        }
        
        
        reportButton.snp.makeConstraints { (make) in
            make.top.equalTo(self.userNameLabel)
            make.right.equalTo(-15)
        }
        
        likeButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.dateLabel.snp.centerY)
            make.right.equalTo(self.reportButton)
        }
        
        
        replyButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.likeButton.snp.centerY)
            make.right.equalTo(-65)
        }
        
        commentBgView.snp.makeConstraints { (make) in
            make.left.equalTo(self.contentLabel)
            make.right.equalTo(-14)
        }
        
        commentTableView.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(10)
            make.bottom.equalTo(10)
        }
    }
    
    func getCellHeight() -> CGFloat{
        
        updateLayout()
        commentBgView.superview?.layoutIfNeeded()
        print("\(commentBgView)")
        return commentBgView.frame.size.height + commentBgView.frame.origin.y + 10
    }
    
    
    //MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "comCell", for: indexPath) as! BBSCommentCell
        cell.backgroundColor = UIColor.clear
        cell.model = _model.response?[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if _model == nil || _model.response == nil || _model.response?.count == 0 {
            return 0
        } else {
            return (_model.response?.count)!
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 19
    }
    
    
    //MARK: -点击事件
    func clickButton(sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 1://举报
            
            break
            
        case 2://点赞
            
            break
            
        case 3://回复
            break
            
        default:
            break
        }
        
        myDelegate?.bbsDetailReplyCell(cell: self, clickButton: tag, model: model)
    }
    
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
