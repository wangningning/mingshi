//
//  TInputView.swift
//  BaiShiXueYiLiving
//
//  Created by apple on 2017/6/18.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class TInputView: UIView,UITextFieldDelegate {

    static var inputViewHeight: CGFloat = 51
    var sendBtn: UIButton!
    var textField: UITextField!
    var inputBgView: UIView!
    
    var clickSend: ((String)->())?
    class func show(atView: UIView, send: ((String)->())?) -> TInputView {
        let view = TInputView(frame:UIScreen.main.bounds )
        view.clickSend = send
        atView.addSubview(view)
        return view
    }
    private override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        
        let disBtn = UIButton(frame: frame)
        self.addSubview(disBtn)
        disBtn.addTarget(self, action: #selector(TInputView.dismissAction), for: .touchUpInside)
        
        let inputView = UIView(frame: CGRect(x: 0, y: kScreenHeight, width: self.bounds.width, height: TInputView.inputViewHeight))
        inputView.backgroundColor = UIColor(hexString: "#f0f3f5")
        inputBgView = inputView
        
       // inputView.backgroundColor = UIColor(hexString: "FFFFFF").withAlphaComponent(0.9)
        self.addSubview(inputView)
        
        
        sendBtn = UIButton()
        sendBtn.isEnabled = false
        sendBtn.setTitle("发送", for: .normal)
        sendBtn.setTitle("发送", for: .disabled)
        sendBtn.setTitleColor(themeColor, for: .normal)
        sendBtn.titleLabel?.font = defaultFont(size: 13)
        sendBtn.addTarget(self, action: #selector(TInputView.sendButtonAction(btn:)), for: .touchUpInside)
        sendBtn.setTitleColor(UIColor(hexString: "#999999"), for: .disabled)
        inputView.addSubview(sendBtn)
        sendBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(inputView.snp.centerY)
            make.right.equalTo(-15)
            make.height.equalTo(36)
            make.width.equalTo(40)
        }
        
        textField = UITextField()
        textField.delegate = self
        textField.returnKeyType = .send
        textField.enablesReturnKeyAutomatically = true
        textField?.attributedPlaceholder = NSAttributedString(string: "说点什么吧", attributes: [NSForegroundColorAttributeName: UIColor(hexString: "#999999")])
        textField.font = defaultFont(size: 13)
        textField?.textColor = UIColor(hexString: "#999999")
        inputView.addSubview(textField!)
        textField?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(inputView.snp.centerY)
            make.left.equalTo(10)
            make.height.equalTo(36)
            make.right.equalTo(sendBtn.snp.left).inset(-10)
        })
        
        NotificationCenter.default.addObserver(self, selector: #selector(TInputView.textFieldTextDidChange(noti:)), name: Notification.Name.UITextFieldTextDidChange, object: textField)
    }
    func textFieldTextDidChange(noti: Notification) {
        let tf = (noti.object as! UITextField)
        if tf.text?.characters.count == 0 {
            sendBtn.isEnabled = false
        } else {
            sendBtn.isEnabled = true
        }
    }
    func sendButtonAction(btn: UIButton) {
        clickSend?(textField.text!)
        textField.text = ""
        self.dismissAction()
    }
    func dismissAction() {
        textField?.resignFirstResponder()
        self.removeFromSuperview()
        textField.text = ""
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        sendButtonAction(btn: sendBtn)
       
        return true
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
