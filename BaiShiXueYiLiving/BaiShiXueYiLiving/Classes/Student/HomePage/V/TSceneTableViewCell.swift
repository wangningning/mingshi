//
//  TSceneTableViewCell.swift
//  BaiShiXueYiLiving
//
//  Created by 梁毅 on 2017/5/25.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class TSceneTableViewCell: UITableViewCell {
    var model: TScenceModel! {
        willSet(m) {
            let str = "已报名" + (m.value ?? "0") + "人"
            let string_to_color = (m.value ?? "0")
            let attributedString = NSMutableAttributedString(string:str)
            attributedString.addAttribute(NSForegroundColorAttributeName, value: themeColor
                , range: (str as NSString).range(of: string_to_color))
            numsL.attributedText = attributedString
            addressL.text = "授课地址：" +  (m.address ?? "上海市")
            titleL.text = m.name
            contentL.text = "满" + (m.limit_value ?? "0") + "人开班"
            img.kf.setImage(with: URL(string: (m.img ??  "")))
        }
    }
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var addressL: UILabel!
    @IBOutlet weak var numsL: UILabel!
    @IBOutlet weak var contentL: UILabel!
    @IBOutlet weak var titleL: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
