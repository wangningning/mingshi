//
//  CurriculumDateModel.h
//  BaiShiXueYiLiving
//
//  Created by 王宁 on 2018/5/23.
//  Copyright © 2018年 liangyi. All rights reserved.
//

#import <Foundation/Foundation.h>
 
@interface CurriculumVideoModel : NSObject

@property (nonatomic, copy) NSString *video_id;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *video_img;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *watch_nums;
@property (nonatomic, copy) NSString *comments;
@property (nonatomic, copy) NSString *share;
@property (nonatomic, copy) NSString *zan;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *intime;

@end

@interface CurriculumDateModel : NSObject

@property (nonatomic, strong) NSArray <CurriculumVideoModel *>*data;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) NSInteger type_id;

@end
