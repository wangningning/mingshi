//
//  CurriculumDateModel.m
//  BaiShiXueYiLiving
//
//  Created by 王宁 on 2018/5/23.
//  Copyright © 2018年 liangyi. All rights reserved.
//

#import "CurriculumDateModel.h"
#import "MJExtension.h"

@implementation CurriculumVideoModel

@end

@implementation CurriculumDateModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{@"data":[CurriculumVideoModel class]};
}
@end
