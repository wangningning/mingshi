//
//  TypeListModel.h
//  BaiShiXueYiLiving
//
//  Created by 王宁 on 2018/5/31.
//  Copyright © 2018年 liangyi. All rights reserved.
//

#import <Foundation/Foundation.h>
/*
 "id":"8",
 "name":"音乐",
 "sort":"1",
 "type_id":"10",
 "publish":"1",
 "level":"0",
 "del_status":"0",
 "create_time":"1527326517",
 "haveSubType":1
 */
@interface TypeListModel : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) NSInteger type_id;
@property (nonatomic, assign) NSInteger haveSubType;

@end
