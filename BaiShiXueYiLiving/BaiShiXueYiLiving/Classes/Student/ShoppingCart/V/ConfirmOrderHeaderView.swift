//
//  ConfirmOrderHeaderView.swift
//  FKDCClient
//
//  Created by 梁毅 on 2017/2/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class ConfirmOrderHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var orderNumber: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var namelabel: UILabel!
    @IBOutlet weak var addressView: UIView!
    
    @IBOutlet weak var payStateHeight: NSLayoutConstraint!
    @IBOutlet weak var payStateView: UIView!
    
    @IBOutlet weak var detailImg: UIImageView!
    var id_AddressList : String?
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func draw(_ rect: CGRect) {
       
    }
    var confirmOrderAddress: ConfirmOrderAddress! {
        willSet(m) {
            self.phoneLabel.text = m.phone
            self.addressLabel.text = "收货地址: " + m.address!
            self.namelabel.text = "收货人: " + m.name!
            self.id_AddressList = m.postage
            
        }
    }
   

}
