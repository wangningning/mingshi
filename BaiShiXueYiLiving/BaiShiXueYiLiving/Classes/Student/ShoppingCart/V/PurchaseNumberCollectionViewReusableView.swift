//
//  PurchaseNumberCollectionViewReusableView.swift
//  FKDCClient
//
//  Created by Luiz on 2017/2/19.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class PurchaseNumberCollectionViewReusableView: UICollectionReusableView {

    
    
    @IBOutlet weak var number: UITextField!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var subBtn: UIButton!
    @IBOutlet weak var price: UILabel!
    
    
    var lastKindOne: KindDetialOFShop?
    var lastKindTwo: KindDetialOFShop?
    
    var sPrice: Double = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        NotificationCenter.default.addObserver(self, selector: #selector(textFieldDidChangeValue(noti:)), name: NSNotification.Name.UITextFieldTextDidChange, object: self.number)
    }
    /// text field text 改变的监听
    func textFieldDidChangeValue(noti: Notification) -> () {
        
    }
        
}
