//
//  PurchaseColorCollectionReusableView.swift
//  FKDCClient
//
//  Created by 梁毅 on 2017/2/11.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class PurchaseColorCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var line: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
