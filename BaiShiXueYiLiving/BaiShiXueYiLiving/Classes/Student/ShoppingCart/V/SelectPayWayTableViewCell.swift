//
//  SelectPayWayTableViewCell.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/22.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class SelectPayWayTableViewCell: UITableViewCell {

    @IBOutlet weak var way: UILabel!
    
    @IBOutlet weak var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
