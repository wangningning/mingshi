//
//  ImageCollectionViewCell.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/14.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
