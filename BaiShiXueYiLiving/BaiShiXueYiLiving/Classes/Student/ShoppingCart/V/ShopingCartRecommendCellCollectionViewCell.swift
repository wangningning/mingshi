//
//  ShopingCartRecommendCellCollectionViewCell.swift
//  FKDCClient
//
//  Created by Luiz on 2017/2/20.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class ShopingCartRecommendCellCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    
//    var model: RecommendModel! {
//        willSet(m) {
//            img.yy_setImage(with: URL(string: m.thumb!), placeholder: UIImage(named: ""))
//            price.text = "￥ " + (m.sale_price)!
//            name.text = m.name
//        }
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
