
//
//  ShopCartCollectionViewCell.swift
//  FKDCClient
//
//  Created by 梁毅 on 2017/2/10.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class ShopCartCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var NumbertextField: UITextField!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var subBtn: UIButton!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var selectBtn: UIButton!
    @IBOutlet weak var kindidLabel1: UILabel!
    @IBOutlet weak var kindidLabel2: UILabel!
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var tinyRoundBtn: UIButton!
    
    var SeletedComplete: ((String) -> ())?
    var shopCart = GoodsCartModel() {
        willSet(m) {
          
            img.kf.setImage(with: URL(string: m.thumb!), placeholder: #imageLiteral(resourceName: "icon_liuxing"), options: nil, progressBlock: nil, completionHandler: nil)
            if  m.is_check == "2" {
                selectBtn.isSelected = true
                self.tinyRoundBtn.isSelected = true
            }
            else {
                selectBtn.isSelected = false
                self.tinyRoundBtn.isSelected = false
            }
            title.text = m.name
            price.text = "￥ " + (m.sale_price)!
            NumbertextField.text = m.number
            if m.kinds_detail?.count == 0{
                self.kindidLabel1.text = "规格：无"
            }else{
                var kinds : NSString = ""
                for str in m.kinds_detail! {
                    kinds = kinds.appending(str.kind_detail! + ",") as NSString
                }
                self.kindidLabel1.text = "规格: " + kinds.substring(to: kinds.length - 2)
            }
            
            
                        
            
        }
    }
    func loadDefaultSeleted() {
        
    }
    @IBAction func subBtnClicked(_ sender: Any) {
        if NumbertextField.text == "1" {
            
            ProgressHUD.showNoticeOnStatusBar(message: "商品数量不能小于1")
            return
        }
        let param = ["id":shopCart.id!]
        NetworkingHandle.fetchNetworkData(url: "/Mall/minus_goods", at: self, params: param as [String: AnyObject], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
            if self.shopCart.is_check == "2" {
                let data = response["data"] as! [String: AnyObject]
                let price = data["price"] as! String
                self.SeletedComplete?(price)
            }
            var ii = Int(self.shopCart.number!)!
            ii = ii - 1
            self.shopCart.number = String(describing: ii)
            self.NumbertextField.text =  self.shopCart.number!
            
        }) {
            
        }

    }
    
    @IBAction func addBtnClicked(_ sender: Any) {
        let param = ["id":shopCart.id!]
        NetworkingHandle.fetchNetworkData(url: "/Mall/plus_goods", at: self, params: param, isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
            
            if self.shopCart.is_check == "2" {
                let data = response["data"] as! [String: AnyObject]
                
                let price = data["price"] as! String
                
                self.SeletedComplete?(price as String)
                
            }
            
            var ii = Int(self.shopCart.number!)!
            ii = ii + 1
            self.shopCart.number = String(describing: ii)
            self.NumbertextField.text =  self.shopCart.number!

            
        }) {
            
        }
    }
    
    @IBAction func selectGoodsAction(_ sender: UIButton) {
        let param = ["id":shopCart.id!]
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Mall&a=set_default_goods", at: self, params: param, isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
            let data = response["data"] as! [String: AnyObject]
            let price = data["price"] as! String
            
            if  self.shopCart.is_check == "1" {
                sender.isSelected = true
                self.tinyRoundBtn.isSelected = true
                self.shopCart.is_check = "2"
                
            }
            else {
                sender.isSelected = false
                self.tinyRoundBtn.isSelected = false
                self.shopCart.is_check = "1"
            }
            
            self.SeletedComplete?(price)
            
        }) {
            
        }

    }
    func selectBtnClicked(sender: UIButton) {
        
//        let param = ["id":shopCart.id!]
//        LYNetWorkRequest.fetchNetworkData(url: "/api.php?m=Api&c=Mall&a=set_default_goods", params: param as [String : AnyObject], isAutoHide: true, isShowOrNot: "2", hasHeaderRefresh: nil, success: { (response) in
//            let data = response["data"] as! [String: AnyObject]
//            let price = data["price"] as! NSNumber
//            
//            if  self.shopCart.is_check == "1" {
//                self.selectBtn.isSelected = true
//                self.shopCart.is_check = "2"
//                
//            }
//            else {
//                self.selectBtn.isSelected = false
//                self.shopCart.is_check = "1"
//                
//            }
//            
//            self.SeletedComplete?(price.stringValue)
//            
//        }) { (errorCode) in
//            
//        }
        
        //
        //        let price = (shopCart.sale_price?.intValue)! * Int(shopCart.number!)!
        //
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.bgView.layer.cornerRadius = 6
        self.bgView.layer.masksToBounds = true
        selectBtn.addTarget(self, action: #selector(selectBtnClicked(sender:)), for: .touchUpInside)
    }
    
}
