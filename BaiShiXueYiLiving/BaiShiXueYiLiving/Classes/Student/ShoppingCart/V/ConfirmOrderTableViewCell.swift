//
//  ConfirmOrderTableViewCell.swift
//  FKDCClient
//
//  Created by 梁毅 on 2017/2/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class ConfirmOrderTableViewCell: UITableViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var kindidLabel1: UILabel!
   
    var globalOrderDetail: GlobalOrderDetail! {
        willSet(m) {
            img.kf.setImage(with: URL(string: m.img!))
            price.text = "￥ " + String(describing: m.sale_price!)
            name.text = m.name
            numberLabel.text = "x" + m.number!
            if  m.kinds1 != nil && m.kinds_detail1 != nil{
                 if  m.kinds1?.characters.count != 0 &&  m.kinds_detail1?.characters.count != 0 {
                    self.kindidLabel1.text = m.kinds1! + ":" + m.kinds_detail1!
                }
                else {
                    self.kindidLabel1.text = ""
                }
                
            }
            else {
                self.kindidLabel1.text = ""
            }
            if  m.kinds2 != nil &&  m.kinds_detail2 != nil{
                if  m.kinds2?.characters.count != 0 &&  m.kinds_detail2?.characters.count != 0 {
                    self.kindidLabel1.text = self.kindidLabel1.text! + m.kinds2! + ":" + m.kinds_detail2!
                }
                
            }
            
        }
    }
    var confirmOrder : ConfirmOrder!{
        willSet(m) {
            if m.kinds_detail?.count == 0{
                self.kindidLabel1.text = "规格：无"
            }else{
                var kinds : NSString = ""
                for str in m.kinds_detail! {
                    kinds = kinds.appending(str.kind_detail! + ",") as NSString
                }
                self.kindidLabel1.text = "规格: " + kinds.substring(to: kinds.length - 2)
            }
            img.kf.setImage(with: URL(string: m.img!))
            price.text = "￥ \(m.sale_price!)"
            name.text = m.name
            numberLabel.text = "x \(m.number!)"
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
