//
//  ShoppingCartViewCell.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/22.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class ShoppingCartViewCell: UITableViewCell {
    @IBOutlet weak var selectBtn: UIButton!

    @IBOutlet weak var goodsImg: UIImageView!
    
    @IBOutlet weak var goodsName: UILabel!
    
    @IBOutlet weak var goodsType: UILabel!
    @IBOutlet weak var goodsPrice: UILabel!
    
    @IBOutlet weak var numTF: UITextField!
    
    @IBOutlet weak var countBGView: UIView!
    
    @IBOutlet weak var subBtn: UIButton!
    
    @IBOutlet weak var addBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.countBGView.layer.cornerRadius = 6
       // self.countBGView.layer.masksToBounds = true
//        self.subBtn.layer.cornerRadius = 6
//        self.subBtn.layer.masksToBounds = true
//        self.addBtn.layer.cornerRadius = 6
//        self.addBtn.layer.masksToBounds = true
        
        
    }

    @IBAction func selectGoodsAction(_ sender: UIButton) {
        if sender.isSelected == false {
            sender.isSelected = true
        } else {
            sender.isSelected = false
        }
    }
    
    @IBAction func subNumAction(_ sender: UIButton) {
        if numTF.text == "1" {
            ProgressHUD.showNoticeOnStatusBar(message: "商品数量不能小于1")
            return
        }
    }
    @IBAction func sumNumAction(_ sender: UIButton) {
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
