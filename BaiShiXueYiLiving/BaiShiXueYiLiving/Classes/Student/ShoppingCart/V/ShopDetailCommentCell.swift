//
//  ShopDetailCommentCell.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/14.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class ShopDetailCommentCell: UITableViewCell{

    var imageMark: TggStarEvaluationView!
    var dateLab: UILabel!
    var content: UILabel!
    var photoView: BBSListPhotoView!
    var user: UILabel!
    var collectionView: UICollectionView!
    var dataArr:[ImgModel] = []
    var model: ShopDetailComment!{
        willSet(m){
            
            self.imageMark.starCount = UInt(m.goods_mark)
            self.dateLab.text = m.date_value
            self.user.text = m.username
            self.content.text = m.content
            self.dataArr = m.comment_img!
           
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func initViews(){
        
        imageMark = TggStarEvaluationView()
        imageMark.spacing = 0.3
        imageMark.isTapEnabled = false
       
        imageMark.backgroundColor = UIColor.white
        self.contentView.addSubview(imageMark)
        
        dateLab = UILabel()
        dateLab.textColor = UIColor(hexString: "#999999")
        dateLab.font = UIFont.systemFont(ofSize: 10.0)
        self.contentView.addSubview(dateLab)
        
        content = UILabel()
        content.text = ""
        content.textColor = UIColor(hexString:"#333333")
        content.font = UIFont.systemFont(ofSize: 12)
        content.numberOfLines = 0
        self.contentView.addSubview(content)
        
        let layout = UICollectionViewFlowLayout()
        let w = (kScreenWidth - 30 - 10 * 3)/4
        layout.itemSize = CGSize(width: w, height: w)
        layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 15)
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 10
        
        self.collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.bounces = false
        collectionView.backgroundColor = UIColor.white
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.isScrollEnabled = false
        collectionView.register(UINib(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionViewCell")
        self.contentView.addSubview(self.collectionView)
        
        user = UILabel()
        user.textColor = UIColor(hexString: "#333333")
        user.font = UIFont.systemFont(ofSize: 12.0)
        self.contentView.addSubview(user)
        
        
    }
//    fileprivate lazy var collectionView: UICollectionView = {
//        
//        
//        return collectionView
//    }()

    override func layoutSubviews() {
        super.layoutSubviews()
        
        layoutUI()
    }
    func layoutUI(){
        
        self.imageMark.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(10)
            make.size.equalTo(CGSize(width: 23*5, height: 20))

        }
        self.dateLab.snp.makeConstraints { (make) in
            make.right.equalTo(-15)
            make.centerY.equalTo(self.imageMark.snp.centerY)
        }
        self.content.snp.makeConstraints { (make) in
            make.top.equalTo(self.imageMark.snp.bottom).offset(13)
            make.left.equalTo(self.imageMark.snp.left)
            make.right.equalTo(self.dateLab.snp.right)
            
        }
        self.collectionView.snp.makeConstraints { (make) in
            make.left.right.equalTo(0)
            make.top.equalTo(self.content.snp.bottom).offset(15)
            make.bottom.equalTo(self.user.snp.top).offset(-10)
        }
        self.collectionView.reloadData()

        self.user.snp.makeConstraints { (make) in
            make.left.equalTo(self.imageMark.snp.left)
            make.bottom.equalTo(-11)
        
        }
        
        
       
    }
    func getCellHeight() -> CGFloat {
        
        layoutUI()
        let width = (kScreenWidth - 30 - 10 * 3)/4
        let w = model.comment_thumb!.count/4
        let col = model.comment_thumb!.count % 4
        if  col == 0{
            if model.content == ""||model.content?.characters.count == 0{
                return CGFloat(width * CGFloat(w) + 110 + CGFloat(w) * 10)
            }
            return CGFloat(width * CGFloat(w) + 110 + CGFloat(w) * 10)
        }else{
            if model.content == ""||model.content?.characters.count == 0{
                return CGFloat(width * CGFloat(w + 1) + 110 + CGFloat(w) * 10)
            }
            return CGFloat(width * CGFloat(w + 1) + 110 + CGFloat(w) * 10)
        }
        


    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension ShopDetailCommentCell:UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
       // cell.image.kf.setImage(with: URL(string:model.comment_thumb![indexPath.row].img!))
        cell.image.kf.setImage(with: URL(string:model.comment_thumb![indexPath.row].img!), placeholder: #imageLiteral(resourceName: "live_default"), options: nil, progressBlock: nil, completionHandler: nil)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataArr.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = PhotoBrowerViewController()
        vc.dataArr = [self.dataArr[indexPath.row].img ?? ""]
        self.responderViewController()?.present(vc, animated: true, completion: nil)

    }
    
    
}
