//
//  PageScrollView.swift
//  LoveFreshBeen
//
//  Created by 维尼的小熊 on 16/1/12.
//  Copyright © 2016年 tianzhongtao. All rights reserved.


import UIKit

class PageScrollView: UIView {
    
    fileprivate let imageViewMaxCount = 3
    var imageScrollView: UIScrollView!
    fileprivate var pageControl: UIPageControl!
    fileprivate var placeholderImage: UIImage?
    fileprivate var imageClick:((_ index: Int) -> ())?
    var banner = [Banner]() {
        didSet {
            
            
            
            if banner.count >= 0 {
                pageControl.numberOfPages = (banner.count)
                pageControl.currentPage = 0
                updatePageScrollView()
            }
        }
    }
   
    var shop_banner = [String]() {
        didSet {
            
            
            
            if shop_banner.count >= 0 {
                pageControl.numberOfPages = (shop_banner.count)
                pageControl.currentPage = 0
                updatePageScrollView()
                
                
            }
        }
    }

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        buildImageScrollView()
        
        buildPageControl()
        
    }
    
    convenience init(frame: CGRect, placeholder: UIImage, focusImageViewClick:@escaping ((_ index: Int) -> Void)) {
        self.init(frame: frame)
        placeholderImage = placeholder
        imageClick = focusImageViewClick
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        imageScrollView.frame = bounds
        imageScrollView.contentSize = CGSize(width:CGFloat(imageViewMaxCount) * width, height:0)
        for i in 0...imageViewMaxCount - 1 {
            let imageView = imageScrollView.subviews[i] as! UIImageView
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            imageView.isUserInteractionEnabled = true
            imageView.frame = CGRect(x:CGFloat(i) * imageScrollView.width, y:0, width:imageScrollView.width, height:imageScrollView.height)
        }
        
        let pageW: CGFloat = 80
        let pageH: CGFloat = 20
        let pageX: CGFloat = (imageScrollView.width - pageW) / 2
        let pageY: CGFloat = imageScrollView.height - pageH
        pageControl.frame = CGRect(x:pageX, y:pageY, width:pageW, height:pageH)
        
        updatePageScrollView()
    }
    
    // MARK: BuildUI
    fileprivate func buildImageScrollView() {
        imageScrollView = UIScrollView()
        imageScrollView.bounces = false
        imageScrollView.showsHorizontalScrollIndicator = false
        imageScrollView.showsVerticalScrollIndicator = false
        imageScrollView.isPagingEnabled = true
        imageScrollView.delegate = self
        addSubview(imageScrollView)
        
        for _ in 0..<3 {
            let imageView = UIImageView()
            let tap = UITapGestureRecognizer(target: self, action: #selector(imageViewClick(tap:)))
            imageView.addGestureRecognizer(tap)
            imageScrollView.addSubview(imageView)
        }
    }
    
    fileprivate func buildPageControl() {
        pageControl = UIPageControl()
        pageControl.hidesForSinglePage = true
        pageControl.pageIndicatorTintColor  = UIColor.gray
//            UIColor(hexString: "#ffffff")
        pageControl.currentPageIndicatorTintColor = UIColor(hexString: "#F08519")
        addSubview(pageControl)
    }
    
    //MARK: 更新内容
    fileprivate func updatePageScrollView() {
        for i in 0 ..< imageScrollView.subviews.count {    
            let imageView = imageScrollView.subviews[i] as! UIImageView
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            var index = pageControl.currentPage
            
            if i == 0 {
                index -= 1
            } else if 2 == i {
                index += 1
            }
            
            if index < 0 {
                index = self.pageControl.numberOfPages - 1
            } else if index >= pageControl.numberOfPages {
                index = 0
            }
            
            imageView.tag = index
            if banner.count > 0 {
               // imageView.yy_setImage(with: URL(string: banner[index].img!), placeholder: placeholderImage!)
                imageView.kf.setImage(with: URL(string:banner[index].img!), placeholder: placeholderImage, options: nil, progressBlock: nil, completionHandler: nil)
            }
        }
        
        imageScrollView.contentOffset = CGPoint(x:imageScrollView.width, y:0)
    }
    
    func next() {
        imageScrollView.setContentOffset(CGPoint(x:2.0 * imageScrollView.frame.size.width, y:0), animated: true)
    }
    
    // MARK: ACTION
    func imageViewClick(tap: UITapGestureRecognizer) {
        if imageClick != nil {
            imageClick!(tap.view!.tag)
        }
    }
}

// MARK:- UIScrollViewDelegate
extension PageScrollView: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var page: Int = 0
        var minDistance: CGFloat = CGFloat(MAXFLOAT)
        for i in 0..<imageScrollView.subviews.count {
            let imageView = imageScrollView.subviews[i] as! UIImageView
            let distance:CGFloat = abs(imageView.x - scrollView.contentOffset.x)
            
            if distance < minDistance {
                minDistance = distance
                page = imageView.tag
            }
        }
        pageControl.currentPage = page
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        updatePageScrollView()
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        updatePageScrollView()
    }
}
