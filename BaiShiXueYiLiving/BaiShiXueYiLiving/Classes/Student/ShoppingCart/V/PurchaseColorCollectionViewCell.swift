
//
//  PurchaseColorCollectionViewCell.swift
//  FKDCClient
//
//  Created by 梁毅 on 2017/2/11.
//  Copyright © 2017年 liangyi. All rights reserved.
//

protocol  PurchaseColorCollectionViewCellDelegate {
    func didSelect(model: kindsDetailModel, indexPath: IndexPath)
}

import UIKit

class PurchaseColorCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var label_backView: UIView!
    
    var delegate: PurchaseColorCollectionViewCellDelegate?
    var indexPath: IndexPath!
    var kindDetialOFShop : kindsDetailModel! {
        willSet(m) {
            label.text = m.kinds_detail
            if  m.is_Seleted == true {
                label_backView.backgroundColor = UIColor(hexString: "#ec6b1a")
                
                label_backView.layer.masksToBounds = true
                label_backView.layer.borderColor = UIColor(hexString: "#ec6b1a").cgColor
                label_backView.layer.borderWidth = 1
                label_backView.layer.masksToBounds = true


                label.textColor = UIColor.white
                label.layer.cornerRadius = 6
                label.layer.masksToBounds = true
                
            }
            else {
                label_backView.backgroundColor = UIColor.white
                label.textColor = UIColor(hexString: "#666666")
                label_backView.layer.cornerRadius = 6
                label_backView.layer.borderColor = UIColor(hexString: "#cdcdcd").cgColor
                label_backView.layer.borderWidth = 1
                label_backView.layer.masksToBounds = true
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self, action: #selector(PurchaseColorCollectionViewCell.viewTap))
        label_backView.addGestureRecognizer(tap)
        if  kScreenWidth == 375 {
            label.font = UIFont.systemFont(ofSize: 13)
        }
        else if kScreenWidth == 320 {
            label.font = UIFont.systemFont(ofSize: 9)
        }
        else if kScreenWidth == 414 {
            label.font = UIFont.systemFont(ofSize: 14)
        }
        else {
            label.font = UIFont.systemFont(ofSize: 9)
        }
    }
    func viewTap() {
        delegate?.didSelect(model: kindDetialOFShop, indexPath: indexPath)
    }
}
