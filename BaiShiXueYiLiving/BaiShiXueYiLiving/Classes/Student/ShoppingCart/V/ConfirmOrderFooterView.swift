//
//  ConfirmOrderFooterView.swift
//  FKDCClient
//
//  Created by 梁毅 on 2017/2/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class ConfirmOrderFooterView: UITableViewHeaderFooterView {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var goodsNumber: UILabel!
    @IBOutlet weak var postageFee: UILabel!
    @IBOutlet weak var messages: UITextField!
    @IBOutlet weak var sendType: UILabel!
 
    
    
    @IBOutlet weak var totalView: UIView!
    
    @IBOutlet weak var sendTypeView: UIView!
    @IBOutlet weak var sendTypeViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var billViewHeight: NSLayoutConstraint!
    @IBOutlet weak var billView: UIView!
    @IBOutlet weak var billTF: UITextField!
    
    
    
    override func awakeFromNib() {
        priceLabel.text = "0"
      //  self.logisticsLayoutConstraint.constant = 0
    }
   

}
