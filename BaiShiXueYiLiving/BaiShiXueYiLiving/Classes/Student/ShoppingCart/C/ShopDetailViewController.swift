//
//  ShopDetailViewController.swift
//  FKDCClient
//
//  Created by 梁毅 on 2017/2/8.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh

class ShopDetailViewController: UIViewController,UIScrollViewDelegate,UIWebViewDelegate{
    
    @IBOutlet weak var CustomerServiceView: UIView!
    
    @IBOutlet weak var collectionBackView: UIView!
    @IBOutlet weak var collectionImg: UIImageView!
    @IBOutlet weak var shoppingCartBtn: UIButton!
    @IBOutlet weak var buyNowBtn: UIButton!
    var type: String?
    var goods_id: String?
    var pageScrollView: PageScrollView?
    var shopDetailData = ShopDetailData()
    var isSelected: Bool = false
    var commentArr: [ShopDetailComment] = []
    var tableView: UITableView!
    var listCell: ShopDetailCommentCell!
    var page = 1
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "商品详情"
        buyNowBtn.addTarget(self, action: #selector(buyNowBtnClicked), for: .touchUpInside)
        CustomerServiceView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CustomerServiceViewClicked)))
        collectionBackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(collectionBackViewClicked)))
        
        
        self.view.backgroundColor = UIColor.lightGray
        self.view.addSubview(self.mainScrollView)
        self.mainScrollView.addSubview(self.shopDetailDataHeadView)
        self.mainScrollView.addSubview(self.webView)
        
        
        self.shopDetailDataHeadView.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 218 + kScreenWidth)
        self.webView.frame = CGRect(x: 0, y:  218 + kScreenWidth, width: kScreenWidth, height: kScreenHeight - 50 - 50 - 64)
        
        listCell = ShopDetailCommentCell.init(style: .default, reuseIdentifier: "ShopDetailCommentCell")
        
        
        self.tableView = UITableView(frame:CGRect(x: 0, y: 218 + kScreenWidth, width: kScreenWidth, height: kScreenHeight - 50 - 50 - 64), style: .plain)
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        self.tableView.separatorColor = UIColor(hexString: "#f1f5f6")

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.mainScrollView.addSubview(self.tableView)
        self.tableView.register(ShopDetailCommentCell.self, forCellReuseIdentifier: "ShopDetailCommentCell")
        self.tableView.isHidden = true
        self.tableView.tableFooterView = UIView()

        
        self.tableView.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: { [unowned self] in
            self.page += 1
            self.loadData()
        })
        //http://bs.tstmobile.com/api.php/Mall/goods_text_image/goods_id/7
        webView.loadRequest(URLRequest(url: URL(string: NetworkingHandle.mainHost + "/Mall/goods_text_image/goods_id/" + self.goods_id!)!))
        self.webView.backgroundColor = UIColor.white
        self.shopDetailDataHeadView.pictrueBtn.addTarget(self, action: #selector(pictrueBtnClicked), for: .touchUpInside)
        self.shopDetailDataHeadView.parametersBtn.addTarget(self, action: #selector(parametersBtnClicked), for: .touchUpInside)
        self.shopDetailDataHeadView.evaluateBtn.addTarget(self, action: #selector(evaluateBtnClicked), for: .touchUpInside)
        
        
        
        let param = ["goods_id":goods_id!]
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Mall&a=goods_info", at: self, params: param as [String : AnyObject], isAuthHide: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
            let data = response["data"] as! [String: AnyObject]
            self.shopDetailData = ShopDetailData.modelWithDictionary(diction: data)
//            self.shopDetailDataHeadView.title.text = self.shopDetailData.brand
            self.shopDetailDataHeadView.dis.text = self.shopDetailData.name
            self.shopDetailDataHeadView.price.text = "RMB " + self.shopDetailData.sale_price!
            //1已收藏  2未收藏
            if  self.shopDetailData.is_collect == 2 {
                self.collectionImg.image = #imageLiteral(resourceName: "spxx_sc")
            }
            else {
                self.collectionImg.image = #imageLiteral(resourceName: "spxx_sc_pre")
            }
            if self.shopDetailData.status == "1" {
                self.buyNowBtn.titleLabel?.text = "已下架"
                
                self.buyNowBtn.setTitle("已下架", for: UIControlState.normal)
                
                self.buyNowBtn.setTitle("已下架", for: UIControlState.selected)
                
                self.buyNowBtn.backgroundColor =                     UIColor(red: 183/255, green: 183/255, blue: 183/255, alpha: 1.0);
                
                self.shoppingCartBtn.backgroundColor = UIColor(red: 183/255, green: 183/255, blue: 183/255, alpha: 1.0);
                
            }else{
                self.buyNowBtn.titleLabel?.text = "立即购买"
                
                self.buyNowBtn.backgroundColor = UIColor(red: 236/255, green: 107/255, blue: 26/255, alpha: 1.0);
                
                self.shoppingCartBtn.backgroundColor = UIColor.white
            }
            self.pageScrollView = PageScrollView(frame: CGRect(x:0, y:0, width:kScreenWidth, height: kScreenWidth), placeholder: UIImage(), focusImageViewClick: { (index) in
            })
            self.pageScrollView?.banner = self.shopDetailData.imgs!
            if self.shopDetailData.imgs?.count == 1 {
                self.pageScrollView?.imageScrollView.isScrollEnabled = false
            } else {
                self.pageScrollView?.imageScrollView.isScrollEnabled = true
            }
            self.shopDetailDataHeadView.topView.addSubview(self.pageScrollView!)
        }) { (errorCode) in
        }
    }
    func CustomerServiceViewClicked() {
        ContactUsViewController.contactCustomerServiceVC(from: self)
    }
    func buyNowBtnClicked() {
        if self.shopDetailData.status == "1" {
            return;
        }
        if  type == "3" {
             PurchaseView.showWithTitle(kindOfShop: shopDetailData.kinds_detail!,sale_price: shopDetailData.sale_price!,type:"3", goodsName: shopDetailData.name!, goodsID: shopDetailData.goods_id!, vc: self)
        }
        else {
             PurchaseView.showWithTitle(kindOfShop: shopDetailData.kinds_detail!,sale_price: shopDetailData.sale_price!,type:"2", goodsName: shopDetailData.name!, goodsID: shopDetailData.goods_id!, vc: self)
        }
       
    }
    
    func collectionBackViewClicked() {
        
        let param = ["goods_id":goods_id!]
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Mall&a=collect_goods", at: self, params: param, isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
            let data = response["data"] as! NSNumber
            if   data.stringValue == "1" {
                self.collectionImg.image = #imageLiteral(resourceName: "spxx_sc_pre")
            }
            else {
                self.collectionImg.image = #imageLiteral(resourceName: "spxx_sc")
            }

        }) {
            
        }
    }
    func homePageViewClicked() {
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func shoppingCartBtnClicked(_ sender: Any) {
        if self.shopDetailData.status == "1"{
            return
        }
        if shopDetailData.kinds_detail?.count == 0 {
            return
        }
        PurchaseView.showWithTitle(kindOfShop: shopDetailData.kinds_detail!,sale_price: shopDetailData.sale_price!,type:"1", goodsName: shopDetailData.name!, goodsID: self.shopDetailData.goods_id!,vc: self)
    }
    func pictrueBtnClicked() {
        self.tableView.isHidden = true
        webView.isHidden = false
        //http://bs.tstmobile.com/api.php/Mall/goods_text_image/goods_id/7
        self.shopDetailDataHeadView.pictrueBtn.isSelected = true
        self.shopDetailDataHeadView.parametersBtn.isSelected = false
        self.shopDetailDataHeadView.evaluateBtn.isSelected = false
        self.shopDetailDataHeadView.lineLeadingConstraint.constant = 0
        webView.loadRequest(URLRequest(url: URL(string: NetworkingHandle.mainHost + "/Mall/goods_text_image/goods_id/" + self.goods_id!)!))
        webView.reloadInputViews()
        
    }
    func parametersBtnClicked() {
        self.tableView.isHidden = true
        webView.isHidden = false
        
        self.shopDetailDataHeadView.lineLeadingConstraint.constant = kScreenWidth/3
        self.shopDetailDataHeadView.pictrueBtn.isSelected = false
        self.shopDetailDataHeadView.parametersBtn.isSelected = true
        self.shopDetailDataHeadView.evaluateBtn.isSelected = false
        webView.loadRequest(URLRequest(url: URL(string: NetworkingHandle.mainHost + "?m=Api&c=Mall&a=goods_param&goods_id=" + goods_id!)!))
        
        webView.reloadInputViews()
    }
    func evaluateBtnClicked() {
        
        self.shopDetailDataHeadView.lineLeadingConstraint.constant = kScreenWidth * 2/3
        self.shopDetailDataHeadView.pictrueBtn.isSelected = false
        self.shopDetailDataHeadView.parametersBtn.isSelected = false
        self.shopDetailDataHeadView.evaluateBtn.isSelected = true
        webView.isHidden = true
        self.tableView.isHidden = false
        self.page = 1
        self.loadData()
       
        
    }
    func loadData(){
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Mall&a=goods_comment", at: self, params: ["goods_id":self.goods_id!,"p":self.page,"page":10], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: self.tableView, success: { (response) in
            
            let data = response["data"] as! [String:AnyObject]
            let list = ShopDetailComment.modelsWithArray(modelArray: data["list"] as! [[String:AnyObject]]) as! [ShopDetailComment]
            if self.page == 1{
                self.commentArr.removeAll()
            }
            if list.count == 0{
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
            }
            self.commentArr += list
            print("+++++==++++\(self.commentArr.count)")
            self.tableView.reloadData()
        }) { 
            
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        self.updateContentSize(size: self.webView.frame.size)
    }
    
//    func updateContentSize(size:CGSize) {
//        var contentSize = size
//        contentSize.height = contentSize.height + 220 + ScreenWidth
//        mainScrollView.contentSize = contentSize
//        var newframe = webView.frame
//        newframe.size.height = size.height
//        webView.frame = newframe
//    }
    lazy var mainScrollView: UIScrollView = {
        let scroll = UIScrollView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight - 64 - 50))
        scroll.delegate = self
        scroll.contentSize = CGSize(width: kScreenWidth, height: kScreenWidth + 218 + kScreenHeight - 64 - 50 - 50)
        return scroll
    }()
    
    fileprivate lazy var webView: UIWebView = {
        var webView = UIWebView()
        webView.scrollView.delegate = self
        return webView
    }()
    func webViewDidStartLoad(_ webView: UIWebView) {
        ProgressHUD.showLoading(toView: self.view)
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        ProgressHUD.hideLoading(toView: self.view)
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        webView.reloadInputViews()
    }
    fileprivate lazy var shopDetailDataHeadView: ShopDetailDataHeadView = {
        let toolBarView = Bundle.main.loadNibNamed("ShopDetailDataHeadView", owner: nil
            , options: nil)!.last as! ShopDetailDataHeadView
        return toolBarView
    }()
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.mainScrollView {
            if scrollView.contentOffset.y > kScreenHeight - kScreenWidth - 170 {
                UIView.animate(withDuration: 0.4, animations: {
                    scrollView.setContentOffset(CGPoint(x: 0, y: kScreenWidth + 168), animated: false)
                })
                scrollView.isScrollEnabled = false
            }
        } else {
            if scrollView.contentOffset.y < -2 {
                UIView.animate(withDuration: 0.4, animations: {
                    self.mainScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
                })
                self.mainScrollView.isScrollEnabled = true
            }
        }
    }
}
extension ShopDetailViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShopDetailCommentCell") as! ShopDetailCommentCell
        cell.model = self.commentArr[indexPath.row]
        cell.selectionStyle = .none
     
        return cell

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentArr.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        let model = self.commentArr[indexPath.row]
        if model.cellHeight == nil{
             listCell.model = model
            model.cellHeight = listCell.getCellHeight()
        }
       
        return model.cellHeight!
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
}
