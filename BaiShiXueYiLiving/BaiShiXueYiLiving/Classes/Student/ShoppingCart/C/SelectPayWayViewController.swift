//
//  SelectPayWayViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/22.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class SelectPayWayViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableview: UITableView!
    var order_no:String?
    var amount:String?
    var payType: String? //1 商城订单 2 名师指点收费 3 会员升级 4 线下课程支付
    var url: String?
    
    var model: ListTPointingModel? //用于线下课程支付成功后进入名师指点的私信
    var img: String?
    var usernameTHEY: String?
    var vcType: String?
    var userId: String?
    var hx_username: String?
    var isPopToPointVC = false
    var successPay: (()->())?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        var vcArr = self.navigationController?.viewControllers
        for (index,vc) in (vcArr?.enumerated())! {
            if vc.isKind(of: ConfirmOrderViewController.self){
                vcArr?.remove(at: index)
            }
        }
        self.navigationController?.setViewControllers(vcArr!, animated: false)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "支付"
        NotificationCenter.default.addObserver(self, selector: #selector(paySuccess), name: NSNotification.Name(rawValue: "paySuccess"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(payfail), name: NSNotification.Name(rawValue: "payFail"), object: nil)
        self.tableview.register(UITableViewCell.self, forCellReuseIdentifier: "SelectPayWayCell")
        self.tableview.register(UINib(nibName:"SelectPayWayTableViewCell", bundle:Bundle.main), forCellReuseIdentifier: "SelectPayWayTableViewCell")

        self.tableview.tableFooterView = UIView()
        
        
        if self.payType == "1"{
            self.url = "/Pingxx/ping1"
        } else if self.payType == "2"{
            self.url = "/Pingxx/ping2"
        } else if self.payType == "3"{
            self.url = "/Pingxx/ping3"
        } else if self.payType == "4"{
            self.url = "/pingxx/ping4"
        }
        // Do any additional setup after loading the view.
    }
    func paySuccess() {
        if payType == "1"{
            
            ProgressHUD.showSuccess(message: "支付成功")
            let vc = PaySuccessViewController()
            vc.orderno = self.order_no
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else if payType == "2" {
            ProgressHUD.showSuccess(message: "付费成功")
            if isPopToPointVC{
                for vc in (self.navigationController?.viewControllers)! {
                    if vc.isKind(of: DirectChatViewController.self){
                        self.successPay?()
                        self.navigationController?.popToViewController(vc, animated: true)
                    }
                }
            }else{
                let vc = DirectChatViewController.init(conversationChatter: self.hx_username, conversationType: EMConversationTypeChat)
                vc?.userId = self.userId
                vc?.usernameTHEY = self.usernameTHEY
                vc?.img = self.img
                vc?.state = "1"
                self.successPay?()
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            
            

        } else if payType == "3"{
            ProgressHUD.showSuccess(message: "支付成功")
            
            self.navigationController?.popViewController(animated: true)

        } else if payType == "4"{
            ProgressHUD.showSuccess(message: "报名成功")
            self.successPay?()
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    func payfail() {
        
        ProgressHUD.showMessage(message: "支付失败,请重新支付")
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
          let cell = UITableViewCell.init(style: .value1, reuseIdentifier: "SelectPayWayCell")
            cell.textLabel?.text = "总计："
            cell.textLabel?.textColor = UIColor(hexString: "#333333")
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
            cell.detailTextLabel?.text = "￥" + self.amount!
            cell.detailTextLabel?.textColor = UIColor(hexString: "ec6b1a")
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 14)
            cell.selectionStyle = .none
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectPayWayTableViewCell") as! SelectPayWayTableViewCell
            if indexPath.row == 0 {
                cell.way.text = "支付宝"
                cell.img.image = #imageLiteral(resourceName: "zhf_zhifubao")
                cell.accessoryType = .disclosureIndicator
                
            } else if indexPath.row == 1 {
                cell.way.text = "微信"
                cell.img.image = #imageLiteral(resourceName: "zhf_weixin")
                cell.accessoryType = .disclosureIndicator
            }
            return cell
            
        }
        
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return nil
        } else {
            let view = UIView(frame: CGRect(x: 0, y: 5, width: kScreenWidth - 15, height: 41))
         //   view.backgroundColor = UIColor.white
            let label = UILabel()
            label.text = "    支付方式"
            label.textColor = UIColor(hexString:"333333")
            label.font = UIFont.systemFont(ofSize: 14)
            label.textAlignment = NSTextAlignment.left
            label.backgroundColor = UIColor.white
            view.addSubview(label)
            label.snp.makeConstraints({ (make) in
                make.top.equalTo(5)
                make.left.equalTo(0)
                make.width.equalTo(kScreenWidth)
                make.height.equalTo(41)
            })
            
            return view
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if indexPath.section == 0{
            return
        }
        if order_no == nil || self.amount == nil || self.url == nil{
            ProgressHUD.showMessage(message: "请重新生成订单")
            return
        }
        
        if indexPath.row == 0 {
           self.payOrderByZFB(payUrl: self.url!)
        } else {
          self.payOrderByWX(payUrl: self.url!)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }
        return 46
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        } else {
            return 2
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 41
        } else {
            return 46
        }
    }
    func payOrderByWX(payUrl: String){
        NetworkingHandle.fetchNetworkData(url: payUrl, at: self, params: ["order_no": self.order_no!,"type":"wx"], isShowError: true, hasHeaderRefresh: self.tableview, success: { (response) in
            let data = response["data"] as! NSObject
            Pingpp.createPayment(data, appURLScheme: "wx0fdf564e5795c6b5", withCompletion: { (result, error) in
                if error != nil{
                    if error?.code == .errWxNotInstalled {
                        ProgressHUD.showMessage(message: "微信未安装")
                    }
                print(error?.description ?? "")
                }
                
            })
        }) { 
            
        }
    }
    func payOrderByZFB(payUrl: String){
        print(payUrl)
        NetworkingHandle.fetchNetworkData(url: payUrl, at: self, params: ["order_no": self.order_no!,"type":"alipay"], isShowError: true, hasHeaderRefresh: self.tableview, success: { (response) in
            
            let data = response["data"] as! NSObject
            Pingpp.createPayment(data, appURLScheme: "com.tst.baishixueyi", withCompletion: { (result, error) in
                if error != nil{
                    print(error?.description ?? "")
                    
                }
                
            })
            
        }) {
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
