//
//  ShoppingCartViewController.swift
//  ShoppingCartViewController
//  FKDCClient
//
//  Created by 梁毅 on 2017/2/9.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh
class ShoppingCartViewController: UIViewController {
    
    var page = 1
    var shopCart = [GoodsCartModel]()
    
    var dataArr: [RecommendModel] = []
    
    @IBOutlet weak var bottomViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var layout: UICollectionViewFlowLayout!
    @IBOutlet weak var label1_GoodCart: UILabel!
    @IBOutlet weak var statementsBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    var is_check : String?//2选中；1未选中
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var allselectedBtn: UIButton!
    var price: String?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "购物车"
        view.backgroundColor = UIColor.white
        
        collectionview.register( UINib(nibName: "ShopCartCollectionReusableView", bundle: Bundle.main), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "ShopCartCollectionReusableView")
        collectionview.register( UINib(nibName: "ShopCartCollectionRecommendedReusableView", bundle: Bundle.main), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "ShopCartCollectionRecommendedReusableView")
        collectionview.register(UINib(nibName: "ShopCartCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ShopCartCollectionViewCell")
        
        collectionview.register(UINib(nibName: "GoodsCollectCell", bundle: nil), forCellWithReuseIdentifier: "GoodsCollectCell")
        
        collectionview.mj_header = MJRefreshNormalHeader(refreshingBlock: { [unowned self] in
            self.loadData()
            self.collectionview.reloadData()
        })
        
        deleteBtn.isHidden = true
        statementsBtn.isHidden = false
        self.collectionview.mj_header.beginRefreshing()
        bottomView.isHidden = true
        allselectedBtn.addTarget(self, action: #selector(allselectedBtnClicked), for: .touchUpInside)
        allselectedBtn.isSelected = false
        self.bottomViewBottomConstraint.constant = 0
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    @IBAction func statementsBtnClicked(_ sender: Any) {
        
        if self.totalPriceLabel.text == "" || self.totalPriceLabel.text == "￥0"{
            ProgressHUD.showNoticeOnStatusBar(message: "您还未向购物车添加商品")
            return
        }
        let vc = ConfirmOrderViewController()
        self.navigationController?.pushViewController(vc, animated: true)
        //接口未实现 跳向确认订单页
//        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Mall&a=refer_goods", at: self, success: { (response) in
//            let vc = ConfirmOrderViewController()
//           // vc.hidesBottomBarWhenPushed = true
//            self.navigationController?.pushViewController(vc, animated: true)
//
//        })
    }
    @IBAction func deleteBtnClicked(_ sender: Any) {
        
        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Mall&a=del_goods_cart", at: self, success:{ (response) in
            self.loadData()
        })
    }
    
    func loadRecommendData() {
        print("推荐商品")
        NetworkingHandle.fetchNetworkData(url: "/mall/maybe_enjoy", at: self, params: ["pagesize": 4], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: self.collectionview, success: { (response) in
            let data = response["data"] as! [[String: AnyObject]]
            self.dataArr = RecommendModel.modelsWithArray(modelArray: data) as! [RecommendModel]
          
            self.collectionview.reloadData()

        }) {
            
        }
    }
    
    func loadData() {

        NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Mall&a=goods_cart", at: self, params: ["":""], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: self.collectionview, success: { (response) in
            let data = response["data"] as! [String:AnyObject]
            self.price = data["price"] as? String
            
            self.totalPriceLabel.text = data["price"] as? String
            self.totalPriceLabel.text = "￥"+self.totalPriceLabel.text!

            let datas = data["list"] as! [[String: AnyObject]]
            
            let dataArr = GoodsCartModel.modelsWithArray(modelArray: datas) as! [GoodsCartModel]
            self.shopCart.removeAll()
            self.shopCart += dataArr
            if self.shopCart.count == 0 {
                self.loadRecommendData()
                self.bottomViewBottomConstraint.constant = -61
                self.layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
                self.bottomView.isHidden = true
            } else {
                self.bottomViewBottomConstraint.constant = 0
                self.layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                self.bottomView.isHidden = false
            }

            self.collectionview.reloadData()
            self.showSelectedOrNot_AllSelectBtn()

        }) {
            
        }
    }
    
    func allselectedBtnClicked(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        //1全不选；2全选
        if  self.is_check == "2" {
            let parma = ["is_check":"1"]
            NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Mall&a=choose_all_goods", at: self, params: parma, isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
                
                self.is_check = "1"
                self.allselectedBtn.isSelected = false
                let data = response["data"] as! [String: AnyObject]
                self.price = data["price"] as? String
                self.totalPriceLabel.text = "￥ " + self.price!
               // self.totalPriceLabel.text = "￥"+self.totalPriceLabel.text!
                for model in self.shopCart {
                    model.is_check = "1"
                }
                self.collectionview.reloadData()
                
            }, failure: {
                
            })
        }
        else {
            let parma = ["is_check":"2"]
            NetworkingHandle.fetchNetworkData(url: "?m=Api&c=Mall&a=choose_all_goods", at: self, params: parma, isAuthHide: true, isShowHUD: true, isShowError:  true, hasHeaderRefresh: nil, success: { (response) in
                self.is_check = "2"
                self.allselectedBtn.isSelected = true
                let data = response["data"] as! [String: AnyObject]
                self.price = data["price"] as? String
                self.totalPriceLabel.text = "￥ " + self.price!
               // self.totalPriceLabel.text = "￥" + self.totalPriceLabel.text!
            
                for model in self.shopCart {
                    model.is_check = "2"
                }
                self.collectionview.reloadData()

            }, failure: {
                
            })
        }
        collectionview.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func showSelectedOrNot_AllSelectBtn() {
        for model in self.shopCart {
            if model.is_check == "2" {
                self.is_check = "2"
            }
            else {
                self.is_check = "1"
                break
            }
        }
        if  self.is_check == "2" {
            self.allselectedBtn.isSelected = true
        }
        else {
            self.allselectedBtn.isSelected = false
        }
    }
}
extension ShoppingCartViewController: UICollectionViewDataSource,UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if shopCart.count == 0 {
            return dataArr.count
          
        }
        return shopCart.count
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        if shopCart.count == 0 {
            return CGSize(width: (kScreenWidth)/2, height: (kScreenWidth)/2 * 191/188)
        }
        return CGSize(width: kScreenWidth, height: 99)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 0, 0, 0)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//  购物车数量为0 显示推荐商品数目
        if shopCart.count == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GoodsCollectCell", for: indexPath) as! GoodsCollectCell
            cell.model = dataArr[indexPath.row]
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShopCartCollectionViewCell", for: indexPath) as! ShopCartCollectionViewCell
        cell.shopCart = shopCart[indexPath.row]
        cell.SeletedComplete = {  Price in
            self.showSelectedOrNot_AllSelectBtn()
            self.totalPriceLabel.text =  "￥" + Price
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,  referenceSizeForHeaderInSection section: Int) -> CGSize{
        if shopCart.count == 0 {
            return CGSize(width: kScreenWidth, height: 200)
        }
        return CGSize(width: kScreenWidth, height: 50)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if shopCart.count == 0 {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ShopCartCollectionRecommendedReusableView", for: indexPath) as! ShopCartCollectionRecommendedReusableView
            return headerView
        }
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ShopCartCollectionReusableView", for: indexPath) as! ShopCartCollectionReusableView
        headerView.title.text = "共" + String(shopCart.count) + "件商品"
        headerView.editBtn.addTarget(self, action: #selector(editBtnClicked(sender:)), for: .touchUpInside)
        return headerView
    }
    func editBtnClicked(sender: UIButton) {
        if  deleteBtn.isHidden == true {
            deleteBtn.isHidden = false
            statementsBtn.isHidden = true
            label1_GoodCart.isHidden = true
//            label2_GoodCart.isHidden = true
            totalPriceLabel.isHidden = true
            sender.setTitle("完成", for: .normal)
        }
        else {
            deleteBtn.isHidden = true
            statementsBtn.isHidden = false
            label1_GoodCart.isHidden = false
           // label2_GoodCart.isHidden = false
            totalPriceLabel.isHidden = false
            sender.setTitle("编辑", for: .normal)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = ShopDetailViewController()
        let barbuttonItem = UIBarButtonItem(title: "", style: .done, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = barbuttonItem
        vc.hidesBottomBarWhenPushed = true
        if shopCart.count == 0 {
            
            vc.goods_id = dataArr[indexPath.row].goods_id
        } else {
            vc.goods_id = shopCart[indexPath.row].goods_id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

