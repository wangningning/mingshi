//
//  PaySuccessViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/5/22.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class PaySuccessViewController: UIViewController {

    var orderno : String!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "支付成功"
//        self.navigationItem.leftBarButtonItem? = UIBarButtonItem.init(image: #imageLiteral(resourceName: "kzhb_qq"), style: .plain, target: self, action: #selector(goback))
        let barItem = UIBarButtonItem()
        barItem.image = #imageLiteral(resourceName: "back")
        barItem.target = self;
        barItem.action = #selector(goback)
        self.navigationItem.leftBarButtonItem = barItem
//        self.navigationItem.leftBarButtonItem?.customView?.isHidden = true
        
    }
 
    @objc func goback(){
        if (self.navigationController?.viewControllers.count)!-3 >= 0 {
            self.navigationController?.popToViewController((self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)!-3])!, animated: true)
        }else{
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    @IBAction func continueToBuy(_ sender: UIButton) {
      
        
        
        self.tabBarController?.selectedIndex = 2
        self.navigationController?.popToRootViewController(animated: false)
        print("继续逛逛")
    }
    @IBAction func orderFormDetailAction(_ sender: UIButton) {
        print("订单详情")
        let vc = OrderDetailViewController()
        vc.order_no = self.orderno
        self.navigationController?.pushViewController(vc, animated: true)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
