//
//  RefundViewController.swift
//  BaiShiXueYiLiving
//
//  Created by sh-lx on 2017/6/22.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit

class RefundViewController: UIViewController,BBSAddPhotoViewDelegate, UITextViewDelegate {

    @IBOutlet weak var content: UITextView!
    
    @IBOutlet weak var photoView: UIView!
    
    @IBOutlet weak var photoViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var confirmBtn: UIButton!
    
    @IBOutlet weak var placeHolder: UILabel!
    private let butWidth = (kScreenWidth - 30 - 5 * 3.0) / 4
    var addPhotoView: BBSAddPhotoView!
    var teacher_id: String!
    var successPost: (()->())?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "退款"
        self.photoViewHeight.constant = butWidth + 107 + 15
        self.content.layer.cornerRadius = 6
        self.content.layer.masksToBounds = true
        self.content.delegate = self
        
        addPhotoView = BBSAddPhotoView()
        addPhotoView.myDelegate = self
        self.photoView.addSubview(addPhotoView)
        
        addPhotoView.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.width.equalTo(kScreenWidth - 30)
            make.top.equalTo(self.content.snp.bottom).offset(15)
            make.height.equalTo(200)
        }
        
        self.confirmBtn.layer.cornerRadius = 6
        self.confirmBtn.layer.masksToBounds = true

        // Do any additional setup after loading the view.
    }
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.characters.count == 0 {
            self.placeHolder.isHidden = false
        }else{
            self.placeHolder.isHidden = true
        }
    }
    //MARK: - BBSAddPhotoViewDelegate
    func bbsAddPhotoView(addPhotoView: BBSAddPhotoView, clickButton index:Int)
    {
        if index == 100 {
            
            let actionSheet = ZLPhotoActionSheet()
            actionSheet.maxSelectCount = 3 - addPhotoView.imagesData.count
            actionSheet.showPhotoLibrary(withSender: self, last: [], completion: {(selectPhotos, selectPhotoModels) in
                
                self.addPhotoView.imagesData = self.addPhotoView.imagesData + selectPhotos
                if addPhotoView.imagesData.count == 3{
                    self.addPhotoView.addButton.isHidden = true
                }else{
                    self.addPhotoView.addButton.isHidden = false
                }
                self.photoViewHeight.constant = self.butWidth * CGFloat((self.addPhotoView.imagesData.count / 4) + 1) + 30
                self.addPhotoView.updateLayout()
            })
            
            
            
        }
    }

    @IBAction func submit(_ sender: UIButton) {


        if self.addPhotoView.imagesData.count > 0{
            
            NetworkingHandle.uploadOneMorePicture(url: "/Tools/upload_picture", atVC: self, images: self.addPhotoView.imagesData, uploadSuccess: { (response) in
                
                let imageData = response["data"] as! [String]
                var str = ""
                for s in imageData{
                    str = str + s + ","
                }
                let sstr = str as NSString
                print(sstr.substring(to: sstr.length - 1))
               
                
                NetworkingHandle.fetchNetworkData(url: "/Home/tutor_returns", at: self, params: ["user_id":self.teacher_id,"reason":self.content.text,"img":sstr.substring(to: sstr.length - 1)], isShowHUD: false, success: { (response) in
                    ProgressHUD.showMessage(message: "退款申请已经提交，请等待平台审核")
                    self.successPost?()
                    self.navigationController?.popViewController(animated: true)
                    
                }, failure: { 
                    
                })

            })
            
        }else{
            NetworkingHandle.fetchNetworkData(url: "/Home/tutor_returns", at: self, params: ["user_id":self.teacher_id,"reason":self.content.text,"img":""], success: { (response) in
                ProgressHUD.showMessage(message: "退款申请已经提交，请等待平台审核")
                self.successPost?()
                self.navigationController?.popViewController(animated: true)
                
            }, failure: { 
                
            })
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
