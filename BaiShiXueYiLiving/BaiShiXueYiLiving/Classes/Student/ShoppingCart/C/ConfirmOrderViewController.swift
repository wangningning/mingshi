//
//  ConfirmOrderViewController.swift
//  FKDCClient
//
//  Created by 梁毅 on 2017/2/16.
//  Copyright © 2017年 liangyi. All rights reserved.
//

import UIKit
import MJRefresh
import IQKeyboardManagerSwift

class ConfirmOrderViewController: UIViewController {
    @IBOutlet weak var priceLabel: UILabel!
    var address_ID: String? = nil
    var message_text:String? = nil
    var goodsArr = [ConfirmOrder]()
    var address = ConfirmOrderAddress()
    var amount: String?  // 总价
    var haspost: String? //1 包邮 2 付费
    var type: String?
    var sectionView: ConfirmOrderFooterView!
    @IBOutlet weak var tableview: UITableView!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        IQKeyboardManager.sharedManager().enable = true
        self.loadData()
       
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.sharedManager().enable = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "确认订单"

        
        tableview.register(UINib(nibName: "ConfirmOrderHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "ConfirmOrderHeaderView")
        tableview.register(UINib(nibName: "ConfirmOrderFooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: "ConfirmOrderFooterView")
        tableview.register(UINib(nibName: "ConfirmOrderTableViewCell", bundle: nil), forCellReuseIdentifier: "ConfirmOrderTableViewCell")
        
        
        
        
    }
    @IBAction func rechargeNowBtnClicked(_ sender: Any) {
        

        if self.address.name == nil {
          
            ProgressHUD.showNoticeOnStatusBar(message: "请选择地址")
            return
        }
        else {

            var param = ["name":self.address.name,"phone":self.address.phone,"address":self.address.address,"amount":self.amount,"has_postage":self.haspost]
            if self.sectionView.billTF.text == "",self.sectionView.billTF.text?.characters.count == 0 {
                param = ["name":self.address.name,"phone":self.address.phone,"address":self.address.address,"send_bill":"1","remark":self.sectionView.messages.text,"amount":self.amount,"has_postage":self.haspost]
            } else {
                param = ["name":self.address.name,"phone":self.address.phone,"address":self.address.address,"send_bill":"2","bill_name":self.sectionView.billTF.text,"remark":self.sectionView.messages.text,"amount":self.amount,"has_postage":self.haspost]
            }
            
            
            
            NetworkingHandle.fetchNetworkData(url: "/mall/set_confirm_order", at: self, params: param as [String:AnyObject], isAuthHide: true, isShowHUD: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
                let data = response["data"] as! [String:AnyObject]
                let vc = SelectPayWayViewController()
                vc.payType = "1"
                vc.amount = String.init(format: "%.2f", Double(self.amount!)! + Double(self.address.postage!)!)
                vc.order_no = data["order_no"] as? String
                self.navigationController?.pushViewController(vc, animated: true)
            }, failure: { 
                
            })
        }
    }
    func loadData() {
    
         NetworkingHandle.fetchNetworkData(url: "/mall/confirm_info", at: self,  isAuthHide: true, isShowError: true, hasHeaderRefresh: nil, success: { (response) in
            let data = response["data"] as! [String:AnyObject]
            
            let address = data["address"] as! [String:AnyObject]
            self.address = ConfirmOrderAddress.modelWithDictionary(diction: address)
            self.amount = data["amount"] as? String
            self.haspost = data["has_postage"] as? String
            var str = ""
            if self.address.address == nil{
                str = String.init(format: "%.2f", Double(self.amount!)!)
            }else{
                str = String.init(format: "%.2f", Double(self.amount!)! + Double(self.address.postage!)!)
            }
            self.priceLabel.text = "￥ " + str
            let goods = ConfirmOrder.modelsWithArray(modelArray: data["goods"] as! [[String : AnyObject]]) as! [ConfirmOrder]
            self.goodsArr.removeAll()
            self.goodsArr += goods

            
            self.tableview.reloadData()
        }) { (errorCode) in
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension ConfirmOrderViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 130
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 180 + 146
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let views = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ConfirmOrderHeaderView") as! ConfirmOrderHeaderView
         views.addressView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addressViewClicked)))
        if self.address.phone != "",self.address.phone != nil{
            views.confirmOrderAddress = self.address
        }else{
            views.namelabel.text = "收货人:未添加"
            views.addressLabel.text = "未添加地址"
            views.phoneLabel.text = ""
        }
        
        return views
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        self.sectionView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ConfirmOrderFooterView") as? ConfirmOrderFooterView
            if self.goodsArr.count > 0 {
            self.sectionView.priceLabel.text =   self.priceLabel.text!
            self.sectionView.goodsNumber.text = "共\(self.goodsArr.count)件商品 合计:"
           
                if self.haspost == "1" {
                    self.sectionView?.postageFee.text = "商家配送"
                    
                }else{
                    if  self.address.postage != nil {
                        
                        self.sectionView?.postageFee.text = "运费: ¥" + String(describing: (self.address.postage!))
                    }
                }
        }
        return self.sectionView
    }
    func addressViewClicked() {
        let vc = AddressListViewController()
        vc.selectComplete = { [unowned self] in
            self.loadData()
        }
        let barbuttonItem = UIBarButtonItem(title: "", style: .done, target: nil, action: nil)
        self.navigationController?.navigationItem.backBarButtonItem = barbuttonItem
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.goodsArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmOrderTableViewCell") as! ConfirmOrderTableViewCell
        cell.selectionStyle = .none
        cell.confirmOrder = self.goodsArr[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 99
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
