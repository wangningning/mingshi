//
//  PhoneLoginViewController.swift
//  Duluo
//
//  Created by sh-lx on 2017/3/20.
//  Copyright © 2017年 tts. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class PhoneLoginViewController: UIViewController {

    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var verification: UITextField!
    @IBOutlet weak var sendVerification: UIButton!
    
    @IBOutlet weak var weiboBtn: UIButton!
    var log = ""
    var lag = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        log = LocationManager.shared.log
        lag = LocationManager.shared.lag
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /// 收起键盘
    func resignTextField() -> () {
        UIApplication.shared.keyWindow?.endEditing(true)
    }
    @IBAction func sendVerificationButtonAction(_ sender: Any) {
        if !(phone.text?.isPhoneNumber)! {
            ProgressHUD.showMessage(message: "请输入正确的手机号")
            return
        }
        resignTextField()
        let timer = fetchVerificationCodeCountdown(button: sendVerification, timeOut: 60)
        NetworkingHandle.fetchNetworkData(url: "/Public/sendSMS", at: self, params: ["mobile": phone.text!, "type": "3"], success: { (result) in
        }, failure: { [unowned self] errorCode in
            timer.cancel()
            self.sendVerification.setTitle("发送验证码", for: .normal)
            self.sendVerification.isEnabled = true
        })
    }
    @IBAction func loginButtonAction(_ sender: Any) {
        if !(phone.text?.isPhoneNumber)! {
            ProgressHUD.showMessage(message: "请输入正确的手机号")
            return
        }
        if (verification.text?.isEmpty)! {
            ProgressHUD.showMessage(message: "请输入验证码")
            return
        }
        resignTextField()
        var params = [String : String]()
        params["phone"] = self.phone.text!
        params["verify"] = verification.text!
        params["log"] = log
        params["lag"] = lag
        NetworkingHandle.fetchNetworkData(url: "/Login/login", at: self, params: params, success: { (result) in
            let data = result["data"]
            let userModel = DLUserInfoModel.modelWithDictionary(diction: data as! Dictionary<String, AnyObject>)
            DLUserInfoHandler.saveUserInfo(model: userModel)
            NetworkingHandle.fetchNetworkData(url: "/Home/kefu", at: self, success: { (response) in
                let data = response["data"] as! [String:AnyObject]
                UserDefaults.standard.set(data["hx_username"], forKey: "service")
                ProgressHUD.showSuccess(message: "登录成功")
                UIApplication.shared.keyWindow?.rootViewController = BaseTabbarController()

            })
        })
    }
    
    @IBAction func userAgreementAction(_ sender: UIButton) {
        let vc = MDWebViewController()
        vc.title = "用户协议"
        vc.url = NetworkingHandle.mainHost + "/Home/xieyi/id/7"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func thirdPartLogin(_ sender: UIButton) {
        if sender.tag == 1001{
            otherLogin(type: .QQ, state: "2")
        }else{
            otherLogin(type: .wechatSession, state: "1")
        }
    }
    func otherLogin(type: UMSocialPlatformType, state: String) {
        UMSocialManager.default().auth(with: type, currentViewController: self) { (result, error) in
            if let data = result as? UMSocialAuthResponse {
                let id = data.openid ?? data.uid
                let param = ["state": state, "openid": id!]
                NetworkingHandle.fetchNetworkData(url: "/login/is_exist_member", at: self, params: param, success: { (response) in
                    let data = response["data"] as! NSDictionary
                    let keyArr = data.allKeys as! [String]
                    var num = 0
                    for m in keyArr{
                        if m == "status"{
                            num = 1
                          let vc = BindPhoneViewController()
                            vc.userInfo = (state, id) as! (String, String)
                            self.navigationController?.pushViewController(vc, animated: true)
                           break
                        }
                    }
                    if num == 0{
                        let userModel = DLUserInfoModel.modelWithDictionary(diction: data as! Dictionary<String, AnyObject>)
                        DLUserInfoHandler.saveUserInfo(model: userModel)
                        NetworkingHandle.fetchNetworkData(url: "/Home/kefu", at: self, success: { (response) in
                            let data = response["data"] as! [String:AnyObject]
                            UserDefaults.standard.set(data["hx_username"], forKey: "service")
                            ProgressHUD.showSuccess(message: "登录成功")
                            UIApplication.shared.keyWindow?.rootViewController = BaseTabbarController()
                            
                        })
                        ProgressHUD.showSuccess(message: "登录成功")
                        UIApplication.shared.keyWindow?.rootViewController = BaseTabbarController()
                    }
                   
                })
            }
        }
    }
    deinit {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
