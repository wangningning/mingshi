//
//  BindPhoneViewController.swift
//  SamuraiMeals
//
//  Created by 王志刚 on 2017/10/25.
//  Copyright © 2017年 王志刚. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class BindPhoneViewController: UIViewController {
    @IBOutlet weak var phoneView: UIView!
    
    @IBOutlet weak var codeView: UIView!
    
    @IBOutlet weak var codeBtn: UIButton!
    
    @IBOutlet weak var confirmBtn: UIButton!
    
    @IBOutlet weak var agreementBtn: UIButton!
    
    @IBOutlet weak var phoneTF: UITextField!
    
    @IBOutlet weak var codeTF: UITextField!
    
    var response : UMSocialUserInfoResponse!
    var userInfo : (state:String, id:String)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "绑定手机号"
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true

//        self.phoneView.layer.cornerRadius = kCornerRadius
//        self.phoneView.layer.masksToBounds = true
//        self.codeView.layer.cornerRadius = kCornerRadius
//        self.codeView.layer.masksToBounds = true
//        self.confirmBtn.layer.cornerRadius = kCornerRadius
//        self.confirmBtn.layer.masksToBounds = true
//        self.codeBtn.layer.cornerRadius = kCornerRadius
//        self.codeBtn.layer.masksToBounds = true
        
//        let attributedString = NSMutableAttributedString.init(string: "我已阅读并同意《服务条款》")
//        attributedString.addAttribute(NSForegroundColorAttributeName, value:UIColor.init(hexString: "F53550"), range: NSRange.init(location: 7, length: 6))
//        attributedString.addAttribute(NSUnderlineColorAttributeName, value: UIColor.init(hexString: "F53550"), range: NSRange.init(location: 7, length: 6))
//        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: 1, range: NSRange.init(location: 7, length: 6))
//        self.agreementBtn.setAttributedTitle(attributedString, for: .normal)
//        self.agreementBtn.addTarget(self, action: #selector(BindPhoneViewController.agreementClickedAction), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    func agreementClickedAction() {
        
    }
    
    @IBAction func getVerificationCodeAction(_ sender: UIButton) {
        if !((self.phoneTF.text?.isPhoneNumber)!) {
            ProgressHUD.showMessage(message: "手机号码格式不正确")
            return
        }
        UIApplication.shared.keyWindow?.endEditing(true)
        let timer = fetchVerificationCodeCountdown(button: codeBtn, timeOut: 60)
        NetworkingHandle.fetchNetworkData(url: "/Public/sendSMS", at: self, params: ["mobile": phoneTF.text!, "type": "3", "state":self.userInfo.state], success: { (result) in
            ProgressHUD.showSuccess(message: "发送成功")
        }, failure: { [unowned self] errorCode in
            timer.cancel()
            self.codeBtn.setTitle("发送验证码", for: .normal)
            self.codeBtn.isEnabled = true
        })
        
    }
    
    @IBAction func confirmAction(_ sender: UIButton) {
        if !((self.phoneTF.text?.isPhoneNumber)!) {
            ProgressHUD.showMessage(message: "手机号码格式不正确")
            return
        }
        if self.codeTF.text?.count == 0 {
            ProgressHUD.showMessage(message: "验证码不能为空")
            return
        }
        NetworkingHandle.fetchNetworkData(url: "/login/third_login", at: self, params: ["openid":self.userInfo.id,"state":self.userInfo.state,"mobile":self.phoneTF.text!, "yzm":self.codeTF.text!], success: { (response) in
            let data = response["data"]
            let userModel = DLUserInfoModel.modelWithDictionary(diction: data as! Dictionary<String, AnyObject>)
            DLUserInfoHandler.saveUserInfo(model: userModel)
            ProgressHUD.showSuccess(message: "绑定成功")
            NetworkingHandle.fetchNetworkData(url: "/Home/kefu", at: self, success: { (response) in
                let data = response["data"] as! [String:AnyObject]
                UserDefaults.standard.set(data["hx_username"], forKey: "service")
                ProgressHUD.showSuccess(message: "登录成功")
                UIApplication.shared.keyWindow?.rootViewController = BaseTabbarController()
                
            })
            UIApplication.shared.keyWindow?.rootViewController = BaseTabbarController()
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
